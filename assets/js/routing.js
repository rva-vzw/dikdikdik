export default {
    getRoute(name, params, absolute = false) {
        // CAREFUL! I assume that the FOSJsRouting bundle is enabled via the surrounding twig.
        // (see datasheetSubmissionOverview.html.twig)

        return window.Routing.generate(name, params, absolute);
    }
}