// Disclaimer: I have no clue about javascript ;-)

const shareData = {
  title: 'MDN',
  text: 'Learn web development on MDN!',
  url: 'https://developer.mozilla.org',
}

const shareTitle = document.querySelector('#share-sheet-button').getAttribute('data-shareTitle');
const shareText = document.querySelector('#share-sheet-button').getAttribute('data-shareText');
const shareUrl = document.querySelector('#share-sheet-button').getAttribute('data-shareUrl');

const btn = document.querySelector('#share-sheet-button');
const resultPara = document.querySelector('.result');

if (navigator.share) {
  // TODO: Test whether this works
  btn.addEventListener('click', async () => {
    try {
      await navigator.share({
        title: shareTitle,
        text: shareText,
        url: shareUrl
      });
      console.log('Shared!');
    } catch(err) {
      console.log('Not shared: ' + err);
    }
  });
} else {
  btn.style.visibility = 'hidden';
}