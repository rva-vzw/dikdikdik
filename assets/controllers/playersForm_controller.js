import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    static targets = [
        'playerForAction',
        'kickButton',
        'rejoinButton',
    ]

    kickPlayer(event) {
        const clickedButton = event.target;
        const id = clickedButton.dataset.playerid;

        this.playerForActionTarget.value = id;
        this.kickButtonTarget.click();
    }

    rejoinPlayer(event) {
        const clickedButton = event.target;
        const id = clickedButton.dataset.playerid;

        this.playerForActionTarget.value = id;
        this.rejoinButtonTarget.click();
    }
}