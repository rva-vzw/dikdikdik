import { Controller }  from '@hotwired/stimulus';
import { getComponent } from '@symfony/ux-live-component';

export default class extends Controller {
    static targets = [
        'playerCount',
    ]

    connect() {
    }

    async initialize() {
        this.component = await getComponent(this.element);

        this.component.on('render:finished', (component) => {
            this.arrangeSubmitButtonVisibility();
        });
    }

    arrangeSubmitButtonVisibility() {
        let submitButton = document.getElementById('submit-button');
        let playerCount = this.playerCountTarget.textContent
        if (playerCount >= 4) {
            submitButton.classList.remove('d-none')
        } else {
            submitButton.classList.add('d-none')
        }
    }
}