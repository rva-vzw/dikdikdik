import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
  static values = {
    allSpecifications: Object
  }
  static targets = [
    'bid',
    'gameDetail',
    'onTopOfTroel',
    'announcingMultiPlayers',
    'announcingPlayer1',
    'announcingPlayer2',
    'trickCount',
    'numberOfTricks',
    'success',
    'winners',
    'noBid',
  ]

  connect() {
    let selectedBid = this.bidTarget.value;
    this.showBidFields(selectedBid);
  }

  trickCountSelected(event) {
    this.trickCountTarget.innerText = event.currentTarget.value;
  }

  bidSelected(event) {
    this.showBidFields(event.currentTarget.value);
  }

  hideAllDetails() {
    // I have the impression that this can be done in a more efficient way.
    this.gameDetailTarget.classList.add('d-none');
    this.noBidTarget.classList.add('d-none');
    this.announcingMultiPlayersTarget.classList.add('d-none');
    this.announcingPlayer1Target.classList.add('d-none');
    this.announcingPlayer2Target.classList.add('d-none');
    this.numberOfTricksTarget.classList.add('d-none');
    this.successTarget.classList.add('d-none');

    let winnersOptions = this.winnersTarget.getElementsByClassName('form-check');
    for(let i = 0; i < winnersOptions.length; i++) {
      winnersOptions[i].classList.add('d-none');
    }
    this.winnersTarget.classList.add('d-none');
  }

  showBidFields(bidName) {
    console.log('Updating fields for ' + bidName);
    if (!bidName) {
      this.hideAllDetails();
      return;
    } else if (!this.allSpecificationsValue[bidName]) {
      this.hideAllDetails();
      this.noBidTarget.classList.remove('d-none');
      return;
    }

    this.noBidTarget.classList.add('d-none');

    let announcementType = this.allSpecificationsValue[bidName].announcementType;
    let outcomeType = this.allSpecificationsValue[bidName].outcomeType;

    // I have the impression that this can be done in a more efficient way
    if (announcementType) {
      this.gameDetailTarget.classList.remove('d-none');
    } else {
      this.gameDetailTarget.classList.add('d-none');
    }

    if (announcementType.includes('SinglePlayerAnnouncement')) {
      this.announcingMultiPlayersTarget.classList.add('d-none');
      this.announcingPlayer1Target.classList.remove('d-none');
      this.announcingPlayer2Target.classList.add('d-none');
    }
    if (announcementType.includes('TwoPlayerAnnouncement')) {
      this.announcingMultiPlayersTarget.classList.add('d-none');
      this.announcingPlayer1Target.classList.remove('d-none');
      this.announcingPlayer2Target.classList.remove('d-none');
    }
    if (announcementType.includes('MultiPlayerAnnouncement')) {
      this.announcingMultiPlayersTarget.classList.remove('d-none');
      this.announcingPlayer1Target.classList.add('d-none');
      this.announcingPlayer2Target.classList.add('d-none');
    }

    if (announcementType.includes('TroelAware')) {
      this.onTopOfTroelTarget.classList.remove('d-none')
    } else {
      this.onTopOfTroelTarget.classList.add('d-none')
    }
    if (outcomeType.includes('NumberOfTricks')) {
      this.numberOfTricksTarget.classList.remove('d-none');
    } else {
      this.numberOfTricksTarget.classList.add('d-none');
    }
    if (outcomeType.includes('AllOrNothing')) {
      this.successTarget.classList.remove('d-none');
    } else {
      this.successTarget.classList.add('d-none');
    }
    if (outcomeType.includes('Winners')) {
      this.winnersTarget.classList.remove('d-none');
    } else {
      this.winnersTarget.classList.add('d-none');
    }
  }

  announcersChanged(event) {
    let correspondingWinner = this.winnersTarget.querySelector(
      'input[value="' + event.target.value + '"]'
    ).parentNode;

    if (event.target.checked) {
      correspondingWinner.classList.remove('d-none');
    } else {
      correspondingWinner.classList.add('d-none');
    }
  }

}