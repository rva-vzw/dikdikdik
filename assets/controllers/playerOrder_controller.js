import { Controller } from "@hotwired/stimulus";
import Sortable from "sortablejs";
import axios from "axios";

export default class extends Controller {
  connect() {
    this.endpoint = this.element.getAttribute('data-endpoint');
    this.sortable = Sortable.create(this.element, {
      onEnd: this.end.bind(this),
      draggable: '.draggable-player',
      animation: 150,
      ghostClass: 'draggable-ghost',
      // Need to use a handle here, otherwise deleting players will not
      // work for Chromium on Android, see #355
      handle: '.handle'
    })
  }

  end(event) {
    const items = event.target.getElementsByClassName('draggable-player');
    let playerOrder = [...items].map(item => item.getAttribute('data-playerid'));

    return axios.put(this.endpoint, playerOrder);
  }
}