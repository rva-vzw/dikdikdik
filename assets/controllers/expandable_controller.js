import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
  static targets = [
    'title',
    'body'
  ]

  static values = {
      initialVisibility: Boolean
  }

  visible = false;

  connect() {
    this.visible = this.initialVisibilityValue;
    if (!this.visible) {
      this.bodyTarget.classList.add('d-none');
    }
    this.titleTarget.addEventListener('click', () => {
      this.visible = !this.visible;

      if (this.visible) {
        this.bodyTarget.classList.remove('d-none');
      } else {
        this.bodyTarget.classList.add('d-none');
      }
    })
  }
}