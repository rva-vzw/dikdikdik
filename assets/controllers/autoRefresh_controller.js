import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    static targets = [
        'refreshButton'
    ]

    connect() {
        const hubUrl = this.element.getAttribute('data-hubUrl');
        const topic = this.element.getAttribute('data-topic');

        console.log('hubUrl: ' + hubUrl);
        console.log('topic:' + topic);

        const u = new URL(hubUrl);
        u.searchParams.append('topic', topic);

        const es = new EventSource(u);

        es.onmessage = e => {
            console.log(e);
            this.refreshButtonTarget.click();
        }
    }
}