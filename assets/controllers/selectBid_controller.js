import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    static targets = [
        'bid',
        'submit',
    ]

    bidSelected(event) {
        this.submitTarget.click();
    }
}