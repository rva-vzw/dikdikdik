<?php

$config = new PhpCsFixer\Config();

return $config->setRules([
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
	    'phpdoc_to_comment' => false,
    ])->setFinder(
        PhpCsFixer\Finder::create()
            ->in([
                'src',
                'tests/unit',
                'tests/integration',
                'tests/api',
                'tests/acceptance',
            ])
            ->exclude([
                'src/Migrations',
            ])
    );
