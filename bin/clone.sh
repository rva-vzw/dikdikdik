#!/usr/bin/env bash

EVENT_STORE_TABLE='event_store';
REMOTE_DATABASE='dikdikdik';

source=$1;
# credentials should match the ones in docker-compose.yml
local_mysql_command="mysql -h 127.0.0.1 -u symfony -psymfony symfony"

if [[ -z "$source" ]]
then
  cat << EOF
USAGE: $0 user@server

  Copies the remote event store, and replays locally.
  It is assumed that a ~/.my.cnf is in place on the server so that
  the script can connect to the database without having to provide a password.
EOF
  exit 1
fi

ssh $source "mysqldump $REMOTE_DATABASE $EVENT_STORE_TABLE" | $local_mysql_command
docker-compose exec php make replay