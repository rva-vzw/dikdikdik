# Dik! Dik! Dik!

Development notes in [doc/dev-environment.md](doc/dev-environment.md)

I wanted to write a well tested event sourced application in PHP. 
And I wanted to use the
[Symfony Messenger Component](https://symfony.com/components/Messenger) 
to implement the command bus and the event bus. As a hobby project.

So I decided to write an app to keep track of the score for 
the card game [Wiezen](https://nl.wikipedia.org/wiki/Wiezen).

The app will be implemented according to the rules of
[Rijk Van Afdronk](https://www.rijkvanafdronk.be) (rva), a committee of Belgian
Whist experts that set the standard of careful card playing
("verzorgd kaarten").

Note that the custom framework for event sourcing and CQRS using
Symfony messenger was moved to a standalone project,
[krakboem](https://gitlab.com/rva-vzw/krakboem). And for now, I
am even moving away from this framework. I am trying to use a
decider class now for the event-sourced write side, defined in the 
`Decider` namespace.

## Release version note

I decided to stop numbering the releases for this project in #310.
The last tagged release was version 6.0 (2023-02-24). But from
2023-10-22 on, I will deploy things as they're ready, without creating
new version numbers.

## Security warning!

I didn't invest a lot of time in making this app secure. There is no
authentication in dikdikdik; the app doesn't know who is doing what.

Whoever knows your table identifier, can log games or mess with the player list.
Note that the table identifier is shown in the url and in the title of the main page.

You can send a read-only link to your supporters or fellow players, which does not 
contain the table identifier. So if you distribute only this link, you should be 
rather safe.

It is recommended to use the software over an https connection.

## Work in progress

This project will always be a work in progress, and the progress will depend
on the amount of time I can/want to spend on it.

I also use this project to try new things out, so the code can be a little messy
or inconsistent. But as long as all tests are green, I'm a happy developer.

I'm not very happy about how the frontend is done right now. Initially, I used
vue.js. After that, I moved to [Symfony turbo](https://github.com/symfony/ux-turbo)
and [Symfony live components](https://github.com/symfony/ux-live-component).

So it is a little messy right now. But for the moment, I don't really care
about how the frontend is implemented. Als long as the tests pass, I'm fine.

## API

Dikdikdik has a [public api](doc/api.md), which you might want to use for your own
frontend. Or even for a full-blown card game, that can use the dikdikdik-backend
to keep track of the scores. But note that this API isn't really used, and hardly
tested.

## Questions

### Where can I test the app

I set up an instance at [score.rijkvanafdronk.be](https://score.rijkvanafdronk.be);
you can test it over there as long as my VPS can handle the load. (Please be gentle!)

### Where does the name of the app stand for?

When you've gloriously won a game of Wiezen, it is called 'Dik gewonnen'. To emphasize
the grandeur of your victory, you can use the fixed expression: "Dik! Dik! Dik!"

### Will you adapt the app so that it also support our way of Wiezen?

Initially I didn't have the intention to make this app configurable. But today,
there's the concepts _table conventions_ and _table tariffs_. So if you want to
adapt the app for your own rules, you will get somewhere by editing
the [conventions.yaml](config/conventions.yaml) file, and providing
an implementation of the [Tariffs](src/Domain/ScoreSheet/Tariffs) interface.

### Timeline

As with most of my projects, there is no timeline. Also, since most of my hobby programming
projects died before they were even useful, I am happy I got this far already 😉. 
We'll see what happens;
You can follow the progress of the project on the [gitlab page](https://gitlab.com/rva-vzw/dikdikdik).
