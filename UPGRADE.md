# Upgrade instructions

## 2024-08-03

I added an index to the table state read model in #329. You can replay, or run this migration:

```bash
./bin/console dikdikdik:upgrade:20240718
```

## 2024-07-18

The table state read model also needs to be updated, see #345.
You can replay, or run this migration:

```bash
./bin/console dikdikdik:upgrade:20240718
```

And I renamed the conventions, which should be reflected in the existing
events:

```bash
./bin/console dikdikdik:upgrade:20240718-2
```

## 2024-07-17

I moved the events around, so reconstituting existing tables and score sheets will
not work anymore. Replay will also be broken.

I provided an event migration:

```bash
./bin/console dikdikdik:upgrade:20240717
```

## 2024-04-01

The table state read model changed. You can either quickly update the
read model:

```
./bin/console dikdikdik:upgrade:20240401
```

or, alternatively, you can run a full replay (which may take a while
if you have lots of events in your event store):

```
make replay
```

## Older versions:

Note that these instructions won't work anymore since version 6.
If you really want to upgrade from older versions, first revert to version
6 first, to have the migration commands available.

## Upgrading minor versions (e.g. from 1.2 to 1.3)

It is recommended to replay the events from the event store, so that all read models are up to date.

```
make replay
```

## Upgrading major versions

### v4 -> v5

```
# put the app in maintenance mode
echo MAINTENANCE=true >> .env.local
# Let's backup the event store, in case something goes wrong.
# (adapt the command below to your needs)
mysqldump dikdikdik event_store | gzip > ~/event-store.sql.gz
# perform the migration; this can take several minutes
# you also may get warnings about players of which the name
# was not found. You can ignore those, but let's save them to
# a file for reference:
./bin/console dikdikdik:migrate:toV5 | tee migration-output.txt
# replay
./bin/console make replay
# disable maintenance mode (may be not needed)
sed -i '/^MAINTENANCE=true$$/d' .env.local
```


### v3 -> v4

```
# put the app in maintenance mode
echo MAINTENANCE=true >> .env.local
# Let's backup the event store, in case something goes wrong.
# (adapt the command below to your needs)
mysqldump dikdikdik event_store | gzip > ~/event-store.sql.gz
# perform the migration; this can take several minutes
./bin/console dikdikdik:migrate:toV4
# replay won't be needed
# disable maintenance mode (may be not needed)
sed -i '/^MAINTENANCE=true$$/d' .env.local
```

### v1 -> v2

To keep your event stream intact when moving to version 2, you'll have to perform
an event migration:

```
# put the app in maintenance mode
echo MAINTENANCE=true >> .env.local
# Let's backup the event store, in case something goes wrong.
# (adapt the command below to your needs)
mysqldump dikdikdik event_store | gzip > ~/event-store.sql.gz
# perform the migration; this can take several minutes
./bin/console dikdikdik:migrate:toV2
# replay, as always
make replay
# disable maintenance mode (may be not needed)
sed -i '/^MAINTENANCE=true$$/d' .env.local
```
