.PHONY: qa fix php-cs-fixer test test-ff reset testdata security-checker test-unit test-integration test-acceptance

qa: php-cs-fixer deptrac psalm phpstan

php-cs-fixer:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer --dry-run -v fix

phpstan:
	php -d memory_limit=4G ./vendor/bin/phpstan analyse --no-progress

psalm:
	./vendor/bin/psalm --no-progress --threads=1

deptrac:
	./vendor/bin/deptrac

fix:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer -vv fix

test-unit:
	./vendor/bin/codecept run unit

test-integration:
	./vendor/bin/codecept run integration

test-acceptance: tests/_support/_generated/IntegrationTesterActions.php testdata
	./vendor/bin/codecept run api,acceptance

test: tests/_support/_generated/IntegrationTesterActions.php test-unit test-integration test-acceptance

test-ff: tests/_support/_generated/IntegrationTesterActions.php testdata
	./vendor/bin/codecept run unit,integration,api,acceptance --fail-fast

tests/_support/_generated/IntegrationTesterActions.php:
	./vendor/bin/codecept build

reset-read-model:
	./bin/console doctrine:schema:drop --force --em=read_model
	./bin/console doctrine:schema:create --em=read_model

# Prevent duplicate deployment with a mkdir statement.

replay: reset-read-model
	mkdir replay_lock
	echo MAINTENANCE=true >> .env.local
	./bin/console cache:clear
	./bin/console eventsourcing:readmodel:replay
	sed -i '/^MAINTENANCE=true$$/d' .env.local
	./bin/console cache:clear
	rmdir replay_lock

reset: reset-read-model
	./bin/console doctrine:schema:drop --force --em=event_store
	./bin/console doctrine:schema:create --em=event_store

testdata: reset
	./bin/console dikdikdik:testdata

