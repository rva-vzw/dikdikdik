<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\Player\Service\SeatedPlayersByScoreSheet;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Command\ReorderPlayers;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\OrderedPlayers;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Webmozart\Assert\Assert;

/**
 * This controller allows to re-arrange the players using an ugly web form.
 *
 * This is a workaround for an issue with selenium and sortable.js.
 * See https://github.com/SortableJS/sortablejs/issues/563
 */
final class PlayerHackController extends AbstractController
{
    /** @var CommandBus */
    private $commandBus;

    public function __construct(
        CommandBus $commandBus
    ) {
        $this->commandBus = $commandBus;
    }

    #[Route(path: '/hack/table/{tableId}/{game}', name: 'dikdikdik.playerPermutation')]
    public function reorderPlayersHack(Request $request, string $tableId, int $game, SeatedPlayersByScoreSheet $playersAtTable): Response
    {
        Assert::notEmpty($tableId);
        $permutation = $request->request->get('permutation');
        if (is_string($permutation)) {
            $tableIdentifier = TableIdentifier::fromString($tableId);
            $originalSeatedPlayers = $playersAtTable->getSeatedPlayers(
                ScoreSheetIdentifier::forTable($tableIdentifier)
            );

            $orderedPlayers = OrderedPlayers::fromArray(
                array_map(
                    function (string $seatString) use ($originalSeatedPlayers): PlayerIdentifier {
                        return $originalSeatedPlayers->getPlayerAtSeat(Seat::fromInteger((int) $seatString));
                    },
                    str_split($permutation)
                )
            );

            $this->commandBus->dispatch(
                new ReorderPlayers(
                    $tableIdentifier,
                    GameNumber::fromInteger($game),
                    $orderedPlayers
                )
            );
        }

        return $this->render('playerHack.html.twig');
    }
}
