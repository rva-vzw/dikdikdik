<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\ScoreLog\Service\CurrentGamesAndPlayers;
use App\Domain\ReadModel\TablePermissions\PermissionsByTable;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\ConfigureTable;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\KickPlayer;
use App\Domain\Table\Command\RejoinKnownPlayer;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\TableIdentifier;
use App\Domain\TableConventions\ConventionsByTable;
use App\Infrastructure\Form\PlayersFormData;
use App\Infrastructure\Form\PlayersFormType;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Webmozart\Assert\Assert;

#[Route(path: '/{_locale}/table/{tableId}/players', name: 'players')]
final class PlayersGroupController extends AbstractController
{
    public function __construct(private readonly CommandBus $commandBus)
    {
    }

    public function __invoke(
        string $_locale,
        string $tableId,
        Request $request,
        CurrentGamesAndPlayers $currentGameAndPlayers,
        ConventionsByTable $conventionsByTable,
        PermissionsByTable $permissionsByTable,
    ): Response {
        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);

        $gameAndPlayers = $currentGameAndPlayers->getGameAndPlayers(
            ScoreSheetIdentifier::forTable($tableIdentifier),
        );
        $tablePermissions = $permissionsByTable->getPermissionsByTable($tableIdentifier);

        $conventions = $conventionsByTable->getConventionsByTable($tableIdentifier);

        $formData = new PlayersFormData(
            newPlayerName: null,
            conventionsName: $conventions->name,
            dealerId: (string) $gameAndPlayers->getDealerIdentifier() ?: null,
            // ID of player to kick/rejoin
            playerIdForAction: null,
        );

        $form = $this->createForm(PlayersFormType::class, $formData, [
            'players' => $gameAndPlayers->playersGroup,
            'permissions' => $tablePermissions,
        ]);

        Assert::isInstanceOf($form, Form::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // This form has a lot of functionalities. Let's see
            // what applies...
            // TODO: create a set of handler classes, that each check what to do
            //  given the form data.

            // Adding new players:
            $clickedButton = $form->getClickedButton();
            Assert::nullOrIsInstanceOf($clickedButton, FormInterface::class);
            if ('addPlayer' === $clickedButton?->getName() && $formData->newPlayerName) {
                $this->commandBus->dispatch(new JoinPlayer(
                    $tableIdentifier,
                    PlayerIdentifier::create(),
                    $gameAndPlayers->currentGameNumber,
                    $formData->newPlayerName,
                    null,
                ));
            }

            // Deleting players:
            if ('kickButton' === $clickedButton?->getName() && $formData->playerIdForAction) {
                $this->commandBus->dispatch(new KickPlayer(
                    $tableIdentifier,
                    PlayerIdentifier::fromString($formData->playerIdForAction),
                    $gameAndPlayers->currentGameNumber,
                ));
            }

            // Re-activating players
            if ('rejoinButton' === $clickedButton?->getName() && $formData->playerIdForAction) {
                $this->commandBus->dispatch(new RejoinKnownPlayer(
                    $tableIdentifier,
                    PlayerIdentifier::fromString($formData->playerIdForAction),
                    $gameAndPlayers->currentGameNumber,
                ));
            }

            // Selecting a dealer:
            if (
                !empty($formData->dealerId)
                && $formData->getDealerIdentifier() != $gameAndPlayers->getDealerIdentifier()
            ) {
                $this->commandBus->dispatch(new AnnounceDealer(
                    $tableIdentifier,
                    PlayerIdentifier::fromString($formData->dealerId),
                    $gameAndPlayers->currentGameNumber,
                ));
            }

            // Selecting conventions
            if (
                !empty($formData->conventionsName)
                && $formData->conventionsName != $conventions->name
            ) {
                $this->commandBus->dispatch(new ConfigureTable(
                    $tableIdentifier,
                    $formData->conventionsName,
                ));
            }

            // Returning to the score sheet:
            if ('submit' === $clickedButton?->getName()) {
                return $this->redirectToRoute('table', ['tableId' => $tableId]);
            }

            return $this->redirectToRoute('players', [
                'tableId' => $tableId,
            ]);
        }

        return $this->render(
            'players.html.twig',
            [
                'form' => $form,
                'gameAndPlayers' => $gameAndPlayers,
                'permissions' => $tablePermissions,
            ],
        );
    }
}
