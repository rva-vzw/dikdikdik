<?php

namespace App\Infrastructure\Controller;

use App\Domain\Table\Loggable\GameSpecification\GameSpecification;
use App\Domain\Table\TableIdentifier;
use App\Domain\TableConventions\ConventionsByTable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Webmozart\Assert\Assert;

final class GamesApiController extends AbstractController
{
    #[Route(path: '/api/games/{tableId}', name: 'api.games', options: ['expose' => true])]
    public function getAllSpecifications(string $tableId, ConventionsByTable $conventionsByTable): Response
    {
        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $specifications = $conventionsByTable->getConventionsByTable($tableIdentifier)->allowedGames;
        $result = [];
        /** @var GameSpecification $specification */
        foreach ($specifications as $specification) {
            $announcementNameMethod = new \ReflectionMethod(
                $specification->getAnnouncementType(),
                'getName'
            );

            $outcomeNameMethod = new \ReflectionMethod(
                $specification->getOutcomeType(),
                'getName'
            );

            $result[] = [
                'name' => $specification->getName(),
                'announcement' => $announcementNameMethod->invoke(null),
                'outcome' => $outcomeNameMethod->invoke(null),
            ];
        }

        return $this->json(
            $result
        );
    }
}
