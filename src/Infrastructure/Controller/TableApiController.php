<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\TableState\TableStates;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\AnnounceFinalRound;
use App\Domain\Table\Command\ConfigureTable;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\KickPlayer;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\Command\ProposeAndNobodyGoesAlong;
use App\Domain\Table\Command\ReorderPlayers;
use App\Domain\Table\Command\StarPlayer;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Player\OrderedPlayers;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\Domain\TableConventions\AvailableConventions;
use App\Infrastructure\Controller\Helper\LogGameCommandFactory;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Webmozart\Assert\Assert;

final class TableApiController extends AbstractController
{
    // Please don't just inject extra services via the constructor.
    // You can inject them via the command actions as well, and that way
    // you won't break existing unit tests.
    public function __construct(
        private readonly CommandBus $commandBus,
        private readonly DenormalizerInterface $denormalizer,
        private readonly AllSpecifiedGames $specifiedGames,
    ) {
    }

    /**
     * Adds a player to the table.
     *
     * Seat number can be specified via the request parameters. If none is provided,
     * a free seat will be assigned.
     */
    #[Route(path: '/api/table/{tableId}/game/{game}/players/{playerId}', name: 'api.table.join', methods: 'PUT', options: ['expose' => true])]
    public function joinPlayer(Request $request, string $tableId, int $game, string $playerId): Response
    {
        Assert::notEmpty($tableId);
        Assert::notEmpty($playerId);
        // I wonder whether there's an easier way to get the params...
        /** @var string $content */
        $content = $request->getContent();
        /** @var array{player_name: string, seat?: string} $params */
        $params = json_decode($content, true);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $playerIdentifier = PlayerIdentifier::fromString($playerId);
        $playerName = $params['player_name'];
        $seat = array_key_exists('seat', $params) ? Seat::fromInteger((int) $params['seat']) : null;
        if (empty($playerName)) {
            // The backend part doesn't really care about player names, but we do block empty player names here,
            // to keep the frontend comprehensible.
            return new Response(
                'Please provide a player name',
                Response::HTTP_FORBIDDEN
            );
        }
        $this->commandBus->dispatch(
            new JoinPlayer($tableIdentifier, $playerIdentifier, GameNumber::fromInteger($game), $playerName, $seat)
        );

        return $this->json(
            [],
            Response::HTTP_NO_CONTENT
        );
    }

    #[Route(path: '/api/table/{tableId}/game/{game}/players/{playerId}', name: 'api.table.kick', methods: 'DELETE', options: ['expose' => true])]
    public function kickPlayer(string $tableId, string $playerId, int $game): Response
    {
        Assert::notEmpty($tableId);
        Assert::notEmpty($playerId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $playerIdentifier = PlayerIdentifier::fromString($playerId);
        $gameNumber = GameNumber::fromInteger($game);
        $this->commandBus->dispatch(
            new KickPlayer($tableIdentifier, $playerIdentifier, $gameNumber)
        );

        return $this->json(
            [],
            Response::HTTP_NO_CONTENT
        );
    }

    #[Route(path: '/api/table/{tableId}/game/{game}/dealer/{playerId}', name: 'api.table.announceDealer', methods: 'PUT', options: ['expose' => true])]
    public function announceDealer(string $tableId, string $playerId, int $game): Response
    {
        Assert::notEmpty($tableId);
        Assert::notEmpty($playerId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $playerIdentifier = PlayerIdentifier::fromString($playerId);
        $gameNumber = GameNumber::fromInteger($game);
        $this->commandBus->dispatch(
            new AnnounceDealer($tableIdentifier, $playerIdentifier, $gameNumber)
        );

        return $this->json(
            [],
            Response::HTTP_NO_CONTENT
        );
    }

    #[Route(path: '/api/table/{tableId}/game/{game}/result/{gameName}', name: 'api.table.logGame', methods: 'POST', options: ['expose' => true])]
    public function logGame(
        Request $request,
        LogGameCommandFactory $logGameCommandFactory,
        string $tableId,
        int $game,
        string $gameName,
    ): Response {
        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $gameNumber = GameNumber::fromInteger($game);
        $gameSpecification = $this->specifiedGames->getByGameName($gameName);
        /** @var string */
        $content = $request->getContent();
        /** @var array{announcement: array<string, string>, outcome: array<string, string>} $params */
        $params = json_decode($content, true);
        $command = $logGameCommandFactory->createCommandFromArray(
            $tableIdentifier,
            $gameNumber,
            $gameSpecification,
            $params
        );
        $this->commandBus->dispatch($command);

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    #[Route(path: '/api/table/{tableId}/game/{game}/pass-around', name: 'api.table.passAround', methods: 'POST', options: ['expose' => true])]
    public function passAround(string $tableId, int $game): Response
    {
        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $gameNumber = GameNumber::fromInteger($game);
        $this->commandBus->dispatch(
            new PassAround($tableIdentifier, $gameNumber)
        );

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    #[Route(path: '/api/table/{tableId}/game/{game}/nobody-along', name: 'api.table.proposeNobodyAlong', methods: 'POST', options: ['expose' => true])]
    public function proposeNobodyAlong(string $tableId, int $game): Response
    {
        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $gameNumber = GameNumber::fromInteger($game);
        $this->commandBus->dispatch(
            new ProposeAndNobodyGoesAlong($tableIdentifier, $gameNumber)
        );

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * Returns 'user permissions' for the current game.
     *
     * We might add a game number as a parameter in the future.
     */
    #[Route(path: '/api/table/{tableId}/permissions', name: 'api.table.permissions', options: ['expose' => true])]
    public function getPermissions(string $tableId, TableStates $tableStates): Response
    {
        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $tableState = $tableStates->getTableState($tableIdentifier);

        // A value object for the actual permissions would have been nice.
        $result = [
            'announce_dealer' => $tableState->dealerChangeable,
            'log_game' => $tableState->canPlay(),
            'announce_final_round' => $tableState->canPlay() && !$tableState->isFinalRound(),
            'add_player' => $tableState->hasFreePlaces(),
            'configure_table' => $tableState->canChangeRuleSet,
        ];

        return $this->json($result);
    }

    #[Route(path: '/api/table/{tableId}/game/{game}/final', name: 'api.table.finalRound', methods: 'POST', options: ['expose' => true])]
    public function announceFinalRound(string $tableId, int $game): Response
    {
        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $gameNumber = GameNumber::fromInteger($game);
        $this->commandBus->dispatch(
            new AnnounceFinalRound($tableIdentifier, $gameNumber)
        );

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    #[Route(path: '/api/table/{tableId}/config', name: 'api.table.configure', methods: 'PUT', options: ['expose' => true])]
    public function configureTable(
        Request $request,
        AvailableConventions $availableRuleSets,
        string $tableId,
    ): Response {
        Assert::notEmpty($tableId);
        /** @var string $content */
        $content = $request->getContent();
        /** @var mixed[] $params */
        $params = json_decode($content, true);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        // I don't use the denormalizer here, because axios generates
        // e.g. {"alone5Enabled":"false"}, where the denormalizer
        // expects {"alone5Enabled":false}
        if ('false' === $params['alone5Enabled'] || empty($params['alone5Enabled'])) {
            $ruleSetName = 'conventions.traditional';
        } else {
            $ruleSetName = 'conventions.alone5';
        }
        $this->commandBus->dispatch(
            new ConfigureTable($tableIdentifier, $ruleSetName),
        );

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    #[Route(path: '/api/table/{tableId}', name: 'api.table.info', methods: 'GET', options: ['expose' => true])]
    public function tableInfo(string $tableId): Response
    {
        Assert::notEmpty($tableId);
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
            TableIdentifier::fromString($tableId)
        );

        return $this->json(
            [
                'table' => $tableId,
                'sheet' => $scoreSheetIdentifier->toString(),
            ]
        );
    }

    #[Route(path: '/api/table/{tableId}/{game}/players', name: 'api.table.playerOrder', methods: 'PUT', options: ['expose' => true])]
    public function changePlayerOrder(Request $request, string $tableId, int $game): Response
    {
        Assert::notEmpty($tableId);
        // I wonder whether there's an easier way to get the params...
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        /** @var OrderedPlayers $playerIdentifiers */
        $playerIdentifiers = $this->denormalizer->denormalize(
            $params,
            OrderedPlayers::class
        );
        $this->commandBus->dispatch(
            new ReorderPlayers($tableIdentifier, GameNumber::fromInteger($game), $playerIdentifiers)
        );

        return $this->json(
            [],
            Response::HTTP_NO_CONTENT
        );
    }

    #[Route(path: '/api/table/{tableId}/{game}/players/{playerId}/star', name: 'api.table.starPlayer', methods: 'POST', options: ['expose' => true])]
    public function starPlayer(string $tableId, int $game, string $playerId): Response
    {
        Assert::notEmpty($tableId);
        Assert::notEmpty($playerId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $playerIdentifier = PlayerIdentifier::fromString($playerId);
        $gameNumber = GameNumber::fromInteger($game);
        $this->commandBus->dispatch(
            new StarPlayer(
                $tableIdentifier,
                $gameNumber,
                $playerIdentifier
            )
        );

        return $this->json(
            [],
            Response::HTTP_NO_CONTENT
        );
    }
}
