<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\Exception\ReadModelNotFoundException;
use App\Domain\ReadModel\Player\Service\CurrentGamePlayers;
use App\Domain\ReadModel\Player\Service\PlayersGroups;
use App\Domain\ReadModel\ScoreLog\Service\ScoreLogs;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayersInvolved;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\WebLink\Link;
use Webmozart\Assert\Assert;

final class ScoreSheetApiController extends AbstractController
{
    /**
     * Returns all the known players (ever) on this table.
     */
    #[Route(path: '/api/sheet/{scoreSheetId}/players', name: 'api.scores.player_details', options: ['expose' => true])]
    public function getKnownPlayerDetails(string $scoreSheetId, PlayersGroups $knownPlayerDetails): Response
    {
        Assert::notEmpty($scoreSheetId);
        try {
            // So maybe the Symfony serializer will do all the work for me?
            return $this->json(
                $knownPlayerDetails->getPlayersGroup(
                    ScoreSheetIdentifier::fromString($scoreSheetId)
                )
            );
        } catch (ReadModelNotFoundException $ex) {
            throw new NotFoundHttpException();
        }
    }

    #[Route(path: '/api/sheet/{scoreSheetId}', name: 'api.scores.sheet', options: ['expose' => true])]
    public function getSheet(string $scoreSheetId, ScoreLogs $scoreLogs, CurrentGamePlayers $playingAtTable, NormalizerInterface $normalizer, Request $request): Response
    {
        Assert::notEmpty($scoreSheetId);
        // This parameter is automatically created by the MercureBundle
        $hubUrl = $this->getParameter('mercure.default_hub');
        assert(is_string($hubUrl));
        // Link: <http://localhost:3000/.well-known/mercure>; rel="mercure"
        $this->addLink($request, new Link('mercure', $hubUrl));
        // TODO: create normalizable value object for this?
        $scoreSheetIdentifier = ScoreSheetIdentifier::fromString($scoreSheetId);
        $scoreLog = $scoreLogs->getScoreLog($scoreSheetIdentifier);
        /** @var mixed[] $normalizedLog */
        $normalizedLog = $normalizer->normalize($scoreLog->getIterator());
        $currentDealer = $scoreLog->getCurrentDealer();
        $playersInvolved = $playingAtTable->findPlayersInvolved($scoreSheetIdentifier);
        $result = [
            'log' => array_values($normalizedLog),
            'dealer' => $currentDealer instanceof PlayerIdentifier ? $currentDealer->toString() : '',
            'current_game' => $scoreLog->getCurrentGameNumber()->toInteger(),
            'playing_players' => $playersInvolved instanceof PlayersInvolved ? $normalizer->normalize($playersInvolved->getPlayingPlayers()) : [],
        ];

        return $this->json($result);
    }
}
