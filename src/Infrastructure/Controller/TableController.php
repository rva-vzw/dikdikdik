<?php

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\Game\Service\CurrentGames;
use App\Domain\ReadModel\ScoreLog\Service\ScoreLogs;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Loggable\Loggable;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\Form\BidData;
use App\Infrastructure\Form\BidType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Webmozart\Assert\Assert;

#[Route(path: '/{_locale}/table/{tableId}', name: 'table')]
final class TableController extends AbstractController
{
    public function __construct(
    ) {
    }

    public function __invoke(
        Request $request,
        string $tableId,
        string $_locale,
        // CurrentGames is not needed here, actually anywhere. But it has
        // an integration test. So by using it at one place, the dependency
        // injection container is aware of it, and the test will still run.
        // FIXME: Run the test on another service, so that we can get rid of the dependency here.
        CurrentGames $currentGames,
        ScoreLogs $scoreLogs,
    ): Response {
        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable($tableIdentifier);
        $scoreLog = $scoreLogs->getScoreLog($scoreSheetIdentifier);

        $bidForm = $this->createForm(
            BidType::class,
            new BidData(null), [
                'table_identifier' => $tableIdentifier,
            ],
        );

        $bidForm->handleRequest($request);
        if ($bidForm->isSubmitted() && $bidForm->isValid()) {
            /** @var BidData $data */
            $data = $bidForm->getData();
            $bid = $data->loggable;
            Assert::isInstanceOf($bid, Loggable::class);

            return $this->redirectToRoute('log', [
                'tableId' => $tableId,
                'bid' => $bid->getName(),
            ]);
        }

        return $this->render(
            'table.html.twig',
            [
                // FIXME: We can probably do without passing the score log
                'scoreLog' => $scoreLog,
                'tableIdentifier' => $tableIdentifier,
                'bidForm' => $bidForm->createView(),
                // The template expects the language; this is important to
                // generate the updates for mercure.
                'lang' => $_locale,
            ],
        );
    }
}
