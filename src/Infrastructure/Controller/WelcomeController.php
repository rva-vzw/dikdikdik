<?php

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\GameCounter\ActiveTablesCounter;
use App\Domain\ReadModel\GameCounter\Month;
use App\Domain\ReadModel\GameCounter\MonthlyTotal;
use App\Domain\ReadModel\GameCounter\MonthlyTotals;
use App\Domain\Table\TableIdentifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

final class WelcomeController extends AbstractController
{
    private const MIN_GAMES_FOR_RELEVANT_TABLE = 4;

    #[Route(path: '/', name: 'language')]
    public function chooseLanguage(ActiveTablesCounter $activeTablesCounter, MonthlyTotals $monthlyTotals, ChartBuilderInterface $chartBuilder): Response
    {
        $currentMonth = Month::current();
        $lastYear = Month::fromDateTimeImmutable(new \DateTimeImmutable('-1 year'));
        $totals = iterator_to_array(
            $monthlyTotals->getByMonth(
                $lastYear,
                $currentMonth,
                self::MIN_GAMES_FOR_RELEVANT_TABLE
            )
        );
        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $data = [
            'labels' => array_map(
                fn (MonthlyTotal $total) => (string) $total->getMonth(),
                $totals,
            ),
            'datasets' => [
                [
                    'type' => 'line',
                    'label' => 'games dealt',
                    'data' => array_map(fn (MonthlyTotal $total) => $total->getTotalGames(), $totals),
                    'borderColor' => 'red',
                    'yAxisID' => 'y-axis-1',
                    'tension' => 0.3,
                ],
                [
                    'type' => 'line',
                    'label' => 'active tables',
                    'data' => array_map(fn (MonthlyTotal $total) => $total->getTotalTables(), $totals),
                    'borderColor' => 'blue',
                    'yAxisID' => 'y-axis-2',
                    'tension' => 0.3,
                ],
            ],
        ];
        $chart->setData($data);
        $chart->setOptions([
            'scales' => [
                'y-axis-1' => [
                    'id' => 'y-axis-1',
                    'type' => 'linear',
                ],
                'y-axis-2' => [
                    'type' => 'linear',
                    'position' => 'right',
                ],
            ],
        ]);

        return $this->render(
            'chooseLanguage.html.twig',
            [
                'tableId' => TableIdentifier::create()->toString(),
                'activeTables' => $activeTablesCounter->getActiveTablesCount(
                    new \DateTimeImmutable(),
                    new \DateTimeImmutable('-20 minutes'),
                    1
                ),
                'chart' => $chart,
            ]
        );
    }

    #[Route(path: '/{_locale}/about', name: 'about')]
    public function about(string $_locale): Response
    {
        return $this->render(
            'about.html.twig',
            [
                'locale' => $_locale,
            ]
        );
    }
}
