<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\GameCounter\ActiveTablesCounter;
use App\Domain\ReadModel\GameCounter\Month;
use App\Domain\ReadModel\GameCounter\MonthlyTotals;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class StatisticsApiController
{
    private const A_WHILE_AGO = '-20 minutes';
    private const MIN_GAMES_FOR_RELEVANT_TABLE = 4;

    public function __construct(
        private NormalizerInterface $normalizer
    ) {
    }

    #[Route(path: '/api/stats/active', name: 'dikdikdik.api.stats.active_tables', options: ['expose' => true])]
    public function getNumberOfActiveTables(ActiveTablesCounter $activeTablesCounter): Response
    {
        $aWhileAgo = new \DateTimeImmutable(self::A_WHILE_AGO);

        return new JsonResponse(
            $activeTablesCounter->getActiveTablesCount(
                new \DateTimeImmutable(),
                $aWhileAgo,
                1
            )
        );
    }

    #[Route(path: '/api/stats/monthly', name: 'dikdikdik.api.stats.monthly', options: ['expose' => true])]
    public function getMonthlyTotals(MonthlyTotals $monthlyTotals): Response
    {
        $currentMonth = Month::current();
        $lastYear = Month::fromDateTimeImmutable(new \DateTimeImmutable('-1 year'));
        $totals = iterator_to_array(
            $monthlyTotals->getByMonth(
                $lastYear,
                $currentMonth,
                self::MIN_GAMES_FOR_RELEVANT_TABLE
            )
        );

        return new JsonResponse(
            $this->normalizer->normalize($totals)
        );
    }
}
