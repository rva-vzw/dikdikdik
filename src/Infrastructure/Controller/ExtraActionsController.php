<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\ScoreLog\Service\CurrentGamesAndPlayers;
use App\Domain\ReadModel\TablePermissions\PermissionsByTable;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Command\AnnounceFinalRound;
use App\Domain\Table\Command\StarPlayer;
use App\Domain\Table\Command\Undo;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\Form\ExtraActionsData;
use App\Infrastructure\Form\ExtraActionsType;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Webmozart\Assert\Assert;

#[Route(path: '/{_locale}/table/{tableId}/extra', name: 'extra')]
final class ExtraActionsController extends AbstractController
{
    public function __invoke(
        Request $request,
        string $tableId,
        CurrentGamesAndPlayers $currentGamesAndPlayers,
        PermissionsByTable $permissionsByTable,
        CommandBus $commandBus,
    ): Response {
        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable($tableIdentifier);
        $currentGameAndPlayers = $currentGamesAndPlayers->getGameAndPlayers($scoreSheetIdentifier);
        $currentGame = $currentGameAndPlayers->currentGameNumber;

        $formData = new ExtraActionsData();

        $form = $this->createForm(
            ExtraActionsType::class,
            $formData,
            [
                'table_identifier' => $tableIdentifier,
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Form $form */
            $clickedButton = $form->getClickedButton();
            $selectedPlayer = $formData->player;

            $command = match ($clickedButton) {
                $form->get('undo') => new Undo($tableIdentifier, $currentGame),
                $form->get('finalRound') => new AnnounceFinalRound($tableIdentifier, $currentGame),
                $form->get('star') => $selectedPlayer instanceof Player
                    ? new StarPlayer($tableIdentifier, $currentGame, $selectedPlayer->getPlayerIdentifier())
                    : null,
                default => throw new \LogicException('Extra Actions: WTF did you click?'),
            };

            if ($command instanceof Command) {
                $commandBus->dispatch($command);

                return $this->redirectToRoute('table', [
                    'tableId' => $tableId,
                    '_fragment' => 'bid_loggable',
                ]);
            }
        }

        return $this->render('extraActions.html.twig', [
            'permissions' => $permissionsByTable->getPermissionsByTable($tableIdentifier),
            'form' => $form->createView(),
        ]);
    }
}
