<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Helper\Exception;

final class NoGameSpecified extends \Exception
{
    public static function create(): self
    {
        return new self('No game specified');
    }
}
