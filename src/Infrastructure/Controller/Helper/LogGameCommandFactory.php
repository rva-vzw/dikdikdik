<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Helper;

use App\Domain\CurrentTime;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TroelAwareMultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TroelAwareSinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\GameSpecification;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Loggable\Outcome\Winners;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\Controller\Helper\Exception\NoGameSpecified;
use App\Infrastructure\Form\LogGameData;
use InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Webmozart\Assert\Assert;

final class LogGameCommandFactory
{
    public function __construct(
        private DenormalizerInterface $denormalizer,
        private CurrentTime $currentTime
    ) {
    }

    /**
     * Create a LogGame command based on the parameters passed to the API.
     *
     * @param array{announcement:array<string,string|string[]>, outcome:array<string,string|string[]>} $params
     *
     * @deprecated please use createCommandFromFormData instead
     */
    public function createCommandFromArray(TableIdentifier $tableIdentifier, GameNumber $gameNumber, GameSpecification $gameSpecification, array $params): LogGame
    {
        switch ($gameSpecification->getAnnouncementType()) {
            case SinglePlayerAnnouncement::class:
                $announcement = new SinglePlayerAnnouncement(
                    PlayerIdentifier::fromString($this->validatedString($params['announcement']['player1']))
                );
            break;
            case TwoPlayerAnnouncement::class:
                $announcement = new TwoPlayerAnnouncement(
                    PlayerIdentifier::fromString($this->validatedString($params['announcement']['player1'])),
                    PlayerIdentifier::fromString($this->validatedString($params['announcement']['player2'])),
                );
            break;
            case MultiPlayerAnnouncement::class:
                /** @var PlayerIdentifiers $playerIdentifiers */
                $playerIdentifiers = $this->denormalizer->denormalize($params['announcement']['playerSet'], PlayerIdentifiers::class);
                $announcement = new MultiPlayerAnnouncement(
                    $playerIdentifiers
                );
            break;
            case TroelAwareMultiPlayerAnnouncement::class:
                /** @var PlayerIdentifiers $playerIdentifiers */
                $playerIdentifiers = $this->denormalizer->denormalize($params['announcement']['playerSet'], PlayerIdentifiers::class);
                $announcement = new TroelAwareMultiPlayerAnnouncement(
                    $playerIdentifiers,
                    !empty($params['announcement']['onTopOfTroel'])
                );
            break;
            case TroelAwareSinglePlayerAnnouncement::class:
                $announcement = new TroelAwareSinglePlayerAnnouncement(
                    PlayerIdentifier::fromString($this->validatedString($params['announcement']['player1'])),
                    !empty($params['announcement']['onTopOfTroel'])
                );
            break;
            default:
                throw new InvalidArgumentException('Announcement type not supported by LogGameCommandFactory.');
        }

        switch ($gameSpecification->getOutcomeType()) {
            case NumberOfTricks::class:
                $outcome = NumberOfTricks::fromNumber(intval($params['outcome']['tricks']));
            break;
            case AllOrNothing::class:
                $outcome = 0 === intval($params['outcome']['success'])
                    ? AllOrNothing::asFailure()
                    : AllOrNothing::asSuccess();
            break;
            case Winners::class:
                /** @var PlayerIdentifiers $playerIdentifiers */
                $playerIdentifiers = $this->denormalizer->denormalize($params['outcome']['winners'], PlayerIdentifiers::class);
                $outcome = new Winners(
                    $playerIdentifiers
                );
            break;
            default:
                throw new InvalidArgumentException('Outcome type not supported by LogGameCommandFactory.');
        }

        return new LogGame(
            $tableIdentifier,
            $gameNumber,
            $gameSpecification,
            $announcement,
            $outcome,
            $this->currentTime->getCurrentTime(),
        );
    }

    public function setCurrentTime(CurrentTime $currentTime): void
    {
        $this->currentTime = $currentTime;
    }

    public function createCommandFromFormData(
        TableIdentifier $tableIdentifier,
        GameNumber $gameNumber,
        LogGameData $logGameData,
    ): LogGame {
        $gameSpecification = $logGameData->loggable;
        if (!$gameSpecification instanceof GameSpecification) {
            throw NoGameSpecified::create();
        }

        $announcement = match ($gameSpecification->getAnnouncementType()) {
            SinglePlayerAnnouncement::class => new SinglePlayerAnnouncement(
                $logGameData->getPlayer1Identifier(),
            ),
            TwoPlayerAnnouncement::class => new TwoPlayerAnnouncement(
                $logGameData->getPlayer1Identifier(),
                $logGameData->getPlayer2Identifier(),
            ),
            MultiPlayerAnnouncement::class => new MultiPlayerAnnouncement(
                $logGameData->getAnnouncingPlayerIdentifiers(),
            ),
            TroelAwareMultiPlayerAnnouncement::class => new TroelAwareMultiPlayerAnnouncement(
                $logGameData->getAnnouncingPlayerIdentifiers(),
                $logGameData->onTopOfTroel,
            ),
            TroelAwareSinglePlayerAnnouncement::class => new TroelAwareSinglePlayerAnnouncement(
                $logGameData->getPlayer1Identifier(),
                $logGameData->onTopOfTroel
            ),
            default => throw new InvalidArgumentException('Announcement type not supported by LogGameCommandFactory.')
        };

        $outcome = match ($gameSpecification->getOutcomeType()) {
            NumberOfTricks::class => NumberOfTricks::fromNumber($logGameData->numberOfTricks),
            AllOrNothing::class => $logGameData->success ? AllOrNothing::asSuccess() : AllOrNothing::asFailure(),
            Winners::class => new Winners($logGameData->getWinningPlayerIdentifiers()),
            // no break
            default => throw new InvalidArgumentException('Outcome type not supported by LogGameCommandFactory.'),
        };

        return new LogGame(
            $tableIdentifier,
            $gameNumber,
            $gameSpecification,
            $announcement,
            $outcome,
            $this->currentTime->getCurrentTime(),
        );
    }

    /**
     * @return non-empty-string
     */
    private function validatedString(mixed $str): string
    {
        Assert::string($str);
        Assert::notEmpty($str);

        return $str;
    }
}
