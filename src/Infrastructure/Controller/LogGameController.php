<?php

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\ScoreLog\Service\CurrentGamesAndPlayers;
use App\Domain\ReadModel\TablePermissions\PermissionsByTable;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\Command\ProposeAndNobodyGoesAlong;
use App\Domain\Table\Exception\InvalidAnnouncement;
use App\Domain\Table\Rules\RulesByTable;
use App\Domain\Table\TableIdentifier;
use App\Domain\TableConventions\ConventionsByTable;
use App\Infrastructure\Controller\Helper\LogGameCommandFactory;
use App\Infrastructure\Form\Exception\AnnouncingPlayerMissing;
use App\Infrastructure\Form\LogGameData;
use App\Infrastructure\Form\LogGameType;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Webmozart\Assert\Assert;

#[Route(path: '/{_locale}/table/{tableId}/log', name: 'log')]
final class LogGameController extends AbstractController
{
    public function __invoke(
        Request $request,
        string $tableId,
        CurrentGamesAndPlayers $currentGamesAndPlayers,
        PermissionsByTable $permissionsByTable,
        RulesByTable $rulesByTable,
        LogGameCommandFactory $logGameCommandFactory,
        CommandBus $commandBus,
        ConventionsByTable $conventionsByTable,
    ): Response {
        // FIXME: being dependent on both conventionsByTable and rulesByTable is weird

        Assert::notEmpty($tableId);
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable($tableIdentifier);

        $currentGameAndPlayers = $currentGamesAndPlayers->getGameAndPlayers($scoreSheetIdentifier);

        $currentGame = $currentGameAndPlayers->currentGameNumber;

        $loggables = $rulesByTable->getLoggables($tableIdentifier);

        $logGameData = new LogGameData();
        $bid = strval($request->query->get('bid'));
        if (!empty($bid)) {
            $logGameData->loggable = $loggables->getByName($bid);
        }

        $logGameForm = $this->createForm(
            LogGameType::class,
            $logGameData,
            [
                'table_identifier' => $tableIdentifier,
            ]
        );

        $logGameForm->handleRequest($request);
        if ($logGameForm->isSubmitted() && $logGameForm->isValid()) {
            try {
                /** @var Form $logGameForm */
                $clickedButton = $logGameForm->getClickedButton();
                $command = match ($clickedButton) {
                    $logGameForm->get('logGame') => $logGameCommandFactory->createCommandFromFormData(
                        $tableIdentifier,
                        $currentGame,
                        $logGameData,
                    ),
                    $logGameForm->get('passedAround') => new PassAround($tableIdentifier, $currentGame),
                    $logGameForm->get('proposedNobodyAlong') => new ProposeAndNobodyGoesAlong($tableIdentifier, $currentGame),
                    default => throw new \LogicException('This should not happen'),
                };
                $commandBus->dispatch($command);

                return $this->redirectToRoute('table', [
                    'tableId' => $tableId,
                    '_fragment' => 'bid_loggable',
                ]);
            } catch (AnnouncingPlayerMissing|InvalidAnnouncement) {
                // Ignore invalid input of stupid user ;-)
            }
        }

        // It's important for the view that the specifications map a game name to a SpecifiedGame class.
        $specifications = $conventionsByTable->getAllowedGames($tableIdentifier);

        return $this->render('log.html.twig', [
            'permissions' => $permissionsByTable->getPermissionsByTable($tableIdentifier),
            'logGameForm' => $logGameForm->createView(),
            'specifiedGames' => iterator_to_array($specifications),
        ]);
    }
}
