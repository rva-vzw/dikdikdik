<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\Player\Service\PlayersGroups;
use App\Domain\ReadModel\ScoreLog\Service\ScoreLogs;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Webmozart\Assert\Assert;

final class ScoreSheetController extends AbstractController
{
    #[Route(path: '/{_locale}/scores/{scoreSheetId}', name: 'watch', options: ['expose' => true])]
    public function __invoke(string $scoreSheetId, string $_locale, PlayersGroups $knownPlayerDetails, ScoreLogs $scoreLogs): Response
    {
        Assert::notEmpty($scoreSheetId);
        $scoreSheetIdentifier = ScoreSheetIdentifier::fromString($scoreSheetId);
        $knownPlayers = $knownPlayerDetails->getPlayersGroup($scoreSheetIdentifier);
        $scoreLog = $scoreLogs->getScoreLog($scoreSheetIdentifier);

        return $this->render(
            'scoreSheet.html.twig',
            [
                'scoreSheetId' => $scoreSheetId,
                'players' => $knownPlayers,
                'scoreLog' => $scoreLog,
                'lang' => $_locale,
            ]
        );
    }
}
