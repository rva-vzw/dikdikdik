<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\ReadModel\ScoreLog\Service\ScoreLogDeleter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Webmozart\Assert\Assert;

/**
 * This class just injects some interfaces.
 *
 * So that they're available via the DI-container, and can be used in
 * the integration tests.
 *
 * (There should be a better way to achieve this)
 */
final class ContainerHacks extends AbstractController
{
    public function __construct(
        private readonly ScoreLogDeleter $scoreLogDeleter,
    ) {
    }

    #[Route('/hack')]
    public function __invoke(): Response
    {
        Assert::isInstanceOf($this->scoreLogDeleter, ScoreLogDeleter::class);

        return new Response('Hello world!');
    }
}
