<?php

declare(strict_types=1);

namespace App\Infrastructure\Upgrade;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Upgrade20240803 extends Command
{
    public function __construct(
        private Connection $connection,
    ) {
        parent::__construct('dikdikdik:upgrade:20240803');
    }

    protected function configure(): void
    {
        $this->setDescription('Adds an index to the table state read model. Note that you can fix this with replay as well');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->connection->executeQuery(
            'CREATE INDEX tableState_sheet ON readmodel_tablestate(score_sheet_id)',
        );

        return Command::SUCCESS;
    }
}
