<?php

declare(strict_types=1);

namespace App\Infrastructure\Upgrade;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Upgrade20240718_2 extends Command
{
    public function __construct(
        private Connection $connection,
    ) {
        parent::__construct('dikdikdik:upgrade:20240718-2');
    }

    protected function configure(): void
    {
        $this->setDescription('Event migration to fix #346.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->connection->executeQuery(
            "UPDATE event_store 
                SET payload = REPLACE(payload, 'ruleset.', 'conventions.')
                WHERE event_type = 'App\\\\Domain\\\\Table\\\\Event\\\\TableConfigured'
                ",
        );

        return Command::SUCCESS;
    }
}
