<?php

declare(strict_types=1);

namespace App\Infrastructure\Upgrade;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Upgrade20240717 extends Command
{
    public function __construct(
        private Connection $connection,
    ) {
        parent::__construct('dikdikdik:upgrade:20240717');
    }

    protected function configure(): void
    {
        $this->setDescription('Updates the event names in the event store,
            to keep reconstituting and replay working.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->connection->executeQuery(
            "UPDATE event_store SET event_type = REPLACE(event_type, 'App\\\\Domain\\\\WriteModel\\\\', 'App\\\\Domain\\\\')",
        );

        return Command::SUCCESS;
    }
}
