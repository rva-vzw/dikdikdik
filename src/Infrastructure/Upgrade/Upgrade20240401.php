<?php

declare(strict_types=1);

namespace App\Infrastructure\Upgrade;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Upgrade20240401 extends Command
{
    public function __construct(
        private Connection $connection,
    ) {
        parent::__construct('dikdikdik:upgrade:20240401');
    }

    protected function configure(): void
    {
        $this->setDescription('Updates TableState read model for rulesets.
            Note that you can also replay instead of running this migration
            (if you have plenty of time 😉.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->connection->executeQuery(
            'ALTER TABLE readmodel_tablestate ADD COLUMN rule_set_name VARCHAR(40)',
        );
        $this->connection->executeQuery(" 
            UPDATE readmodel_tablestate SET rule_set_name='ruleset.traditional' WHERE tricks_required_for_alone = 6
        ");
        $this->connection->executeQuery(" 
            UPDATE readmodel_tablestate SET rule_set_name='ruleset.alone5' WHERE tricks_required_for_alone = 5
        ");
        $this->connection->executeQuery(
            'ALTER TABLE readmodel_tablestate DROP COLUMN tricks_required_for_alone',
        );

        return 0;
    }
}
