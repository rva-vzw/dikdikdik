<?php

declare(strict_types=1);

namespace App\Infrastructure\Upgrade;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Upgrade20240718 extends Command
{
    public function __construct(
        private Connection $connection,
    ) {
        parent::__construct('dikdikdik:upgrade:20240718');
    }

    protected function configure(): void
    {
        $this->setDescription('Update table state read model, to fix #345. Note that you can fix this with replay as well');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->connection->executeQuery(
            "UPDATE readmodel_tablestate SET rule_set_name = REPLACE(rule_set_name, 'ruleset.', 'conventions.')",
        );

        return Command::SUCCESS;
    }
}
