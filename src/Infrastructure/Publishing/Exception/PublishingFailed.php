<?php

declare(strict_types=1);

namespace App\Infrastructure\Publishing\Exception;

final class PublishingFailed extends \Exception
{
    public static function couldNotRenderTwig(string $topic, string $message): self
    {
        return new self("Publishing failed: {$topic}: could not render twig: {$message}");
    }
}
