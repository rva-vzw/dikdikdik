<?php

declare(strict_types=1);

namespace App\Infrastructure\Publishing;

use App\Domain\ReadModel\ReadModelUpdateNotification;
use App\Domain\ReadModel\ReadModelUpdateNotifier;

final class PublishersNotifier implements ReadModelUpdateNotifier
{
    /** @var Publisher[] */
    private array $publishers;

    /**
     * @param \Traversable<Publisher> $publishers
     */
    public function __construct(
        \Traversable $publishers
    ) {
        $this->publishers = iterator_to_array($publishers);
    }

    public function notify(ReadModelUpdateNotification $notification): void
    {
        foreach ($this->publishers as $publisher) {
            if ($publisher->supports($notification)) {
                $publisher->publish($notification);
            }
        }
    }
}
