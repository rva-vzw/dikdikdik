<?php

declare(strict_types=1);

namespace App\Infrastructure\Publishing;

use App\Domain\ReadModel\ReadModelUpdateNotification;
use App\Domain\ReadModel\ScoreLog\ScoreLogUpdateNotification;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

/**
 * Publishes updates of the rendered score sheet to mercure.
 */
final class RenderedSheetPublisher implements Publisher
{
    public const TOPIC = 'sheet_%s';

    public function __construct(
        private HubInterface $hub,
    ) {
    }

    public function supports(ReadModelUpdateNotification $notification): bool
    {
        return $notification instanceof ScoreLogUpdateNotification;
    }

    public function publish(ReadModelUpdateNotification $notification): void
    {
        if (!$notification instanceof ScoreLogUpdateNotification) {
            throw new \InvalidArgumentException('RenderedSheetPublisher: Expected ScoreLogNotification, got '.get_class($notification));
        }

        $tableIdentifier = $notification->getTableIdentifier();
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable($tableIdentifier);

        $topicRo = sprintf(
            self::TOPIC,
            $scoreSheetIdentifier->toString(),
        );

        $this->hub->publish(
            new Update(
                $topicRo,
                $notification::class,
            ),
        );
    }
}
