<?php

declare(strict_types=1);

namespace App\Infrastructure\Publishing;

use App\Domain\ReadModel\ReadModelUpdateNotification;

interface Publisher
{
    public function supports(ReadModelUpdateNotification $notification): bool;

    public function publish(ReadModelUpdateNotification $notification): void;
}
