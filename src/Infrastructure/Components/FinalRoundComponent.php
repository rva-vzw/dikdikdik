<?php

declare(strict_types=1);

namespace App\Infrastructure\Components;

use App\Domain\Table\Command\AnnounceFinalRound;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent('finalRound')]
final class FinalRoundComponent
{
    use DefaultActionTrait;

    #[LiveProp]
    public TableIdentifier $tableIdentifier;
    #[LiveProp]
    public GameNumber $gameNumber;

    public function __construct(
        private CommandBus $commandBus,
    ) {
    }

    #[LiveAction]
    public function announceFinalRound(): void
    {
        $this->commandBus->dispatch(
            new AnnounceFinalRound(
                $this->tableIdentifier,
                $this->gameNumber,
            ),
        );
    }
}
