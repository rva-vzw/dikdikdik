<?php

declare(strict_types=1);

namespace App\Infrastructure\Components;

use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\Player\Service\PlayersGroups;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Command\StarPlayer;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent('starPlayer')]
final class StarPlayerComponent
{
    use DefaultActionTrait;

    #[LiveProp]
    public TableIdentifier $tableIdentifier;
    #[LiveProp]
    public GameNumber $gameNumber;
    #[LiveProp(writable: true)]
    public ?PlayerIdentifier $playerIdentifier = null;

    public function __construct(
        private CommandBus $commandBus,
        private PlayersGroups $playersGroups,
    ) {
    }

    #[LiveAction]
    public function starPlayer(): void
    {
        if ($this->playerIdentifier instanceof PlayerIdentifier) {
            $this->commandBus->dispatch(
                new StarPlayer(
                    $this->tableIdentifier,
                    $this->gameNumber,
                    $this->playerIdentifier,
                )
            );
            $this->playerIdentifier = null;
        }
    }

    public function getKnownPlayers(): PlayersGroup
    {
        return $this->playersGroups->getPlayersGroup(
            ScoreSheetIdentifier::forTable($this->tableIdentifier)
        );
    }
}
