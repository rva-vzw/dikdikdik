<?php

declare(strict_types=1);

namespace App\Infrastructure\Components;

use App\Domain\ReadModel\TablePermissions\PermissionsByTable;
use App\Domain\ReadModel\TablePermissions\TablePermissions;
use App\Domain\Table\Command\ConfigureTable;
use App\Domain\Table\TableIdentifier;
use App\Domain\TableConventions\AvailableConventions;
use App\Domain\TableConventions\ConventionsByTable;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Webmozart\Assert\Assert;

#[AsLiveComponent('tableRules')]
final class TableRulesComponent
{
    use DefaultActionTrait;

    /** @var non-empty-string */
    #[LiveProp]
    public string $tableId;
    #[LiveProp(writable: true)]
    public string $conventionsName = '';

    public function __construct(
        private readonly CommandBus $commandBus,
        private readonly PermissionsByTable $permissionsByTable,
        private readonly ConventionsByTable $conventionsByTable,
        public readonly AvailableConventions $availableConventions,
    ) {
    }

    #[LiveAction]
    public function configureTable(): void
    {
        Assert::stringNotEmpty($this->conventionsName, 'TableRulesComponent: ruleSetName should not be empty');
        $this->commandBus->dispatch(
            new ConfigureTable(
                TableIdentifier::fromString($this->tableId),
                $this->conventionsName,
            ),
        );
    }

    /**
     * @param non-empty-string $tableId
     */
    public function mount(string $tableId): void
    {
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $ruleset = $this->conventionsByTable->getConventionsByTable($tableIdentifier);

        $this->tableId = $tableId;
        $this->conventionsName = $ruleset->name;
    }

    public function getTablePermissions(): TablePermissions
    {
        // I admit this is not very efficient:
        return $this->permissionsByTable->getPermissionsByTable(
            TableIdentifier::fromString($this->tableId),
        );
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return TableIdentifier::fromString($this->tableId);
    }
}
