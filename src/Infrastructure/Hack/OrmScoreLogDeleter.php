<?php

declare(strict_types=1);

namespace App\Infrastructure\Hack;

use App\Domain\ReadModel\ScoreLog\Service\LogEntryRepository;
use App\Domain\ReadModel\ScoreLog\Service\ScoreLogDeleter;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Infrastructure\ReadModel\Entity\OrmDetailedTableState;
use App\Infrastructure\ReadModel\Repository\DetailedTableStateOrmRepository;

final class OrmScoreLogDeleter implements ScoreLogDeleter
{
    public function __construct(
        private readonly DetailedTableStateOrmRepository $detailedTableStateOrmRepository,
        private readonly LogEntryRepository $logEntryRepository,
    ) {
    }

    public function deleteScoreLog(ScoreSheetIdentifier $scoreSheetIdentifier): void
    {
        $tableState = $this->detailedTableStateOrmRepository->findDetailedTableStateForScoreSheet(
            $scoreSheetIdentifier,
        );
        if ($tableState instanceof OrmDetailedTableState) {
            $this->detailedTableStateOrmRepository->remove($tableState);
        }
        $this->logEntryRepository->deleteForScoreSheet($scoreSheetIdentifier);
    }
}
