<?php

declare(strict_types=1);

namespace App\Infrastructure\TableRules;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\ScoreSheet\Tariffs\Tariffs;
use App\Domain\Table\Loggable\GameSpecification\GameSpecification;
use App\Domain\Table\Loggable\Loggable;
use App\Domain\Table\Rules\Loggables;
use App\Domain\Table\Rules\PassAroundRules;
use App\Domain\Table\Rules\RulesByTable;
use App\Domain\Table\Rules\Whist;
use App\Domain\Table\TableIdentifier;
use App\Domain\TableConventions\AvailableConventions;
use App\Domain\TableConventions\Conventions;
use App\Domain\TableConventions\ConventionsByTable;
use App\Infrastructure\ReadModel\Repository\DetailedTableStateOrmRepository;

/**
 * Retrieves table conventions from the table state orm repository.
 */
final readonly class OrmTableStateConventions implements ConventionsByTable, RulesByTable
{
    public function __construct(
        private AvailableConventions $availableConventions,
        private DetailedTableStateOrmRepository $tableStateOrmRepository,
    ) {
    }

    public function getConventionsByTable(TableIdentifier $tableIdentifier): Conventions
    {
        $tableState = $this->tableStateOrmRepository->getTableState($tableIdentifier);

        return $this->availableConventions->getConventionsByName(
            $tableState->conventionsName ?: Whist::DEFAULT_CONVENTIONS_NAME,
        );
    }

    public function getTariffsForTable(TableIdentifier $tableIdentifier): Tariffs
    {
        return $this->getConventionsByTable($tableIdentifier)->tariffs;
    }

    /**
     * @return \Traversable<string, GameSpecification>
     */
    public function getAvailableGames(TableIdentifier $tableIdentifier): \Traversable
    {
        return $this->getConventionsByTable($tableIdentifier)->getAll();
    }

    public function getPassAroundRules(TableIdentifier $tableIdentifier): PassAroundRules
    {
        return $this->getConventionsByTable($tableIdentifier)->passAroundRules;
    }

    public function getConventionsByScoreSheet(ScoreSheetIdentifier $scoreSheetIdentifier): Conventions
    {
        $tableState = $this->tableStateOrmRepository->getTableStateByScoreSheet(
            $scoreSheetIdentifier,
        );

        return $this->availableConventions->getConventionsByName(
            $tableState->conventionsName ?: Whist::DEFAULT_CONVENTIONS_NAME,
        );
    }

    public function getLoggables(TableIdentifier $tableIdentifier): Loggables
    {
        $games = $this->getAvailableGames($tableIdentifier);

        $loggableGames = Loggables::fromSpecifiedGames($games);

        if (PassAroundRules::Simplified === $this->getPassAroundRules($tableIdentifier)) {
            return $loggableGames;
        }

        return $loggableGames->prependedWith(new class() implements Loggable {
            public function getLabel(): string
            {
                return 'game.no_bid_accepted';
            }

            public function getName(): string
            {
                return 'no_bid_accepted';
            }
        });
    }

    public function getAllowedGames(TableIdentifier $tableIdentifier): \Traversable
    {
        foreach ($this->getConventionsByTable($tableIdentifier)->allowedGames as $gameSpecification) {
            yield $gameSpecification->getName() => $gameSpecification;
        }
    }
}
