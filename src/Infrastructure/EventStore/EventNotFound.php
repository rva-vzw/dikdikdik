<?php

declare(strict_types=1);

namespace App\Infrastructure\EventStore;

use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class EventNotFound extends \Exception
{
    public static function create(AggregateRootIdentifier $aggregateRootIdentifier, int $aggregateVersion): self
    {
        return new self("Event for version {$aggregateVersion} of aggregate {$aggregateRootIdentifier->toString()} not found.");
    }
}
