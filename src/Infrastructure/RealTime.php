<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\CurrentTime;

final class RealTime implements CurrentTime
{
    public function getCurrentTime(): \DateTimeImmutable
    {
        return new \DateTimeImmutable();
    }
}
