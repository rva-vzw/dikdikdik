<?php

declare(strict_types=1);

namespace App\Infrastructure\Form\Exception;

final class AnnouncingPlayerMissing extends \Exception
{
    public static function forPlayer1(): self
    {
        return new self('Announcing player 1 missing');
    }

    public static function forPlayer2(): self
    {
        return new self('Announcing player 2 missing');
    }
}
