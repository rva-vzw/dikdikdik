<?php

declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\ReadModel\Player\Model\Player;

final class ExtraActionsData
{
    public function __construct(
        public ?Player $player = null,
    ) {
    }
}
