<?php

declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Service\PlayersGroups;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\TableIdentifier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ExtraActionsType extends AbstractType
{
    public function __construct(
        private readonly PlayersGroups $playersGroups,
    ) {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'table_identifier' => null,
            'data_class' => ExtraActionsData::class,
        ]);
        $resolver->setAllowedTypes('table_identifier', TableIdentifier::class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var TableIdentifier $tableIdentifier */
        $tableIdentifier = $options['table_identifier'];
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable($tableIdentifier);

        $playersGroup = $this->playersGroups->getPlayersGroup($scoreSheetIdentifier);

        $builder->add('undo', SubmitType::class, [
            'label' => 'correction.undo',
        ])->add('finalRound', SubmitType::class, [
            'label' => 'finalRound.finalRound',
        ])->add('player', ChoiceType::class, [
            'choices' => $playersGroup,
            'choice_value' => 'playerIdentifier',
            'choice_label' => fn (?Player $details) => $details?->getName(),
            'label' => 'table.player',
            'placeholder' => 'app.choose',
            'required' => false,
        ])->add('star', SubmitType::class, [
            'label' => 'log.star',
        ]);
    }
}
