<?php

declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\TablePermissions\PermissionParameters;
use App\Domain\TableConventions\AvailableConventions;
use App\Domain\TableConventions\Conventions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\Assert\Assert;

final class PlayersFormType extends AbstractType
{
    public function __construct(
        private readonly AvailableConventions $availableConventions,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $players = $options['players'];
        Assert::isInstanceOf($players, PlayersGroup::class);
        $permissions = $options['permissions'];
        Assert::isInstanceOf($permissions, PermissionParameters::class);

        $builder->add('playerIdForAction', HiddenType::class, [
            'attr' => ['data-playersForm-target' => 'playerForAction'],
        ])->add('kickButton', SubmitType::class, [
            'attr' => [
                'class' => 'd-none',
                'data-playersForm-target' => 'kickButton',
            ],
        ])->add('rejoinButton', SubmitType::class, [
            'attr' => [
                'class' => 'd-none',
                'data-playersForm-target' => 'rejoinButton',
            ],
        ]);

        // Note that we shouldn't add things to the symfony form that don't apply, because even when we don't
        // render these elements in our twig file, Symfony will render them for us, leading to unexpected
        // situations, e.g. #362.

        if ($permissions->canConfigureGame()) {
            $builder->add('conventionsName', ChoiceType::class, [
                'label' => 'configuration.chooseConventions',
                'expanded' => true,
                'choices' => array_map(
                    fn (Conventions $c) => [$c->name => $c->name],
                    iterator_to_array($this->availableConventions),
                ),
            ]);
        }

        if ($permissions->canAddPlayer()) {
            $builder->add('newPlayerName', TextType::class, [
                'label' => 'sheet.newPlayer',
                'required' => false,
            ])->add('addPlayer', SubmitType::class, [
                'label' => 'sheet.add',
            ]);
        }

        if ($players->hasEnoughPlayers()) {
            $builder->add('dealerId', ChoiceType::class, [
                'label' => 'sheet.dealer',
                'expanded' => false,
                'choices' => array_reduce(
                    iterator_to_array($players->getPresent()),
                    function (array $arr, Player $p) {
                        $arr[$p->getName()] = $p->getPlayerIdentifier()->toString();

                        return $arr;
                    },
                    []
                ),
            ])->add('submit', SubmitType::class, [
                'label' => 'configuration.submit',
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PlayersFormData::class,
            'players' => PlayersGroup::none(),
            'permissions' => null,
        ]);
    }
}
