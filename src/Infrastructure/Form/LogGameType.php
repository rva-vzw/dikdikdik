<?php

declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\ReadModel\Player\Model\PresentPlayers;
use App\Domain\ReadModel\Player\Service\PlayersGroups;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Loggable\Loggable;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Player\NamedPlayer;
use App\Domain\Table\Rules\RulesByTable;
use App\Domain\Table\TableIdentifier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class LogGameType extends AbstractType
{
    public function __construct(
        private readonly RulesByTable $rulesByTable,
        private readonly PlayersGroups $playersGroups,
    ) {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'table_identifier' => null,
            'data_class' => LogGameData::class,
        ]);
        $resolver->setAllowedTypes('table_identifier', TableIdentifier::class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var TableIdentifier $tableIdentifier */
        $tableIdentifier = $options['table_identifier'];
        $loggables = iterator_to_array($this->rulesByTable->getLoggables($tableIdentifier));
        $presentPlayers = PresentPlayers::fromPlayersGroup(
            $this->playersGroups->getPlayersGroup(ScoreSheetIdentifier::forTable($tableIdentifier)),
        );
        $playingPlayers = iterator_to_array($presentPlayers->getPlayingPlayers());

        // game to log
        $builder->add('loggable', ChoiceType::class, [
            'choices' => $loggables,
            'choice_value' => 'name',
            'choice_label' => fn (?Loggable $loggable) => $loggable?->getLabel(),
            'label' => 'log.logGame',
            'placeholder' => 'app.choose',
        ])->add('onTopOfTroel', CheckboxType::class, [
            'label' => 'game.troel',
            'required' => false,
        // Multiple announcers, in case of misere (on the table)
        ])->add('announcers', ChoiceType::class, [
            'choices' => $playingPlayers,
            'choice_value' => 'playerIdentifier',
            'choice_label' => fn (?NamedPlayer $details) => $details?->getPlayerName(),
            'label' => 'log.announcedBy',
            'expanded' => true,
            'multiple' => true,
        // Announcing players 1 and 2 (for non-misere games)
        ])->add('announcingPlayer1', ChoiceType::class, [
            'choices' => $playingPlayers,
            'choice_value' => 'playerIdentifier',
            'choice_label' => fn (?NamedPlayer $details) => $details?->getPlayerName(),
            'label' => 'log.announcedBy',
            'placeholder' => 'app.choose',
            'required' => false,
        // 2nd announcing player is not relevant when playing alone, abundance or solo.
        ])->add('announcingPlayer2', ChoiceType::class, [
            'choices' => $playingPlayers,
            'choice_value' => 'playerIdentifier',
            'choice_label' => fn (?NamedPlayer $details) => $details?->getPlayerName(),
            'label' => 'log.announcedBy',
            'placeholder' => 'app.choose',
            'required' => false,
        // Outcomes:
        // In case of prop-and-cop, alone, troel:
        ])->add('numberOfTricks', RangeType::class, [
            'label' => 'log.noOfTricks',
            'attr' => [
                'min' => 0,
                'max' => NumberOfTricks::MAX_NUMBER_OF_TRICKS,
                'step' => 1,
            ],
        // In case of abundance, solo:
        ])->add('success', ChoiceType::class, [
            'choices' => [
                'log.successYes' => true,
                'log.successNo' => false,
            ],
            'label' => 'log.success',
            'expanded' => true,
            'multiple' => false,
        // In case of misere (on the table)
        ])->add('winningPlayers', ChoiceType::class, [
            'choices' => $playingPlayers,
            'choice_value' => 'playerIdentifier',
            'choice_label' => fn (?NamedPlayer $details) => $details?->getPlayerName(),
            'label' => 'log.success',
            'expanded' => true,
            'multiple' => true,
        // the submit buttons need a value attribute, otherwise getClickedButton() doesn't work with Symfony Turbo 😒
        // https://github.com/symfony/ux/issues/95
        ])->add('logGame', SubmitType::class, [
            'label' => 'log.logGame',
            'attr' => ['value' => 'logGame'],
        ])->add('passedAround', SubmitType::class, [
            'label' => 'log.passedAround',
            'attr' => ['class' => 'btn-outline-primary', 'value' => 'passedAround'],
        ])->add('proposedNobodyAlong', SubmitType::class, [
            'label' => 'log.proposedNobodyAlong',
            'attr' => ['class' => 'btn-outline-primary', 'value' => 'proposedNobodyAlong'],
        ]);
    }
}
