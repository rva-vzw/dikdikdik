<?php

declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\Table\Loggable\Loggable;

final class BidData
{
    public function __construct(
        public ?Loggable $loggable = null,
    ) {
    }
}
