<?php

declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\Table\Player\PlayerIdentifier;

final class PlayersFormData
{
    public function __construct(
        public ?string $newPlayerName,
        public ?string $conventionsName,
        public ?string $dealerId,
        public ?string $playerIdForAction,
    ) {
    }

    public function getDealerIdentifier(): ?PlayerIdentifier
    {
        return empty($this->dealerId)
            ? null
            : PlayerIdentifier::fromString($this->dealerId);
    }
}
