<?php

declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\Table\Loggable\Loggable;
use App\Domain\Table\Player\NamedPlayer;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Infrastructure\Form\Exception\AnnouncingPlayerMissing;

final class LogGameData
{
    public function __construct(
        public ?Loggable $loggable = null,
        public bool $onTopOfTroel = false,
        /** @var NamedPlayer[] */
        public array $announcers = [],
        public ?NamedPlayer $announcingPlayer1 = null,
        public ?NamedPlayer $announcingPlayer2 = null,
        public int $numberOfTricks = 8,
        public bool $success = true,
        /** @var NamedPlayer[] */
        public array $winningPlayers = [],
    ) {
    }

    public function getAnnouncingPlayerIdentifiers(): PlayerIdentifiers
    {
        return PlayerIdentifiers::fromArray(
            array_map(
                fn (NamedPlayer $details) => $details->getPlayerIdentifier(),
                $this->announcers
            )
        );
    }

    public function getWinningPlayerIdentifiers(): PlayerIdentifiers
    {
        return PlayerIdentifiers::fromArray(
            array_map(
                fn (NamedPlayer $details) => $details->getPlayerIdentifier(),
                $this->winningPlayers
            )
        );
    }

    public function getPlayer1Identifier(): PlayerIdentifier
    {
        if ($this->announcingPlayer1 instanceof NamedPlayer) {
            return $this->announcingPlayer1->getPlayerIdentifier();
        }
        throw AnnouncingPlayerMissing::forPlayer1();
    }

    public function getPlayer2Identifier(): PlayerIdentifier
    {
        if ($this->announcingPlayer2 instanceof NamedPlayer) {
            return $this->announcingPlayer2->getPlayerIdentifier();
        }
        throw AnnouncingPlayerMissing::forPlayer2();
    }
}
