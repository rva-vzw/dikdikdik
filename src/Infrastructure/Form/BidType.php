<?php

declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\Table\Loggable\Loggable;
use App\Domain\Table\Rules\RulesByTable;
use App\Domain\Table\TableIdentifier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\Assert\Assert;

final class BidType extends AbstractType
{
    public function __construct(
        private readonly RulesByTable $rulesByTable,
    ) {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'table_identifier' => null,
            'data_class' => BidData::class,
        ]);
        $resolver->setAllowedTypes('table_identifier', TableIdentifier::class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $tableIdentifier = $options['table_identifier'];
        Assert::isInstanceOf($tableIdentifier, TableIdentifier::class);

        $loggables = iterator_to_array($this->rulesByTable->getLoggables(
            $tableIdentifier,
        ));

        $builder->add('loggable', ChoiceType::class, [
            'choices' => $loggables,
            'choice_value' => 'name',
            'choice_label' => fn (?Loggable $loggable) => $loggable?->getLabel(),
            'label' => 'log.logGame',
            'placeholder' => 'log.logGame',
        ])->add('submit', SubmitType::class, [
            // 'attr' => ['class' => 'd-none'],
        ]);
    }
}
