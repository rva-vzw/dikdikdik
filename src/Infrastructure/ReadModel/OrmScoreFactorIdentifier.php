<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use RvaVzw\KrakBoem\Id\Uuid5Identifier;

final readonly class OrmScoreFactorIdentifier extends Uuid5Identifier
{
    protected static function getNamespace(): string
    {
        return 'e11424a2-dee3-4e04-9f98-88e115bf6711';
    }

    public static function forGame(
        GameNumber $gameNumber,
        ScoreSheetIdentifier $scoreSheetIdentifier
    ): self {
        return self::fromName(implode(
            '+',
            [$scoreSheetIdentifier->toString(), (string) $gameNumber->toInteger()]
        ));
    }
}
