<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Domain\ReadModel\Exception\ReadModelUpdateException;
use App\Domain\ReadModel\ScoreLog\Model\ScoreLogEntry;
use App\Domain\ReadModel\ScoreLog\Service\LogEntryRepository;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Exception\IncorrectGameNumber;
use App\Domain\Table\GameNumber;
use App\Infrastructure\ReadModel\Entity\OrmStoredLogEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Webmozart\Assert\Assert;

/**
 * @extends ServiceEntityRepository<OrmStoredLogEntry>
 */
final class StoredScoreLogOrmRepository extends ServiceEntityRepository implements LogEntryRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmStoredLogEntry::class);
    }

    /**
     * @throws ReadModelUpdateException
     */
    public function save(OrmStoredLogEntry $storedLogEntry): void
    {
        try {
            $this->_em->persist($storedLogEntry);
            $this->_em->flush();
        } catch (ORMException $e) {
            throw new ReadModelUpdateException();
        }
    }

    public function remove(OrmStoredLogEntry $entry): void
    {
        try {
            $this->_em->remove($entry);
            $this->_em->flush();
        } catch (OptimisticLockException|ORMException $e) {
            throw new ReadModelUpdateException();
        }
    }

    /**
     * @return \Traversable<OrmStoredLogEntry>
     */
    public function generateForScoreSheet(ScoreSheetIdentifier $scoreSheetIdentifier): \Traversable
    {
        $q = $this->_em->createQuery('select i from '.OrmStoredLogEntry::class.' i where i.scoreSheetId=:scoreSheetId order by i.scoreLineNumber');
        $q->setParameter('scoreSheetId', $scoreSheetIdentifier->toString());

        foreach ($q->toIterable() as $entry) {
            Assert::isInstanceOf($entry, OrmStoredLogEntry::class);
            yield $entry->getScoreLineNumber()->toInteger() => $entry;
            $this->_em->detach($entry);
        }
    }

    public function deleteForScoreSheet(ScoreSheetIdentifier $scoreSheetIdentifier): void
    {
        $q = $this->_em->createQuery('delete from '.OrmStoredLogEntry::class.' i where i.scoreSheetId=:scoreSheetId');
        $q->setParameter('scoreSheetId', $scoreSheetIdentifier->toString());

        $q->execute();
    }

    public function findLogEntryForGame(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        GameNumber $gameNumber
    ): ?OrmStoredLogEntry {
        $q = $this->_em->createQuery(
            'SELECT i FROM '.OrmStoredLogEntry::class.' i
                WHERE i.scoreSheetId=:scoreSheetId AND i.gameNumber = :gameNumber
                ORDER BY i.scoreLineNumber DESC'
        );
        $q->setParameter('scoreSheetId', $scoreSheetIdentifier->toString());
        $q->setParameter('gameNumber', $gameNumber->toInteger());
        $q->setMaxResults(1);

        try {
            /** @var OrmStoredLogEntry $entry */
            $entry = $q->getSingleResult();
        } catch (NoResultException) {
            $entry = null;
        }

        return $entry;
    }

    public function findLastEntry(ScoreSheetIdentifier $scoreSheetIdentifier): ?ScoreLogEntry
    {
        $q = $this->_em->createQuery(
            'SELECT i FROM '.OrmStoredLogEntry::class.' i
                WHERE i.scoreSheetId=:scoreSheetId
                ORDER BY i.scoreLineNumber DESC'
        );
        $q->setParameter('scoreSheetId', $scoreSheetIdentifier->toString());
        $q->setMaxResults(1);

        try {
            /** @var OrmStoredLogEntry $entry */
            $entry = $q->getSingleResult();
        } catch (NoResultException) {
            $entry = null;
        }

        return $entry;
    }

    public function getOrCreateEntryByGameAndLine(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        GameNumber $gameNumber,
        ScoreLineNumber $scoreLineNumber
    ): OrmStoredLogEntry {
        $entry = $this->findLogEntryByLine($scoreSheetIdentifier, $scoreLineNumber);
        if ($entry instanceof OrmStoredLogEntry) {
            if ($entry->getGameNumber() == $gameNumber) {
                return $entry;
            }

            throw IncorrectGameNumber::create($gameNumber, $entry->getGameNumber());
        }

        return new OrmStoredLogEntry(
            $gameNumber,
            $scoreSheetIdentifier,
            null,
            ScoreFactor::single(),
            ScoreLine::empty($scoreLineNumber),
        );
    }

    /**
     * @return \Traversable<ScoreLogEntry>
     */
    public function getRecentEntriesUntilLine(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        ScoreLineNumber $scoreLineNumber
    ): \Traversable {
        $q = $this->_em->createQuery(
            'SELECT i FROM '.OrmStoredLogEntry::class.' i
                WHERE i.scoreSheetId = :scoreSheetId AND i.scoreLineNumber >= :scoreLineNumber
                ORDER BY i.scoreLineNumber DESC'
        );
        $q->setParameter('scoreSheetId', $scoreSheetIdentifier->toString());
        $q->setParameter('scoreLineNumber', $scoreLineNumber->toInteger());

        foreach ($q->toIterable() as $entry) {
            Assert::isInstanceOf($entry, OrmStoredLogEntry::class);
            yield $entry;
            $this->_em->detach($entry);
        }
    }

    private function findLogEntryByLine(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        ScoreLineNumber $scoreLineNumber
    ): ?OrmStoredLogEntry {
        $entry = $this->findOneBy([
            'scoreSheetId' => $scoreSheetIdentifier->toString(),
            'scoreLineNumber' => $scoreLineNumber->toInteger(),
        ]);

        return $entry;
    }

    public function getOrCreateEntryForGame(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        GameNumber $gameNumber
    ): ScoreLogEntry {
        $entry = $this->findLogEntryForGame($scoreSheetIdentifier, $gameNumber);

        if ($entry instanceof ScoreLogEntry) {
            return $entry;
        }

        $lastEntry = $this->findLastEntry($scoreSheetIdentifier);
        $newScoreLineNumber = $lastEntry instanceof ScoreLogEntry
            ? $lastEntry->getScoreLineNumber()->getNext() : ScoreLineNumber::first();

        return new OrmStoredLogEntry(
            $gameNumber,
            $scoreSheetIdentifier,
            null,
            ScoreFactor::single(),
            ScoreLine::empty($newScoreLineNumber),
        );
    }

    /**
     * @return \Traversable<ScoreLogEntry>
     */
    public function getLogEntriesForSheet(ScoreSheetIdentifier $scoreSheetIdentifier): \Traversable
    {
        $storedEntries = $this->generateForScoreSheet($scoreSheetIdentifier);

        /** @var OrmStoredLogEntry $logEntry */
        foreach ($storedEntries as $logEntry) {
            yield $logEntry->getScoreLineNumber()->toInteger() => $logEntry;
        }
    }

    public function saveLogEntry(ScoreLogEntry $logEntry): void
    {
        $ormEntry = $this->getOrCreateEntryByGameAndLine(
            $logEntry->getScoreSheetIdentifier(),
            $logEntry->getGameNumber(),
            $logEntry->getScoreLineNumber(),
        );

        $ormEntry->setDealer($logEntry->getDealer());
        $ormEntry->setScoreFactor($logEntry->getScoreFactor());
        $ormEntry->setScoreLine($logEntry->getScoreLine());

        $this->save($ormEntry);
    }

    public function deleteLogEntry(ScoreLogEntry $logEntry): void
    {
        $ormEntry = $this->findLogEntryByLine($logEntry->getScoreSheetIdentifier(), $logEntry->getScoreLineNumber());
        if ($ormEntry instanceof OrmStoredLogEntry) {
            $this->remove($ormEntry);
        }
    }
}
