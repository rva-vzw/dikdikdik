<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Domain\ReadModel\GameCounter\ActiveTablesCounter;
use App\Domain\ReadModel\GameCounter\GameCounter;
use App\Domain\ReadModel\GameCounter\GameCounterDeleter;
use App\Domain\ReadModel\GameCounter\GameCounterRepository;
use App\Domain\ReadModel\GameCounter\Month;
use App\Domain\ReadModel\GameCounter\MonthlyTotal;
use App\Domain\ReadModel\GameCounter\MonthlyTotals;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\ReadModel\Entity\OrmGameCounter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Webmozart\Assert\Assert;

/**
 * @extends ServiceEntityRepository<OrmGameCounter>
 */
final class GameCounterOrmRepository extends ServiceEntityRepository implements GameCounterRepository, ActiveTablesCounter, MonthlyTotals, GameCounterDeleter
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmGameCounter::class);
    }

    public function save(OrmGameCounter $ormGameCounter): void
    {
        $this->_em->persist($ormGameCounter);
        $this->_em->flush();
    }

    public function findCounterForTable(TableIdentifier $tableIdentifier): ?GameCounter
    {
        return $this->findOneBy(['tableId' => $tableIdentifier->toString()])?->toGameCounter();
    }

    public function saveGameCounter(GameCounter $counter): void
    {
        $ormCounter = $this->findOneBy(['tableId' => $counter->getTableIdentifier()->toString()]);
        if (null === $ormCounter) {
            $ormCounter = OrmGameCounter::createNew(
                $counter->getTableIdentifier(),
                $counter->getFirstGameLoggedAt()
            );
        }
        $this->mapToOrm($counter, $ormCounter);
        $this->save($ormCounter);
    }

    private function mapToOrm(GameCounter $counter, OrmGameCounter $ormCounter): void
    {
        $ormCounter->setGameCount($counter->getGameCount());
        $ormCounter->setLatestGameLoggedAt($counter->getLatestGameLoggedAt());
    }

    public function removeForTable(TableIdentifier $tableIdentifier): void
    {
        $ormGameCounter = $this->findOneBy(['tableId' => $tableIdentifier->toString()]);

        if ($ormGameCounter instanceof OrmGameCounter) {
            $this->_em->remove($ormGameCounter);
            $this->_em->flush();
        }
    }

    public function getActiveTablesCount(
        \DateTimeImmutable $startedBefore,
        \DateTimeImmutable $stillBusyAt,
        int $minimumGameCount
    ): int {
        $query = $this->_em->createQueryBuilder()
            ->select('COUNT(q.tableId)')
            ->from(OrmGameCounter::class, 'q')
            ->where('q.latestGameLoggedAt >= :stillBusyAt')
            ->andWhere('q.firstGameLoggedAt <= :startedBefore')
            ->andWhere('q.gameCount >= :minimumGameCount')
            ->setParameter('stillBusyAt', $stillBusyAt)
            ->setParameter('startedBefore', $startedBefore)
            ->setParameter('minimumGameCount', $minimumGameCount)
            ->getQuery();

        $result = $query->getSingleScalarResult();

        return intval($result);
    }

    /**
     * @return \Traversable<MonthlyTotal>
     */
    public function getByMonth(Month $from, Month $to, int $minimumGameCount): \Traversable
    {
        // We could also create a dedicated read model that just keeps track of one record each month,
        // and increases the counters when a new game starts/the first game on a table starts,
        // reducing the impact on the database.

        $startDate = $from->getStartDate();
        $endDate = $to->next()->getStartDate();

        $query = $this->_em->createQueryBuilder()
            ->select('q.year, q.month, COUNT(q.tableId) AS totalTables, SUM(q.gameCount) AS totalGames')
            ->from(OrmGameCounter::class, 'q')
            ->where('q.firstGameLoggedAt >= :startDate')
            ->andWhere('q.firstGameLoggedAt < :endDate')
            ->andWhere('q.gameCount >= :minimumGameCount')
            ->groupBy('q.year, q.month')
            ->orderBy('q.year, q.month')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('minimumGameCount', $minimumGameCount)
            ->getQuery();

        foreach ($query->toIterable() as $row) {
            Assert::isArray($row);
            yield new MonthlyTotal(
                Month::fromYearAndDay((int) $row['year'], (int) $row['month']),
                (int) $row['totalTables'],
                (int) $row['totalGames'],
            );
        }
    }
}
