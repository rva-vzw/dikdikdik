<?php

namespace App\Infrastructure\ReadModel\Repository;

use App\Domain\ReadModel\Exception\ReadModelNotFoundException;
use App\Domain\ReadModel\Exception\ReadModelUpdateException;
use App\Domain\ReadModel\TablesBySheet\TablesBySheet;
use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ReadModel\TableState\TableStateRepository;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Rules\Whist;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\ReadModel\Entity\OrmDetailedTableState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmDetailedTableState>
 */
final class DetailedTableStateOrmRepository extends ServiceEntityRepository implements TablesBySheet, TableStateRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, OrmDetailedTableState::class);
    }

    public function save(OrmDetailedTableState $ormDetailedTableState): void
    {
        try {
            $this->_em->persist($ormDetailedTableState);
            $this->_em->flush();
        } catch (OptimisticLockException|ORMException) {
            throw new ReadModelUpdateException();
        }
    }

    public function remove(OrmDetailedTableState $tableState): void
    {
        try {
            $this->_em->remove($tableState);
            $this->_em->flush();
        } catch (OptimisticLockException|ORMException $e) {
            throw new ReadModelUpdateException();
        }
    }

    public function findDetailedTableStateForScoreSheet(ScoreSheetIdentifier $scoreSheetIdentifier): ?OrmDetailedTableState
    {
        return $this->findOneBy([
            'scoreSheetId' => $scoreSheetIdentifier->toString(),
        ]);
    }

    private function getOrCreateOrmTableState(TableIdentifier $tableIdentifier): OrmDetailedTableState
    {
        $existing = $this->findOneBy([
            'tableId' => $tableIdentifier->toString(),
        ]);
        if ($existing instanceof OrmDetailedTableState) {
            return $existing;
        }

        $newOrmTableState = OrmDetailedTableState::create($tableIdentifier);
        $newOrmTableState->ruleSetName = Whist::DEFAULT_CONVENTIONS_NAME;

        return $newOrmTableState;
    }

    public function create(TableIdentifier $tableIdentifier): OrmDetailedTableState
    {
        return OrmDetailedTableState::create($tableIdentifier);
    }

    public function deleteTableState(TableIdentifier $tableIdentifier): void
    {
        $tableState = $this->getOrCreateOrmTableState($tableIdentifier);
        $this->remove($tableState);
    }

    public function saveTableState(TableState $tableState): void
    {
        $playerIdentifier = $tableState->dealer;
        $finalGameNumber = $tableState->finalGameNumber;

        $ormTableState = $this->getOrCreateOrmTableState($tableState->tableIdentifier);
        $ormTableState->ruleSetName = $tableState->conventionsName;
        $ormTableState->dealerId = $playerIdentifier instanceof PlayerIdentifier
            ? $playerIdentifier->toString() : null;
        $ormTableState->canChangeRuleSet = $tableState->canChangeRuleSet;
        $ormTableState->dealerChangeable = $tableState->dealerChangeable;
        $ormTableState->gameNr = $tableState->currentGameNumber->toInteger();
        $ormTableState->numberOfPlayers = $tableState->numberOfPresentPlayers;

        $ormTableState->finalGameNumber = $finalGameNumber instanceof GameNumber
            ? $finalGameNumber->toInteger() : null;

        $this->save($ormTableState);
    }

    public function getTableByScoreSheet(ScoreSheetIdentifier $scoreSheetIdentifier): TableIdentifier
    {
        $ormTableState = $this->findDetailedTableStateForScoreSheet($scoreSheetIdentifier);
        if ($ormTableState instanceof OrmDetailedTableState) {
            return TableIdentifier::fromString($ormTableState->tableId);
        }
        throw new ReadModelNotFoundException();
    }

    public function getTableState(TableIdentifier $tableIdentifier): TableState
    {
        // FIXME: the 'get or create' part should be at the domain level, not at the
        // infrastructure level, because it involves assigning the default ruleset.
        return $this->getOrCreateOrmTableState($tableIdentifier)->toTableState();
    }

    public function getTableStateByScoreSheet(ScoreSheetIdentifier $scoreSheetIdentifier): TableState
    {
        $ormTableState = $this->findDetailedTableStateForScoreSheet($scoreSheetIdentifier);

        if ($ormTableState instanceof OrmDetailedTableState) {
            return $ormTableState->toTableState();
        }

        throw new ReadModelNotFoundException();
    }
}
