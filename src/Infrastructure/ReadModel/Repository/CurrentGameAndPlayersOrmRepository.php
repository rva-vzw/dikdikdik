<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Domain\ReadModel\Exception\ReadModelUpdateException;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\Player\Service\PlayersGroups;
use App\Domain\ReadModel\ScoreLog\Model\CurrentGameAndPlayers;
use App\Domain\ReadModel\ScoreLog\Service\CurrentGameAndPlayersRepository;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Infrastructure\ReadModel\Entity\OrmCurrentGamesAndPlayer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @extends ServiceEntityRepository<OrmCurrentGamesAndPlayer>
 */
final class CurrentGameAndPlayersOrmRepository extends ServiceEntityRepository implements CurrentGameAndPlayersRepository, PlayersGroups
{
    public function __construct(
        ManagerRegistry $registry,
        private readonly NormalizerInterface $normalizer,
        private readonly DenormalizerInterface $denormalizer,
    ) {
        parent::__construct($registry, OrmCurrentGamesAndPlayer::class);
    }

    public function save(OrmCurrentGamesAndPlayer $ormTableState): void
    {
        try {
            $this->_em->persist($ormTableState);
            $this->_em->flush();
        } catch (OptimisticLockException|ORMException) {
            throw new ReadModelUpdateException();
        }
    }

    public function findOrCreate(ScoreSheetIdentifier $scoreSheetIdentifier): OrmCurrentGamesAndPlayer
    {
        $existing = $this->findOneBy([
            'scoreSheetId' => $scoreSheetIdentifier->toString(),
        ]);
        if ($existing instanceof OrmCurrentGamesAndPlayer) {
            return $existing;
        }

        return OrmCurrentGamesAndPlayer::create($scoreSheetIdentifier);
    }

    public function saveGameAndPlayers(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        CurrentGameAndPlayers $currentGameAndPlayers,
    ): void {
        $ormGameAndPlayers = $this->findOrCreate($scoreSheetIdentifier);
        $ormGameAndPlayers->setGameNumber($currentGameAndPlayers->currentGameNumber);
        /** @var array<string,mixed> $normalizedPlayers */
        $normalizedPlayers = $this->normalizer->normalize($currentGameAndPlayers->playersGroup);
        $ormGameAndPlayers->setNormalizedPlayers($normalizedPlayers);
        $this->save($ormGameAndPlayers);
    }

    public function getGameAndPlayers(ScoreSheetIdentifier $scoreSheetIdentifier): CurrentGameAndPlayers
    {
        $ormGameAndPlayers = $this->findOrCreate($scoreSheetIdentifier);
        /** @var PlayersGroup $playersGroup */
        $playersGroup = $this->denormalizer->denormalize(
            $ormGameAndPlayers->getNormalizedPlayers(),
            PlayersGroup::class,
        );

        return new CurrentGameAndPlayers(
            $ormGameAndPlayers->getGameNumber(),
            $playersGroup,
        );
    }

    public function removeCurrentGameAndPlayers(ScoreSheetIdentifier $scoreSheetIdentifier): void
    {
        $ormGameAndPlayers = $this->findOneBy(['scoreSheetId' => $scoreSheetIdentifier->toString()]);

        if ($ormGameAndPlayers instanceof OrmCurrentGamesAndPlayer) {
            $this->_em->remove($ormGameAndPlayers);
            $this->_em->flush();
        }
    }

    public function getPlayersGroup(ScoreSheetIdentifier $scoreSheetIdentifier): PlayersGroup
    {
        return $this->getGameAndPlayers($scoreSheetIdentifier)->playersGroup;
    }
}
