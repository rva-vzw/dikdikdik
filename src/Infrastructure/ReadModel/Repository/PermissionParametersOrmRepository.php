<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Domain\ReadModel\TablePermissions\PermissionParameters;
use App\Domain\ReadModel\TablePermissions\PermissionParametersRepository;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\ReadModel\Entity\OrmPermissionParameters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @extends ServiceEntityRepository<OrmPermissionParameters>
 */
final class PermissionParametersOrmRepository extends ServiceEntityRepository implements PermissionParametersRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private readonly NormalizerInterface $normalizer,
        private readonly DenormalizerInterface $denormalizer,
    ) {
        parent::__construct($registry, OrmPermissionParameters::class);
    }

    public function getPermissionParameters(TableIdentifier $tableIdentifier): PermissionParameters
    {
        $ormPermissionParameters = $this->findOneBy([
            'tableId' => $tableIdentifier->toString(),
        ]);

        return $ormPermissionParameters instanceof OrmPermissionParameters
            ? $this->denormalizer->denormalize($ormPermissionParameters->normalizedPermissionParameters, PermissionParameters::class)
            : PermissionParameters::forNewTable($tableIdentifier);
    }

    public function savePermissionParameters(PermissionParameters $permissionParameters): void
    {
        $ormPermissionParameters = $this->findOneBy([
            'tableId' => $permissionParameters->tableIdentifier,
        ]);

        /** @var array<string, mixed> $normalizedParameters */
        $normalizedParameters = $this->normalizer->normalize($permissionParameters);

        if ($ormPermissionParameters instanceof OrmPermissionParameters) {
            $ormPermissionParameters->normalizedPermissionParameters = $normalizedParameters;
        } else {
            $ormPermissionParameters = new OrmPermissionParameters(
                $permissionParameters->tableIdentifier->toString(),
                $normalizedParameters,
                null,
            );
        }

        $this->_em->persist($ormPermissionParameters);
        $this->_em->flush();
    }

    public function deletePermissionParameters(
        TableIdentifier $tableIdentifier
    ): void {
        $permissionParameters = $this->findOneBy([
            'tableId' => $tableIdentifier->toString(),
        ]);

        if ($permissionParameters instanceof OrmPermissionParameters) {
            $this->_em->remove($permissionParameters);
        }
    }
}
