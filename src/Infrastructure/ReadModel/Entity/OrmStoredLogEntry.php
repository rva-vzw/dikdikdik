<?php

namespace App\Infrastructure\ReadModel\Entity;

use App\Domain\ReadModel\ScoreLog\Model\ScoreLogEntry;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineBuilder;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Infrastructure\ReadModel\OrmLogEntryIdentifier;
use App\Infrastructure\ReadModel\Repository\StoredScoreLogOrmRepository;
use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

#[ORM\Table(name: 'readmodel_log_entry')]
#[ORM\Entity(repositoryClass: StoredScoreLogOrmRepository::class)]
#[ORM\Index(name: 'entry_table_game_index', columns: ['score_sheet_id', 'game_number'])]
#[ORM\UniqueConstraint('entry_log_entry_id', ['log_entry_id'])]
class OrmStoredLogEntry implements ScoreLogEntry
{
    #[ORM\Column(type: 'guid')] private string $logEntryId;
    #[ORM\Column(type: 'integer')] private int $gameNumber;
    #[ORM\Column(type: 'guid')] private string $dealerId;
    /**
     * @var array<string, int>
     */
    #[ORM\Column(type: 'json')] private array $scores;
    #[ORM\Column(type: 'integer')] private int $scoreFactor;
    #[ORM\Column(type: 'string')] private string $note;
    /** @var non-empty-string */
    #[ORM\Column(type: 'guid')] private string $scoreSheetId;
    #[ORM\Column(type: 'integer')] private int $scoreLineNumber;
    /** @phpstan-ignore-next-line */
    #[ORM\Id] #[ORM\GeneratedValue] #[ORM\Column(type: 'integer')] private ?int $id = null;

    public function __construct(
        GameNumber $gameNumber,
        ScoreSheetIdentifier $scoreSheetIdentifier,
        ?PlayerIdentifier $dealer,
        ScoreFactor $scoreFactor,
        ScoreLine $scoreLine
    ) {
        $this->logEntryId = OrmLogEntryIdentifier::forScoreLine($scoreSheetIdentifier, $scoreLine->getScoreLineNumber())->toString();
        $this->gameNumber = $gameNumber->toInteger();
        $this->scores = [];
        $this->scoreSheetId = $scoreSheetIdentifier->toString();
        $this->dealerId = $dealer?->toString() ?? '';
        $this->scoreFactor = $scoreFactor->toInteger();
        $this->setScoreLine($scoreLine);
    }

    public function getGameNumber(): GameNumber
    {
        return GameNumber::fromInteger($this->gameNumber);
    }

    public function getDealerIdentifier(): ?PlayerIdentifier
    {
        return empty($this->dealerId) ? null : PlayerIdentifier::fromString($this->dealerId);
    }

    public function getScoreFactor(): ScoreFactor
    {
        return ScoreFactor::fromInteger($this->scoreFactor);
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setDealer(?PlayerIdentifier $dealerIdentifier): void
    {
        $this->dealerId = $dealerIdentifier?->toString() ?? '';
    }

    public function getDealer(): ?PlayerIdentifier
    {
        return empty($this->dealerId) ? null : PlayerIdentifier::fromString($this->dealerId);
    }

    public function getScoreSheetIdentifier(): ScoreSheetIdentifier
    {
        /* @noinspection PhpUnhandledExceptionInspection */
        return ScoreSheetIdentifier::fromString($this->scoreSheetId);
    }

    public function setScoreFactor(ScoreFactor $newScoreFactor): void
    {
        $this->scoreFactor = $newScoreFactor->toInteger();
    }

    public function setNote(string $note): void
    {
        $this->note = $note;
    }

    public function setScoreLine(ScoreLine $scoreLine): void
    {
        $this->note = $scoreLine->getNote();

        $scoreLine->getKnownPlayerIdentifiers()->each(
            function (PlayerIdentifier $playerIdentifier) use ($scoreLine): void {
                $this->scores[$playerIdentifier->toString()] = $scoreLine->getFrancsForPlayer($playerIdentifier);
            }
        );

        $this->scoreLineNumber = $scoreLine->getScoreLineNumber()->toInteger();
    }

    public function getScoreLine(): ScoreLine
    {
        $builder = ScoreLineBuilder::create($this->note, $this->getScoreLineNumber());

        foreach ($this->scores as $playerId => $francs) {
            Assert::notEmpty($playerId);
            $builder = $builder->withFrancsForPlayer(
                PlayerIdentifier::fromString($playerId),
                $francs
            );
        }

        return $builder->build();
    }

    public function getScoreLineNumber(): ScoreLineNumber
    {
        return ScoreLineNumber::fromInteger($this->scoreLineNumber);
    }

    public function getLogEntryId(): string
    {
        return $this->logEntryId;
    }

    public function hasScores(): bool
    {
        return !empty($this->scores);
    }
}
