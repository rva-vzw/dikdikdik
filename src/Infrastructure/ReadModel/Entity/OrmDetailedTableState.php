<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Entity;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\ReadModel\Repository\DetailedTableStateOrmRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Doctrine entity that contains the information on the (ordered) players at the table.
 */
#[ORM\Table(
    name: 'readmodel_tablestate',
)]
#[ORM\Entity(repositoryClass: DetailedTableStateOrmRepository::class)]
#[ORM\UniqueConstraint('tablestate_table', ['table_id'])]
#[ORM\UniqueConstraint('tableState_sheet', ['score_sheet_id'])]
class OrmDetailedTableState
{
    public const RULESET_UNKNOWN = 'ruleset.unknown';

    /**
     * @param non-empty-string $tableId
     *
     * @noinspection PhpPropertyOnlyWrittenInspection
     */
    private function __construct(
        #[ORM\Column(type: 'guid')] public string $tableId,
        #[ORM\Column(type: 'integer')] public int $gameNr,
        #[ORM\Column(type: 'guid', nullable: true)] public ?string $dealerId,
        // Keep on using the old column name; I don't want to change the DB too much
        #[ORM\Column(type: 'boolean', name: 'alone_tricks_configurable')] public bool $canChangeRuleSet,
        #[ORM\Column(type: 'boolean')] public bool $dealerChangeable,
        #[ORM\Column(type: 'integer', nullable: true)] public ?int $finalGameNumber,
        #[ORM\Column(type: 'guid')] public string $scoreSheetId,
        #[ORM\Column(type: 'integer')] public int $numberOfPlayers,
        #[ORM\Column(type: 'string', length: 40)] public string $ruleSetName,
        /** @phpstan-ignore-next-line */
        #[ORM\Id] #[ORM\GeneratedValue] #[ORM\Column(type: 'integer')] private ?int $id = null,
    ) {
    }

    public static function create(TableIdentifier $tableIdentifier): self
    {
        return new OrmDetailedTableState(
            $tableIdentifier->toString(),
            1,
            null,
            true,
            false,
            null,
            ScoreSheetIdentifier::forTable($tableIdentifier)->toString(),
            0,
            self::RULESET_UNKNOWN,
        );
    }

    public function getDealerIdentifier(): ?PlayerIdentifier
    {
        return empty($this->dealerId) ? null : PlayerIdentifier::fromString($this->dealerId);
    }

    public function isFinalRound(): bool
    {
        return is_int($this->finalGameNumber);
    }

    public function getFinalGameNumber(): ?GameNumber
    {
        return is_int($this->finalGameNumber)
            ? GameNumber::fromInteger($this->finalGameNumber) : null;
    }

    public function toTableState(): TableState
    {
        return new TableState(
            TableIdentifier::fromString($this->tableId),
            $this->getDealerIdentifier(),
            GameNumber::fromInteger($this->gameNr),
            $this->canChangeRuleSet,
            $this->dealerChangeable,
            $this->numberOfPlayers,
            $this->getFinalGameNumber(),
            $this->ruleSetName,
        );
    }
}
