<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Entity;

use App\Domain\ReadModel\GameCounter\GameCounter;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\ReadModel\Repository\GameCounterOrmRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'readmodel_game_counter')]
#[ORM\UniqueConstraint('game_counter_table', ['table_id'])]
#[ORM\Entity(repositoryClass: GameCounterOrmRepository::class)]
#[ORM\Index(name: 'game_counter_first', columns: ['first_game_logged_at'])]
#[ORM\Index(name: 'game_counter_latest', columns: ['latest_game_logged_at'])]
#[ORM\Index(name: 'game_counter_date_parts', columns: ['year', 'month'])]
class OrmGameCounter
{
    /**
     * @param non-empty-string $tableId
     */
    private function __construct(
        #[ORM\Column(type: 'guid')] private string $tableId,
        #[ORM\Column(type: 'integer')] private int $gameCount,
        #[ORM\Column(type: 'datetime_immutable')] private \DateTimeImmutable $firstGameLoggedAt,
        #[ORM\Column(type: 'datetime_immutable')] private \DateTimeImmutable $latestGameLoggedAt,
        #[ORM\Column(type: 'integer')] private int $year,
        #[ORM\Column(type: 'integer')] private int $month,
        /** @phpstan-ignore-next-line */
        #[ORM\Id] #[ORM\GeneratedValue] #[ORM\Column(type: 'integer')] private ?int $id = null,
    ) {
    }

    public static function createNew(TableIdentifier $tableIdentifier, \DateTimeImmutable $dateTime): self
    {
        return new self(
            $tableIdentifier->toString(),
            0,
            $dateTime,
            $dateTime,
            (int) $dateTime->format('Y'),
            (int) $dateTime->format('m'),
        );
    }

    public function getTableId(): string
    {
        return $this->tableId;
    }

    public function getGameCount(): int
    {
        return $this->gameCount;
    }

    public function setGameCount(int $gameCount): void
    {
        $this->gameCount = $gameCount;
    }

    public function getFirstGameLoggedAt(): \DateTimeImmutable
    {
        return $this->firstGameLoggedAt;
    }

    public function getLatestGameLoggedAt(): \DateTimeImmutable
    {
        return $this->latestGameLoggedAt;
    }

    public function setLatestGameLoggedAt(\DateTimeImmutable $latestGameLoggedAt): void
    {
        $this->latestGameLoggedAt = $latestGameLoggedAt;
        $this->year = (int) $latestGameLoggedAt->format('Y');
        $this->month = (int) $latestGameLoggedAt->format('m');
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function toGameCounter(): GameCounter
    {
        return GameCounter::createNew(
            TableIdentifier::fromString($this->tableId),
            $this->firstGameLoggedAt
        )->withGamesCounted(
            $this->gameCount,
            $this->latestGameLoggedAt
        );
    }
}
