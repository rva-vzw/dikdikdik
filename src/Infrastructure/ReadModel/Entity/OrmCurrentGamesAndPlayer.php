<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Entity;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Infrastructure\ReadModel\Repository\CurrentGameAndPlayersOrmRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(
    name: 'readmodel_scorelog_game_and_players',
)]
#[ORM\Entity(repositoryClass: CurrentGameAndPlayersOrmRepository::class)]
#[ORM\UniqueConstraint('tablestate_sheet', ['score_sheet_id'])]
class OrmCurrentGamesAndPlayer
{
    /**
     * @param non-empty-string $scoreSheetId
     *
     * @noinspection PhpPropertyOnlyWrittenInspection
     */
    private function __construct(
        #[ORM\Column(type: 'guid')] private string $scoreSheetId,
        /** @var mixed[] */
        #[ORM\Column(type: 'json')] private array $normalizedPlayers,
        #[ORM\Column(type: 'integer')] private int $gameNr,
        /** @phpstan-ignore-next-line */
        #[ORM\Id] #[ORM\GeneratedValue] #[ORM\Column(type: 'integer')] private ?int $id = null,
    ) {
    }

    public static function create(
        ScoreSheetIdentifier $scoreSheetIdentifier,
    ): self {
        return new self(
            $scoreSheetIdentifier->toString(),
            [],
            1,
        );
    }

    /**
     * @return mixed[]
     */
    public function getNormalizedPlayers(): array
    {
        return $this->normalizedPlayers;
    }

    /**
     * @param mixed[] $normalizedPlayers
     */
    public function setNormalizedPlayers(array $normalizedPlayers): void
    {
        $this->normalizedPlayers = $normalizedPlayers;
    }

    public function getScoreSheetIdentifier(): ScoreSheetIdentifier
    {
        return ScoreSheetIdentifier::fromString($this->scoreSheetId);
    }

    public function setGameNumber(GameNumber $gameNumber): void
    {
        $this->gameNr = $gameNumber->toInteger();
    }

    public function getGameNumber(): GameNumber
    {
        return GameNumber::fromInteger($this->gameNr);
    }
}
