<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Entity;

use App\Infrastructure\ReadModel\Repository\PermissionParametersOrmRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'readmodel_permission_parameters')]
#[ORM\Entity(repositoryClass: PermissionParametersOrmRepository::class)]
#[ORM\UniqueConstraint(name: 'permission_parameters_table', columns: ['table_id'])]
class OrmPermissionParameters
{
    /**
     * @param array<string, mixed> $normalizedPermissionParameters
     */
    public function __construct(
        #[ORM\Column(type: 'guid')] public string $tableId,
        #[ORM\Column(type: 'json')] public array $normalizedPermissionParameters,
        /** @phpstan-ignore-next-line  */
        #[ORM\Id] #[ORM\GeneratedValue] #[ORM\Column(type: 'integer')] private ?int $id = null,
    ) {
    }
}
