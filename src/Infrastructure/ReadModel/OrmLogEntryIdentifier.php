<?php

namespace App\Infrastructure\ReadModel;

use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use RvaVzw\KrakBoem\Id\Uuid5Identifier;

final readonly class OrmLogEntryIdentifier extends Uuid5Identifier
{
    protected static function getNamespace(): string
    {
        return '3f5851bc-fe7a-4d79-9117-b31b27cec9ae';
    }

    public static function forScoreLine(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        ScoreLineNumber $scoreLineNumber
    ): self {
        return self::fromName(implode(
            '+',
            [$scoreSheetIdentifier->toString(), (string) $scoreLineNumber->toInteger()]
        ));
    }
}
