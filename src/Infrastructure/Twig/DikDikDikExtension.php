<?php

declare(strict_types=1);

namespace App\Infrastructure\Twig;

use App\Domain\ReadModel\Player\Model\Player;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class DikDikDikExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('playerName', [$this, 'playerName']),
        ];
    }

    /**
     * Shows player name followed by player stars.
     */
    public function playerName(Player $player): string
    {
        return $player->getName().str_repeat('<i class="fa fa-star"></i>', $player->getNumberOfStars());
    }
}
