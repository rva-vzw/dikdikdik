<?php

declare(strict_types=1);

namespace App\Infrastructure\HealthCheck;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

final class TestMercureCommand extends Command
{
    public function __construct(
        private readonly HubInterface $hub,
    ) {
        parent::__construct('dikdikdik:check:mercure');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->hub->publish(
            new Update(
                'health_check',
                'ping!',
            ),
        );
        $output->writeln('<info>done</info>');

        return 0;
    }
}
