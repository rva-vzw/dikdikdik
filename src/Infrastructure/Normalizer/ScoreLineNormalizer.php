<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineBuilder;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class ScoreLineNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed   $data
     * @param mixed[] $context
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): ScoreLine
    {
        Assert::isArray($data);

        /** @var int[] $francs */
        $francs = $data['scores'];

        $builder = ScoreLineBuilder::create(
            (string) $data['note'],
            ScoreLineNumber::fromInteger((int) $data['line'])
        );

        foreach ($francs as $playerId => $francsForPlayer) {
            Assert::notEmpty($playerId);
            $playerIdentifier = PlayerIdentifier::fromString($playerId);
            $builder = $builder->withFrancsForPlayer(
                $playerIdentifier,
                $francsForPlayer
            );
        }

        return $builder->build();
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return ScoreLine::class === $type;
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        if (!$object instanceof ScoreLine) {
            throw new \InvalidArgumentException();
        }

        $data = [
            'note' => $object->getNote(),
            'line' => $object->getScoreLineNumber()->toInteger(),
            'scores' => [],
        ];

        /** @var PlayerIdentifier $playerIdentifier */
        foreach ($object->getKnownPlayerIdentifiers() as $playerIdentifier) {
            $data['scores'][$playerIdentifier->toString()] = $object->getFrancsForPlayer($playerIdentifier);
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ScoreLine;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            ScoreLine::class => true,
        ];
    }
}
