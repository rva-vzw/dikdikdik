<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer\Event;

use App\Domain\Table\Event\Undo\GameUndone;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;
use InvalidArgumentException;
use ReflectionMethod;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

/**
 * This is the counterpart of {@see GamePlayedNormalizer}.
 *
 * Maybe we can consolidate those two normalizers over time.
 */
final readonly class GameUndoneNormalizer implements NormalizerInterface, DenormalizerInterface
{
    public function __construct(
        #[Autowire(service: 'serializer.normalizer.object')]
        private NormalizerInterface & DenormalizerInterface $parentNormalizer,
        private AllSpecifiedGames $specifiedGames,
    ) {
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        if (!$object instanceof GameUndone) {
            throw new InvalidArgumentException();
        }

        $gameSpecification = $this->specifiedGames->getByUndoEvent($object);

        $normalized = [
            '__undo' => $gameSpecification->getName(),
            'tableIdentifier' => $object->getTableIdentifier()->toString(),
            'gameNumber' => $object->getGameNumber()->toInteger(),
            'announcement' => $this->parentNormalizer->normalize(
                $object->getAnnouncement(),
                $format,
                $context
            ),
            'outcome' => $this->parentNormalizer->normalize(
                $object->getOutcome(),
                $format,
                $context
            ),
            'playersInvolved' => $this->parentNormalizer->normalize($object->getPlayersInvolved()),
            'passedAroundCount' => $object->getPassedAroundCount(),
        ];

        return $normalized;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof GameUndone;
    }

    /**
     * @return GameUndone<GameAnnouncement, GameOutcome>
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): GameUndone
    {
        Assert::isArray($data);
        if (!array_key_exists('__undo', $data)) {
            throw new \InvalidArgumentException('sorry!');
        }
        $gameSpecification = $this->specifiedGames->getByGameName($data['__undo']);

        $createMethod = new ReflectionMethod(
            $gameSpecification->getUndoEventType(),
            'create'
        );

        /** @var GameAnnouncement $announcement */
        $announcement = $this->parentNormalizer->denormalize(
            $data['announcement'],
            $gameSpecification->getAnnouncementType(),
            $format,
            $context
        );

        /** @var GameOutcome $outcome */
        $outcome = $this->parentNormalizer->denormalize(
            $data['outcome'],
            $gameSpecification->getOutcomeType(),
            $format,
            $context
        );

        $playersInvolved = $this->parentNormalizer->denormalize(
            $data['playersInvolved'],
            PlayersInvolved::class,
            $format,
            $context
        );

        /** @var GameUndone<GameAnnouncement, GameOutcome> $result */
        $result = $createMethod->invoke(
        // invoke static method
            null,
            TableIdentifier::fromString($data['tableIdentifier']),
            GameNumber::fromInteger($data['gameNumber']),
            $announcement,
            $outcome,
            $playersInvolved,
            (int) $data['passedAroundCount'],
        );

        return $result;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        if ('[]' === substr($type, -2, 2)) {
            return false;
        }

        /** @var string[] $interfaces */
        $interfaces = class_implements($type);

        return in_array(GameUndone::class, $interfaces);
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            GameUndone::class => true,
        ];
    }
}
