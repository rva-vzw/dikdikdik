<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer\Event;

use App\Domain\Table\Event\TableConfigured;
use App\Domain\Table\TableIdentifier;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Webmozart\Assert\Assert;

/**
 * Dedicated denormalizer for {@see TableConfigured}.
 *
 * The {@see TableConfigured} event changed for #324. Make sure that normalized versions
 * of the old events are correctly denormalized.
 */
final readonly class TableConfiguredDenormalizer implements DenormalizerInterface
{
    #[\Override]
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        Assert::isArray($data);
        /** @var array{tableIdentifier: non-empty-string, ruleSetName?: non-empty-string, tableConfiguration?: array<mixed>} $data */
        $fixed = $this->fixNormalizedData($data);

        return new TableConfigured(
            TableIdentifier::fromString($fixed['tableIdentifier']),
            $fixed['ruleSetName'],
        );
    }

    #[\Override]
    public function supportsDenormalization(mixed $data, string $type, string $format = null)
    {
        return TableConfigured::class === $type;
    }

    /**
     * @param array{tableIdentifier: non-empty-string, ruleSetName?: non-empty-string, tableConfiguration?: array<mixed>} $data
     *
     * @return array{tableIdentifier: non-empty-string, ruleSetName: non-empty-string, tableConfiguration?: array<mixed>}
     */
    private function fixNormalizedData(array $data): array
    {
        if (array_key_exists('ruleSetName', $data)) {
            Assert::stringNotEmpty($data['ruleSetName']);

            return $data;
        }

        $configuration = $data['tableConfiguration'] ?? null;
        Assert::isArray($configuration);
        $data['ruleSetName'] = $configuration['alone5Enabled'] ? 'conventions.alone5' : 'conventions.traditional';

        return $data;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            TableConfigured::class => true,
        ];
    }
}
