<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer\Event;

use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;
use DateTimeImmutable;
use InvalidArgumentException;
use ReflectionMethod;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

/**
 * Normalizer and denormalizer for played game events.
 *
 * I wrote a dedicated normalizer, because the interfaces in the abstract
 * game event seemed to confuse the default property normalizer.
 *
 * I'm not very happy with this class. But as long as it works, it will do.
 */
final readonly class GamePlayedNormalizer implements NormalizerInterface, DenormalizerInterface
{
    public function __construct(
        #[Autowire(service: 'serializer.normalizer.property')]
        private NormalizerInterface & DenormalizerInterface $parentNormalizer,
        private AllSpecifiedGames $specifiedGames,
    ) {
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        if (!$object instanceof GamePlayed) {
            throw new InvalidArgumentException();
        }

        $gameSpecification = $this->specifiedGames->getByEvent($object);

        /** @var mixed[] $normalized */
        $normalized = [
            '__gameName' => $gameSpecification->getName(),
            'tableIdentifier' => $object->getTableIdentifier()->toString(),
            'gameNumber' => $object->getGameNumber()->toInteger(),
            'announcement' => $this->parentNormalizer->normalize(
                $object->getAnnouncement(),
                $format,
                $context
            ),
            'outcome' => $this->parentNormalizer->normalize(
                $object->getOutcome(),
                $format,
                $context
            ),
            'playersInvolved' => $this->parentNormalizer->normalize($object->getPlayersInvolved()),
            'loggedAt' => $object->getLoggedAt()->format('c'),
        ];

        return $normalized;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof GamePlayed;
    }

    /**
     * @return GamePlayed<GameAnnouncement, GameOutcome>
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): GamePlayed
    {
        Assert::isArray($data);

        $gameSpecification = $this->specifiedGames->getByGameName($data['__gameName']);

        $createMethod = new ReflectionMethod(
            $gameSpecification->getEventType(),
            'create'
        );

        /** @var GameAnnouncement $announcement */
        $announcement = $this->parentNormalizer->denormalize(
            $data['announcement'],
            $gameSpecification->getAnnouncementType(),
            $format,
            $context
        );

        /** @var GameOutcome $outcome */
        $outcome = $this->parentNormalizer->denormalize(
            $data['outcome'],
            $gameSpecification->getOutcomeType(),
            $format,
            $context
        );

        $playersInvolved = $this->parentNormalizer->denormalize(
            $data['playersInvolved'],
            PlayersInvolved::class,
            $format,
            $context
        );

        /** @var GamePlayed<GameAnnouncement, GameOutcome> $result */
        $result = $createMethod->invoke(
            // invoke static method
            null,
            TableIdentifier::fromString($data['tableIdentifier']),
            GameNumber::fromInteger($data['gameNumber']),
            $announcement,
            $outcome,
            $playersInvolved,
            new DateTimeImmutable($data['loggedAt'])
        );

        return $result;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        if ('[]' === substr($type, -2, 2)) {
            return false;
        }

        /** @var string[] $interfaces */
        $interfaces = class_implements($type);

        return in_array(GamePlayed::class, $interfaces);
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            GamePlayed::class => true,
        ];
    }
}
