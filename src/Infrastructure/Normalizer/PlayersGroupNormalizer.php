<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class PlayersGroupNormalizer implements DenormalizerInterface, NormalizerInterface
{
    public function __construct(
        private readonly PlayerNormalizer $parentNormalizer
    ) {
    }

    /**
     * @param mixed   $data
     * @param mixed[] $context
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): PlayersGroup
    {
        Assert::isArray($data);
        if (PlayersGroup::class === $type) {
            /** @var Player[] $players */
            $players = array_map(
                fn (array $normalizedDetails) => $this->parentNormalizer->denormalize(
                    $normalizedDetails,
                    Player::class,
                    $format,
                    $context
                ),
                $data,
            );

            return PlayersGroup::fromArray($players);
        }

        throw new InvalidArgumentException();
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return PlayersGroup::class === $type;
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        if (!$object instanceof PlayersGroup) {
            throw new InvalidArgumentException();
        }

        $data = [];
        foreach ($object as $playerDetails) {
            $data[] = $this->parentNormalizer->normalize($playerDetails);
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof PlayersGroup;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            PlayersGroup::class => true,
        ];
    }
}
