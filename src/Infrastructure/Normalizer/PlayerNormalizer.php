<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayerStatus;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Seat;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class PlayerNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        Assert::isArray($data);

        if (Player::class === $type) {
            $playerIdentifier = PlayerIdentifier::fromString($data['playerIdentifier']);
            $seat = Seat::fromInteger($data['lastKnownSeat']);
            $player = match ($data['status']) {
                PlayerStatus::NEW->value => Player::createNew($playerIdentifier, $seat, $data['name']),
                PlayerStatus::ACTIVE->value => Player::createActive($playerIdentifier, $seat, $data['name']),
                PlayerStatus::GONE->value => Player::createGone($playerIdentifier, $seat, $data['name']),
                default => throw new \LogicException('PlayerNormalizer: invalid normalized player status'),
            };
            $player = $player->withStars($data['stars']);

            return $data['dealer'] ? $player->dealing() : $player;
        }

        throw new \InvalidArgumentException('PlayerNormalizer: expected to denormalize a Player');
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null)
    {
        return Player::class === $type;
    }

    /**
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        if (!$object instanceof Player) {
            throw new \InvalidArgumentException('PlayerNormalizer: expected a Player to normalize');
        }

        return [
            'name' => $object->getName(),
            'status' => $object->getStatus()->value,
            'playerIdentifier' => $object->getPlayerIdentifier()->toString(),
            'lastKnownSeat' => $object->getLastKnownSeat()->toInteger(),
            'stars' => $object->getNumberOfStars(),
            'dealer' => $object->isDealer(),
        ];
    }

    public function supportsNormalization(mixed $data, string $format = null)
    {
        return $data instanceof Player;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            Player::class => true,
        ];
    }
}
