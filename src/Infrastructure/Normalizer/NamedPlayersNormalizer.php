<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\Table\Player\NamedPlayer;
use App\Domain\Table\Player\NamedPlayers;
use App\Domain\Table\Player\PlayerIdentifier;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class NamedPlayersNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed   $data
     * @param mixed[] $context
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): NamedPlayers
    {
        Assert::isArray($data);
        if (NamedPlayers::class === $type) {
            $namedPlayers = NamedPlayers::none();
            foreach ($data as $id => $name) {
                Assert::notEmpty($id);
                $namedPlayers = $namedPlayers->with(new NamedPlayer(
                    PlayerIdentifier::fromString($id),
                    $name,
                ));
            }

            return $namedPlayers;
        }

        throw new InvalidArgumentException();
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return NamedPlayers::class === $type;
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        if ($object instanceof NamedPlayers) {
            $array = [];
            /** @var NamedPlayer $namedPlayer */
            foreach ($object as $namedPlayer) {
                $array[$namedPlayer->getPlayerIdentifier()->toString()] = $namedPlayer->getPlayerName();
            }

            return $array;
        }

        throw new InvalidArgumentException();
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof NamedPlayers;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            NamedPlayersNormalizer::class => true,
        ];
    }
}
