<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\ReadModel\Player\Model\PlayerStatus;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

// FIXME: Can't the Symfony normalizer take care of this?
final class PlayerStatusNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        Assert::eq($type, PlayerStatus::class);
        Assert::string($data);

        return PlayerStatus::from(strval($data));
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null)
    {
        return PlayerStatus::class === $type;
    }

    /**
     * @param mixed[] $context
     *
     * @return 'active'|'inactive'|'new'
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        Assert::isInstanceOf($object, PlayerStatus::class);

        return $object->value;
    }

    public function supportsNormalization(mixed $data, string $format = null)
    {
        return $data instanceof PlayerStatus;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            PlayerStatus::class => true,
        ];
    }
}
