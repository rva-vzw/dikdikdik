<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\Table\Player\ArraySerializablePlayers;
use App\Domain\Table\Player\PlayerIdentifier;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

/**
 * A normalizer for a set of player identifiers.
 */
final class PlayersArrayNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed   $data
     * @param mixed[] $context
     *
     * @return ArraySerializablePlayers
     *
     * @throws ReflectionException
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        Assert::isArray($data);

        /** @var class-string $classString */
        $classString = $type;

        $reflectionMethod = new ReflectionMethod($classString, 'fromArray');

        /** @var ArraySerializablePlayers $playerSet */
        $playerSet = $reflectionMethod->invoke(
            null,
            array_map(
                function (string $playerId): PlayerIdentifier {
                    Assert::notEmpty($playerId);

                    return PlayerIdentifier::fromString($playerId);
                },
                $data
            )
        );

        return $playerSet;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        if ('[]' === substr($type, -2, 2)) {
            return false;
        }

        /** @var string[] $interfaces */
        $interfaces = class_implements($type);

        return in_array(ArraySerializablePlayers::class, $interfaces);
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        /** @var ArraySerializablePlayers $playerSet */
        $playerSet = $object;

        return array_map(
            function (PlayerIdentifier $playerIdentifier): string {
                return $playerIdentifier->toString();
            },
            array_values($playerSet->toArray())
        );
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ArraySerializablePlayers;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            ArraySerializablePlayers::class => true,
        ];
    }
}
