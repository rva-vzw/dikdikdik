<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\ReadModel\Player\Model\PresentPlayer;
use App\Domain\ReadModel\Player\Model\PresentPlayers;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final readonly class PresentPlayersNormalizer implements NormalizerInterface, DenormalizerInterface
{
    public function __construct(
        #[Autowire(service: 'serializer.normalizer.object')]
        private NormalizerInterface & DenormalizerInterface $parentNormalizer
    ) {
    }

    /**
     * @param mixed   $data
     * @param mixed[] $context
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): PresentPlayers
    {
        Assert::isArray($data);
        if (PresentPlayers::class === $type) {
            return new PresentPlayers(
                array_map(
                    fn (array $normalizedPlayer) => $this->parentNormalizer->denormalize(
                        $normalizedPlayer,
                        PresentPlayer::class
                    ),
                    $data,
                ),
            );
        }

        throw new InvalidArgumentException();
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return PresentPlayers::class === $type;
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        if (!$object instanceof PresentPlayers) {
            throw new InvalidArgumentException();
        }

        $data = [];
        foreach ($object as $seatNr => $player) {
            $data[$seatNr] = $this->parentNormalizer->normalize($player);
        }

        return $data;
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof PresentPlayers;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            PresentPlayers::class => true,
        ];
    }
}
