<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\Score\PlainWinningsBuilder;
use App\Domain\Table\Player\PlayerIdentifier;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class PlainWinningsNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed   $data
     * @param mixed[] $context
     *
     * @return PlainWinnings
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        /** @var int[] $francs */
        $francs = $data;

        $builder = PlainWinningsBuilder::create();

        foreach ($francs as $playerId => $francsForPlayer) {
            Assert::notEmpty($playerId);
            $playerIdentifier = PlayerIdentifier::fromString($playerId);

            if ($francsForPlayer > 0) {
                $builder = $builder->withWinner($playerIdentifier, $francsForPlayer);
            }
            if ($francsForPlayer < 0) {
                $builder = $builder->withLoser($playerIdentifier, -$francsForPlayer);
            }
        }

        return $builder->build();
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return PlainWinnings::class === $type;
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        if (!$object instanceof PlainWinnings) {
            throw new \InvalidArgumentException();
        }

        $data = [];

        /** @var PlayerIdentifier $playerIdentifier */
        foreach ($object->getKnownPlayerIdentifiers() as $playerIdentifier) {
            $data[$playerIdentifier->toString()] = $object->getFrancsForPlayer($playerIdentifier);
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof PlainWinnings;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            PlainWinnings::class => true,
        ];
    }
}
