<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\ReadModel\ScoreLog\Model\SimpleScoreLogEntry;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

/**
 * SimpleScoreLogEntryNormalizer - only used for the api.
 */
final class SimpleScoreLogEntryNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /** @var ScoreLineNormalizer */
    private $scoreLineNormalizer;

    public function __construct(ScoreLineNormalizer $scoreLineNormalizer)
    {
        $this->scoreLineNormalizer = $scoreLineNormalizer;
    }

    /**
     * @param mixed   $data
     * @param mixed[] $context
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): SimpleScoreLogEntry
    {
        Assert::isArray($data);

        /** @var ScoreLine $scoreLine */
        $scoreLine = $this->scoreLineNormalizer->denormalize(
            $data,
            ScoreLine::class
        );

        return new SimpleScoreLogEntry(
            empty($data['dealer']) ? null : PlayerIdentifier::fromString($data['dealer']),
            ScoreFactor::fromInteger((int) $data['factor']),
            GameNumber::fromInteger((int) $data['game']),
            $scoreLine,
            ScoreSheetIdentifier::fromString($data['score_sheet_id']),
        );
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return SimpleScoreLogEntry::class === $type;
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        if (!$object instanceof SimpleScoreLogEntry) {
            throw new \InvalidArgumentException();
        }

        /** @var mixed[] $data */
        $data = $this->scoreLineNormalizer->normalize($object->getScoreLine());
        $data['game'] = $object->getGameNumber()->toInteger();
        $data['factor'] = $object->getScoreFactor()->toInteger();
        $data['score_sheet_id'] = $object->getScoreSheetIdentifier()->toString();

        $dealer = $object->getDealer();
        if ($dealer instanceof PlayerIdentifier) {
            $data['dealer'] = $dealer->toString();
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof SimpleScoreLogEntry;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            SimpleScoreLogEntry::class => true,
        ];
    }
}
