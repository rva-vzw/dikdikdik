<?php

declare(strict_types=1);

namespace App\Infrastructure\Normalizer;

use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Seat;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

/**
 * A normalizer for {@see PresentPlayers}.
 *
 * Note that we can't just use {@see PlayersArrayNormalizer}, because
 * we also need to know which seats are empty.
 */
final class SeatedPlayersNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed   $data
     * @param mixed[] $context
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): SeatedPlayers
    {
        Assert::isArray($data);

        return SeatedPlayers::fromArray(
            array_map(
                function (string $playerId): PlayerIdentifier {
                    Assert::notEmpty($playerId);

                    return PlayerIdentifier::fromString($playerId);
                },
                array_filter(
                    $data,
                    function (?string $playerId): bool {
                        // leave out null values
                        return is_string($playerId);
                    }
                )
            )
        );
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return SeatedPlayers::class === $type;
    }

    /**
     * @param mixed   $object
     * @param mixed[] $context
     *
     * @return mixed[]
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        if (!$object instanceof SeatedPlayers) {
            throw new \InvalidArgumentException();
        }

        $playersArray = $object->toArray();

        return array_map(
            function (int $seatNumber) use ($playersArray): ?string {
                return array_key_exists($seatNumber, $playersArray)
                    ? $playersArray[$seatNumber]->toString()
                    : null;
            },
            range(0, Seat::NUMBER_OF_SEATS - 1)
        );
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof SeatedPlayers;
    }

    /**
     * @return array<class-string, true>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            SeatedPlayers::class => true,
        ];
    }
}
