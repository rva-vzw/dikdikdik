<?php

namespace App\TestData;

use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\AnnounceFinalRound;
use App\Domain\Table\Command\ConfigureTable;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\KickPlayer;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\Command\ProposeAndNobodyGoesAlong;
use App\Domain\Table\Command\StarPlayer;
use App\Domain\Table\Command\Undo;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TroelAwareMultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AbundanceSpecification;
use App\Domain\Table\Loggable\GameSpecification\Alone5Specification;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\GameSpecification\MisereOnTheTableSpecification;
use App\Domain\Table\Loggable\GameSpecification\MisereSpecification;
use App\Domain\Table\Loggable\GameSpecification\PropAndCopSpecification;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Loggable\Outcome\Winners;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestPlayer;
use App\TestData\TestDataBuilders\TestTable;
use App\TestData\TestDataBuilders\TestTableBuilder;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * This (symfony console) command loads test data needed for the acceptance test.
 *
 * Note that this is not a domain command!
 */
final class TestDataCommand extends Command
{
    private const COMMAND_NAME = 'dikdikdik:testdata';

    public function __construct(
        private readonly CommandBus $commandBus,
        private readonly TestTableBuilder $testTableBuilder,
    ) {
        parent::__construct(self::COMMAND_NAME);
    }

    protected function configure(): void
    {
        $this->setDescription('Load test data for acceptance tests.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->commandBus->dispatch(
            new JoinPlayer(
                TableIdentifier::fromString(TestIds::TEST_TABLE_1),
                PlayerIdentifier::fromString(TestIds::PLAYER_PENNINGMEESTER),
                GameNumber::first(),
                'penningmeester',
                Seat::three(),
            ),
        );

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(
            TestTable::table2(),
        );
        $this->commandBus->dispatch(
            // log a game, so that every new player becomes active
            new LogGame(
                TestTable::table2(),
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::dtl()),
                new NumberOfTricks(6),
                new \DateTimeImmutable('2022-05-20'),
            ),
        );

        $this->testTableBuilder->buildTableWithPlayers(
            TestTable::table3(),
            4
        );
        $this->commandBus->dispatch(
            new AnnounceDealer(TestTable::table3(), TestPlayer::penningmeester(), GameNumber::first())
        );

        $this->testTableBuilder->buildTableWithPlayers(TestTable::table4(), 4);
        $this->testTableBuilder->buildTableWithPlayers(TestTable::table5(), 4);
        $this->testTableBuilder->buildTableWithPlayers(TestTable::table6(), 4);
        $this->commandBus->dispatch(
            // log a game, so that every new player becomes active
            new LogGame(
                TestTable::table6(),
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::dtl()),
                new NumberOfTricks(6),
                new \DateTimeImmutable('2022-05-20'),
            ),
        );

        $this->testTableBuilder->buildTableWithPlayers(TestTable::table7(), 4);
        $this->commandBus->dispatch(
            new AnnounceDealer(TestTable::table7(), TestPlayer::penningmeester(), GameNumber::first()),
            new ProposeAndNobodyGoesAlong(TestTable::table7(), GameNumber::first()),
            new PassAround(TestTable::table7(), GameNumber::first()),
            new PassAround(TestTable::table7(), GameNumber::first()),
            new PassAround(TestTable::table7(), GameNumber::first()),
            new ProposeAndNobodyGoesAlong(TestTable::table7(), GameNumber::first()),
            new PassAround(TestTable::table7(), GameNumber::first())
        );

        $this->testTableBuilder->buildTableWithPlayers(TestTable::table8(), 4);
        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table9());
        $this->testTableBuilder->buildTableWithFivePlayersReadyToPlay(TestTable::table10());

        $this->testTableBuilder->buildTableWithPlayers(TestTable::table11(), 5);
        $this->commandBus->dispatch(
            new KickPlayer(TestTable::table11(), TestPlayer::penningmeester(), GameNumber::first())
        );

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table12());
        $this->commandBus->dispatch(
            new LogGame(
                TestTable::table12(),
                GameNumber::first(),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::ddb()),
                new NumberOfTricks(8),
                new \DateTimeImmutable('2021-04-03'),
            )
        );

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table13());
        $this->commandBus->dispatch(
            new LogGame(
                TestTable::table13(),
                GameNumber::first(),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::ddb()),
                new NumberOfTricks(8),
                new \DateTimeImmutable('2021-04-03'),
            )
        );

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table14());
        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table15());

        $this->testTableBuilder->buildTableWithFivePlayersReadyToPlay(TestTable::table16());
        $this->commandBus->dispatch(
            new AnnounceFinalRound(
                TestTable::table16(),
                GameNumber::first()
            )
        );

        $this->testTableBuilder->buildTableWithPlayers(TestTable::table17(), 4);
        $this->commandBus->dispatch(
            new AnnounceDealer(
                TestTable::table17(),
                TestPlayer::secretaris(),
                GameNumber::first()
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::first(),
                new MisereSpecification(),
                new MultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::ddb())),
                new Winners(PlayerIdentifiers::fromSingle(TestPlayer::ddb())),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::second(),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::secretaris(), TestPlayer::penningmeester()),
                new NumberOfTricks(8),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(3),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::penningmeester()),
                new NumberOfTricks(9),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(4),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::ddb(), TestPlayer::secretaris()),
                new NumberOfTricks(11),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new PassAround(
                TestTable::table17(),
                GameNumber::fromInteger(5)
            ),
            new PassAround(
                TestTable::table17(),
                GameNumber::fromInteger(5)
            ),
            new PassAround(
                TestTable::table17(),
                GameNumber::fromInteger(5)
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(5),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::ddb(), TestPlayer::penningmeester()),
                new NumberOfTricks(10),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new PassAround(
                TestTable::table17(),
                GameNumber::fromInteger(6)
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(6),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::ddb(), TestPlayer::secretaris()),
                new NumberOfTricks(11),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(7),
                new MisereSpecification(),
                new MultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::dtl())),
                new Winners(PlayerIdentifiers::fromSingle(TestPlayer::dtl())),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(8),
                new MisereSpecification(),
                new MultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::dtl())),
                Winners::none(),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(9),
                new AbundanceSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::secretaris()),
                AllOrNothing::asSuccess(),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new PassAround(
                TestTable::table17(),
                GameNumber::fromInteger(10)
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(10),
                new MisereOnTheTableSpecification(),
                new TroelAwareMultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::penningmeester()), true),
                new Winners(PlayerIdentifiers::fromSingle(TestPlayer::penningmeester())),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(11),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::secretaris()),
                new NumberOfTricks(10),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(12),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::penningmeester()),
                new NumberOfTricks(9),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new PassAround(
                TestTable::table17(),
                GameNumber::fromInteger(13)
            ),
            new LogGame(
                TestTable::table17(),
                GameNumber::fromInteger(13),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::dtl()),
                NumberOfTricks::fromNumber(7),
                new \DateTimeImmutable('2021-04-03'),
            )
        );

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table18());
        $this->testTableBuilder->buildTableWithFivePlayersReadyToPlay(TestTable::table19());

        $this->commandBus->dispatch(
            new PassAround(
                TestTable::table19(),
                GameNumber::first()
            ),
            new LogGame(
                TestTable::table19(),
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::secretaris()),
                new NumberOfTricks(5),
                new \DateTimeImmutable('2021-04-03'),
            )
        );

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table20());
        $this->commandBus->dispatch(
            new ConfigureTable(
                TestTable::table20(),
                'conventions.alone5',
            ),
            new LogGame(
                TestTable::table20(),
                GameNumber::first(),
                new Alone5Specification(),
                new SinglePlayerAnnouncement(TestPlayer::secretaris()),
                new NumberOfTricks(5),
                new \DateTimeImmutable('2021-04-03'),
            )
        );

        $this->testTableBuilder->buildTableWithSixPlayersReadyToPlay(TestTable::table21());

        // Table 22 is just a new table.

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table23());
        $this->commandBus->dispatch(
            new PassAround(TestTable::table23(), GameNumber::first()),
        );

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table24());

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table25());

        $this->commandBus->dispatch(
            new LogGame(
                TestTable::table25(),
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::secretaris()),
                new NumberOfTricks(5),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                TestTable::table25(),
                GameNumber::second(),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::penningmeester()),
                new NumberOfTricks(9),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new Undo(
                TestTable::table25(),
                GameNumber::third(),
            ),
            new Undo(
                TestTable::table25(),
                GameNumber::second(),
            ),
        );

        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table26());
        $this->testTableBuilder->buildTableWithFourPlayersReadyToPlay(TestTable::table27());
        $this->commandBus->dispatch(
        // log a game, so that every new player becomes active
            new LogGame(
                TestTable::table27(),
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::dtl()),
                new NumberOfTricks(6),
                new \DateTimeImmutable('2022-05-20'),
            ),
        );

        $this->testTableBuilder->buildTableWithSixPlayersReadyToPlay(TestTable::table28());
        $this->commandBus->dispatch(
            // log a game, so that every new player becomes active
            new LogGame(
                TestTable::table28(),
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::secretaris()),
                new NumberOfTricks(6),
                new \DateTimeImmutable('2022-05-30'),
            ),
            // kick a player
            new KickPlayer(TestTable::table28(), TestPlayer::ddb(), GameNumber::second()),
            // add a new player
            new JoinPlayer(TestTable::table28(), TestPlayer::ddl(), GameNumber::second(), 'ddl', null),
            // star a player
            new StarPlayer(TestTable::table28(), GameNumber::second(), TestPlayer::secretaris()),
        );

        $io->success('Test data loaded.');

        return 0;
    }
}
