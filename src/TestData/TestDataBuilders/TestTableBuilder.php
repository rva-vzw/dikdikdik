<?php

namespace App\TestData\TestDataBuilders;

use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;

final class TestTableBuilder
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    // secr, dtl, ddb, penn; dtl deals
    public function buildTableWithFourPlayersReadyToPlay(TableIdentifier $tableIdentifier): void
    {
        $this->buildTableWithPlayers($tableIdentifier, 4);
        $this->commandBus->dispatch(
            new AnnounceDealer(
                $tableIdentifier,
                TestPlayer::dtl(),
                GameNumber::first()
            )
        );
    }

    public function buildTableWithFivePlayersReadyToPlay(TableIdentifier $tableIdentifier): void
    {
        $this->buildTableWithPlayers($tableIdentifier, 5);
        $this->commandBus->dispatch(
            new AnnounceDealer(
                $tableIdentifier,
                TestPlayer::dvl(),
                GameNumber::first()
            )
        );
    }

    public function buildTableWithPlayers(TableIdentifier $tableIdentifier, int $numberOfPlayers): void
    {
        $players = array_slice(
            [
            'secr' => TestPlayer::secretaris(),
            'dtl' => TestPlayer::dtl(),
            'ddb' => TestPlayer::ddb(),
            'penn' => TestPlayer::penningmeester(),
            'dvl' => TestPlayer::dvl(),
            'dpb' => TestPlayer::dpb(),
            ],
            0,
            $numberOfPlayers
        );

        $i = 0;

        foreach ($players as $name => $playerIdentifier) {
            $this->commandBus->dispatch(
                new JoinPlayer(
                    $tableIdentifier,
                    $playerIdentifier,
                    GameNumber::first(),
                    $name,
                    Seat::fromInteger($i++)
                ),
            );
        }
    }

    public function buildTableWithSixPlayersReadyToPlay(TableIdentifier $tableIdentifier): void
    {
        $this->buildTableWithPlayers($tableIdentifier, 6);
        $this->commandBus->dispatch(
            new AnnounceDealer(
                $tableIdentifier,
                TestPlayer::dvl(),
                GameNumber::first()
            )
        );
    }
}
