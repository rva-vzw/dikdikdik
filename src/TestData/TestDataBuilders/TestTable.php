<?php

namespace App\TestData\TestDataBuilders;

use App\Domain\Table\TableIdentifier;

// FIXME: This is a lousy class.

final class TestTable
{
    public static function table1(): TableIdentifier
    {
        return TableIdentifier::fromString(TestIds::TEST_TABLE_1);
    }

    public static function table2(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_2
        );
    }

    public static function table3(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_3
        );
    }

    public static function table4(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_4
        );
    }

    public static function table5(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_5
        );
    }

    public static function table6(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_6
        );
    }

    public static function table7(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_7
        );
    }

    public static function table8(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_8
        );
    }

    public static function table9(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_9
        );
    }

    public static function table10(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_10
        );
    }

    public static function table11(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_11
        );
    }

    public static function table12(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_12
        );
    }

    public static function table13(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_13
        );
    }

    public static function table14(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_14
        );
    }

    public static function table15(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_15
        );
    }

    public static function table16(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_16
        );
    }

    public static function table17(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_17
        );
    }

    public static function table18(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_18
        );
    }

    public static function table19(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_19
        );
    }

    public static function table20(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_20
        );
    }

    public static function table21(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_21
        );
    }

    public static function table23(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_23
        );
    }

    public static function table24(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_24
        );
    }

    public static function table25(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_25
        );
    }

    public static function table26(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_26,
        );
    }

    public static function table27(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_27,
        );
    }

    public static function table28(): TableIdentifier
    {
        return TableIdentifier::fromString(
            TestIds::TEST_TABLE_28,
        );
    }
}
