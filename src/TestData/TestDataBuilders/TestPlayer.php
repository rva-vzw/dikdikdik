<?php

namespace App\TestData\TestDataBuilders;

use App\Domain\Table\Player\PlayerIdentifier;

/**
 * This class provides some static player identifiers.
 *
 * Technically, I should call this class TestPlayerIdentifiers. But since in the context of
 * a table, there is no fundamental difference between a player and it's ID, I called this
 * TestPlayer to make the tests more readable.
 */
final class TestPlayer
{
    public static function secretaris(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_SECRETARIS);
    }

    public static function penningmeester(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_PENNINGMEESTER);
    }

    public static function dtl(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DTL);
    }

    public static function dlb(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DLB);
    }

    public static function dvl(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DVL);
    }

    public static function ddb(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DDB);
    }

    public static function dbfc(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DBFC);
    }

    public static function dpb(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DPB);
    }

    public static function diu(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DIU);
    }

    public static function ddl(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DDL);
    }

    public static function dcb(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DCB);
    }

    public static function dvt(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString(TestIds::PLAYER_DVT);
    }
}
