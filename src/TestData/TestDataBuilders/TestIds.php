<?php

namespace App\TestData\TestDataBuilders;

/**
 * Some UUIDs used by various tests.
 *
 * A better place for these would probably be somewhere under the tests
 * directory, but I put them here to make them accessible for the
 * {@see TestDataCommand}.
 */
final class TestIds
{
    public const TEST_TABLE_1 = '089ed959-084f-4471-b191-176584f24311';
    public const TEST_TABLE_2 = '55f6cccf-0f6e-4175-bb71-b5e88753e278';
    public const TEST_TABLE_3 = '93583faf-46c8-4490-9e5c-9f138ba3a13f';
    public const TEST_TABLE_4 = '90abd681-a4b6-414f-98bc-cd77b45fffff';
    public const TEST_TABLE_5 = 'be1cfe86-dec2-4e66-a738-19451a182b3f';
    public const TEST_TABLE_6 = '76d511f0-2240-4e3d-90c9-2c6abc4da28f';
    public const TEST_TABLE_7 = '8803a0ec-efd8-4b17-a13b-2c9c8b5e5b6e';
    public const TEST_TABLE_8 = '9F37CFC7-B3EA-400F-AE01-BDE80ACDFC91';
    public const TEST_TABLE_9 = '9AA3B26F-C9D9-4151-BF96-01A84AC7DEB7';
    public const TEST_TABLE_10 = 'B11890EF-A39D-4822-B423-C6578A3A6832';
    public const TEST_TABLE_11 = '1585C3A2-2D82-4169-9B65-DABF744349AD';
    public const TEST_TABLE_12 = 'A038ECFF-61C6-4E1E-9500-07C51E7EB144';
    public const TEST_TABLE_13 = 'A94AACEE-E959-49B6-84BD-8A31FACBA234';
    public const TEST_TABLE_14 = '29C49458-6E73-4323-95F8-7E8B2AD11010';
    public const TEST_TABLE_15 = 'CFE662CB-544C-4C38-8ADB-0C2944146D03';
    public const TEST_TABLE_16 = 'B1ABFD91-2143-491A-8B80-41C857B88895';
    public const TEST_TABLE_17 = 'A489A432-A898-498A-B1B5-6A3CFE4280A0';
    public const TEST_TABLE_18 = '0CA72D91-4CC6-4188-B90B-9F34B1AD5916';
    public const TEST_TABLE_19 = '6B1F8723-BEBF-4BC1-B79C-24058B0ABF6B';
    public const TEST_TABLE_20 = '6d2fb96a-3833-4fe9-834f-f460c5c9feb9';
    public const TEST_TABLE_21 = '4170acc0-35f8-445b-ab71-fdbfd74ed99b';
    public const TEST_TABLE_22 = '9c860e1a-5962-449f-b40e-e6ce45499a79';
    public const TEST_TABLE_23 = 'db1a561e-6556-44b3-859f-aba16804155a';
    public const TEST_TABLE_24 = '9eb8ce9c-6eb4-4fef-9ddf-49ee909153f8';
    public const TEST_TABLE_25 = '12a0701b-d8b5-41f9-909f-353d7f1f8a7f';
    public const TEST_TABLE_26 = 'cdecb052-8b01-423f-89a4-f5aae1fab866';
    public const TEST_TABLE_27 = 'dd2ef02c-853a-4815-b479-09e43b3dccab';
    public const TEST_TABLE_28 = 'e267dae9-9fa7-49da-b3be-7cc77ef0ba1e';
    public const TEST_TABLE_29 = '0cdcf417-15a2-48e8-a800-d49bb8b07b45';
    public const TEST_TABLE_30 = 'e75a79b4-9a1c-4de6-9d58-5ce83d700c69';
    public const TEST_TABLE_31 = '8eed17eb-fe83-491a-9a15-6ef5dc8175af';

    public const PLAYER_SECRETARIS = '87bdf1ee-5102-4c19-9975-2cb0a6b1d6f8';
    public const PLAYER_PENNINGMEESTER = '5d46004d-eb1b-41a6-af37-9a92c0acde2a';
    public const PLAYER_DTL = '4941cd94-a72e-4593-8f19-98695e0937fe';
    public const PLAYER_DLB = '11502b30-06f3-4f9b-a422-635930e52ac9';
    public const PLAYER_DVL = '72ac9027-02b4-4bf7-a10e-c4658b2c37d8';
    public const PLAYER_DDB = '5fba351e-5703-4d8d-8b0d-7b4d76f07e0f';
    public const PLAYER_DBFC = '536bfc63-7711-4b2d-b326-775ab2813be3';
    public const PLAYER_DPB = 'ebdc0ee4-a880-4920-aa34-b78bf6578291';
    public const PLAYER_DIU = '566ca0fc-7cf9-4605-8e36-417a837e205e';
    public const PLAYER_DDL = '730577e7-b669-4d39-b655-f87593c38b63';
    public const PLAYER_DCB = '9a12cc77-fcbb-452b-9e93-c0b1564615a5';
    public const PLAYER_DVT = 'da1a9d20-8264-4287-bea3-c8104a3f056c';
}
