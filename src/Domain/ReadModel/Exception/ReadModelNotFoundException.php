<?php

namespace App\Domain\ReadModel\Exception;

final class ReadModelNotFoundException extends \Exception
{
}
