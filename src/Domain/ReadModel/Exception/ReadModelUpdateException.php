<?php

namespace App\Domain\ReadModel\Exception;

final class ReadModelUpdateException extends \Exception
{
}
