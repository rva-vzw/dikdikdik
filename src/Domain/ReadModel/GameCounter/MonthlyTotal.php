<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

final class MonthlyTotal
{
    public function __construct(
        private Month $month,
        private int $totalTables,
        private int $totalGames,
    ) {
    }

    public function getMonth(): Month
    {
        return $this->month;
    }

    public function getTotalTables(): int
    {
        return $this->totalTables;
    }

    public function getTotalGames(): int
    {
        return $this->totalGames;
    }
}
