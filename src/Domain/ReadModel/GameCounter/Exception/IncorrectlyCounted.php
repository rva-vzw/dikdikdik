<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter\Exception;

use App\Domain\Table\TableIdentifier;

final class IncorrectlyCounted extends \Exception
{
    public static function create(
        TableIdentifier $tableIdentifier,
        int $gameCount,
        \DateTimeImmutable $latestGameStartedAt
    ): self {
        return new self(
            "Incorrect game count for table {$tableIdentifier->toString()} at {$latestGameStartedAt->format('Y-m-d h:i:s')}: $gameCount."
        );
    }
}
