<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

use App\Domain\Table\TableIdentifier;

interface GameCounterDeleter
{
    public function removeForTable(TableIdentifier $tableIdentifier): void;
}
