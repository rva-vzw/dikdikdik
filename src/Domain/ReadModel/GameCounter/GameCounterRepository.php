<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

use App\Domain\Table\TableIdentifier;

interface GameCounterRepository extends GameCounterDeleter
{
    public function findCounterForTable(TableIdentifier $tableIdentifier): ?GameCounter;

    public function saveGameCounter(GameCounter $counter): void;
}
