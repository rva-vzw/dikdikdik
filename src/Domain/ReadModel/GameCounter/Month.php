<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

final class Month
{
    private function __construct(
        private int $year,
        private int $month
    ) {
    }

    public static function fromYearAndDay(
        int $year,
        int $month
    ): self {
        if (0 != $year && $month >= 1 && $month <= 12) {
            return new self($year, $month);
        }

        throw new \InvalidArgumentException('Invalid year or month');
    }

    public static function fromDateTimeImmutable(\DateTimeImmutable $dateTimeImmutable): self
    {
        $year = (int) $dateTimeImmutable->format('Y');
        $month = (int) $dateTimeImmutable->format('m');

        return new self($year, $month);
    }

    public static function current(): self
    {
        return self::fromDateTimeImmutable(new \DateTimeImmutable());
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return new \DateTimeImmutable("{$this->year}-{$this->month}-01");
    }

    public function next(): self
    {
        return 12 === $this->month
            ? new self($this->year + 1, 1)
            : new self($this->year, $this->month + 1);
    }

    public function __toString(): string
    {
        return "{$this->year}-{$this->month}";
    }
}
