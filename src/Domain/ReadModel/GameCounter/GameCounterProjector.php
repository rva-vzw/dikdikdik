<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Event\Undo\GameUndone;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\EventListener\EventListener;
use RvaVzw\KrakBoem\EventSourcing\Replay\Replayable;

final class GameCounterProjector implements EventListener, Replayable
{
    public function __construct(
        private GameCounterRepository $gameCounterRepository,
    ) {
    }

    public function __invoke(Event $event): void
    {
        if ($event instanceof GamePlayed) {
            $this->increaseCounter($event->getTableIdentifier(), $event->getLoggedAt());
        }
        if ($event instanceof GameUndone) {
            // Time is irrelevant for undo-events (see #226). Keep the counter's time, decrement the count.
            $this->decreaseCounter($event->getTableIdentifier());
        }
    }

    private function increaseCounter(
        TableIdentifier $tableIdentifier,
        \DateTimeImmutable $loggedAt
    ): void {
        $counter = $this->gameCounterRepository->findCounterForTable($tableIdentifier);
        if (null === $counter) {
            $counter = GameCounter::createNew($tableIdentifier, $loggedAt);
        }

        $this->gameCounterRepository->saveGameCounter(
            $counter->incrementedAt($loggedAt)
        );
    }

    private function decreaseCounter(TableIdentifier $tableIdentifier): void
    {
        $counter = $this->gameCounterRepository->findCounterForTable($tableIdentifier);

        if (1 === $counter?->getGameCount()) {
            // If the last game is undone, remove the counter.
            $this->gameCounterRepository->removeForTable($tableIdentifier);

            return;
        }

        if ($counter instanceof GameCounter) {
            $this->gameCounterRepository->saveGameCounter(
                $counter->decremented()
            );
        }
    }
}
