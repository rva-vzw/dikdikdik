<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

use App\Domain\ReadModel\GameCounter\Exception\IncorrectlyCounted;
use App\Domain\Table\TableIdentifier;

final class GameCounter
{
    private function __construct(
        private TableIdentifier $tableIdentifier,
        private \DateTimeImmutable $firstGameLoggedAt,
        private \DateTimeImmutable $latestGameLoggedAt,
        private int $gameCount
    ) {
    }

    public static function createNew(
        TableIdentifier $tableIdentifier,
        \DateTimeImmutable $startedAt
    ): self {
        return new self($tableIdentifier, $startedAt, $startedAt, 0);
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getFirstGameLoggedAt(): \DateTimeImmutable
    {
        return $this->firstGameLoggedAt;
    }

    public function getLatestGameLoggedAt(): \DateTimeImmutable
    {
        return $this->latestGameLoggedAt;
    }

    public function getGameCount(): int
    {
        return $this->gameCount;
    }

    public function incrementedAt(\DateTimeImmutable $dateTime): self
    {
        $result = clone $this;
        $result->latestGameLoggedAt = $dateTime;
        ++$result->gameCount;

        return $result;
    }

    public function withGamesCounted(int $gameCount, \DateTimeImmutable $latestGameLoggedAt): self
    {
        if ($gameCount > 0 && $latestGameLoggedAt >= $this->firstGameLoggedAt) {
            $result = clone $this;
            $result->gameCount = $gameCount;
            $result->latestGameLoggedAt = $latestGameLoggedAt;

            return $result;
        }

        throw IncorrectlyCounted::create($this->tableIdentifier, $gameCount, $latestGameLoggedAt);
    }

    public function decremented(): self
    {
        $result = clone $this;
        --$result->gameCount;

        return $result;
    }
}
