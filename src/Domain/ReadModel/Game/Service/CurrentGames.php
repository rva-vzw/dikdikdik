<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Game\Service;

use App\Domain\ReadModel\TableState\TableStates;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;

final class CurrentGames
{
    public function __construct(
        private readonly TableStates $tableStates,
    ) {
    }

    public function getCurrentGameNumber(TableIdentifier $tableIdentifier): GameNumber
    {
        return $this->tableStates->getTableState($tableIdentifier)->currentGameNumber;
    }
}
