<?php

declare(strict_types=1);

namespace App\Domain\ReadModel;

/**
 * Announces read model updates, to send server side rendered html via mercure to the browser.
 */
interface ReadModelUpdateNotifier
{
    public function notify(ReadModelUpdateNotification $notification): void;
}
