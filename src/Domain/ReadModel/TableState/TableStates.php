<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\Table\Exception\GameNotFound;
use App\Domain\Table\FinalGames;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;

final readonly class TableStates implements FinalGames
{
    public function __construct(
        private TableStateRepository $tableStateRepository,
    ) {
    }

    public function getTableState(TableIdentifier $tableIdentifier): TableState
    {
        return $this->tableStateRepository->getTableState($tableIdentifier);
    }

    public function isEnding(TableIdentifier $tableIdentifier): bool
    {
        $tableState = $this->tableStateRepository->getTableState($tableIdentifier);

        return $tableState->isFinalRound();
    }

    public function getFinalGame(TableIdentifier $tableIdentifier): GameNumber
    {
        $tableState = $this->tableStateRepository->getTableState($tableIdentifier);
        $finalGameNumber = $tableState->finalGameNumber;

        if ($finalGameNumber instanceof GameNumber) {
            return $finalGameNumber;
        }

        throw GameNotFound::finalRoundNotAnnounced($tableIdentifier);
    }
}
