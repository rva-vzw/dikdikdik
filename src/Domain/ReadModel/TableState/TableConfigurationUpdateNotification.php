<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\ReadModel\ReadModelUpdateNotification;
use App\Domain\Table\TableIdentifier;

/**
 * Notification for changes in _table configuration_ (players or game rules).
 */
final class TableConfigurationUpdateNotification implements ReadModelUpdateNotification
{
    public function __construct(private TableIdentifier $tableIdentifier)
    {
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
