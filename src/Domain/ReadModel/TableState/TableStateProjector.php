<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\Event\FinalRoundAnnounced;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Event\PassedAround;
use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\Event\PlayerLeft;
use App\Domain\Table\Event\PlayersReordered;
use App\Domain\Table\Event\TableCleared;
use App\Domain\Table\Event\TableConfigured;
use App\Domain\Table\Event\Undo\FinalRoundAnnouncementUndone;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

readonly final class TableStateProjector extends AbstractProjector
{
    public function __construct(
        private readonly TableStateRepository $tableStateRepository,
    ) {
    }

    public function applyTableCleared(TableCleared $event): void
    {
        $this->tableStateRepository->deleteTableState($event->tableIdentifier);
        // Don't publish this, see #285
    }

    public function applyPlayerJoined(PlayerJoined $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->tableIdentifier);
        $tableState = $tableState->withAdditionalPlayer();
        $this->tableStateRepository->saveTableState($tableState);
    }

    public function applyPlayerLeft(PlayerLeft $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->tableIdentifier);
        $tableState = $tableState->withPlayerLeft($event->playerIdentifier);
        $this->tableStateRepository->saveTableState($tableState);
    }

    public function applyDealerAnnounced(DealerAnnounced $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->tableIdentifier);
        $tableState = $tableState->forGame($event->gameNumber)
            ->withDealer($event->playerIdentifier);
        $this->tableStateRepository->saveTableState($tableState);
    }

    public function applyTableConfigured(TableConfigured $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->tableIdentifier);
        $tableState = $tableState->withConventions($event->ruleSetName);

        $this->tableStateRepository->saveTableState($tableState);

        // Don't publish table state when table configured; I think it makes the tests
        // unstable.
    }

    public function applyFinalRoundAnnounced(FinalRoundAnnounced $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->tableIdentifier);
        $tableState = $tableState->withFinalGame($event->end);
        $this->tableStateRepository->saveTableState($tableState);
    }

    public function __invoke(Event $event): void
    {
        if ($event instanceof GamePlayed || $event instanceof PassedAround) {
            $tableState = $this->tableStateRepository->getTableState($event->getTableIdentifier());

            if ($tableState->dealerChangeable || $tableState->canChangeRuleSet) {
                $this->tableStateRepository->saveTableState(
                    $tableState->withDealerLocked()->agreedOnRuleSet()
                );
            }
        }
        parent::__invoke($event);
    }

    public function applyFinalRoundAnnouncementUndone(FinalRoundAnnouncementUndone $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->tableIdentifier);
        $tableState = $tableState->withNoFinalGame();
        $this->tableStateRepository->saveTableState($tableState);
    }

    public function applyPlayersReordered(PlayersReordered $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->tableIdentifier);

        if ($tableState->hasEnoughPlayers() && !$tableState->dealerChangeable) {
            $tableState = $tableState->withDealerChangeable();
            $this->tableStateRepository->saveTableState(
                $tableState,
            );
        }
    }
}
