<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\GameSpecification\Alone5Specification;
use App\Domain\Table\Loggable\GameSpecification\GameSpecifications;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Rules\Whist;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use Spatie\Cloneable\Cloneable;

/**
 * Information for the 'table configuration' forms.
 */
final readonly class TableState
{
    use Cloneable;

    public function __construct(
        public TableIdentifier $tableIdentifier,
        public ?PlayerIdentifier $dealer,
        public GameNumber $currentGameNumber,
        public bool $canChangeRuleSet,
        public bool $dealerChangeable,
        public int $numberOfPresentPlayers,
        public ?GameNumber $finalGameNumber,
        public string $conventionsName,
    ) {
    }

    public function forGame(GameNumber $gameNumber): self
    {
        return $this->with(currentGameNumber: $gameNumber);
    }

    public function withDealer(PlayerIdentifier $playerIdentifier): self
    {
        return $this->with(dealer: $playerIdentifier);
    }

    public function withDealerLocked(): self
    {
        if ($this->dealerChangeable) {
            return $this->with(dealerChangeable: false);
        }

        return $this;
    }

    public function agreedOnRuleSet(): self
    {
        if ($this->canChangeRuleSet) {
            return $this->with(canChangeRuleSet: false);
        }

        return $this;
    }

    public function withPlayerLeft(PlayerIdentifier $playerIdentifier): self
    {
        $clone = $this->with(numberOfPresentPlayers: $this->numberOfPresentPlayers - 1);

        /** @noinspection PhpNonStrictObjectEqualityInspection */
        if ($clone->dealer == $playerIdentifier) {
            $clone = $clone->with(dealer: $playerIdentifier);
        }
        if ($clone->numberOfPresentPlayers < Whist::PLAYERS_NEEDED_TO_PLAY) {
            $clone = $clone->with(
                dealerChangeable: false,
                dealer: null,
            );
        } else {
            // Changing the dealer when new players were added/removed should always
            // be possible (if there's enough players)
            $clone = $clone->with(dealerChangeable: true);
        }

        return $clone;
    }

    public function hasFreePlaces(): bool
    {
        return $this->numberOfPresentPlayers < Seat::NUMBER_OF_SEATS && !$this->isFinalRound();
    }

    public function withAdditionalPlayer(): self
    {
        return $this->with(
            numberOfPresentPlayers: $this->numberOfPresentPlayers + 1,
            dealerChangeable: ($this->numberOfPresentPlayers + 1) >= Whist::PLAYERS_NEEDED_TO_PLAY,
        );
    }

    public function isFinalRound(): bool
    {
        return $this->finalGameNumber instanceof GameNumber;
    }

    public function canPlay(): bool
    {
        return $this->hasEnoughPlayers()
            && $this->dealer instanceof PlayerIdentifier;
    }

    public function withDealerChangeable(): self
    {
        return $this->with(dealerChangeable: true);
    }

    public function hasEnoughPlayers(): bool
    {
        return $this->numberOfPresentPlayers >= Whist::PLAYERS_NEEDED_TO_PLAY;
    }

    public function withAllowedGames(GameSpecifications $allowedGames): self
    {
        return $this->with(tricksRequiredForPlayingAlone: $allowedGames->hasBySpecificationClass(Alone5Specification::class) ? 5 : 6);
    }

    public function withFinalGame(GameNumber $gameNumber): self
    {
        return $this->with(
            finalGameNumber: $gameNumber,
        );
    }

    public function withNoFinalGame(): self
    {
        return $this->with(
            finalGameNumber: null,
        );
    }

    public function withConventions(string $conventionsName): self
    {
        return $this->with(conventionsName: $conventionsName);
    }
}
