<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\Table\TableIdentifier;

interface TableStateRepository
{
    public function saveTableState(TableState $tableState): void;

    public function deleteTableState(TableIdentifier $tableIdentifier): void;

    public function getTableState(TableIdentifier $tableIdentifier): TableState;
}
