<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Player\Exception;

use App\Domain\ReadModel\Player\Model\Player;
use Exception;

final class PlayerNotPresent extends Exception
{
    public static function byPlayer(Player $player): self
    {
        return new self("Player {$player->getName()} ({$player->getPlayerIdentifier()}) is not present at the table");
    }
}
