<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Player\Service;

use App\Domain\ReadModel\ScoreLog\Service\ScoreLogs;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\Player\SeatedPlayers;

final class PlayersGroupsReadModel implements CurrentGamePlayers, SeatedPlayersByScoreSheet
{
    public function __construct(
        private ScoreLogs $scoreLogs
    ) {
    }

    public function findPlayersInvolved(ScoreSheetIdentifier $scoreSheetIdentifier): ?PlayersInvolved
    {
        return $this->scoreLogs->getScoreLog($scoreSheetIdentifier)
            ->getPlayersInvolved();
    }

    public function getSeatedPlayers(ScoreSheetIdentifier $scoreSheetIdentifier): SeatedPlayers
    {
        return $this->scoreLogs->getScoreLog($scoreSheetIdentifier)
            ->getSeatedPlayers();
    }
}
