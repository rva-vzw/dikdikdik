<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Player\Service;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Player\SeatedPlayers;

/**
 * Returns player-IDs by sheet number for a score sheet.
 */
interface SeatedPlayersByScoreSheet
{
    public function getSeatedPlayers(ScoreSheetIdentifier $scoreSheetIdentifier): SeatedPlayers;
}
