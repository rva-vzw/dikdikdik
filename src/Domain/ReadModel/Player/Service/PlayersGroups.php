<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Player\Service;

use App\Domain\ReadModel\Exception\ReadModelNotFoundException;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;

interface PlayersGroups
{
    /**
     * Returns the details of all known players for a given score sheet.
     *
     * This includes the last known seat!
     *
     * @throws ReadModelNotFoundException
     */
    public function getPlayersGroup(ScoreSheetIdentifier $scoreSheetIdentifier): PlayersGroup;
}
