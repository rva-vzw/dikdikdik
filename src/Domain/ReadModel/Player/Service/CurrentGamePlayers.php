<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Player\Service;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Player\PlayersInvolved;

/**
 * Returns for the current score sheet the player-ID's that can announce a game, and the dealer ID.
 */
interface CurrentGamePlayers
{
    public function findPlayersInvolved(ScoreSheetIdentifier $scoreSheetIdentifier): ?PlayersInvolved;
}
