<?php

namespace App\Domain\ReadModel\Player\Model;

use App\Domain\ReadModel\Player\Exception\PlayerNotPresent;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Seat;

final class Player
{
    private function __construct(
        private string $name,
        private PlayerStatus $status,
        private PlayerIdentifier $playerIdentifier,
        private Seat $lastKnownSeat,
        private int $numberOfStars,
        private bool $dealer
    ) {
    }

    public static function createGone(PlayerIdentifier $playerIdentifier, Seat $seat, string $playerName): self
    {
        return new self($playerName, PlayerStatus::GONE, $playerIdentifier, $seat, 0, false);
    }

    public static function createNew(PlayerIdentifier $playerIdentifier, Seat $seat, string $playerName): self
    {
        return new self($playerName, PlayerStatus::NEW, $playerIdentifier, $seat, 0, false);
    }

    public static function createActive(PlayerIdentifier $playerIdentifier, Seat $seat, string $playerName): self
    {
        return new self($playerName, PlayerStatus::ACTIVE, $playerIdentifier, $seat, 0, false);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStatus(): PlayerStatus
    {
        return $this->status;
    }

    public function gone(): self
    {
        $result = clone $this;
        $result->status = PlayerStatus::GONE;
        $result->dealer = false;

        return $result;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function asActive(): self
    {
        $result = clone $this;
        $result->status = PlayerStatus::ACTIVE;

        return $result;
    }

    public function getLastKnownSeat(): Seat
    {
        return $this->lastKnownSeat;
    }

    public function seatedAt(Seat $seat): self
    {
        $result = clone $this;
        $result->lastKnownSeat = $seat;
        if (PlayerStatus::GONE === $result->status) {
            $result->status = PlayerStatus::ACTIVE;
        }

        return $result;
    }

    public function withStars(int $numberOfStars): self
    {
        $result = clone $this;
        $result->numberOfStars = $numberOfStars;

        return $result;
    }

    public function getNumberOfStars(): int
    {
        return $this->numberOfStars;
    }

    public function isDealer(): bool
    {
        return $this->dealer;
    }

    public function dealing(): self
    {
        if ($this->dealer) {
            return $this;
        }

        $result = clone $this;
        $result->dealer = true;

        return $result;
    }

    public function notDealing(): self
    {
        if ($this->dealer) {
            $result = clone $this;
            $result->dealer = false;

            return $result;
        }

        return $this;
    }

    public function isGone(): bool
    {
        return PlayerStatus::GONE === $this->status;
    }

    public function getSeat(): Seat
    {
        if ($this->isPresent()) {
            return $this->lastKnownSeat;
        }

        throw PlayerNotPresent::byPlayer($this);
    }

    public function isPresent(): bool
    {
        return $this->status->isPresent();
    }

    public function named(string $playerName): self
    {
        $clone = clone $this;
        $clone->name = $playerName;

        return $clone;
    }
}
