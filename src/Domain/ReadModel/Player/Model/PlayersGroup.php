<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Player\Model;

use App\Domain\Table\Exception\UnknownPlayer;
use App\Domain\Table\Player\NamedPlayer;
use App\Domain\Table\Player\NamedPlayers;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Rules\Whist;
use App\Domain\Table\Seat;
use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * All players (active and inactive) by a table.
 *
 * PlayersGroup isn't aware of the pissers. If you need that information,
 * use {@see PresentPlayers::fromPlayersGroup()}.
 *
 * @implements IteratorAggregate<int,Player>
 */
final class PlayersGroup implements IteratorAggregate
{
    /**
     * @param array<string, Player> $players associative array mapping player id to details
     */
    private function __construct(private array $players)
    {
    }

    /**
     * @param Player[] $players
     */
    public static function fromArray(array $players): self
    {
        $group = self::none();

        $dealer = null;

        /** @var Player $player */
        foreach ($players as $player) {
            if ($player->isPresent()) {
                // Assure that a seat can only be taken by a single present player
                $group = $group->withNoPlayerOnSeat($player->getSeat());
            }
            $group->players[$player->getPlayerIdentifier()->toString()] = $player;
            if ($player->isDealer()) {
                $dealer = $player->getPlayerIdentifier();
            }
        }

        // Ensure at most 1 dealer

        return $dealer instanceof PlayerIdentifier
            ? $group->withDealer($dealer)
            : $group;
    }

    public static function none(): self
    {
        return new self([]);
    }

    public function getPlayer(PlayerIdentifier $playerIdentifier): Player
    {
        if ($this->has($playerIdentifier)) {
            return $this->players[$playerIdentifier->toString()];
        }

        throw UnknownPlayer::withIdentifier($playerIdentifier);
    }

    public function withPlayer(PlayerIdentifier $playerIdentifier, Seat $seat, string $playerName): self
    {
        $clone = $this->withNoPlayerOnSeat($seat);

        $player = $this->has($playerIdentifier)
            ? $this->getPlayer($playerIdentifier)
                ->seatedAt($seat)
                ->named($playerName)
                ->asActive()
            : Player::createNew(
                $playerIdentifier,
                $seat,
                $playerName,
            );

        $clone->players[$player->getPlayerIdentifier()->toString()] = $player;

        return $clone;
    }

    /**
     * @return Traversable<int, Player>
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator(array_values($this->players));
    }

    public function has(PlayerIdentifier $playerIdentifier): bool
    {
        return array_key_exists($playerIdentifier->toString(), $this->players);
    }

    public function withPlayerStars(PlayerIdentifier $playerIdentifier, int $numberOfStars): self
    {
        $playerId = $playerIdentifier->toString();

        $result = clone $this;
        $result->players[$playerId] = $this->players[$playerId]->withStars($numberOfStars);

        return $result;
    }

    /**
     * Returns only the players that are present.
     *
     * This is used to group the present players and gone players
     * in the score sheet.
     *
     * @return Traversable<Player>
     */
    public function getPresent(): Traversable
    {
        $activePlayers = array_filter(
            $this->players,
            fn (Player $player) => $player->isPresent()
        );
        uasort(
            $activePlayers,
            fn (Player $p1, Player $p2) => $p1->getLastKnownSeat() <=> $p2->getLastKnownSeat()
        );

        return new ArrayIterator($activePlayers);
    }

    /**
     * @return Traversable<int, Player>
     */
    public function getInactive(): Traversable
    {
        return new ArrayIterator(
            array_filter(
                array_values($this->players),
                fn (Player $player) => !$player->isPresent()
            )
        );
    }

    public function getSeatedPlayers(): SeatedPlayers
    {
        $result = SeatedPlayers::none();
        /** @var Player $playerDetails */
        foreach ($this->players as $playerDetails) {
            $seat = $playerDetails->getLastKnownSeat();

            if ($playerDetails->getStatus()->isPresent()) {
                $result = $result->withSeatedPlayer(
                    $seat,
                    $playerDetails->getPlayerIdentifier()
                );
            }
        }

        return $result;
    }

    public function getPlayerIdentifiers(): PlayerIdentifiers
    {
        return PlayerIdentifiers::fromArray(
            array_map(
                fn (Player $details) => $details->getPlayerIdentifier(),
                $this->players
            )
        );
    }

    public function getDealerIdentifier(): ?PlayerIdentifier
    {
        foreach ($this->players as $player) {
            if ($player->isDealer()) {
                return $player->getPlayerIdentifier();
            }
        }

        return null;
    }

    public function withDealer(PlayerIdentifier $dealerIdentifier): self
    {
        return new self(
            array_map(
                fn (Player $player) => $player->getPlayerIdentifier() == $dealerIdentifier
                    ? $player->dealing()
                    : $player->notDealing(),
                $this->players
            )
        );
    }

    public function isEmpty(): bool
    {
        return empty($this->players);
    }

    public function withoutPlayer(PlayerIdentifier $playerIdentifier): self
    {
        if (!$this->has($playerIdentifier)) {
            return $this;
        }

        $player = $this->getPlayer($playerIdentifier);
        $clone = clone $this;

        if (PlayerStatus::NEW === $player->getStatus()) {
            unset($clone->players[$playerIdentifier->toString()]);

            return $clone;
        }

        $clone->players[$playerIdentifier->toString()] = $player->gone();

        return $clone;
    }

    public function withPlayerOnSeat(PlayerIdentifier $playerIdentifier, Seat $seat): self
    {
        $player = $this->getPlayer($playerIdentifier);

        if (!$player->isPresent()) {
            $player = $player->asActive();
        }

        $clone = clone $this;
        $clone->players[$playerIdentifier->toString()] = $player->seatedAt($seat);

        return $clone;
    }

    public function withNoPlayerOnSeat(Seat $seat): self
    {
        $clone = clone $this;

        $clone->players = array_map(
            fn (Player $player) => $player->isPresent() && $player->getSeat() == $seat
                ? $player->gone() : $player,
            $clone->players,
        );

        return $clone;
    }

    public function getFreeSeat(): Seat
    {
        return $this->getSeatedPlayers()->getFreeSeat();
    }

    public function hasFreeSeat(): bool
    {
        return $this->getSeatedPlayers()->hasFreeSeat();
    }

    public function isNotEmpty(): bool
    {
        return !empty($this->players);
    }

    public function getNamedPlayers(): NamedPlayers
    {
        return NamedPlayers::fromArray(
            array_map(
                fn (Player $p) => new NamedPlayer($p->getPlayerIdentifier(), $p->getName()),
                $this->players,
            ),
        );
    }

    public function reordered(SeatedPlayers $newPlayerOrder): self
    {
        $group = clone $this;
        $dealer = $this->getDealerIdentifier();

        foreach ($newPlayerOrder as $seatNumber => $playerIdentifier) {
            $group = $group->withPlayerOnSeat($playerIdentifier, Seat::fromInteger($seatNumber));
        }

        // Make sure the dealer didn't get lost when reordering players
        return $dealer instanceof PlayerIdentifier
            ? $group->withDealer($dealer)
            : $group;
    }

    public function withPlayerActivated(PlayerIdentifier $playerIdentifier): self
    {
        $clone = clone $this;

        $clone->players[$playerIdentifier->toString()]
             = $clone->players[$playerIdentifier->toString()]->asActive();

        return $clone;
    }

    public function hasEnoughPlayers(): bool
    {
        return count(
            array_filter(
                $this->players,
                fn (Player $player) => $player->isPresent(),
            )
        ) >= Whist::PLAYERS_NEEDED_TO_PLAY;
    }

    public function isTableFull(): bool
    {
        return count(
                array_filter(
                    $this->players,
                    fn (Player $player) => $player->isPresent(),
                )
            ) >= Seat::NUMBER_OF_SEATS;
    }
}
