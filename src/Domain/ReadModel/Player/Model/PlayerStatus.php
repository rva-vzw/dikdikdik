<?php

namespace App\Domain\ReadModel\Player\Model;

enum PlayerStatus: string
{
    case NEW = 'new';
    case ACTIVE = 'active';
    case GONE = 'inactive';
    public function isPresent(): bool
    {
        return match ($this) {
            self::NEW, self::ACTIVE => true,
            self::GONE => false,
        };
    }
}
