<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Player\Model;

use App\Domain\Table\Player\NamedPlayer;
use App\Domain\Table\Player\PlayerIdentifier;

final class PresentPlayer
{
    public function __construct(
        private PlayerIdentifier $playerIdentifier,
        private string $playerName,
        private bool $playing,
    ) {
    }

    public static function createInactive(PlayerIdentifier $playerIdentifier, string $playerName): self
    {
        return new self($playerIdentifier, $playerName, false);
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getPlayerName(): string
    {
        return $this->playerName;
    }

    public function isPlaying(): bool
    {
        return $this->playing;
    }

    public function asNamedPlayer(): NamedPlayer
    {
        return new NamedPlayer($this->playerIdentifier, $this->playerName);
    }

    public function named(string $name): self
    {
        $result = clone $this;
        $result->playerName = $name;

        return $result;
    }

    public function inactivated(): self
    {
        $result = clone $this;
        $result->playing = false;

        return $result;
    }

    public function playing(): self
    {
        $result = clone $this;
        $result->playing = true;

        return $result;
    }
}
