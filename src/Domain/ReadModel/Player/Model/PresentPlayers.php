<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Player\Model;

use App\Domain\Table\Exception\UnknownPlayer;
use App\Domain\Table\Player\NamedPlayer;
use App\Domain\Table\Player\NamedPlayers;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Seat;
use Traversable;

/**
 * The present players are the players that are still on the table.
 *
 * They can be active or inactive (if more than 4 players are on the table).
 * But they can't be gone.
 *
 * @implements \IteratorAggregate<int, PresentPlayer>
 */
final class PresentPlayers implements \IteratorAggregate
{
    public function __construct(
        /** @var array<int, PresentPlayer> */
        private array $players,
    ) {
    }

    public static function fromPlayersGroup(PlayersGroup $playersGroup): self
    {
        $presentPlayers = self::none();

        foreach ($playersGroup->getPresent() as $player) {
            $presentPlayers = $presentPlayers->withInactivePlayer(
                $player->getSeat(),
                new NamedPlayer($player->getPlayerIdentifier(), $player->getName()),
            );
        }

        $dealer = $playersGroup->getDealerIdentifier();

        return $dealer instanceof PlayerIdentifier
            ? $presentPlayers->withDealer($dealer)
            : $presentPlayers;
    }

    public static function none(): self
    {
        return new self([]);
    }

    public function getPlayingPlayers(): NamedPlayers
    {
        $playingPlayers = NamedPlayers::none();

        /** @var PresentPlayer $player */
        foreach ($this->players as $player) {
            if ($player->isPlaying()) {
                $playingPlayers = $playingPlayers->with($player->asNamedPlayer());
            }
        }

        return $playingPlayers;
    }

    public function hasFourActive(): bool
    {
        return 4 === count(
            array_filter(
                $this->players,
                fn (PresentPlayer $player) => $player->isPlaying(),
            ),
        );
    }

    public function withInactivePlayer(Seat $seat, NamedPlayer $namedPlayer): self
    {
        $result = clone $this;
        $result->players[$seat->toInteger()] = new PresentPlayer(
            $namedPlayer->getPlayerIdentifier(),
            $namedPlayer->getPlayerName(),
            false,
        );

        return $result;
    }

    public function withoutPlayer(PlayerIdentifier $playerIdentifier): self
    {
        $result = new self(
            array_filter(
                $this->players,
                fn (PresentPlayer $player) => $player->getPlayerIdentifier() != $playerIdentifier,
            ),
        );

        return $result->hasFourActive()
            ? $result
            : $result->allInactive();
    }

    public function allInactive(): self
    {
        return new self(
            array_map(
                fn (PresentPlayer $p) => $p->inactivated(),
                $this->players,
            ),
        );
    }

    public function withDealer(PlayerIdentifier $dealerIdentifier): self
    {
        $playerCount = count($this->players);

        if ($playerCount < 4) {
            return $this;
        }

        $result = clone $this;

        $result->players = array_map(
            fn (PresentPlayer $p) => $p->playing(),
            $result->players,
        );

        if (4 === $playerCount) {
            return $result;
        }

        $dealerSeat = $this->getPlayerSeat($dealerIdentifier);
        $result->players[$dealerSeat->toInteger()] = $result->players[$dealerSeat->toInteger()]->inactivated();

        if (5 === $playerCount) {
            return $result;
        }

        // 6 players

        $result->players[$dealerSeat->getOpposite()->toInteger()]
            = $result->players[$dealerSeat->getOpposite()->toInteger()]->inactivated();

        return $result;
    }

    private function getPlayerSeat(PlayerIdentifier $playerIdentifier): Seat
    {
        foreach ($this->players as $seatNr => $player) {
            if ($player->getPlayerIdentifier() == $playerIdentifier) {
                return Seat::fromInteger($seatNr);
            }
        }

        throw UnknownPlayer::withIdentifier($playerIdentifier);
    }

    /**
     * @return \ArrayIterator<int,PresentPlayer>
     */
    public function getIterator(): Traversable
    {
        return new \ArrayIterator($this->players);
    }

    public function getByPlayerIdentifier(PlayerIdentifier $playerIdentifier): PresentPlayer
    {
        foreach ($this->players as $player) {
            if ($player->getPlayerIdentifier() == $playerIdentifier) {
                return $player;
            }
        }

        throw UnknownPlayer::withIdentifier($playerIdentifier);
    }
}
