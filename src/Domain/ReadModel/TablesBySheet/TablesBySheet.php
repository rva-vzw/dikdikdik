<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TablesBySheet;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\TableIdentifier;

interface TablesBySheet
{
    public function getTableByScoreSheet(ScoreSheetIdentifier $scoreSheetIdentifier): TableIdentifier;
}
