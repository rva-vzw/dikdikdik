<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Model;

use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Seat;

final class CurrentGameAndPlayers
{
    public function __construct(
        public readonly GameNumber $currentGameNumber,
        public readonly PlayersGroup $playersGroup,
    ) {
    }

    public static function forNewTable(): self
    {
        return new self(GameNumber::first(), PlayersGroup::none());
    }

    public function withPlayer(PlayerIdentifier $playerIdentifier, string $playerName, Seat $seat): self
    {
        return new self(
            $this->currentGameNumber,
            $this->playersGroup->withPlayer($playerIdentifier, $seat, $playerName),
        );
    }

    public function withoutPlayer(PlayerIdentifier $playerIdentifier): self
    {
        return new self(
            $this->currentGameNumber,
            $this->playersGroup->withoutPlayer($playerIdentifier),
        );
    }

    public function withDealer(PlayerIdentifier $playerIdentifier): self
    {
        return new self(
            $this->currentGameNumber,
            $this->playersGroup->withDealer($playerIdentifier),
        );
    }

    public function withPlayersReordered(SeatedPlayers $newPlayerOrder): self
    {
        return new self(
            $this->currentGameNumber,
            $this->playersGroup->reordered($newPlayerOrder),
        );
    }

    public function forGame(GameNumber $gameNumber): self
    {
        return new self(
            $gameNumber,
            $this->playersGroup,
        );
    }

    public function withPlayerStarred(PlayerIdentifier $playerIdentifier, int $numberOfStars): self
    {
        return new self(
            $this->currentGameNumber,
            $this->playersGroup->withPlayerStars($playerIdentifier, $numberOfStars),
        );
    }

    public function getDealerIdentifier(): ?PlayerIdentifier
    {
        return $this->playersGroup->getDealerIdentifier();
    }
}
