<?php

namespace App\Domain\ReadModel\ScoreLog\Model;

use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;

interface ScoreLogEntry
{
    public function getNote(): string;

    public function getDealer(): ?PlayerIdentifier;

    public function getScoreFactor(): ScoreFactor;

    public function getGameNumber(): GameNumber;

    public function getScoreLine(): ScoreLine;

    public function getScoreLineNumber(): ScoreLineNumber;

    public function getScoreSheetIdentifier(): ScoreSheetIdentifier;

    public function hasScores(): bool;
}
