<?php

namespace App\Domain\ReadModel\ScoreLog\Model;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Rules\Whist;
use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * @implements IteratorAggregate<int, ScoreLogEntry>
 */
final class ScoreLog implements IteratorAggregate
{
    /**
     * @param ScoreLogEntry[] $logEntries
     */
    private function __construct(
        private array $logEntries,
        private GameNumber $currentGameNumber,
        private PlayersGroup $playersGroup,
        private ScoreSheetIdentifier $scoreSheetIdentifier,
        private ?PlayerIdentifier $currentDealer,
    ) {
    }

    /**
     * @param Traversable<ScoreLogEntry> $scoreLogEntries
     */
    public static function fromEntries(
        Traversable $scoreLogEntries,
        GameNumber $currentGameNumber,
        PlayersGroup $playersGroup,
        ScoreSheetIdentifier $scoreSheetIdentifier,
    ): self {
        $array = [];

        /** @var ScoreLogEntry $scoreLogEntry */
        foreach ($scoreLogEntries as $scoreLogEntry) {
            // convert to SimpleScoreLogEntries, so that I can use the normalizer.
            $array[$scoreLogEntry->getScoreLineNumber()->toInteger()] = SimpleScoreLogEntry::fromLogEntry($scoreLogEntry);
        }

        return new self($array, $currentGameNumber, $playersGroup, $scoreSheetIdentifier, $playersGroup->getDealerIdentifier());
    }

    public static function forPlayers(PlayersGroup $playersGroup, ScoreSheetIdentifier $scoreSheetIdentifier): self
    {
        return new self([], GameNumber::first(), $playersGroup, $scoreSheetIdentifier, null);
    }

    public function getCurrentGameNumber(): GameNumber
    {
        return $this->currentGameNumber;
    }

    /**
     * @return Traversable<int, ScoreLogEntry>
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->logEntries);
    }

    public function getCurrentDealer(): ?PlayerIdentifier
    {
        return $this->currentDealer;
    }

    public function getNumberOfEntries(): int
    {
        return count($this->logEntries);
    }

    public function getPlayersGroup(): PlayersGroup
    {
        return $this->playersGroup;
    }

    public function getPlayer(PlayerIdentifier $playerIdentifier): Player
    {
        return $this->playersGroup->getPlayer($playerIdentifier);
    }

    public function getSeatedPlayers(): SeatedPlayers
    {
        return $this->playersGroup->getSeatedPlayers();
    }

    public function getPlayersInvolved(): ?PlayersInvolved
    {
        $dealerIdentifier = $this->playersGroup->getDealerIdentifier();
        $seatedPlayers = $this->playersGroup->getSeatedPlayers();

        if ($seatedPlayers->getCount() < Whist::PLAYERS_NEEDED_TO_PLAY) {
            return null;
        }

        if ($dealerIdentifier instanceof PlayerIdentifier) {
            return PlayersInvolved::from(
                $seatedPlayers,
                $dealerIdentifier,
            );
        }

        return null;
    }

    public function getScoreSheetIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }

    public function getLastLoggedLine(): ScoreLine
    {
        /** @var ScoreLogEntry $entry */
        foreach (array_reverse($this->logEntries, true) as $lineNumber => $entry) {
            if ($entry->getScoreLine() == ScoreLine::empty(ScoreLineNumber::fromInteger($lineNumber))) {
                continue;
            }

            return $entry->getScoreLine();
        }

        return ScoreLine::empty(ScoreLineNumber::first());
    }

    public function hasEntries(): bool
    {
        return !empty($this->logEntries);
    }
}
