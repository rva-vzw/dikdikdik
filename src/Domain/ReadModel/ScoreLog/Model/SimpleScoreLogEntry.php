<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Model;

use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;

final class SimpleScoreLogEntry implements ScoreLogEntry
{
    public function __construct(
        private ?PlayerIdentifier $dealer,
        private ScoreFactor $scoreFactor,
        private readonly GameNumber $gameNumber,
        private ScoreLine $scoreLine,
        private readonly ScoreSheetIdentifier $scoreSheetIdentifier,
    ) {
    }

    public static function create(
        GameNumber $gameNumber,
        ScoreLineNumber $scoreLineNumber,
        ScoreSheetIdentifier $scoreSheetIdentifier
    ): self {
        return new self(
            null,
            ScoreFactor::single(),
            $gameNumber,
            ScoreLine::empty($scoreLineNumber),
            $scoreSheetIdentifier,
        );
    }

    public static function fromLogEntry(ScoreLogEntry $entry): self
    {
        return new self(
            $entry->getDealer(),
            $entry->getScoreFactor(),
            $entry->getGameNumber(),
            $entry->getScoreLine(),
            $entry->getScoreSheetIdentifier(),
        );
    }

    public function getNote(): string
    {
        return $this->scoreLine->getNote();
    }

    public function getDealer(): ?PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getScoreFactor(): ScoreFactor
    {
        return $this->scoreFactor;
    }

    public function getGameNumber(): GameNumber
    {
        return $this->gameNumber;
    }

    public function getScoreLine(): ScoreLine
    {
        return $this->scoreLine;
    }

    public function getScoreLineNumber(): ScoreLineNumber
    {
        return $this->scoreLine->getScoreLineNumber();
    }

    public function withDealer(PlayerIdentifier $dealer): self
    {
        $result = clone $this;
        $result->dealer = $dealer;

        return $result;
    }

    public function getScoreSheetIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }

    public function withScoreLine(ScoreLine $scoreLine): self
    {
        $result = clone $this;
        $result->scoreLine = $scoreLine;

        return $result;
    }

    public function withScoreFactor(ScoreFactor $scoreFactor): self
    {
        $result = clone $this;
        $result->scoreFactor = $scoreFactor;

        return $result;
    }

    public function shiftedDown(): self
    {
        $result = clone $this;
        $result->scoreLine = $result->scoreLine->shiftedDown();

        return $result;
    }

    public function withGameNumber(GameNumber $gameNumber): self
    {
        return new self(
            $this->dealer,
            $this->scoreFactor,
            $gameNumber,
            $this->scoreLine,
            $this->scoreSheetIdentifier
        );
    }

    public function hasScores(): bool
    {
        return $this->scoreLine->hasFrancs();
    }
}
