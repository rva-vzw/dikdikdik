<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ReadModel\ScoreLog\Model\ScoreLogEntry;
use App\Domain\ReadModel\ScoreLog\Model\SimpleScoreLogEntry;
use App\Domain\Table\Player\PlayerIdentifier;
use Webmozart\Assert\Assert;

final class ScoreLogCorrector
{
    public function __construct(
        private readonly LogEntryRepository $logEntryRepository,
    ) {
    }

    public function insertCorrectionEntry(ScoreLogEntry $correctionEntry): void
    {
        $existingByGameNumber = $this->logEntryRepository->getOrCreateEntryForGame(
            $correctionEntry->getScoreSheetIdentifier(),
            $correctionEntry->getGameNumber(),
        );

        $existingDealer = $existingByGameNumber->getDealer();

        Assert::isInstanceOf($existingDealer, PlayerIdentifier::class);
        $correctionEntry = SimpleScoreLogEntry::fromLogEntry($correctionEntry)->withDealer($existingDealer);

        $this->insertEntryByLineNumber($correctionEntry);
    }

    public function insertUndoEntry(ScoreLogEntry $undoEntry): void
    {
        $existingByGameNumber = $this->logEntryRepository->getOrCreateEntryForGame(
            $undoEntry->getScoreSheetIdentifier(),
            $undoEntry->getGameNumber(),
        );

        $existingDealer = $existingByGameNumber->getDealer();

        Assert::isInstanceOf($existingDealer, PlayerIdentifier::class);
        $undoEntry = SimpleScoreLogEntry::fromLogEntry($undoEntry)->withDealer($existingDealer);

        $this->insertEntryByLineNumber($undoEntry);

        // In case of undo, we need to insert an extra line, a place to log the
        // new score for the undone game.

        $originalScoreFactorEntry = SimpleScoreLogEntry::create(
            $undoEntry->getGameNumber(),
            $undoEntry->getScoreLineNumber()->getNext(),
            $undoEntry->getScoreSheetIdentifier(),
        )->withScoreFactor($existingByGameNumber->getScoreFactor());

        $existingDealer = $existingByGameNumber->getDealer();
        if ($existingDealer instanceof PlayerIdentifier) {
            $originalScoreFactorEntry = $originalScoreFactorEntry->withDealer($existingDealer);
        }

        $this->insertEntryByLineNumber(
            $originalScoreFactorEntry,
        );
    }

    private function insertEntryByLineNumber(ScoreLogEntry $entry): void
    {
        $entries = $this->logEntryRepository->getRecentEntriesUntilLine(
            $entry->getScoreSheetIdentifier(),
            $entry->getScoreLineNumber(),
        );

        /** @var ScoreLogEntry $existingEntry */
        foreach ($entries as $existingEntry) {
            // Delete old log entry first
            $this->logEntryRepository->deleteLogEntry($existingEntry);
            $this->logEntryRepository->saveLogEntry(
                SimpleScoreLogEntry::fromLogEntry($existingEntry)->shiftedDown(),
            );
        }

        $this->logEntryRepository->saveLogEntry($entry);
    }
}
