<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ReadModel\ScoreLog\Model\CurrentGameAndPlayers;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;

interface CurrentGameAndPlayersRepository extends CurrentGamesAndPlayers
{
    public function saveGameAndPlayers(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        CurrentGameAndPlayers $currentGameAndPlayers,
    ): void;

    public function removeCurrentGameAndPlayers(ScoreSheetIdentifier $scoreSheetIdentifier): void;
}
