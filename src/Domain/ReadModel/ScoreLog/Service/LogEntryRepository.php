<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ReadModel\ScoreLog\Model\ScoreLogEntry;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Exception\IncorrectGameNumber;
use App\Domain\Table\GameNumber;

interface LogEntryRepository
{
    public function getOrCreateEntryForGame(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        GameNumber $gameNumber
    ): ScoreLogEntry;

    /**
     * @return \Traversable<ScoreLogEntry>
     */
    public function getLogEntriesForSheet(ScoreSheetIdentifier $scoreSheetIdentifier): \Traversable;

    public function deleteForScoreSheet(ScoreSheetIdentifier $scoreSheetIdentifier): void;

    /**
     * @throws IncorrectGameNumber
     */
    public function saveLogEntry(ScoreLogEntry $logEntry): void;

    public function getOrCreateEntryByGameAndLine(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        GameNumber $gameNumber,
        ScoreLineNumber $scoreLineNumber
    ): ScoreLogEntry;

    /**
     * @return \Traversable<ScoreLogEntry>
     */
    public function getRecentEntriesUntilLine(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        ScoreLineNumber $scoreLineNumber
    ): \Traversable;

    public function deleteLogEntry(ScoreLogEntry $logEntry): void;
}
