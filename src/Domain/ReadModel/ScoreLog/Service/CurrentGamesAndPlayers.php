<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ReadModel\ScoreLog\Model\CurrentGameAndPlayers;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;

interface CurrentGamesAndPlayers
{
    public function getGameAndPlayers(ScoreSheetIdentifier $scoreSheetIdentifier): CurrentGameAndPlayers;
}
