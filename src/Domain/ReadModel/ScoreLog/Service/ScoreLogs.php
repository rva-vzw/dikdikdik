<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ReadModel\ScoreLog\Model\ScoreLog;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;

final class ScoreLogs
{
    public function __construct(
        private readonly CurrentGamesAndPlayers $currentGamesAndPlayers,
        private readonly LogEntryRepository $scoreLogEntries,
    ) {
    }

    public function getScoreLog(ScoreSheetIdentifier $scoreSheetIdentifier): ScoreLog
    {
        $gameAndPlayers = $this->currentGamesAndPlayers->getGameAndPlayers($scoreSheetIdentifier);

        return ScoreLog::fromEntries(
            $this->scoreLogEntries->getLogEntriesForSheet($scoreSheetIdentifier),
            $gameAndPlayers->currentGameNumber,
            $gameAndPlayers->playersGroup,
            $scoreSheetIdentifier,
        );
    }
}
