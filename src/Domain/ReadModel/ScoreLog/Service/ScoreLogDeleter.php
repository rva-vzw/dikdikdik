<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;

interface ScoreLogDeleter
{
    public function deleteScoreLog(ScoreSheetIdentifier $scoreSheetIdentifier): void;
}
