<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Projection;

use App\Domain\ReadModel\Exception\ReadModelNotFoundException;
use App\Domain\ReadModel\ReadModelUpdateNotifier;
use App\Domain\ReadModel\ScoreLog\Model\SimpleScoreLogEntry;
use App\Domain\ReadModel\ScoreLog\ScoreLogUpdateNotification;
use App\Domain\ReadModel\ScoreLog\Service\LogEntryRepository;
use App\Domain\ReadModel\ScoreLog\Service\ScoreLogCorrector;
use App\Domain\ReadModel\TablesBySheet\TablesBySheet;
use App\Domain\ScoreSheet\Event\ScoreFactorDecreased;
use App\Domain\ScoreSheet\Event\ScoreFactorIncreased;
use App\Domain\ScoreSheet\Event\ScoresCorrected;
use App\Domain\ScoreSheet\Event\ScoresLogged;
use App\Domain\ScoreSheet\Event\UndoLogged;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\Event\TableCleared;
use App\Domain\Table\Exception\IncorrectGameNumber;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use Psr\Log\LoggerInterface;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

readonly final class LogEntryProjector extends AbstractProjector
{
    public function __construct(
        private TablesBySheet $tablesBySheet,
        private ReadModelUpdateNotifier $notifier,
        private LogEntryRepository $logEntryRepository,
        private ScoreLogCorrector $scoreLogCorrector,
        private LoggerInterface $logger,
    ) {
    }

    public function applyScoreFactorIncreased(ScoreFactorIncreased $event): void
    {
        $this->changeScoreFactor(
            $event->scoreSheetIdentifier,
            $event->gameNumber,
            $event->getNewScoreFactor(),
        );
    }

    public function applyScoreFactorDecreased(ScoreFactorDecreased $event): void
    {
        $scoreSheetIdentifier = $event->scoreSheetIdentifier;
        $gameNumber = $event->gameNumber;
        $scoreFactor = $event->newScoreFactor;

        $this->changeScoreFactor($scoreSheetIdentifier, $gameNumber, $scoreFactor);
    }

    public function applyTableCleared(TableCleared $event): void
    {
        $this->logEntryRepository->deleteForScoreSheet(
            ScoreSheetIdentifier::forTable($event->tableIdentifier)
        );
    }

    public function applyDealerAnnounced(DealerAnnounced $event): void
    {
        $logEntry = $this->logEntryRepository->getOrCreateEntryForGame(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
            $event->gameNumber,
        );
        $this->logEntryRepository->saveLogEntry(
            SimpleScoreLogEntry::fromLogEntry($logEntry)->withDealer(
                $event->playerIdentifier,
            ),
        );
    }

    private function changeScoreFactor(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        GameNumber $gameNumber,
        ScoreFactor $scoreFactor,
    ): void {
        $logEntry = $this->logEntryRepository->getOrCreateEntryForGame(
            $scoreSheetIdentifier,
            $gameNumber,
        );
        $this->logEntryRepository->saveLogEntry(
            SimpleScoreLogEntry::fromLogEntry($logEntry)->withScoreFactor($scoreFactor),
        );
        $this->notifyUpdateForSheet($scoreSheetIdentifier);
    }

    private function notifyUpdateForSheet(ScoreSheetIdentifier $scoreSheetIdentifier): void
    {
        // I wish I had provided a TableIdentifier in every event this projector listens to.
        try {
            $tableIdentifier = $this->tablesBySheet->getTableByScoreSheet($scoreSheetIdentifier);
            $this->notifyUpdate($tableIdentifier);
        } catch (ReadModelNotFoundException) {
            $this->logger->error("No table found for score sheet {$scoreSheetIdentifier}; skip sending update notification.");
        }
    }

    private function notifyUpdate(TableIdentifier $tableIdentifier): void
    {
        $this->notifier->notify(
            new ScoreLogUpdateNotification($tableIdentifier),
        );
    }

    public function applyScoresLogged(ScoresLogged $event): void
    {
        $entry = $this->logEntryRepository->getOrCreateEntryForGame(
            $event->scoreSheetIdentifier,
            $event->gameNumber,
        );

        $updatedEntry = SimpleScoreLogEntry::fromLogEntry($entry)->withScoreLine(
            $event->scoreLine,
        );
        try {
            $this->logEntryRepository->saveLogEntry(
                $updatedEntry
            );
        } catch (IncorrectGameNumber $ex) {
            $this->logger->error(
                "Score sheet {$event->scoreSheetIdentifier}: Incorrect game number trying to log {$entry->getNote()} for game {$entry->getGameNumber()} on line {$entry->getScoreLineNumber()}: {$ex->getMessage()}",
            );
        }

        $this->notifyUpdateForSheet($event->scoreSheetIdentifier);
    }

    public function applyScoresCorrected(ScoresCorrected $event): void
    {
        $entry = SimpleScoreLogEntry::create(
            $event->gameNumber,
            $event->scoreLine->getScoreLineNumber(),
            $event->scoreSheetIdentifier
        )->withScoreLine($event->scoreLine);

        $this->scoreLogCorrector->insertCorrectionEntry(
            $entry,
        );

        $this->notifyUpdateForSheet($event->scoreSheetIdentifier);
    }

    public function applyUndoLogged(UndoLogged $event): void
    {
        $entry = SimpleScoreLogEntry::create(
            $event->gameNumber,
            $event->scoreLine->getScoreLineNumber(),
            $event->scoreSheetIdentifier,
        )->withScoreLine($event->scoreLine);

        $this->scoreLogCorrector->insertUndoEntry(
            $entry,
        );

        $this->notifyUpdateForSheet($event->scoreSheetIdentifier);
    }
}
