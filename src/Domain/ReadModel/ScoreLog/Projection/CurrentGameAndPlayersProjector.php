<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog\Projection;

use App\Domain\ReadModel\Player\Model\PlayerStatus;
use App\Domain\ReadModel\ReadModelUpdateNotifier;
use App\Domain\ReadModel\ScoreLog\Model\CurrentGameAndPlayers;
use App\Domain\ReadModel\ScoreLog\ScoreLogUpdateNotification;
use App\Domain\ReadModel\ScoreLog\Service\CurrentGameAndPlayersRepository;
use App\Domain\ScoreSheet\Event\PaidOut;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\Event\PlayerLeft;
use App\Domain\Table\Event\PlayersReordered;
use App\Domain\Table\Event\PlayerStarred;
use App\Domain\Table\Event\TableCleared;
use App\Domain\Table\Event\Undo\PlayerStarUndone;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

readonly final class CurrentGameAndPlayersProjector extends AbstractProjector
{
    public function __construct(
        private readonly CurrentGameAndPlayersRepository $currentGameAndPlayersRepository,
        private readonly ReadModelUpdateNotifier $notifier,
    ) {
    }

    public function applyTableCleared(TableCleared $event): void
    {
        $this->currentGameAndPlayersRepository->saveGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
            CurrentGameAndPlayers::forNewTable(),
        );
    }

    public function applyPlayerJoined(PlayerJoined $event): void
    {
        $gameAndPlayers = $this->currentGameAndPlayersRepository->getGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
        );

        $this->currentGameAndPlayersRepository->saveGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
            $gameAndPlayers->withPlayer(
                $event->playerIdentifier,
                $event->playerName,
                $event->seat,
            )
        );
        $this->notifyUpdate($event->tableIdentifier);
    }

    public function applyPlayerLeft(PlayerLeft $event): void
    {
        $gameAndPlayers = $this->currentGameAndPlayersRepository->getGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
        );
        $this->currentGameAndPlayersRepository->saveGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
            $gameAndPlayers->withoutPlayer($event->playerIdentifier),
        );
        $this->notifyUpdate($event->tableIdentifier);
    }

    public function applyDealerAnnounced(DealerAnnounced $event): void
    {
        $gameAndPlayers = $this->currentGameAndPlayersRepository->getGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
        );
        $this->currentGameAndPlayersRepository->saveGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
            $gameAndPlayers->forGame($event->gameNumber)
                ->withDealer($event->playerIdentifier),
        );
        $this->notifyUpdate($event->tableIdentifier);
    }

    public function applyPlayersReordered(PlayersReordered $event): void
    {
        $gameAndPlayers = $this->currentGameAndPlayersRepository->getGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
        );
        $this->currentGameAndPlayersRepository->saveGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
            $gameAndPlayers->withPlayersReordered($event->newPlayerOrder),
        );
        $this->notifyUpdate($event->tableIdentifier);
    }

    public function applyPlayerStarred(PlayerStarred $event): void
    {
        $gameAndPlayers = $this->currentGameAndPlayersRepository->getGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
        );
        $this->currentGameAndPlayersRepository->saveGameAndPlayers(
            ScoreSheetIdentifier::forTable($event->tableIdentifier),
            $gameAndPlayers->withPlayerStarred($event->playerIdentifier, $event->numberOfStars),
        );
        $this->notifyUpdate($event->tableIdentifier);
    }

    public function applyPlayerStarUndone(PlayerStarUndone $event): void
    {
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable($event->tableIdentifier);
        $gameAndPlayers = $this->currentGameAndPlayersRepository->getGameAndPlayers(
            $scoreSheetIdentifier,
        );
        $this->currentGameAndPlayersRepository->saveGameAndPlayers(
            $scoreSheetIdentifier,
            $gameAndPlayers->withPlayerStarred($event->playerIdentifier, $event->numberOfStars - 1),
        );
        $this->notifyUpdate($event->tableIdentifier);
    }

    public function applyPaidOut(PaidOut $event): void
    {
        // A new player that gets paid, should get status active
        $gameAndPlayers = $this->currentGameAndPlayersRepository->getGameAndPlayers(
            $event->scoreSheetIdentifier,
        );

        $playersGroup = $gameAndPlayers->playersGroup;
        $needsUpdate = false;

        foreach ($event->getAffectedPlayers() as $playerIdentifier) {
            $player = $playersGroup->getPlayer($playerIdentifier);
            if (PlayerStatus::NEW === $player->getStatus()) {
                $playersGroup = $playersGroup->withPlayerActivated($player->getPlayerIdentifier());
                $needsUpdate = true;
            }
        }

        if ($needsUpdate) {
            $this->currentGameAndPlayersRepository->saveGameAndPlayers(
                $event->scoreSheetIdentifier,
                new CurrentGameAndPlayers(
                    $gameAndPlayers->currentGameNumber,
                    $playersGroup,
                )
            );
        }
    }

    private function notifyUpdate(TableIdentifier $tableIdentifier): void
    {
        $this->notifier->notify(new ScoreLogUpdateNotification($tableIdentifier));
    }
}
