<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ScoreLog;

use App\Domain\ReadModel\ReadModelUpdateNotification;
use App\Domain\Table\TableIdentifier;

final class ScoreLogUpdateNotification implements ReadModelUpdateNotification
{
    public function __construct(
        private TableIdentifier $tableIdentifier,
    ) {
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
