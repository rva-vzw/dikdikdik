<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TablePermissions;

// NOTE TO SELF: I used to have an interface like this, and I deleted it.
// Now I'm adding it back. So please think twice before you remove it again.
// (It doesn't hurt if it's here unused either.)

// All these permissions live in the same context, so I guess it's ok
// to check them using a single class.

use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;

interface TablePermissions
{
    public function canAddPlayer(): bool;

    public function canRemovePlayer(): bool;

    public function canLogGame(): bool;

    public function canAnnounceFinalRound(): bool;

    public function canUndo(): bool;

    public function canConfigureGame(): bool;

    public function getCurrentGameNumber(): GameNumber;

    public function getTableIdentifier(): TableIdentifier;
}
