<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TablePermissions;

use App\Domain\Table\TableIdentifier;

interface PermissionsByTable
{
    public function getPermissionsByTable(TableIdentifier $tableIdentifier): TablePermissions;
}
