<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TablePermissions;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Rules\Whist;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;

// Since there are no real boundaries in this project, we can
// use one class to retrieve any kind of permissions.

/**
 * @deprecated Let's replace this class by the TableState class.
 *
 * (The TablePermission repository should still be around!)
 */
final readonly class PermissionParameters implements TablePermissions
{
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public ?PlayerIdentifier $dealerIdentifier,
        public int $playerCount,
        public bool $gameStarted,
        public GameNumber $currentGameNumber,
        public ?GameNumber $finalGameNumber,
        public bool $gameOver,
    ) {
    }

    public static function forNewTable(TableIdentifier $tableIdentifier): self
    {
        return new self(
            $tableIdentifier,
            null,
            0,
            false,
            GameNumber::first(),
            null,
            false,
        );
    }

    public function with(mixed ...$properties): self
    {
        // This is Brent's hack:
        // https://www.youtube.com/watch?v=HW4o1K2us2E
        $properties = $properties + (array) $this;

        // Not very safe, alas:
        /** @phpstan-ignore-next-line */
        return new self(...$properties);
    }

    private function isFinalRound(): bool
    {
        return $this->finalGameNumber instanceof GameNumber;
    }

    public function canAddPlayer(): bool
    {
        return $this->playerCount < Seat::NUMBER_OF_SEATS
            && !$this->isFinalRound()
            && !$this->gameOver;
    }

    public function canRemovePlayer(): bool
    {
        return $this->playerCount > 0
            && !$this->isFinalRound()
            && !$this->gameOver;
    }

    public function canLogGame(): bool
    {
        return $this->playerCount <= Seat::NUMBER_OF_SEATS
            && $this->playerCount >= Whist::PLAYERS_NEEDED_TO_PLAY
            && $this->dealerIdentifier instanceof PlayerIdentifier
            && !$this->gameOver;
    }

    public function canAnnounceFinalRound(): bool
    {
        return !$this->isFinalRound();
    }

    public function canUndo(): bool
    {
        return $this->gameStarted;
    }

    public function withAdditionalPlayer(): self
    {
        return $this->with(playerCount: $this->playerCount + 1);
    }

    public function withOnePlayerLess(): self
    {
        return $this->with(playerCount: $this->playerCount - 1);
    }

    public function withDealer(PlayerIdentifier $dealerIdentifier): self
    {
        return $this->with(dealerIdentifier: $dealerIdentifier);
    }

    public function withoutDealer(): self
    {
        return $this->with(dealerIdentifier: null);
    }

    public function withFinalGame(GameNumber $finalGameNumber): self
    {
        return $this->with(finalGameNumber: $finalGameNumber);
    }

    public function forNonFinalRound(): self
    {
        return $this->with(finalGameNumber: null);
    }

    public function forGame(GameNumber $gameNumber): self
    {
        return $this->with(currentGameNumber: $gameNumber);
    }

    public function forEndedGame(): self
    {
        return $this->with(gameOver: true);
    }

    public function asStarted(): self
    {
        return $this->with(gameStarted: true);
    }

    public function allUndone(): self
    {
        return $this->with(gameStarted: false);
    }

    public function canConfigureGame(): bool
    {
        return !$this->gameStarted;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getCurrentGameNumber(): GameNumber
    {
        return $this->currentGameNumber;
    }
}
