<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TablePermissions;

use App\Domain\Table\TableIdentifier;

final readonly class PermissionParameterPermissions implements PermissionsByTable
{
    public function __construct(
        private PermissionParametersRepository $permissionParametersRepository
    ) {
    }

    public function getPermissionsByTable(TableIdentifier $tableIdentifier): TablePermissions
    {
        return $this->permissionParametersRepository->getPermissionParameters(
            $tableIdentifier,
        );
    }
}
