<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TablePermissions;

use App\Domain\Table\TableIdentifier;

interface PermissionParametersRepository
{
    public function getPermissionParameters(TableIdentifier $tableIdentifier): PermissionParameters;

    public function savePermissionParameters(PermissionParameters $permissionParameters): void;
}
