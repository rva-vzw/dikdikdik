<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TablePermissions;

use App\Domain\Table\Event\AllUndone;
use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\Event\DealerLeft;
use App\Domain\Table\Event\FinalRoundAnnounced;
use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\Event\PlayerLeft;
use App\Domain\Table\Event\TableStartedPlaying;
use App\Domain\Table\Event\Undo\FinalRoundAnnouncementUndone;
use App\Domain\Table\GameNumber;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

readonly final class PermissionParametersProjector extends AbstractProjector
{
    public function __construct(
        private readonly PermissionParametersRepository $permissionParametersRepository,
    ) {
    }

    public function applyPlayerJoined(PlayerJoined $event): void
    {
        $parameters = $this->permissionParametersRepository->getPermissionParameters($event->tableIdentifier);
        $this->permissionParametersRepository->savePermissionParameters(
            $parameters->withAdditionalPlayer(),
        );
    }

    public function applyPlayerLeft(PlayerLeft $event): void
    {
        $parameters = $this->permissionParametersRepository->getPermissionParameters($event->tableIdentifier);
        $this->permissionParametersRepository->savePermissionParameters(
            $parameters->withOnePlayerLess(),
        );
    }

    public function applyDealerAnnounced(DealerAnnounced $event): void
    {
        $parameters = $this->permissionParametersRepository->getPermissionParameters($event->tableIdentifier)
            ->forGame($event->gameNumber)
            ->withDealer($event->playerIdentifier);

        if (
            $parameters->finalGameNumber instanceof GameNumber
            && $event->gameNumber->isBiggerThan($parameters->finalGameNumber)
        ) {
            // FIXME: this should not be the way to find out the game is over.
            $parameters = $parameters->forEndedGame();
        }

        $this->permissionParametersRepository->savePermissionParameters(
            $parameters->forGame($event->gameNumber)->withDealer($event->playerIdentifier),
        );
    }

    public function applyDealerLeft(DealerLeft $event): void
    {
        $parameters = $this->permissionParametersRepository->getPermissionParameters($event->tableIdentifier);
        $this->permissionParametersRepository->savePermissionParameters(
            $parameters->withoutDealer(),
        );
    }

    public function applyFinalRoundAnnounced(FinalRoundAnnounced $event): void
    {
        $parameters = $this->permissionParametersRepository->getPermissionParameters($event->tableIdentifier);
        $this->permissionParametersRepository->savePermissionParameters(
            $parameters->withFinalGame($event->end),
        );
    }

    public function applyFinalRoundAnnouncementUndone(FinalRoundAnnouncementUndone $event): void
    {
        $parameters = $this->permissionParametersRepository->getPermissionParameters($event->tableIdentifier);
        $this->permissionParametersRepository->savePermissionParameters(
            $parameters->forNonFinalRound(),
        );
    }

    public function applyTableStartedPlaying(TableStartedPlaying $event): void
    {
        $parameters = $this->permissionParametersRepository->getPermissionParameters($event->tableIdentifier);
        $this->permissionParametersRepository->savePermissionParameters(
            $parameters->asStarted(),
        );
    }

    public function applyAllUndone(AllUndone $event): void
    {
        $parameters = $this->permissionParametersRepository->getPermissionParameters($event->tableIdentifier);
        $this->permissionParametersRepository->savePermissionParameters(
            $parameters->allUndone(),
        );
    }
}
