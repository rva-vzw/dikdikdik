<?php

declare(strict_types=1);

namespace App\Domain\TableConventions\TableTariffs;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\Tariffs\Tariffs;
use App\Domain\Table\Event\Game\Abundance;
use App\Domain\Table\Event\Game\Abundance10;
use App\Domain\Table\Event\Game\Abundance11;
use App\Domain\Table\Event\Game\Abundance12;
use App\Domain\Table\Event\Game\Alone5;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Event\Game\Misere;
use App\Domain\Table\Event\Game\MisereOnTheTable;
use App\Domain\Table\Event\Game\PropAndCop;
use App\Domain\Table\Event\Game\Solo;
use App\Domain\Table\Event\Game\Soloslim;
use App\Domain\Table\Event\Game\Troel;
use App\Domain\Table\Event\Game\Troela;
use App\Domain\Table\Exception\UnsupportedByRules;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\TableConventions\TableTariffs\Tariff\FixedTariff;
use App\Domain\TableConventions\TableTariffs\Tariff\Tariff;
use App\Domain\TableConventions\TableTariffs\Tariff\TrickTariff;

final readonly class IwwaTariffs implements Tariffs
{
    #[\Override]
    public function getName(): string
    {
        return 'calculator.iwwa';
    }

    /**
     * @template Announcement of GameAnnouncement
     * @template Outcome of GameOutcome
     *
     * @param GamePlayed<Announcement, Outcome> $event
     */
    #[\Override]
    public function calculateWinnings(GamePlayed $event): PlainWinnings
    {
        /** @var class-string $eventClass */
        $eventClass = $event::class;

        /** @var Tariff<Announcement, Outcome> $tariff */
        $tariff = match ($eventClass) {
            Alone5::class => new TrickTariff(
                requiredTricks: 5,
                francsWhenWinning: 2,
                francsForWinningExtraTrick: 1,
                francsWhenUnderTheTable: 20,
                francsWhenLoosing: 2,
                francsForLoosingPerTrick: 2,
            ),
            PropAndCop::class => new TrickTariff(
                requiredTricks: 8,
                francsWhenWinning: 2,
                francsForWinningExtraTrick: 1,
                francsWhenUnderTheTable: 14,
                francsWhenLoosing: 4,
                francsForLoosingPerTrick: 2,
            ),
            Abundance::class => new FixedTariff(5),
            Abundance10::class => new FixedTariff(6),
            Misere::class => new FixedTariff(7),
            Abundance11::class => new FixedTariff(8),
            Abundance12::class => new FixedTariff(9),
            Troel::class => new TrickTariff(
                requiredTricks: 8,
                francsWhenWinning: 4,
                francsForWinningExtraTrick: 2,
                francsWhenUnderTheTable: 28,
                francsWhenLoosing: 4,
                francsForLoosingPerTrick: 2,
            ),
            Troela::class => new TrickTariff(
                requiredTricks: 9,
                francsWhenWinning: 4,
                francsForWinningExtraTrick: 2,
                francsWhenUnderTheTable: 24,
                francsWhenLoosing: 4,
                francsForLoosingPerTrick: 2,
            ),
            MisereOnTheTable::class => new FixedTariff(14),
            Solo::class => new FixedTariff(25),
            Soloslim::class => new FixedTariff(30),
            default => throw UnsupportedByRules::byGameType($event::class),
        };

        return $tariff->appliedTo($event);
    }
}
