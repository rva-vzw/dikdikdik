<?php

declare(strict_types=1);

namespace App\Domain\TableConventions\TableTariffs;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\Tariffs\Tariffs;
use App\Domain\Table\Event\Game\Abundance;
use App\Domain\Table\Event\Game\Alone5;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Event\Game\Misere;
use App\Domain\Table\Event\Game\MisereOnTheTable;
use App\Domain\Table\Event\Game\PropAndCop;
use App\Domain\Table\Event\Game\Solo;
use App\Domain\Table\Event\Game\Troel;
use App\Domain\Table\Exception\UnsupportedByRules;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\TableConventions\TableTariffs\Tariff\FixedTariff;
use App\Domain\TableConventions\TableTariffs\Tariff\Tariff;
use App\Domain\TableConventions\TableTariffs\Tariff\TrickTariff;

final readonly class BertsTariffs implements Tariffs
{
    public function getName(): string
    {
        return 'calculator.bert';
    }

    /**
     * @template Announcement of GameAnnouncement
     * @template Outcome of GameOutcome
     *
     * @param GamePlayed<Announcement, Outcome> $event
     */
    public function calculateWinnings(GamePlayed $event): PlainWinnings
    {
        /** @var class-string $eventClass */
        $eventClass = $event::class;

        /** @var Tariff<Announcement, Outcome> $tariff */
        $tariff = match ($eventClass) {
            Alone5::class => new TrickTariff(
                requiredTricks: 5,
                francsWhenWinning: 10,
                francsForWinningExtraTrick: 5,
                francsWhenUnderTheTable: 50,
                francsWhenLoosing: 10,
                francsForLoosingPerTrick: 5,
            ),
            PropAndCop::class => new TrickTariff(
                requiredTricks: 8,
                francsWhenWinning: 10,
                francsForWinningExtraTrick: 5,
                francsWhenUnderTheTable: 35,
                francsWhenLoosing: 10,
                francsForLoosingPerTrick: 5,
            ),
            Troel::class => new TrickTariff(
                requiredTricks: 8,
                francsWhenWinning: 20,
                francsForWinningExtraTrick: 10,
                francsWhenUnderTheTable: 70,
                francsWhenLoosing: 20,
                francsForLoosingPerTrick: 10,
            ),
            Abundance::class => new FixedTariff(50),
            Misere::class => new FixedTariff(50),
            MisereOnTheTable::class => new FixedTariff(100),
            Solo::class => new FixedTariff(100),
            default => throw UnsupportedByRules::byGameType($event::class),
        };

        return $tariff->appliedTo($event);
    }
}
