<?php

declare(strict_types=1);

namespace App\Domain\TableConventions\TableTariffs;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\Tariffs\Tariffs;
use App\Domain\Table\Event\Game\Abundance;
use App\Domain\Table\Event\Game\Alone;
use App\Domain\Table\Event\Game\Alone5;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Event\Game\Misere;
use App\Domain\Table\Event\Game\MisereOnTheTable;
use App\Domain\Table\Event\Game\PropAndCop;
use App\Domain\Table\Event\Game\Solo;
use App\Domain\Table\Event\Game\Soloslim;
use App\Domain\Table\Event\Game\Troel;
use App\Domain\Table\Exception\UnsupportedByRules;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\TableConventions\TableTariffs\Tariff\FixedTariff;
use App\Domain\TableConventions\TableTariffs\Tariff\Tariff;
use App\Domain\TableConventions\TableTariffs\Tariff\TrickTariff;

final readonly class TraditionalTariffs implements Tariffs
{
    #[\Override]
    public function getName(): string
    {
        return 'calculator.traditional';
    }

    /**
     * @template Announcement of GameAnnouncement
     * @template Outcome of GameOutcome
     *
     * @param GamePlayed<Announcement, Outcome> $event
     */
    #[\Override]
    public function calculateWinnings(GamePlayed $event): PlainWinnings
    {
        /** @var class-string $eventClass */
        $eventClass = $event::class;

        /** @var Tariff<Announcement, Outcome> $tariff */
        $tariff = match ($eventClass) {
            // Depending on the ruleset, Alone or Alone5 is supported.
            // (They're usually not supported both.)
            Alone::class => new TrickTariff(
                requiredTricks: 6,
                francsWhenWinning: 3,
                francsForWinningExtraTrick: 1,
                francsWhenUnderTheTable: 20,
                francsWhenLoosing: 2,
                francsForLoosingPerTrick: 1,
            ),
            Alone5::class => new TrickTariff(
                requiredTricks: 5,
                francsWhenWinning: 2,
                francsForWinningExtraTrick: 1,
                francsWhenUnderTheTable: 20,
                francsWhenLoosing: 2,
                francsForLoosingPerTrick: 1,
            ),
            PropAndCop::class => new TrickTariff(
                requiredTricks: 8,
                francsWhenWinning: 2,
                francsForWinningExtraTrick: 1,
                francsWhenUnderTheTable: 14,
                francsWhenLoosing: 2,
                francsForLoosingPerTrick: 1,
            ),
            Troel::class => new TrickTariff(
                requiredTricks: 8,
                francsWhenWinning: 4,
                francsForWinningExtraTrick: 2,
                francsWhenUnderTheTable: 28,
                francsWhenLoosing: 4,
                francsForLoosingPerTrick: 2,
            ),
            Abundance::class => new FixedTariff(5),
            Misere::class => new FixedTariff(5),
            MisereOnTheTable::class => new FixedTariff(15),
            Solo::class => new FixedTariff(75),
            Soloslim::class => new FixedTariff(150),
            default => throw UnsupportedByRules::byGameType($event::class),
        };

        return $tariff->appliedTo($event);
    }
}
