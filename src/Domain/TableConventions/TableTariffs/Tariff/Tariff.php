<?php

declare(strict_types=1);

namespace App\Domain\TableConventions\TableTariffs\Tariff;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;

/**
 * @template Announcement of GameAnnouncement
 * @template Outcome of GameOutcome
 */
interface Tariff
{
    /**
     * @param GamePlayed<Announcement, Outcome> $event
     */
    public function appliedTo(GamePlayed $event): PlainWinnings;
}
