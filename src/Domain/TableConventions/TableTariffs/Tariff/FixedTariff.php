<?php

declare(strict_types=1);

namespace App\Domain\TableConventions\TableTariffs\Tariff;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Announcement\TroelAwareAnnouncement;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;
use App\Domain\Table\Loggable\Outcome\Winners;
use App\Domain\Table\Player\IndividualPlayer;
use App\Domain\Table\Player\PlayerIdentifier;
use Webmozart\Assert\Assert;

/**
 * @implements Tariff<GameAnnouncement, AllOrNothing|Winners>
 */
final readonly class FixedTariff implements Tariff
{
    /**
     * @param int<0, 100000> $francsPerPlayer
     */
    public function __construct(
        public readonly int $francsPerPlayer,
    ) {
    }

    #[\Override]
    public function appliedTo(GamePlayed $event): PlainWinnings
    {
        $announcement = $event->getAnnouncement();
        $outcome = $event->getOutcome();

        if ($outcome instanceof Winners) {
            // Misere and misere on the table
            /** @var GamePlayed<GameAnnouncement, Winners> $event */
            $nonTroelWinnings = $this->appliedToWinners($event);
        }

        if ($outcome instanceof AllOrNothing) {
            /** @var GamePlayed<GameAnnouncement, AllOrNothing> $event */
            $nonTroelWinnings = $this->appliedToAllOrNothing($event);
        }

        // Psalm doesn't notice that $outcome is either Winners either AllOrNothing.
        /** @psalm-suppress PossiblyUndefinedVariable */
        Assert::isInstanceOf($nonTroelWinnings, PlainWinnings::class);

        if ($announcement instanceof TroelAwareAnnouncement && $announcement->isTroel()) {
            return $nonTroelWinnings->doubled();
        }

        return $nonTroelWinnings;
    }

    /**
     * @param GamePlayed<GameAnnouncement, AllOrNothing> $event
     */
    private function appliedToAllOrNothing(GamePlayed $event): PlainWinnings
    {
        $outcome = $event->getOutcome();
        $announcement = $event->getAnnouncement();

        Assert::isInstanceOf($outcome, AllOrNothing::class);
        Assert::isInstanceOf($announcement, IndividualPlayer::class);

        $others = $event->getPlayersInvolved()->getPlayingPlayers()->without(
            $announcement->getPlayer(),
        );

        return $outcome->isSuccessful()
            ? PlainWinnings::singlePlayerWins(
                $announcement->getPlayer(),
                $others,
                $this->francsPerPlayer,
            ) : PlainWinnings::singlePlayerPays(
                $announcement->getPlayer(),
                $others,
                $this->francsPerPlayer,
            );
    }

    /**
     * @param GamePlayed<GameAnnouncement, Winners> $event
     */
    private function appliedToWinners(GamePlayed $event): PlainWinnings
    {
        return array_reduce(
            $event->getAnnouncement()->getPlayers()->toArray(),
            function (PlainWinnings $winnings, PlayerIdentifier $playerIdentifier) use ($event): PlainWinnings {
                $others = $event->getPlayersInvolved()->getPlayingPlayers()->without(
                    $playerIdentifier,
                );

                if ($event->getOutcome()->getWinners()->contains($playerIdentifier)) {
                    return $winnings->addedWith(
                        PlainWinnings::singlePlayerWins(
                            $playerIdentifier,
                            $others,
                            $this->francsPerPlayer,
                        )
                    );
                }

                return $winnings->addedWith(
                    PlainWinnings::singlePlayerPays(
                        $playerIdentifier,
                        $others,
                        $this->francsPerPlayer,
                    )
                );
            },
            PlainWinnings::none()
        );
    }
}
