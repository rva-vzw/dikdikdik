<?php

declare(strict_types=1);

namespace App\Domain\TableConventions\TableTariffs\Tariff;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use Webmozart\Assert\Assert;

/**
 * @implements Tariff<GameAnnouncement, NumberOfTricks>
 */
final readonly class TrickTariff implements Tariff
{
    /**
     * @param int<0, 13>     $requiredTricks
     * @param int<0, 100000> $francsWhenWinning
     * @param int<0, 100000> $francsForWinningExtraTrick
     * @param int<0, 100000> $francsWhenUnderTheTable
     * @param int<0, 100000> $francsWhenLoosing
     * @param int<0, 100000> $francsForLoosingPerTrick
     */
    public function __construct(
        public readonly int $requiredTricks,
        public readonly int $francsWhenWinning,
        public readonly int $francsForWinningExtraTrick,
        public readonly int $francsWhenUnderTheTable,
        public readonly int $francsWhenLoosing,
        public readonly int $francsForLoosingPerTrick,
    ) {
    }

    #[\Override]
    public function appliedTo(GamePlayed $event): PlainWinnings
    {
        $outcome = $event->getOutcome();
        Assert::isInstanceOf($outcome, NumberOfTricks::class);

        // good and bad is Wiezen terminology:
        // 'de goeien' en 'de slechten'

        $good = $event->getAnnouncement()->getPlayers();
        $bad = $event->getPlayersInvolved()->getPlayingPlayers()
            ->without(...$good);

        $extraTricks = $outcome->getNumber() - $this->requiredTricks;

        if ($outcome->underTheTable()) {
            return PlainWinnings::create(
                winners: $good,
                losers: $bad,
                francsPerPlayer: $this->francsWhenUnderTheTable,
            );
        }

        if ($extraTricks >= 0) {
            return PlainWinnings::create(
                winners: $good,
                losers: $bad,
                francsPerPlayer: $this->francsWhenWinning + $extraTricks * $this->francsForWinningExtraTrick,
            );
        }

        // Note that 'extraTricks' are negative here.
        return PlainWinnings::create(
            winners: $bad,
            losers: $good,
            francsPerPlayer: $this->francsWhenLoosing - $extraTricks * $this->francsForLoosingPerTrick,
        );
    }
}
