<?php

declare(strict_types=1);

namespace App\Domain\TableConventions\Exception;

final class RuleSetNotFound extends \Exception
{
    /**
     * @param non-empty-string $ruleSetName
     */
    public static function byName(string $ruleSetName): self
    {
        return new self("RuleSet {$ruleSetName} not found");
    }

    public static function unclear(): self
    {
        return new self('Ruleset unclear');
    }
}
