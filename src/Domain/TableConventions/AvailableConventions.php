<?php

declare(strict_types=1);

namespace App\Domain\TableConventions;

use App\Domain\Table\Loggable\GameSpecification\GameSpecifications;
use App\Domain\Table\Rules\PassAroundRules;
use App\Domain\Table\Rules\RulesByConvention;
use App\Domain\TableConventions\Exception\RuleSetNotFound;
use Traversable;

/**
 * @implements \IteratorAggregate<Conventions>
 */
final readonly class AvailableConventions implements RulesByConvention, \IteratorAggregate
{
    /**
     * @param iterable<Conventions> $allPossibleConventions
     */
    public function __construct(
        public iterable $allPossibleConventions,
    ) {
    }

    /**
     * @param non-empty-string $conventionName
     */
    public function getConventionsByName(string $conventionName): Conventions
    {
        foreach ($this->allPossibleConventions as $ruleSet) {
            if ($ruleSet->name === $conventionName) {
                return $ruleSet;
            }
        }

        throw RuleSetNotFound::byName($conventionName);
    }

    #[\Override]
    public function getIterator(): Traversable
    {
        yield from $this->allPossibleConventions;
    }

    /**
     * @param non-empty-string $conventionName
     *
     * @throws RuleSetNotFound
     */
    public function getPassAroundRules(string $conventionName): PassAroundRules
    {
        return $this->getConventionsByName($conventionName)->passAroundRules;
    }

    /**
     * @param non-empty-string $conventionName
     *
     * @throws RuleSetNotFound
     */
    public function getPermissibleGames(string $conventionName): GameSpecifications
    {
        // The conventions class seems to implement GameSpecifications.

        return $this->getConventionsByName($conventionName);
    }
}
