<?php

declare(strict_types=1);

namespace App\Domain\TableConventions;

use App\Domain\ScoreSheet\Tariffs\TariffsByTable;
use App\Domain\Table\Loggable\GameSpecification\GameSpecification;
use App\Domain\Table\TableIdentifier;

/**
 * Conventions that are agreed upon by table.
 *
 * This includes the games that you can bid, the tariffs, the rules concerning passing around.
 */
interface ConventionsByTable extends TariffsByTable
{
    public function getConventionsByTable(TableIdentifier $tableIdentifier): Conventions;

    /**
     * @return \Traversable<string, GameSpecification>
     */
    public function getAllowedGames(TableIdentifier $tableIdentifier): \Traversable;
}
