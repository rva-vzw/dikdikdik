<?php

declare(strict_types=1);

namespace App\Domain\TableConventions;

use App\Domain\ScoreSheet\Tariffs\Tariffs;
use App\Domain\Table\Loggable\GameSpecification\AllowedGamesTrait;
use App\Domain\Table\Loggable\GameSpecification\GameSpecification;
use App\Domain\Table\Loggable\GameSpecification\GameSpecifications;
use App\Domain\Table\Rules\PassAroundRules;
use App\Domain\Table\Rules\Whist;
use Traversable;

final readonly class Conventions implements GameSpecifications, Whist
{
    use AllowedGamesTrait;

    /**
     * @param list<GameSpecification> $allowedGames
     */
    public function __construct(
        public string $name,
        public array $allowedGames,
        public Tariffs $tariffs,
        public PassAroundRules $passAroundRules,
    ) {
    }

    public function getAll(): Traversable
    {
        foreach ($this->allowedGames as $gameSpecification) {
            yield $gameSpecification->getName() => $gameSpecification;
        }
    }
}
