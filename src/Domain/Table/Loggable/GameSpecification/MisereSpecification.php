<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Misere;
use App\Domain\Table\Event\Undo\MisereUndone;
use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\Winners;

final readonly class MisereSpecification extends AbstractGameSpecification
{
    public const NAME = 'misere';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return MultiPlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return Winners::class;
    }

    public function getEventType(): string
    {
        return Misere::class;
    }

    public function getUndoEventType(): string
    {
        return MisereUndone::class;
    }
}
