<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

/**
 * Just a list with game specifications.
 *
 * @extends \IteratorAggregate<GameSpecification>
 */
interface GameSpecifications extends \IteratorAggregate
{
    /**
     * Returns an array that maps game names to game specifications.
     *
     * @return \Traversable<string, GameSpecification>
     */
    public function getAll(): \Traversable;

    /**
     * @param class-string<GameSpecification> $gameSpecificationClass
     */
    public function hasBySpecificationClass(string $gameSpecificationClass): bool;
}
