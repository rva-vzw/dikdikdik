<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Event\Undo\GameUndone;
use App\Domain\Table\Exception\GameNotFound;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use Traversable;

/**
 * This class contains *all* specified games (independent of the ruleset).
 *
 * @implements \IteratorAggregate<string, GameSpecification>
 */
final readonly class AllSpecifiedGames implements \IteratorAggregate
{
    /**
     * @var array<string, GameSpecification>
     */
    private array $gameSpecifications;

    /**
     * @param Traversable<GameSpecification> $specifications
     */
    public function __construct(Traversable $specifications)
    {
        $gameSpecifications = [];

        /** @var GameSpecification $gameSpecification */
        foreach ($specifications as $gameSpecification) {
            $gameSpecifications[$gameSpecification->getName()] = $gameSpecification;
        }

        $this->gameSpecifications = $gameSpecifications;
    }

    public function default(): self
    {
        $games = clone $this;

        return $games->without(Alone5Specification::NAME);
    }

    public function allowingAlone5(): self
    {
        $games = clone $this;

        return $games->without(AloneSpecification::NAME);
    }

    /**
     * @param GamePlayed<GameAnnouncement, GameOutcome> $event
     */
    public function getByEvent(GamePlayed $event): GameSpecification
    {
        $eventType = get_class($event);

        foreach ($this->gameSpecifications as $gameSpecification) {
            if ($gameSpecification->getEventType() === $eventType) {
                return $gameSpecification;
            }
        }

        throw new GameNotFound();
    }

    public function getByGameName(string $gameName): GameSpecification
    {
        return $this->gameSpecifications[$gameName];
    }

    public function without(string $gameName): self
    {
        return new self(
            new \ArrayIterator(array_filter(
                $this->gameSpecifications,
                fn (string $name) => $name != $gameName,
                ARRAY_FILTER_USE_KEY,
            )),
        );
    }

    /**
     * @return Traversable<string, GameSpecification>
     */
    public function getIterator(): Traversable
    {
        return new \ArrayIterator($this->gameSpecifications);
    }

    /**
     * @param GameUndone<GameAnnouncement, GameOutcome> $event
     */
    public function getByUndoEvent(GameUndone $event): GameSpecification
    {
        $eventType = get_class($event);

        foreach ($this->gameSpecifications as $gameSpecification) {
            if ($gameSpecification->getUndoEventType() === $eventType) {
                return $gameSpecification;
            }
        }

        throw new GameNotFound();
    }

    // FIXME: Move this to somewhere else (abstract game specification?)
    public static function getUndoGameName(string $gameName): string
    {
        return "undo_{$gameName}";
    }
}
