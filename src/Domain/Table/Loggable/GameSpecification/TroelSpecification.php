<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Troel;
use App\Domain\Table\Event\Undo\TroelUndone;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;

final readonly class TroelSpecification extends AbstractGameSpecification
{
    public const NAME = 'troel';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return TwoPlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return NumberOfTricks::class;
    }

    public function getEventType(): string
    {
        return Troel::class;
    }

    public function getUndoEventType(): string
    {
        return TroelUndone::class;
    }
}
