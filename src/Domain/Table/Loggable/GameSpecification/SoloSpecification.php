<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Solo;
use App\Domain\Table\Event\Undo\SoloUndone;
use App\Domain\Table\Loggable\Announcement\TroelAwareSinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;

/**
 * Solo is also know as 'abundance declared'.
 *
 * https://en.wikipedia.org/wiki/Solo_whist#Bidding
 */
final readonly class SoloSpecification extends AbstractGameSpecification
{
    public const NAME = 'solo';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return TroelAwareSinglePlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return AllOrNothing::class;
    }

    public function getEventType(): string
    {
        return Solo::class;
    }

    public function getUndoEventType(): string
    {
        return SoloUndone::class;
    }
}
