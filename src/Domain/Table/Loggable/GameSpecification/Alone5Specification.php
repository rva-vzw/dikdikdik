<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Alone5;
use App\Domain\Table\Event\Undo\Alone5Undone;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;

final readonly class Alone5Specification extends AbstractGameSpecification
{
    public const NAME = 'alone5';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        // Use a concrete class here instead of an interface,
        // to avoid issues with serialization.
        return SinglePlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return NumberOfTricks::class;
    }

    public function getEventType(): string
    {
        return Alone5::class;
    }

    public function getUndoEventType(): string
    {
        return Alone5Undone::class;
    }
}
