<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Alone;
use App\Domain\Table\Event\Undo\AloneUndone;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;

final readonly class AloneSpecification extends AbstractGameSpecification
{
    public const NAME = 'alone';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        // Use a concrete class here instead of an interface,
        // to avoid issues with serialization.
        return SinglePlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return NumberOfTricks::class;
    }

    public function getEventType(): string
    {
        return Alone::class;
    }

    public function getUndoEventType(): string
    {
        return AloneUndone::class;
    }
}
