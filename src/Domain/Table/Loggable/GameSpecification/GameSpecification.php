<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Loggable\Loggable;

interface GameSpecification extends Loggable
{
    public function getName(): string;

    /**
     * The expected concrete announcement implementation type. (So no interface!).
     *
     * @return class-string
     */
    public function getAnnouncementType(): string;

    /**
     * @return class-string
     */
    public function getOutcomeType(): string;

    /**
     * @return class-string
     */
    public function getEventType(): string;

    /**
     * @return class-string
     */
    public function getUndoEventType(): string;
}
