<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

abstract readonly class AbstractGameSpecification implements GameSpecification
{
    public function getLabel(): string
    {
        return 'game.'.$this->getName();
    }
}
