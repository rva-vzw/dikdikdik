<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Abundance11;
use App\Domain\Table\Event\Undo\Abundance11Undone;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;

final readonly class Abundance11Specification extends AbstractGameSpecification
{
    public const NAME = 'abundance11';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return SinglePlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return AllOrNothing::class;
    }

    public function getEventType(): string
    {
        return Abundance11::class;
    }

    public function getUndoEventType(): string
    {
        return Abundance11Undone::class;
    }
}
