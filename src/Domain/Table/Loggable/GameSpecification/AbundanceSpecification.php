<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Abundance;
use App\Domain\Table\Event\Undo\AbundanceUndone;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;

final readonly class AbundanceSpecification extends AbstractGameSpecification
{
    public const NAME = 'abundance';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return SinglePlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return AllOrNothing::class;
    }

    public function getEventType(): string
    {
        return Abundance::class;
    }

    public function getUndoEventType(): string
    {
        return AbundanceUndone::class;
    }
}
