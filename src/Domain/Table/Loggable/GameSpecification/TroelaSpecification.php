<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Troela;
use App\Domain\Table\Event\Undo\TroelaUndone;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;

final readonly class TroelaSpecification extends AbstractGameSpecification
{
    public const NAME = 'troela';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return TwoPlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return NumberOfTricks::class;
    }

    public function getEventType(): string
    {
        return Troela::class;
    }

    public function getUndoEventType(): string
    {
        return TroelaUndone::class;
    }
}
