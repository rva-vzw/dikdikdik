<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\PropAndCop;
use App\Domain\Table\Event\Undo\PropAndCopUndone;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;

final readonly class PropAndCopSpecification extends AbstractGameSpecification
{
    public const NAME = 'prop_and_cop';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return TwoPlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return NumberOfTricks::class;
    }

    public function getEventType(): string
    {
        return PropAndCop::class;
    }

    public function getUndoEventType(): string
    {
        return PropAndCopUndone::class;
    }
}
