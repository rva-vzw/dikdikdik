<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use Webmozart\Assert\Assert;

trait AllowedGamesTrait
{
    /**
     * @param class-string<GameSpecification> $gameSpecificationClass
     */
    public function hasBySpecificationClass(string $gameSpecificationClass): bool
    {
        Assert::isInstanceOf($this, GameSpecifications::class);
        foreach ($this->getAll() as $allowedGame) {
            if ($allowedGame instanceof $gameSpecificationClass) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return \Traversable<GameSpecification>
     */
    #[\Override]
    public function getIterator(): \Traversable
    {
        Assert::isInstanceOf($this, GameSpecifications::class);

        return $this->getAll();
    }
}
