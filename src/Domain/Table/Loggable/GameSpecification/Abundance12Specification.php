<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Abundance12;
use App\Domain\Table\Event\Undo\Abundance12Undone;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;

final readonly class Abundance12Specification extends AbstractGameSpecification
{
    public const NAME = 'abundance12';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return SinglePlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return AllOrNothing::class;
    }

    public function getEventType(): string
    {
        return Abundance12::class;
    }

    public function getUndoEventType(): string
    {
        return Abundance12Undone::class;
    }
}
