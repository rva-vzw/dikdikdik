<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\MisereOnTheTable;
use App\Domain\Table\Event\Undo\MisereOnTheTableUndone;
use App\Domain\Table\Loggable\Announcement\TroelAwareMultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\Winners;

final readonly class MisereOnTheTableSpecification extends AbstractGameSpecification
{
    public const NAME = 'misere_ot_table';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return TroelAwareMultiPlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return Winners::class;
    }

    public function getEventType(): string
    {
        return MisereOnTheTable::class;
    }

    public function getUndoEventType(): string
    {
        return MisereOnTheTableUndone::class;
    }
}
