<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Event\Game\Soloslim;
use App\Domain\Table\Event\Undo\SoloslimUndone;
use App\Domain\Table\Loggable\Announcement\TroelAwareSinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;

final readonly class SoloslimSpecification extends AbstractGameSpecification
{
    public const NAME = 'soloslim';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getAnnouncementType(): string
    {
        return TroelAwareSinglePlayerAnnouncement::class;
    }

    public function getOutcomeType(): string
    {
        return AllOrNothing::class;
    }

    public function getEventType(): string
    {
        return Soloslim::class;
    }

    public function getUndoEventType(): string
    {
        return SoloslimUndone::class;
    }
}
