<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Exception\InvalidAnnouncement;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use Symfony\Component\Serializer\Attribute\Ignore;

/**
 * A 2-player announcement.
 *
 * I made this a value object without an interface, because I wasn't sure anymore
 * why I created the interface for single player announcements. If it works out,
 * I can get rid of {@see SinglePlayerAnnouncement} in favour of {@see SimpleSinglePlayerAnnouncement}.
 */
final class TwoPlayerAnnouncement implements GameAnnouncement
{
    /** @var PlayerIdentifier */
    private $player1;

    /** @var PlayerIdentifier */
    private $player2;

    public function __construct(PlayerIdentifier $player1, PlayerIdentifier $player2)
    {
        if ($player1 == $player2) {
            throw new InvalidAnnouncement();
        }

        $this->player1 = $player1;
        $this->player2 = $player2;
    }

    public function getPlayer1(): PlayerIdentifier
    {
        return $this->player1;
    }

    public function getPlayer2(): PlayerIdentifier
    {
        return $this->player2;
    }

    #[Ignore]
    public static function getName(): string
    {
        return 'two_player';
    }

    #[Ignore]
    public function getPlayers(): PlayerIdentifiers
    {
        return PlayerIdentifiers::fromArray([$this->player1, $this->player2]);
    }
}
