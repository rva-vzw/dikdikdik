<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Announcement\ParticularValidators;

use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Player\Players;
use App\Domain\Table\Player\PlayersInvolved;

final class PlayerSetAnnouncementValidator implements ParticularAnnouncementValidator
{
    public function supports(GameAnnouncement $announcement): bool
    {
        return $announcement instanceof Players;
    }

    public function isValidAnnouncement(GameAnnouncement $announcement, PlayersInvolved $playersInvolved): bool
    {
        return $announcement->getPlayers()->isSubsetOf($playersInvolved->getPlayingPlayers());
    }
}
