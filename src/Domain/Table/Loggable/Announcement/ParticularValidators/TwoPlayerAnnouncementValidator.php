<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Announcement\ParticularValidators;

use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Player\PlayersInvolved;
use InvalidArgumentException;

final class TwoPlayerAnnouncementValidator implements ParticularAnnouncementValidator
{
    public function supports(GameAnnouncement $announcement): bool
    {
        return $announcement instanceof TwoPlayerAnnouncement;
    }

    public function isValidAnnouncement(GameAnnouncement $announcement, PlayersInvolved $playersInvolved): bool
    {
        if (!$announcement instanceof TwoPlayerAnnouncement) {
            throw new InvalidArgumentException();
        }

        return $playersInvolved->isPlaying($announcement->getPlayer1())
            && $playersInvolved->isPlaying($announcement->getPlayer2());
    }
}
