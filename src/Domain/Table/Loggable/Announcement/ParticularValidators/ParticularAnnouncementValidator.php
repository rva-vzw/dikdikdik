<?php

namespace App\Domain\Table\Loggable\Announcement\ParticularValidators;

use App\Domain\Table\Loggable\Announcement\AnnouncementValidator;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;

interface ParticularAnnouncementValidator extends AnnouncementValidator
{
    public function supports(GameAnnouncement $announcement): bool;
}
