<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Announcement\ParticularValidators;

use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Player\IndividualPlayer;
use App\Domain\Table\Player\PlayersInvolved;
use InvalidArgumentException;

final class IndividualPlayerAnnouncementValidator implements ParticularAnnouncementValidator
{
    public function isValidAnnouncement(GameAnnouncement $announcement, PlayersInvolved $playersInvolved): bool
    {
        if (!$announcement instanceof IndividualPlayer) {
            throw new InvalidArgumentException();
        }

        return $playersInvolved->isPlaying($announcement->getPlayer());
    }

    public function supports(GameAnnouncement $announcement): bool
    {
        return $announcement instanceof IndividualPlayer;
    }
}
