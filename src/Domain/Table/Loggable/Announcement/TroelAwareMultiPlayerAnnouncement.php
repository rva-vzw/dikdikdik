<?php

namespace App\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Exception\InvalidAnnouncement;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;

final class TroelAwareMultiPlayerAnnouncement implements TroelAwareAnnouncement
{
    public function __construct(
        private PlayerIdentifiers $players,
        private bool $troel
    ) {
        if ($players->isEmpty()) {
            throw InvalidAnnouncement::noAnnouncer();
        }

        if ($players->getCount() > 4) {
            throw InvalidAnnouncement::tooManyAnnouncers();
        }
    }

    public function getPlayers(): PlayerIdentifiers
    {
        return $this->players;
    }

    public static function fromSingle(
        PlayerIdentifier $playerIdentifier,
        bool $troel
    ): self {
        return new self(
            PlayerIdentifiers::fromArray([$playerIdentifier]),
            $troel
        );
    }

    public function isTroel(): bool
    {
        return $this->troel;
    }

    public static function getName(): string
    {
        return 'troel_aware_multiplayer';
    }
}
