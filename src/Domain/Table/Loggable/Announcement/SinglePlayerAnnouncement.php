<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Player\IndividualPlayer;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;

final class SinglePlayerAnnouncement implements GameAnnouncement, IndividualPlayer
{
    /** @var PlayerIdentifier */
    private $player;

    public function __construct(PlayerIdentifier $player)
    {
        $this->player = $player;
    }

    public function getPlayer(): PlayerIdentifier
    {
        return $this->player;
    }

    public static function getName(): string
    {
        return 'single_player';
    }

    #[\Override]
    public function getPlayers(): PlayerIdentifiers
    {
        return PlayerIdentifiers::fromSingle($this->player);
    }
}
