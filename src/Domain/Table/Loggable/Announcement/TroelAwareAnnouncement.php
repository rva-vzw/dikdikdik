<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Announcement;

interface TroelAwareAnnouncement extends GameAnnouncement
{
    public function isTroel(): bool;
}
