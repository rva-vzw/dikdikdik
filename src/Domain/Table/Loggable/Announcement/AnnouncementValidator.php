<?php

namespace App\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Player\PlayersInvolved;

interface AnnouncementValidator
{
    public function isValidAnnouncement(GameAnnouncement $announcement, PlayersInvolved $playersInvolved): bool;
}
