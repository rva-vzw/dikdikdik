<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Player\IndividualPlayer;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;

final class TroelAwareSinglePlayerAnnouncement implements TroelAwareAnnouncement, IndividualPlayer
{
    /** @var PlayerIdentifier */
    private $player;

    /** @var bool */
    private $troel;

    public function __construct(PlayerIdentifier $player, bool $troel)
    {
        $this->player = $player;
        $this->troel = $troel;
    }

    public function getPlayer(): PlayerIdentifier
    {
        return $this->player;
    }

    public function isTroel(): bool
    {
        return $this->troel;
    }

    public static function getName(): string
    {
        return 'troel_aware_single_player';
    }

    #[\Override]
    public function getPlayers(): PlayerIdentifiers
    {
        return PlayerIdentifiers::fromSingle($this->player);
    }
}
