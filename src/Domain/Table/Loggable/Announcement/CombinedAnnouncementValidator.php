<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Loggable\Announcement\ParticularValidators\ParticularAnnouncementValidator;
use App\Domain\Table\Player\PlayersInvolved;
use Traversable;

final class CombinedAnnouncementValidator implements AnnouncementValidator
{
    /** @var Traversable<mixed, ParticularAnnouncementValidator> */
    private $particularValidators;

    /**
     * CombinedAnnouncementValidator constructor.
     *
     * @param Traversable<mixed, ParticularAnnouncementValidator> $particularValidators
     */
    public function __construct(Traversable $particularValidators)
    {
        $this->particularValidators = $particularValidators;
    }

    public function isValidAnnouncement(
        GameAnnouncement $announcement,
        PlayersInvolved $playersInvolved
    ): bool {
        foreach ($this->particularValidators as $validator) {
            if (!$validator->supports($announcement)) {
                continue;
            }
            if (!$validator->isValidAnnouncement($announcement, $playersInvolved)) {
                return false;
            }
        }

        return true;
    }
}
