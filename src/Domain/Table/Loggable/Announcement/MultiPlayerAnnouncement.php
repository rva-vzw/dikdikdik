<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Exception\InvalidAnnouncement;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;

final class MultiPlayerAnnouncement implements GameAnnouncement
{
    public function __construct(private PlayerIdentifiers $players)
    {
        if ($players->isEmpty()) {
            throw InvalidAnnouncement::noAnnouncer();
        }

        if ($players->getCount() > 4) {
            throw InvalidAnnouncement::tooManyAnnouncers();
        }
    }

    public function getPlayers(): PlayerIdentifiers
    {
        return $this->players;
    }

    public static function fromSingle(PlayerIdentifier $playerIdentifier): self
    {
        return new self(PlayerIdentifiers::fromArray([$playerIdentifier]));
    }

    public static function getName(): string
    {
        return 'multiplayer';
    }
}
