<?php

namespace App\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Player\Players;

interface GameAnnouncement extends Players
{
    public static function getName(): string;
}
