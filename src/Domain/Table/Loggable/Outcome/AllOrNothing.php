<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Outcome;

final class AllOrNothing implements GameOutcome
{
    /** @var bool */
    private $successful;

    public function __construct(bool $successful)
    {
        $this->successful = $successful;
    }

    public function isSuccessful(): bool
    {
        return $this->successful;
    }

    public static function asSuccess(): self
    {
        return new self(true);
    }

    public static function asFailure(): self
    {
        return new self(false);
    }

    public static function getName(): string
    {
        return 'all_or_nothing';
    }
}
