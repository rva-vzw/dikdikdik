<?php

namespace App\Domain\Table\Loggable\Outcome;

interface GameOutcome
{
    public static function getName(): string;
}
