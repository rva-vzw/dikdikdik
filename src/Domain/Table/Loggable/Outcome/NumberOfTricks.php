<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Outcome;

use App\Domain\Table\Exception\InvalidNumberOfTricks;

final class NumberOfTricks implements GameOutcome
{
    public const MAX_NUMBER_OF_TRICKS = 13;

    /** @var int */
    private $number;

    /**
     * Public constructor.
     *
     * This constructor needs to be public to avoid normalization problems, see also #43.
     */
    public function __construct(int $number)
    {
        if ($number < 0 || $number > self::MAX_NUMBER_OF_TRICKS) {
            throw new InvalidNumberOfTricks();
        }

        $this->number = $number;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public static function fromNumber(int $number): self
    {
        return new self($number);
    }

    public static function all(): self
    {
        return new self(self::MAX_NUMBER_OF_TRICKS);
    }

    public function underTheTable(): bool
    {
        return self::MAX_NUMBER_OF_TRICKS === $this->number;
    }

    public static function getName(): string
    {
        return 'number_of_tricks';
    }
}
