<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable\Outcome;

use App\Domain\Table\Player\PlayerIdentifiers;

final class Winners implements GameOutcome
{
    /** @var PlayerIdentifiers */
    private $winners;

    public function __construct(PlayerIdentifiers $winners)
    {
        $this->winners = $winners;
    }

    public function getWinners(): PlayerIdentifiers
    {
        return $this->winners;
    }

    public static function none(): self
    {
        return new self(PlayerIdentifiers::none());
    }

    public static function getName(): string
    {
        return 'winners';
    }
}
