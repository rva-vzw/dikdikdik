<?php

declare(strict_types=1);

namespace App\Domain\Table\Loggable;

interface Loggable
{
    public function getLabel(): string;

    public function getName(): string;
}
