<?php

declare(strict_types=1);

namespace App\Domain\Table;

interface FinalGames
{
    public function isEnding(TableIdentifier $tableIdentifier): bool;

    public function getFinalGame(TableIdentifier $tableIdentifier): GameNumber;
}
