<?php

declare(strict_types=1);

namespace App\Domain\Table\Event;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class DealerFailed implements Event
{
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $gameNumber,
        public PlayerIdentifier $failingDealer
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
