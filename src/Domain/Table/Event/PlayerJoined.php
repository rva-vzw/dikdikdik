<?php

declare(strict_types=1);

namespace App\Domain\Table\Event;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class PlayerJoined implements Event
{
    /**
     * @param non-empty-string $playerName
     */
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public PlayerIdentifier $playerIdentifier,
        public GameNumber $gameNumber,
        public Seat $seat,
        public string $playerName,
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
