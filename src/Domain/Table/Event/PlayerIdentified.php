<?php

namespace App\Domain\Table\Event;

use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

/**
 * A player is now known by some name.
 *
 * I'm not completely sure this should be under the table aggregate. On the
 * other hand, the name of the player is irrelevant for most of the application
 * (except the read side), and I don't need to store any personal information.
 *
 * So I guess this is acceptable.
 *
 * @deprecated player name was added to {@see PlayerJoined}
 */
final readonly class PlayerIdentified implements Event
{
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public PlayerIdentifier $playerIdentifier,
        public string $playerName
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
