<?php

namespace App\Domain\Table\Event;

use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class TableCleared implements Event
{
    public function __construct(
        public TableIdentifier $tableIdentifier
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
