<?php

declare(strict_types=1);

namespace App\Domain\Table\Event;

use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class TableConfigured implements Event
{
    /**
     * @param non-empty-string $ruleSetName
     */
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public string $ruleSetName,
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
