<?php

namespace App\Domain\Table\Event;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class PassedAround implements Event
{
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $gameNumber,
        public int $count,
        public PlayersInvolved $playersInvolved
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
