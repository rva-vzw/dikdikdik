<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;

/**
 * @extends AbstractGameUndoneEvent<SinglePlayerAnnouncement, NumberOfTricks>
 * @implements GameUndone<SinglePlayerAnnouncement, NumberOfTricks>
 */
final readonly class Alone5Undone extends AbstractGameUndoneEvent implements GameUndone
{
}
