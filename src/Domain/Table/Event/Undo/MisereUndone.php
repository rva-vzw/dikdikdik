<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\Winners;

/**
 * @extends AbstractGameUndoneEvent<MultiPlayerAnnouncement,Winners>
 * @implements GameUndone<MultiPlayerAnnouncement,Winners>
 */
final readonly class MisereUndone extends AbstractGameUndoneEvent implements GameUndone
{
}
