<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\TableIdentifier;

final readonly class FinalRoundAnnouncementUndone implements UndoEvent
{
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $start,
        public GameNumber $end,
        public PlayerIdentifier $dealer,
        public int $passedAroundCount,
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getGameNumber(): GameNumber
    {
        return $this->start;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getPassedAroundCount(): int
    {
        return $this->passedAroundCount;
    }
}
