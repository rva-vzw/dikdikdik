<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\Event\Game\GameEvent;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;

/**
 * An event that indicates that a logged game was 'undone'.
 *
 * @template TAnnouncement of GameAnnouncement
 * @template TOutcome of GameOutcome
 * @extends GameEvent<TAnnouncement, TOutcome>
 */
interface GameUndone extends GameEvent, UndoEvent
{
}
