<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\Event\Game\AbstractGameEvent;
use App\Domain\Table\Event\Game\GameEvent;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;

/**
 * @template TA of GameAnnouncement
 * @template TO of GameOutcome
 * @extends AbstractGameEvent<TA, TO>
 * @implements GameUndone<TA, TO>
 */
abstract readonly class AbstractGameUndoneEvent extends AbstractGameEvent implements GameUndone
{
    /**
     * @param TA $announcement
     * @param TO $outcome
     */
    final public function __construct(
        TableIdentifier $tableIdentifier,
        GameNumber $gameNumber,
        GameAnnouncement $announcement,
        GameOutcome $outcome,
        PlayersInvolved $playersInvolved,
        private int $passedAroundCount,
    ) {
        parent::__construct($tableIdentifier, $gameNumber, $announcement, $outcome, $playersInvolved);
    }

    /**
     * @template TAnn of GameAnnouncement
     * @template TOut of GameOutcome
     *
     * @param TAnn $gameAnnouncement
     * @param TOut $gameOutcome
     *
     * @return GameUndone<TAnn, TOut>
     */
    public static function create(
        TableIdentifier $tableIdentifier,
        GameNumber $gameNumber,
        GameAnnouncement $gameAnnouncement,
        GameOutcome $gameOutcome,
        PlayersInvolved $playersInvolved,
        int $passedAroundCount,
    ): GameEvent {
        return new static($tableIdentifier, $gameNumber, $gameAnnouncement, $gameOutcome, $playersInvolved, $passedAroundCount);
    }

    public function getPassedAroundCount(): int
    {
        return $this->passedAroundCount;
    }
}
