<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\Loggable\Announcement\TroelAwareMultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\Winners;

/**
 * @extends AbstractGameUndoneEvent<TroelAwareMultiPlayerAnnouncement, Winners>
 * @implements GameUndone<TroelAwareMultiPlayerAnnouncement, Winners>
 */
final readonly class MisereOnTheTableUndone extends AbstractGameUndoneEvent implements GameUndone
{
}
