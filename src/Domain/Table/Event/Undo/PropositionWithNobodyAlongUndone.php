<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\TableIdentifier;

final class PropositionWithNobodyAlongUndone implements UndoEvent
{
    public function __construct(
        private readonly TableIdentifier $tableIdentifier,
        private readonly GameNumber $gameNumber,
        private readonly PlayerIdentifier $dealer,
        private readonly int $passedAroundCount,
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getGameNumber(): GameNumber
    {
        return $this->gameNumber;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPassedAroundCount(): int
    {
        return $this->passedAroundCount;
    }
}
