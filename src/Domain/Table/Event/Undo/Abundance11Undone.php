<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;

/**
 * @extends AbstractGameUndoneEvent<SinglePlayerAnnouncement, AllOrNothing>
 */
final readonly class Abundance11Undone extends AbstractGameUndoneEvent
{
}
