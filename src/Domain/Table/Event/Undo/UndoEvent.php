<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

interface UndoEvent extends Event
{
    public function getGameNumber(): GameNumber;

    public function getDealer(): PlayerIdentifier;

    public function getPassedAroundCount(): int;
}
