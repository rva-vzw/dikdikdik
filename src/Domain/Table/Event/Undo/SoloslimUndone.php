<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\Loggable\Announcement\TroelAwareSinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;

/**
 * @extends AbstractGameUndoneEvent<TroelAwareSinglePlayerAnnouncement, AllOrNothing>
 * @implements GameUndone<TroelAwareSinglePlayerAnnouncement, AllOrNothing>
 */
final readonly class SoloslimUndone extends AbstractGameUndoneEvent implements GameUndone
{
}
