<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;

final readonly class PassAroundUndone implements UndoEvent
{
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $gameNumber,
        public PlayersInvolved $playersInvolved,
        public int $count,
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getGameNumber(): GameNumber
    {
        return $this->gameNumber;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->playersInvolved->getDealerIdentifier();
    }

    public function getPassedAroundCount(): int
    {
        return $this->count - 1;
    }
}
