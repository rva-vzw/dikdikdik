<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Undo;

use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;

/**
 * @extends AbstractGameUndoneEvent<TwoPlayerAnnouncement, NumberOfTricks>
 * @implements GameUndone<TwoPlayerAnnouncement, NumberOfTricks>
 */
final readonly class PropAndCopUndone extends AbstractGameUndoneEvent implements GameUndone
{
}
