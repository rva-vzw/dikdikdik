<?php

namespace App\Domain\Table\Event;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class PlayerLeft implements Event
{
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public PlayerIdentifier $playerIdentifier,
        public GameNumber $gameNumber
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
