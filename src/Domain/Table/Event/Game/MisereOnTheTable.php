<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Game;

use App\Domain\Table\Loggable\Announcement\TroelAwareMultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\Winners;

/**
 * @extends AbstractGamePlayedEvent<TroelAwareMultiPlayerAnnouncement, Winners>
 */
final readonly class MisereOnTheTable extends AbstractGamePlayedEvent
{
}
