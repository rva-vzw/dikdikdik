<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Game;

use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;

/**
 * @extends AbstractGamePlayedEvent<TwoPlayerAnnouncement, NumberOfTricks>
 */
final readonly class Troel extends AbstractGamePlayedEvent
{
}
