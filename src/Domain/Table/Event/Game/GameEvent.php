<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Game;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

/**
 * A logged game, or a game that was undone.
 *
 * @template TAnnouncement of GameAnnouncement
 * @template TOutcome of GameOutcome
 */
interface GameEvent extends Event
{
    public function getTableIdentifier(): TableIdentifier;

    public function getGameNumber(): GameNumber;

    /** @return TAnnouncement */
    public function getAnnouncement(): GameAnnouncement;

    /** @return TOutcome */
    public function getOutcome(): GameOutcome;

    public function getPlayersInvolved(): PlayersInvolved;
}
