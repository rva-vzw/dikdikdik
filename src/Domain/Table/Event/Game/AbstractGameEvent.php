<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Game;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;

/**
 * @template TAnnouncement of GameAnnouncement
 * @template TOutcome of GameOutcome
 *
 * @implements GameEvent<TAnnouncement, TOutcome>
 */
abstract readonly class AbstractGameEvent implements GameEvent
{
    /**
     * @param TAnnouncement $announcement
     * @param TOutcome      $outcome
     */
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $gameNumber,
        public GameAnnouncement $announcement,
        public GameOutcome $outcome,
        private PlayersInvolved $playersInvolved,
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getGameNumber(): GameNumber
    {
        return $this->gameNumber;
    }

    /**
     * @return TAnnouncement
     */
    public function getAnnouncement(): GameAnnouncement
    {
        return $this->announcement;
    }

    /**
     * @return TOutcome
     */
    public function getOutcome(): GameOutcome
    {
        return $this->outcome;
    }

    public function getPlayersInvolved(): PlayersInvolved
    {
        return $this->playersInvolved;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->getPlayersInvolved()->getDealerIdentifier();
    }
}
