<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Game;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;

/**
 * @template TA of GameAnnouncement
 * @template TO of GameOutcome
 * @extends AbstractGameEvent<TA, TO>
 * @implements GamePlayed<TA, TO>
 */
abstract readonly class AbstractGamePlayedEvent extends AbstractGameEvent implements GamePlayed
{
    /**
     * @param TA $announcement
     * @param TO $outcome
     */
    final public function __construct(
        TableIdentifier $tableIdentifier,
        GameNumber $gameNumber,
        GameAnnouncement $announcement,
        GameOutcome $outcome,
        PlayersInvolved $playersInvolved,
        private \DateTimeImmutable $loggedAt,
    ) {
        parent::__construct($tableIdentifier, $gameNumber, $announcement, $outcome, $playersInvolved);
    }

    public function getLoggedAt(): \DateTimeImmutable
    {
        return $this->loggedAt;
    }

    /**
     * @template TAnn of GameAnnouncement
     * @template TOut of GameOutcome
     *
     * @param TAnn $gameAnnouncement
     * @param TOut $gameOutcome
     *
     * @return GamePlayed<TAnn, TOut>
     */
    public static function create(
        TableIdentifier $tableIdentifier,
        GameNumber $gameNumber,
        GameAnnouncement $gameAnnouncement,
        GameOutcome $gameOutcome,
        PlayersInvolved $playersInvolved,
        \DateTimeImmutable $loggedAt,
    ): GameEvent {
        return new static($tableIdentifier, $gameNumber, $gameAnnouncement, $gameOutcome, $playersInvolved, $loggedAt);
    }
}
