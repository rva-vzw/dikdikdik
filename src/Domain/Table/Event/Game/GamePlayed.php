<?php

namespace App\Domain\Table\Event\Game;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;
use DateTimeImmutable;

/**
 * An event that indicates that a game was played and logged.
 *
 * @template TAnnouncement of GameAnnouncement
 * @template TOutcome of GameOutcome
 * @extends GameEvent<TAnnouncement, TOutcome>
 */
interface GamePlayed extends GameEvent
{
    /**
     * Static constructor used by the denormalizer, and by Table::logSpecifiedGame().
     *
     * @template TA of GameAnnouncement
     * @template TO of GameOutcome
     *
     * @param TA $gameAnnouncement
     * @param TO $gameOutcome
     *
     * @return GamePlayed<TA, TO>
     */
    public static function create(
        TableIdentifier $tableIdentifier,
        GameNumber $gameNumber,
        GameAnnouncement $gameAnnouncement,
        GameOutcome $gameOutcome,
        PlayersInvolved $playersInvolved,
        DateTimeImmutable $loggedAt,
    ): GameEvent;

    public function getLoggedAt(): DateTimeImmutable;
}
