<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Game;

use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;

/**
 * @extends AbstractGamePlayedEvent<SinglePlayerAnnouncement, NumberOfTricks>
 */
final readonly class Alone5 extends AbstractGamePlayedEvent
{
}
