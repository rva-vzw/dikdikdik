<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Game;

use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\Winners;

/**
 * @extends AbstractGamePlayedEvent<MultiPlayerAnnouncement,Winners>
 */
final readonly class Misere extends AbstractGamePlayedEvent
{
}
