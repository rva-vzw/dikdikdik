<?php

declare(strict_types=1);

namespace App\Domain\Table\Event\Game;

use App\Domain\Table\Loggable\Announcement\TroelAwareSinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;

/**
 * @extends AbstractGamePlayedEvent<TroelAwareSinglePlayerAnnouncement, AllOrNothing>
 */
final readonly class Solo extends AbstractGamePlayedEvent
{
}
