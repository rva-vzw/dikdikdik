<?php

declare(strict_types=1);

namespace App\Domain\Table\Event;

use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

/**
 * Event that records nothing.
 *
 * This is a dummy event. I use it to easily remove events form the event stream. Instead of actually
 * removing an event, which would involve renumbering the subsequent events for the same aggregate, I can
 * just replace the faulty events by this dummy event.
 */
final readonly class NothingHappened implements Event
{
    public function __construct(
        private readonly TableIdentifier $tableIdentifier,
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
