<?php

declare(strict_types=1);

namespace App\Domain\Table\Event;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class PlayersReordered implements Event
{
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $gameNumber,
        public SeatedPlayers $newPlayerOrder
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
