<?php

declare(strict_types=1);

namespace App\Domain\Table\Event;

use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class FinalRoundAnnounced implements Event
{
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $start,
        public GameNumber $end
    ) {
    }

    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
