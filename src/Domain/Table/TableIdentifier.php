<?php

namespace App\Domain\Table;

use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;
use RvaVzw\KrakBoem\Id\Uuid4Identifier;

final readonly class TableIdentifier extends Uuid4Identifier implements AggregateRootIdentifier
{
    public function __toString(): string
    {
        return $this->toString();
    }
}
