<?php

declare(strict_types=1);

namespace App\Domain\Table\Player;

final readonly class OrderedPlayers extends AbstractUniquePlayerIdentifiers implements ArraySerializablePlayers
{
    /**
     * @param PlayerIdentifier[] $identifiers
     *
     * @return self
     */
    public static function fromArray(array $identifiers): ArraySerializablePlayers
    {
        return new self(array_values($identifiers));
    }

    public function toArray(): array
    {
        return $this->playerIdentifiers;
    }

    public function intersectedWith(PlayerCollection $other): self
    {
        return new self(
            array_filter(
                $this->playerIdentifiers,
                function (PlayerIdentifier $identifier) use ($other): bool {
                    return $other->has($identifier);
                }
            )
        );
    }
}
