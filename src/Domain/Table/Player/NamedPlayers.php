<?php

declare(strict_types=1);

namespace App\Domain\Table\Player;

/**
 * @implements \IteratorAggregate<string, NamedPlayer>
 */
final class NamedPlayers implements \IteratorAggregate
{
    /**
     * @param array<string, NamedPlayer> $players
     */
    private function __construct(private array $players = [])
    {
    }

    public static function none(): self
    {
        return new self([]);
    }

    /**
     * @param NamedPlayer[] $namedPlayers
     */
    public static function fromArray(array $namedPlayers): self
    {
        $arr = [];
        foreach ($namedPlayers as $player) {
            $arr[$player->getPlayerIdentifier()->toString()] = $player;
        }

        return new self($arr);
    }

    /**
     * @return \Traversable<string, NamedPlayer>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->players);
    }

    public function with(NamedPlayer $namedPlayer): self
    {
        $result = clone $this;
        $result->players[$namedPlayer->getPlayerIdentifier()->toString()] = $namedPlayer;

        return $result;
    }

    public function hasPlayer(PlayerIdentifier $playerIdentifier): bool
    {
        return array_key_exists($playerIdentifier->toString(), $this->players);
    }

    public function without(PlayerIdentifier $playerIdentifier): self
    {
        $result = clone $this;
        unset($result->players[$playerIdentifier->toString()]);

        return $result;
    }

    public function count(): int
    {
        return count($this->players);
    }

    public function getPlayerIdentifiers(): PlayerIdentifiers
    {
        return PlayerIdentifiers::fromArray(
            array_map(
                fn (NamedPlayer $player) => $player->getPlayerIdentifier(),
                $this->players,
            ),
        );
    }
}
