<?php

declare(strict_types=1);

namespace App\Domain\Table\Player;

final class NamedPlayer
{
    public function __construct(
        private PlayerIdentifier $playerIdentifier,
        private string $playerName,
    ) {
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getPlayerName(): string
    {
        return $this->playerName;
    }
}
