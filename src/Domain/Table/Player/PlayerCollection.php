<?php

declare(strict_types=1);

namespace App\Domain\Table\Player;

interface PlayerCollection
{
    public function has(PlayerIdentifier $playerIdentifier): bool;
}
