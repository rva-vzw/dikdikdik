<?php

declare(strict_types=1);

namespace App\Domain\Table\Player;

use App\Domain\Table\Exception\DuplicatePlayer;

/**
 * Unique player identifiers. Respects order.
 *
 * @implements \IteratorAggregate<mixed, PlayerIdentifier>
 */
abstract readonly class AbstractUniquePlayerIdentifiers implements \IteratorAggregate, PlayerCollection
{
    /** @var PlayerIdentifier[] */
    protected array $playerIdentifiers;

    /**
     * @param PlayerIdentifier[] $orderedIdentifiers
     */
    protected function __construct(array $orderedIdentifiers)
    {
        self::guardUniqueness($orderedIdentifiers);
        $this->playerIdentifiers = $orderedIdentifiers;
    }

    /**
     * @param PlayerIdentifier[] $identifiers
     *
     * @throws DuplicatePlayer
     */
    private static function guardUniqueness(array $identifiers): void
    {
        $playerIdentifiers = PlayerIdentifiers::fromArray($identifiers);
        if (count($identifiers) === $playerIdentifiers->getCount()) {
            return;
        }

        throw new DuplicatePlayer();
    }

    /**
     * @return \Traversable<int|string,PlayerIdentifier>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->playerIdentifiers);
    }

    public function has(PlayerIdentifier $playerIdentifier): bool
    {
        return false !== array_search($playerIdentifier, $this->playerIdentifiers);
    }

    public function hasSamePlayers(self $other): bool
    {
        /* @noinspection PhpNonStrictObjectEqualityInspection */
        return PlayerIdentifiers::fromArray($this->playerIdentifiers) == PlayerIdentifiers::fromArray($other->playerIdentifiers);
    }
}
