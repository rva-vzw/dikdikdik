<?php

namespace App\Domain\Table\Player;

interface ArraySerializablePlayers
{
    /**
     * @param PlayerIdentifier[] $identifiers
     *
     * @return static
     */
    public static function fromArray(array $identifiers): self;

    /**
     * @return PlayerIdentifier[]
     */
    public function toArray(): array;
}
