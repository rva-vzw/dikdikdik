<?php

declare(strict_types=1);

namespace App\Domain\Table\Player;

interface Players
{
    public function getPlayers(): PlayerIdentifiers;
}
