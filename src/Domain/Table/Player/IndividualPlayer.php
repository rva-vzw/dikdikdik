<?php

namespace App\Domain\Table\Player;

interface IndividualPlayer
{
    public function getPlayer(): PlayerIdentifier;
}
