<?php

namespace App\Domain\Table\Player;

use RvaVzw\KrakBoem\Id\Uuid4Identifier;
use Webmozart\Assert\Assert;

final readonly class PlayerIdentifier extends Uuid4Identifier
{
    /**
     * @return non-empty-string
     */
    public function __toString(): string
    {
        $stringRepresentation = $this->toString();
        // TODO: Fix dikdikdik issue #25
        Assert::notEmpty($stringRepresentation);

        return $stringRepresentation;
    }
}
