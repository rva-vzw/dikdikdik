<?php

declare(strict_types=1);

namespace App\Domain\Table\Player;

use App\Domain\Table\Exception\DealerUnknown;
use App\Domain\Table\Exception\ItTakesFourToPlay;
use Spatie\Cloneable\Cloneable;

/**
 * The players that are actually playing (so the players that can announce), and the dealer.
 */
final class PlayersInvolved
{
    use Cloneable;

    /** @var PlayerIdentifier */
    private $dealerIdentifier;

    /** @var PlayerIdentifiers */
    private $playingPlayers;

    public function __construct(PlayerIdentifiers $playingPlayers, PlayerIdentifier $dealerIdentifier)
    {
        self::guardFourPlayers($playingPlayers);
        $this->dealerIdentifier = $dealerIdentifier;
        $this->playingPlayers = $playingPlayers;
    }

    public static function from(SeatedPlayers $seatedPlayers, PlayerIdentifier $dealerIdentifier): self
    {
        $relevantPlayers = self::onlyPlayingPlayers($seatedPlayers, $dealerIdentifier);

        return new self($relevantPlayers, $dealerIdentifier);
    }

    public function getPlayingPlayers(): PlayerIdentifiers
    {
        return $this->playingPlayers;
    }

    private static function guardDealerKnown(SeatedPlayers $playersOrder, PlayerIdentifier $dealerIdentifier): void
    {
        if (!$playersOrder->has($dealerIdentifier)) {
            throw DealerUnknown::byPlayerIdentifier($dealerIdentifier);
        }
    }

    public function isPlaying(PlayerIdentifier $playerIdentifier): bool
    {
        return $this->playingPlayers->contains($playerIdentifier);
    }

    private static function onlyPlayingPlayers(
        SeatedPlayers $seatedPlayers,
        PlayerIdentifier $dealerIdentifier
    ): PlayerIdentifiers {
        self::guardDealerKnown($seatedPlayers, $dealerIdentifier);
        switch ($seatedPlayers->getCount()) {
            case 4:
                $relevantPlayers = $seatedPlayers->getPlayerIdentifiers();
                break;
            case 5:
                $relevantPlayers = $seatedPlayers->getPlayerIdentifiers()->without($dealerIdentifier);
                break;
            case 6:
                $seat = $seatedPlayers->getSeatOfPlayer($dealerIdentifier);
                $oppositeSeat = $seat->getOpposite();
                $oppositePlayer = $seatedPlayers->getPlayerAtSeat($oppositeSeat);
                $relevantPlayers = $seatedPlayers->getPlayerIdentifiers()->without(
                    $dealerIdentifier,
                    $oppositePlayer
                );
                break;
            default:
                return PlayerIdentifiers::none();
        }

        return $relevantPlayers;
    }

    private function guardFourPlayers(PlayerIdentifiers $relevantPlayers): void
    {
        if (4 === $relevantPlayers->getCount()) {
            return;
        }

        throw ItTakesFourToPlay::got($relevantPlayers->getCount());
    }

    public function getDealerIdentifier(): PlayerIdentifier
    {
        return $this->dealerIdentifier;
    }

    public function dealerIsPlaying(): bool
    {
        return $this->playingPlayers->contains($this->dealerIdentifier);
    }

    public function withDealer(PlayerIdentifier $dealerIdentifier): self
    {
        return $this->with(dealerIdentifier: $dealerIdentifier);
    }
}
