<?php

declare(strict_types=1);

namespace App\Domain\Table\Player;

use Traversable;
use Webmozart\Assert\Assert;

/**
 * @implements \IteratorAggregate<int, PlayerIdentifier>
 */
final class PlayerIdentifiers implements ArraySerializablePlayers, \IteratorAggregate
{
    /** @var PlayerIdentifier[] */
    private $identifiers;

    /**
     * @param PlayerIdentifier[] $identifiers associative array mapping string representation to identifier
     */
    private function __construct(array $identifiers)
    {
        $this->identifiers = $identifiers;
    }

    /**
     * @param PlayerIdentifier[] $identifiers
     *
     * @return self
     */
    public static function fromArray(array $identifiers): ArraySerializablePlayers
    {
        // This is very inefficient for large arrays, but for the moment
        // we only need small identifier lists.

        $associativeArray = array_reduce(
            $identifiers,
            function (array $associativeArray, PlayerIdentifier $identifier): array {
                $associativeArray[$identifier->toString()] = $identifier;

                return $associativeArray;
            },
            []
        );

        return new self($associativeArray);
    }

    public static function fromSingle(PlayerIdentifier $playerIdentifier): self
    {
        return self::fromArray([$playerIdentifier]);
    }

    public function contains(PlayerIdentifier $playerIdentifier): bool
    {
        return array_key_exists($playerIdentifier->toString(), $this->identifiers);
    }

    public function getCount(): int
    {
        return count($this->identifiers);
    }

    public function without(PlayerIdentifier ...$playerIdentifiers): self
    {
        $result = clone $this;

        foreach ($playerIdentifiers as $playerIdentifier) {
            $key = $playerIdentifier->toString();
            if (array_key_exists($key, $result->identifiers)) {
                unset($result->identifiers[$key]);
            }
        }

        return $result;
    }

    /**
     * @return list<PlayerIdentifier>
     */
    public function toArray(): array
    {
        return array_values($this->identifiers);
    }

    public function each(\Closure $callback): void
    {
        array_walk($this->identifiers, $callback);
    }

    public function isEmpty(): bool
    {
        return empty($this->identifiers);
    }

    public static function none(): self
    {
        return new self([]);
    }

    public function isSubsetOf(self $superset): bool
    {
        foreach ($this->identifiers as $playerIdentifier) {
            if (!$superset->contains($playerIdentifier)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return Traversable<int,PlayerIdentifier>
     */
    public function getIterator(): Traversable
    {
        return new \ArrayIterator($this->identifiers);
    }

    /**
     * @return string[]
     */
    public function toScalarList(): array
    {
        return array_keys($this->identifiers);
    }

    public function getSingle(): PlayerIdentifier
    {
        Assert::count($this->identifiers, 1);

        return array_values($this->identifiers)[0];
    }
}
