<?php

declare(strict_types=1);

namespace App\Domain\Table\Player;

use App\Domain\Table\Exception\DuplicatePlayer;
use App\Domain\Table\Exception\InvalidPlayerOrder;
use App\Domain\Table\Exception\NoSuchSeat;
use App\Domain\Table\Exception\SeatAlreadyTaken;
use App\Domain\Table\Exception\TableFull;
use App\Domain\Table\Exception\UnknownPlayer;
use App\Domain\Table\Seat;

/**
 * Seated players; player-ID's by seat number.
 *
 * Note that this is not a {@see ArraySerializablePlayers}, because we also need to know
 * which seats are empty.
 */
final readonly class SeatedPlayers extends AbstractUniquePlayerIdentifiers implements PlayerCollection
{
    /**
     * @param PlayerIdentifier[] $orderedIdentifiers associative array mapping seat no to identifier
     *
     * @return self
     */
    public static function fromArray(array $orderedIdentifiers): AbstractUniquePlayerIdentifiers
    {
        self::guardSeatNumbers(array_keys($orderedIdentifiers));

        // Order on search key
        ksort($orderedIdentifiers);

        return new self($orderedIdentifiers);
    }

    public function getCount(): int
    {
        return count($this->playerIdentifiers);
    }

    public function getPlayerAtSeat(Seat $seat): PlayerIdentifier
    {
        return $this->playerIdentifiers[$seat->toInteger()];
    }

    public function getPlayerIdentifiers(): PlayerIdentifiers
    {
        return PlayerIdentifiers::fromArray($this->playerIdentifiers);
    }

    public function getSeatOfPlayer(PlayerIdentifier $playerIdentifier): Seat
    {
        $result = $this->findSeatOfPlayer($playerIdentifier);

        if (null === $result) {
            throw new UnknownPlayer();
        }

        return $result;
    }

    /**
     * Returns player identifiers, ordered by seat number.
     *
     * @return PlayerIdentifier[]
     */
    public function toArray(): array
    {
        return $this->playerIdentifiers;
    }

    public function withSeatedPlayer(Seat $seat, PlayerIdentifier $playerIdentifier): self
    {
        if ($this->hasPlayerAtSeat($seat, $playerIdentifier)) {
            // nothing to be done
            return $this;
        }

        if ($this->seatIsTaken($seat)) {
            throw new SeatAlreadyTaken();
        }

        if ($this->has($playerIdentifier)) {
            throw DuplicatePlayer::byIdentifier($playerIdentifier);
        }

        $newPlayerIdentifiers = $this->playerIdentifiers + [$seat->toInteger() => $playerIdentifier];

        ksort($newPlayerIdentifiers);

        return new self($newPlayerIdentifiers);
    }

    /**
     * Player leaves, seat becomes empty.
     */
    public function withoutSeatedPlayer(PlayerIdentifier $playerIdentifier): self
    {
        $newPlayerIdentifiers = $this->playerIdentifiers;
        $seat = $this->findSeatOfPlayer($playerIdentifier);
        if ($seat instanceof Seat) {
            unset($newPlayerIdentifiers[$seat->toInteger()]);
        }

        return new self($newPlayerIdentifiers);
    }

    /** @psalm-suppress InvalidReturnType */
    public function getPlayerNextTo(PlayerIdentifier $playerIdentifier): PlayerIdentifier
    {
        $seat = $this->getSeatOfPlayer($playerIdentifier);

        while (true) {
            $seat = $seat->getNext();
            if ($this->seatIsTaken($seat)) {
                return $this->getPlayerAtSeat($seat);
            }
        }
    }

    public function seatIsTaken(Seat $seat): bool
    {
        return array_key_exists($seat->toInteger(), $this->playerIdentifiers)
            && $this->playerIdentifiers[$seat->toInteger()] instanceof PlayerIdentifier;
    }

    /**
     * @param int[] $seatNumbers
     */
    private static function guardSeatNumbers(array $seatNumbers): void
    {
        if (empty($seatNumbers)) {
            return;
        }

        if (min($seatNumbers) >= 0 && max($seatNumbers) < Seat::NUMBER_OF_SEATS) {
            return;
        }

        throw new NoSuchSeat();
    }

    public static function none(): self
    {
        return new self([]);
    }

    public function hasPlayerAtSeat(Seat $seat, PlayerIdentifier $playerIdentifier): bool
    {
        $seatNumber = $seat->toInteger();

        return array_key_exists($seatNumber, $this->playerIdentifiers)
            && $this->playerIdentifiers[$seatNumber] == $playerIdentifier;
    }

    private function findSeatOfPlayer(PlayerIdentifier $playerIdentifier): ?Seat
    {
        $position = array_search($playerIdentifier, $this->playerIdentifiers);

        if (false === $position) {
            return null;
        }

        /** @var int $positionInt */
        $positionInt = $position;

        return Seat::fromInteger($positionInt);
    }

    public function getFreeSeat(): Seat
    {
        $seat = Seat::zero();

        while ($this->seatIsTaken($seat)) {
            $seat = $seat->getNext();

            if ($seat == Seat::zero()) {
                throw TableFull::noFreeSeats();
            }
        }

        return $seat;
    }

    public function reordered(OrderedPlayers $newPlayerOrder): self
    {
        if (!$this->hasSamePlayers($newPlayerOrder)) {
            throw new InvalidPlayerOrder();
        }

        $combinedIterable = new \MultipleIterator(\MultipleIterator::MIT_KEYS_ASSOC);
        $combinedIterable->attachIterator(
            new \IteratorIterator($this->getOccupiedSeats()),
            'seat'
        );
        $combinedIterable->attachIterator(
            new \IteratorIterator($newPlayerOrder),
            'player'
        );

        $result = SeatedPlayers::none();
        foreach ($combinedIterable as $item) {
            $result = $result->withSeatedPlayer(
                $item['seat'],
                $item['player']
            );
        }

        return $result;
    }

    /**
     * @return \Traversable<Seat>
     */
    public function getOccupiedSeats(): \Traversable
    {
        foreach (array_keys($this->playerIdentifiers) as $seatNr) {
            yield Seat::fromInteger($seatNr);
        }
    }

    /**
     * @return \Traversable<int, PlayerIdentifier>
     */
    public function getIterator(): \Traversable
    {
        /** @var \Traversable<int, PlayerIdentifier> $result */
        $result = parent::getIterator();

        return $result;
    }

    public function getPlayerAtPreviousSeat(PlayerIdentifier $playerIdentifier): PlayerIdentifier
    {
        $seat = $this->getSeatOfPlayer($playerIdentifier);

        for ($i = 0; $i < Seat::NUMBER_OF_SEATS; ++$i) {
            $seat = $seat->getPrevious();
            if ($this->seatIsTaken($seat)) {
                return $this->getPlayerAtSeat($seat);
            }
        }

        throw UnknownPlayer::withIdentifier($playerIdentifier);
    }

    public function hasFreeSeat(): bool
    {
        $seat = Seat::zero();

        while ($this->seatIsTaken($seat)) {
            $seat = $seat->getNext();

            if ($seat == Seat::zero()) {
                return false;
            }
        }

        return true;
    }
}
