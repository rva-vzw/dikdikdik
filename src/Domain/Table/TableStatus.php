<?php

declare(strict_types=1);

namespace App\Domain\Table;

enum TableStatus
{
    case NEW;
    case BUSY;
    case ON_HOLD;
    case DONE;
    public function hasStarted(): bool
    {
        return self::BUSY === $this || self::ON_HOLD === $this;
    }
}
