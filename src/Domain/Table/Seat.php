<?php

declare(strict_types=1);

namespace App\Domain\Table;

use App\Domain\Table\Exception\NoSuchSeat;
use RvaVzw\KrakBoem\Id\IntWrapper;

/**
 * 0-based seat number.
 */
final class Seat implements IntWrapper
{
    public const NUMBER_OF_SEATS = 6;

    /** @var int */
    private $value;

    private function __construct(int $value)
    {
        $this->value = $value;
    }

    public static function fromInteger(int $value): self
    {
        self::guardSeatNumber($value);

        return new self($value);
    }

    public function toInteger(): int
    {
        return $this->value;
    }

    private static function guardSeatNumber(int $number): void
    {
        if ($number < 0 || $number >= self::NUMBER_OF_SEATS) {
            throw new NoSuchSeat();
        }
    }

    public static function zero(): self
    {
        return self::fromInteger(0);
    }

    public static function one(): self
    {
        return self::fromInteger(1);
    }

    public static function two(): self
    {
        return self::fromInteger(2);
    }

    public static function three(): self
    {
        return self::fromInteger(3);
    }

    public static function four(): self
    {
        return self::fromInteger(4);
    }

    public static function five(): self
    {
        return self::fromInteger(5);
    }

    public function getOpposite(): self
    {
        return self::fromInteger(
            ($this->toInteger() + self::NUMBER_OF_SEATS / 2) % self::NUMBER_OF_SEATS
        );
    }

    public function getNext(): self
    {
        return self::fromInteger(
            ($this->toInteger() + 1) % self::NUMBER_OF_SEATS
        );
    }

    public function getPrevious(): self
    {
        return self::fromInteger(
            ($this->toInteger() + self::NUMBER_OF_SEATS - 1) % self::NUMBER_OF_SEATS
        );
    }
}
