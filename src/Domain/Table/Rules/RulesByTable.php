<?php

declare(strict_types=1);

namespace App\Domain\Table\Rules;

use App\Domain\Table\TableIdentifier;

interface RulesByTable
{
    public function getPassAroundRules(TableIdentifier $tableIdentifier): PassAroundRules;

    public function getLoggables(TableIdentifier $tableIdentifier): Loggables;
}
