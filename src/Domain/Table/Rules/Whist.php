<?php

declare(strict_types=1);

namespace App\Domain\Table\Rules;

/**
 * This interface just holds some global constants.
 *
 * It may be that I'll move {@see self::MAX_DEALER_ATTEMPTS} to
 * {@see \App\Domain\TableConventions\Conventions} some day; not sure yet.
 */
interface Whist
{
    public const PLAYERS_NEEDED_TO_PLAY = 4;

    public const MAX_DEALER_ATTEMPTS = 3;

    public const DEFAULT_CONVENTIONS_NAME = 'conventions.traditional';
}
