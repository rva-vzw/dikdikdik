<?php

declare(strict_types=1);

namespace App\Domain\Table\Rules;

enum PassAroundRules: string
{
    case Traditional = 'traditional';
    case Simplified = 'simplified';
}
