<?php

declare(strict_types=1);

namespace App\Domain\Table\Rules;

use App\Domain\Table\Exception\UnknownLoggable;
use App\Domain\Table\Loggable\GameSpecification\GameSpecification;
use App\Domain\Table\Loggable\Loggable;
use Traversable;

/**
 * @implements \IteratorAggregate<Loggable>
 */
final readonly class Loggables implements \IteratorAggregate
{
    /**
     * @param Loggable[] $loggables
     */
    private function __construct(
        private array $loggables,
    ) {
    }

    /**
     * @param Traversable<GameSpecification> $specifiedGames
     */
    public static function fromSpecifiedGames(Traversable $specifiedGames): self
    {
        return new self(iterator_to_array($specifiedGames));
    }

    public function prependedWith(Loggable $loggable): self
    {
        $loggables = $this->loggables;
        array_unshift($loggables, $loggable);

        return new self($loggables);
    }

    public function getByName(string $name): Loggable
    {
        foreach ($this->loggables as $loggable) {
            if ($loggable->getName() == $name) {
                return $loggable;
            }
        }

        throw UnknownLoggable::byName($name);
    }

    /**
     * @return Traversable<Loggable>
     */
    public function getIterator(): Traversable
    {
        return new \ArrayIterator($this->loggables);
    }
}
