<?php

declare(strict_types=1);

namespace App\Domain\Table\Rules;

use App\Domain\Table\Loggable\GameSpecification\GameSpecifications;

interface RulesByConvention
{
    /**
     * @param non-empty-string $conventionName
     */
    public function getPermissibleGames(string $conventionName): GameSpecifications;

    /**
     * @param non-empty-string $conventionName
     */
    public function getPassAroundRules(string $conventionName): PassAroundRules;
}
