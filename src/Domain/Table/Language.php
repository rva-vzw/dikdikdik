<?php

declare(strict_types=1);

namespace App\Domain\Table;

final class Language
{
    /** @var string[] */
    private static array $supportedLanguageCodes = [
        'en',
        'nl',
    ];

    final private function __construct(private string $languageCode)
    {
    }

    public static function fromString(string $languageCode): self
    {
        $languageCode = strtolower($languageCode);
        if (in_array($languageCode, self::$supportedLanguageCodes)) {
            return new self($languageCode);
        }

        throw new \InvalidArgumentException("Unsupported language {$languageCode}.");
    }

    public static function en(): self
    {
        return new self('en');
    }

    public function getLanguageCode(): string
    {
        return $this->languageCode;
    }

    /**
     * @return \Traversable<self>
     */
    public static function supportedLanguages(): \Traversable
    {
        foreach (self::$supportedLanguageCodes as $languageCode) {
            yield new self($languageCode);
        }
    }

    public function __toString(): string
    {
        return $this->languageCode;
    }
}
