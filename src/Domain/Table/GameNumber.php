<?php

namespace App\Domain\Table;

use RvaVzw\KrakBoem\Id\IntWrapper;

final class GameNumber implements IntWrapper
{
    /**
     * @var int
     */
    private $value;

    private function __construct(int $value)
    {
        $this->value = $value;
        $this->guardStrictlyPositive();
    }

    public static function third(): self
    {
        return self::fromInteger(3);
    }

    public function toInteger(): int
    {
        return $this->value;
    }

    /**
     * @return self
     */
    public static function fromInteger(int $value): IntWrapper
    {
        return new self($value);
    }

    public function getNext(): self
    {
        return self::fromInteger($this->toInteger() + 1);
    }

    public static function first(): self
    {
        return self::fromInteger(1);
    }

    public function increased(int $count): self
    {
        return self::fromInteger($this->toInteger() + $count);
    }

    private function guardStrictlyPositive(): void
    {
        if ($this->value > 0) {
            return;
        }

        throw new \InvalidArgumentException();
    }

    public function getPrevious(): self
    {
        return self::fromInteger($this->toInteger() - 1);
    }

    public function isSmallerThan(self $other): bool
    {
        return $this->toInteger() < $other->toInteger();
    }

    public static function second(): self
    {
        return self::fromInteger(2);
    }

    public function isNotBiggerThan(GameNumber $other): bool
    {
        return $this->toInteger() <= $other->toInteger();
    }

    public function isBiggerThan(GameNumber $other): bool
    {
        return $this->toInteger() > $other->toInteger();
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }
}
