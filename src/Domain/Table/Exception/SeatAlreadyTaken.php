<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

use Exception;

final class SeatAlreadyTaken extends Exception
{
}
