<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

use Exception;

final class UnsupportedByRules extends Exception
{
    public static function byGameType(string $gameTypeName): self
    {
        return new self("Game type $gameTypeName not supported by the current rules");
    }

    public static function traditionalPassAroundNotSupported(): self
    {
        return new self('Traditional passing around not supported by current rules');
    }
}
