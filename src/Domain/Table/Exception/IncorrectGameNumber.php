<?php

namespace App\Domain\Table\Exception;

use App\Domain\Table\GameNumber;

final class IncorrectGameNumber extends \Exception
{
    public static function create(
        GameNumber $expectedGameNumber,
        GameNumber $actualGameNumber,
    ): self {
        return new self("Unexpected game number. Expected {$expectedGameNumber}, got {$actualGameNumber}");
    }
}
