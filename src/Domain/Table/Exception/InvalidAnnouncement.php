<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

use Exception;

final class InvalidAnnouncement extends Exception
{
    public static function noAnnouncer(): self
    {
        return new self('At least one player has to announce');
    }

    public static function tooManyAnnouncers(): self
    {
        return new self('At most 4 players can announce');
    }
}
