<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

final class InvalidNumberOfTricks extends \Exception
{
}
