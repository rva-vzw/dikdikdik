<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

use App\Domain\Table\TableIdentifier;
use Exception;

class GameNotFound extends Exception
{
    public static function finalRoundNotAnnounced(TableIdentifier $tableIdentifier): self
    {
        return new self(
            "Final game not announced on table {$tableIdentifier}",
        );
    }
}
