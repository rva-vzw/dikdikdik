<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

use Exception;

final class TableFull extends Exception
{
    public static function noFreeSeats(): self
    {
        return new self('No free seats at table.');
    }
}
