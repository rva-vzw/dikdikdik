<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

final class UnknownLoggable extends \LogicException
{
    public static function byName(string $name): self
    {
        return new self("Unknown loggable: {$name}");
    }
}
