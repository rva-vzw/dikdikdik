<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

use App\Domain\Table\Player\PlayerIdentifier;
use Exception;

final class UnknownPlayer extends Exception
{
    public static function withIdentifier(PlayerIdentifier $playerIdentifier): self
    {
        return new self(
            sprintf('Player with identifier %s not found', $playerIdentifier->toString())
        );
    }
}
