<?php

namespace App\Domain\Table\Exception;

use App\Domain\Table\Player\PlayerIdentifier;

final class DealerUnknown extends \Exception
{
    public static function byPlayerIdentifier(PlayerIdentifier $playerIdentifier): self
    {
        return new self("Unknown dealer: {$playerIdentifier}");
    }

    public static function noDealerAnnounced(): self
    {
        return new self('Unknown dealer: no dealer was announced');
    }
}
