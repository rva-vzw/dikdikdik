<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

use Exception;

final class ItTakesFourToPlay extends Exception
{
    public static function got(int $count): self
    {
        return new self("It takes 4 players to play; got {$count} player(s)");
    }
}
