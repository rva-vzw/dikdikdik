<?php

declare(strict_types=1);

namespace App\Domain\Table\Exception;

use App\Domain\Table\Player\PlayerIdentifier;
use Exception;

final class DuplicatePlayer extends Exception
{
    public static function byIdentifier(PlayerIdentifier $playerIdentifier): self
    {
        return new self("Duplicate player: {$playerIdentifier}.");
    }
}
