<?php

declare(strict_types=1);

namespace App\Domain\Table\Command;

use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;

/**
 * Marker interface for commands handled by the TableDecider.
 */
interface TableCommand extends Command
{
}
