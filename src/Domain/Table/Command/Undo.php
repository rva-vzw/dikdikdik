<?php

declare(strict_types=1);

namespace App\Domain\Table\Command;

use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;

final class Undo implements TableCommand
{
    use TableCommandTrait;

    /**
     * @param GameNumber $currentGameNumber The current game number. This is just used to
     *                                      prevent race conditions. Don't pass the game number
     *                                      of the action you want to undo; just pass the
     *                                      current game number.
     */
    public function __construct(
        public readonly TableIdentifier $tableIdentifier,
        public readonly GameNumber $currentGameNumber,
    ) {
    }
}
