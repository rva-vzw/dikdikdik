<?php

declare(strict_types=1);

namespace App\Domain\Table\Command;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;

final readonly class JoinPlayer implements TableCommand
{
    use TableCommandTrait;

    /**
     * @param non-empty-string $playerName
     */
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public PlayerIdentifier $playerIdentifier,
        public GameNumber $gameNumber,
        public string $playerName,
        public ?Seat $seat,
    ) {
    }
}
