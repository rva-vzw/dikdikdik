<?php

namespace App\Domain\Table\Command;

use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;

final readonly class PassAround implements TableCommand
{
    use TableCommandTrait;

    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $gameNumber
    ) {
    }
}
