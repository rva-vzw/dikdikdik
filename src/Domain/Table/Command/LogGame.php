<?php

declare(strict_types=1);

namespace App\Domain\Table\Command;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\GameSpecification;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\TableIdentifier;

final readonly class LogGame implements TableCommand
{
    use TableCommandTrait;

    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $gameNumber,
        public GameSpecification $gameSpecification,
        public GameAnnouncement $gameAnnouncement,
        public GameOutcome $gameOutcome,
        public \DateTimeImmutable $loggedAt,
    ) {
    }
}
