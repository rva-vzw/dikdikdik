<?php

declare(strict_types=1);

namespace App\Domain\Table\Command;

use App\Domain\Table\TableIdentifier;

final readonly class ClearTable implements TableCommand
{
    use TableCommandTrait;

    public function __construct(
        public TableIdentifier $tableIdentifier,
    ) {
    }
}
