<?php

declare(strict_types=1);

namespace App\Domain\Table\Command;

use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\TableIdentifier;

final readonly class StarPlayer implements TableCommand
{
    use TableCommandTrait;

    public function __construct(
        public TableIdentifier $tableIdentifier,
        public GameNumber $gameNumber,
        public PlayerIdentifier $playerIdentifier
    ) {
    }
}
