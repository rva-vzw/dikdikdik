<?php

declare(strict_types=1);

namespace App\Domain\Table\Command;

use App\Domain\Table\TableIdentifier;

trait TableCommandTrait
{
    public function getAggregateRootIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
