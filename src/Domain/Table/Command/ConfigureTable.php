<?php

declare(strict_types=1);

namespace App\Domain\Table\Command;

use App\Domain\Table\TableIdentifier;

final readonly class ConfigureTable implements TableCommand
{
    use TableCommandTrait;

    /**
     * @param non-empty-string $ruleSetName
     */
    public function __construct(
        public TableIdentifier $tableIdentifier,
        public string $ruleSetName,
    ) {
    }
}
