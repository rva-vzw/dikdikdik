<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use Exception;

final class GameOver extends Exception
{
}
