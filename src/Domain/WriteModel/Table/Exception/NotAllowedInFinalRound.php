<?php

namespace App\Domain\WriteModel\Table\Exception;

use Exception;

final class NotAllowedInFinalRound extends Exception
{
}
