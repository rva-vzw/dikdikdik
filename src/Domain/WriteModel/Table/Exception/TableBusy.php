<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use App\Domain\Table\TableIdentifier;
use Exception;

final class TableBusy extends Exception
{
    public static function byTableIdentifier(TableIdentifier $tableIdentifier): self
    {
        return new self("Table {$tableIdentifier} is busy");
    }
}
