<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use App\Domain\Table\TableIdentifier;

final class NothingToUndo extends \Exception
{
    public static function atTable(TableIdentifier $tableIdentifier): self
    {
        return new self("Nothing to undo at table {$tableIdentifier}");
    }
}
