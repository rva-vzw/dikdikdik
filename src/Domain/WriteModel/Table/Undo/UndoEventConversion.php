<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Undo;

use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Event\Undo\GameUndone;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Loggable\Outcome\GameOutcome;

/**
 * This class converts GamePlayed events to UndoEvents and vice versa.
 *
 * I'm not very confident yet in this class 😉
 */
final readonly class UndoEventConversion
{
    public function __construct(
        private AllSpecifiedGames $specifiedGames,
    ) {
    }

    /**
     * @template TAnnouncement of GameAnnouncement
     * @template TOutcome of GameOutcome
     *
     * @param GamePlayed<TAnnouncement, TOutcome> $event
     *
     * @return GameUndone<TAnnouncement, TOutcome>
     */
    public function getCorrespondingUndoEvent(GamePlayed $event, int $passedAroundCount): GameUndone
    {
        $gameSpecification = $this->specifiedGames->getByEvent($event);

        $reflectionMethod = new \ReflectionMethod($gameSpecification->getUndoEventType(), 'create');
        /** @var GameUndone<TAnnouncement, TOutcome> $event */
        $event = $reflectionMethod->invoke(
            null,
            $event->getTableIdentifier(),
            $event->getGameNumber(),
            $event->getAnnouncement(),
            $event->getOutcome(),
            $event->getPlayersInvolved(),
            $passedAroundCount,
        );

        return $event;
    }

    /**
     * @template TAnnouncement of GameAnnouncement
     * @template TOutcome of GameOutcome
     *
     * @param GameUndone<TAnnouncement, TOutcome> $event
     *
     * @return GamePlayed<TAnnouncement, TOutcome>
     */
    public function getCorrespondingEventForUndo(GameUndone $event): GamePlayed
    {
        $gameSpecification = $this->specifiedGames->getByUndoEvent($event);
        $reflectionMethod = new \ReflectionMethod($gameSpecification->getEventType(), 'create');

        /** @var GamePlayed<TAnnouncement, TOutcome> $event */
        $event = $reflectionMethod->invoke(
            null,
            $event->getTableIdentifier(),
            $event->getGameNumber(),
            $event->getAnnouncement(),
            $event->getOutcome(),
            $event->getPlayersInvolved(),
            // 😢
            new \DateTimeImmutable(),
        );

        return $event;
    }
}
