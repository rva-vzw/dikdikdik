<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table;

use App\Domain\Table\Command\TableCommand;
use App\Domain\WriteModel\Table\Aggregate\TableDecider;
use App\Domain\WriteModel\Table\Aggregate\TableInternalState;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandHandler;
use RvaVzw\KrakBoem\Cqrs\Decider\EventSourcedCommandHandler;
use RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;

/**
 * @extends EventSourcedCommandHandler<TableInternalState, TableCommand>
 */
final readonly class TableCommandHandler extends EventSourcedCommandHandler implements CommandHandler
{
    public function __construct(EventStore $eventStore, EventBus $eventBus, TableDecider $decider)
    {
        parent::__construct($eventStore, $eventBus, $decider);
    }

    public function __invoke(TableCommand $command): void
    {
        $this->handle($command);
    }
}
