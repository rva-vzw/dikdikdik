<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Aggregate;

use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\AnnounceFinalRound;
use App\Domain\Table\Command\ClearTable;
use App\Domain\Table\Command\ConfigureTable;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\KickPlayer;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\Command\ProposeAndNobodyGoesAlong;
use App\Domain\Table\Command\RejoinKnownPlayer;
use App\Domain\Table\Command\ReorderPlayers;
use App\Domain\Table\Command\StarPlayer;
use App\Domain\Table\Command\Undo;
use App\Domain\Table\Event\AllUndone;
use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\Event\DealerFailed;
use App\Domain\Table\Event\DealerLeft;
use App\Domain\Table\Event\FinalRoundAnnounced;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Event\PassedAround;
use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\Event\PlayerLeft;
use App\Domain\Table\Event\PlayersReordered;
use App\Domain\Table\Event\PlayerStarred;
use App\Domain\Table\Event\ProposedWithNobodyAlong;
use App\Domain\Table\Event\TableCleared;
use App\Domain\Table\Event\TableConfigured;
use App\Domain\Table\Event\TableStartedPlaying;
use App\Domain\Table\Event\Undo\FinalRoundAnnouncementUndone;
use App\Domain\Table\Event\Undo\PassAroundUndone;
use App\Domain\Table\Event\Undo\PlayerStarUndone;
use App\Domain\Table\Event\Undo\PropositionWithNobodyAlongUndone;
use App\Domain\Table\Event\Undo\UndoEvent;
use App\Domain\Table\Exception\IncorrectGameNumber;
use App\Domain\Table\Exception\InvalidAnnouncement;
use App\Domain\Table\Exception\UnsupportedByRules;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\AnnouncementValidator;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Rules\RulesByConvention;
use App\Domain\Table\Rules\Whist;
use App\Domain\Table\TableIdentifier;
use App\Domain\Table\TableStatus;
use App\Domain\WriteModel\Table\Exception\NothingToUndo;
use App\Domain\WriteModel\Table\Exception\TableBusy;
use App\Domain\WriteModel\Table\Undo\UndoEventConversion;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Cqrs\Decider\AggregateInternalState;
use RvaVzw\KrakBoem\Cqrs\Decider\Decider;
use RvaVzw\KrakBoem\EventSourcing\Event;
use Webmozart\Assert\Assert;

/**
 * The 'table' in this class refers to a piece of furniture, not a database thing 😉.
 *
 * @implements Decider<TableInternalState>
 */
final readonly class TableDecider implements Decider
{
    public function __construct(
        private AnnouncementValidator $announcementValidator,
        private RulesByConvention $rulesByConvention,
        private UndoEventConversion $undoEventConversion,
    ) {
    }

    public function decide(AggregateInternalState $internalState, Command $command): \Traversable
    {
        yield from match ($command::class) {
            ClearTable::class => $this->clearTable($internalState, $command),
            JoinPlayer::class => $this->joinPlayer($internalState, $command),
            KickPlayer::class => $this->kickPlayer($internalState, $command),
            AnnounceDealer::class => $this->announceDealer($internalState, $command),
            PassAround::class => $this->passAround($internalState, $command),
            ProposeAndNobodyGoesAlong::class => $this->proposeWithNobodyAlong($internalState, $command),
            LogGame::class => $this->logGame($internalState, $command),
            StarPlayer::class => $this->starPlayer($internalState, $command),
            Undo::class => $this->undo($internalState, $command),
            ReorderPlayers::class => $this->reorderPlayers($internalState, $command),
            AnnounceFinalRound::class => $this->announceFinalRound($internalState, $command),
            ConfigureTable::class => $this->configureTable($internalState, $command),
            RejoinKnownPlayer::class => $this->rejoinPlayer($internalState, $command),
            default => [], // return no events if we don't know how to handle the command
        };
    }

    public function evolve(AggregateInternalState $internalState, Event $event): AggregateInternalState
    {
        // handle undo-events in dedicated function:
        if ($event instanceof UndoEvent) {
            return $this->afterUndo($internalState, $event);
        }

        // when a game is played, we just need to push the undo-event;
        // the played games do not affect the internal state of the decider.
        if ($event instanceof GamePlayed) {
            return $internalState->withUndoEvent(
                $this->undoEventConversion->getCorrespondingUndoEvent($event, $internalState->passedAroundCount),
            );
        }

        return match ($event::class) {
            PlayerJoined::class => $this->afterPlayerJoined($internalState, $event),
            DealerAnnounced::class => $this->afterDealerAnnounced($internalState, $event),
            DealerLeft::class => $this->afterDealerLeft($internalState, $event),
            PlayerLeft::class => $this->afterPlayerLeft($internalState, $event),
            TableStartedPlaying::class => $this->afterTableStartedPlaying($internalState, $event),
            PassedAround::class => $this->afterPassedAround($internalState, $event),
            ProposedWithNobodyAlong::class => $this->afterProposedWithNobodyAlong($internalState, $event),
            PlayerStarred::class => $this->afterPlayerStarred($internalState, $event),
            AllUndone::class => $this->afterAllUndone($internalState),
            PlayersReordered::class => $this->afterPlayersReordered($internalState, $event),
            FinalRoundAnnounced::class => $this->afterFinalRoundAnnounced($internalState, $event),
            TableConfigured::class => $this->afterTableConfigured($internalState, $event),
            default => $internalState,
        };
    }

    public function getInitialState(): AggregateInternalState
    {
        return TableInternalState::createEmpty()->with(
            allowedGames: $this->rulesByConvention->getPermissibleGames(
                Whist::DEFAULT_CONVENTIONS_NAME,
            ),
        );
    }

    public function isTerminal(AggregateInternalState $internalState): bool
    {
        return TableStatus::DONE === $internalState->tableStatus;
    }

    /** @return \Traversable<Event> */
    private function clearTable(
        TableInternalState $internalState,
        ClearTable $command,
    ): \Traversable {
        yield new TableCleared($command->tableIdentifier);
    }

    /** @return \Traversable<Event> */
    private function joinPlayer(
        TableInternalState $internalState,
        JoinPlayer $command,
    ): \Traversable {
        $internalState->guardValidGameNumber($command->gameNumber);
        $internalState->guardFinalRoundNotAnnounced();

        $seatedPlayers = $internalState->seatedPlayers;

        if (
            $internalState->hasSeatedPlayer($command->playerIdentifier)
            && (
                null === $command->seat // seat doesn't matter
                || $seatedPlayers->getSeatOfPlayer($command->playerIdentifier) == $command->seat
            )
        ) {
            // player already there, and seat is OK, so we're fine.
            yield from [];

            return;
        }

        $seat = $command->seat ?? $seatedPlayers->getFreeSeat();

        // Check whether we can actually put the new player on their seat. If this is not possible,
        // an exception will be thrown. Note that we don't update the internal state here; that happens in the
        // 'evolve' function.
        $newPlayerCount = $seatedPlayers->withSeatedPlayer($seat, $command->playerIdentifier)
            ->getCount();

        yield new PlayerJoined(
            $command->tableIdentifier,
            $command->playerIdentifier,
            $command->gameNumber,
            $seat,
            $command->playerName,
        );

        if (Whist::PLAYERS_NEEDED_TO_PLAY <= $newPlayerCount && !$internalState->tableStatus->hasStarted()) {
            // We can already announce a dealer, so that the user doesn't need to bother if he doesn't want
            // to. (see #172)
            yield new DealerAnnounced(
                $command->tableIdentifier,
                $command->playerIdentifier,
                $command->gameNumber,
            );
        }
    }

    private function afterPlayerJoined(TableInternalState $internalState, PlayerJoined $event): TableInternalState
    {
        return $internalState->withSeatedPlayer(
            $event->seat,
            $event->playerIdentifier,
            $event->playerName,
        );
    }

    private function afterDealerAnnounced(
        TableInternalState $internalState,
        DealerAnnounced $event
    ): TableInternalState {
        return $internalState->readyForGame(
            // We need to pass the game number, because in some undo-cases, it is possible
            // that a dealer is announced for the previous game.
            $event->gameNumber,
            $event->playerIdentifier,
        );
    }

    private function afterUndo(TableInternalState $internalState, UndoEvent $event): TableInternalState
    {
        $remainingUndoEvents = $internalState->undoEvents;
        array_pop($remainingUndoEvents);

        $updatedState = $internalState->with(
            currentGame: $event->getGameNumber(),
            passedAroundCount: $event->getPassedAroundCount(),
            undoEvents: $remainingUndoEvents,
        );

        // This is admittedly rather hacky:

        return match ($event::class) {
            PassAroundUndone::class => $updatedState->with(passedAroundCount: $event->getPassedAroundCount() - 1),
            PlayerStarUndone::class => $updatedState->withPlayerStars($event->playerIdentifier, $event->numberOfStars - 1),
            default => $updatedState,
        };
    }

    /**
     * @return \Traversable<Event>
     */
    private function kickPlayer(
        TableInternalState $internalState,
        KickPlayer $command,
    ): \Traversable {
        $internalState->guardValidGameNumber($command->gameNumber);
        $internalState->guardFinalRoundNotAnnounced();
        $internalState->guardPlayerIsPresent($command->playerIdentifier);

        if ($internalState->dealerIdentifier == $command->playerIdentifier) {
            // Whoa, dealer is leaving!
            if (Whist::PLAYERS_NEEDED_TO_PLAY < $internalState->getNumberOfPlayers()) {
                // If still enough players, announce next dealer.
                yield new DealerAnnounced(
                    $command->tableIdentifier,
                    $internalState->seatedPlayers->getPlayerNextTo($command->playerIdentifier),
                    $command->gameNumber,
                );
            } else {
                yield new DealerLeft($command->tableIdentifier, $command->gameNumber);
            }
        }

        yield new PlayerLeft(
            $command->tableIdentifier,
            $command->playerIdentifier,
            $command->gameNumber,
        );
    }

    private function afterDealerLeft(TableInternalState $internalState, DealerLeft $event): TableInternalState
    {
        return $internalState->with(dealerIdentifier: null);
    }

    private function afterPlayerLeft(TableInternalState $internalState, PlayerLeft $event): TableInternalState
    {
        return $internalState->withPlayerLeft($event->playerIdentifier);
    }

    /**
     * @return \Traversable<Event>
     */
    private function announceDealer(TableInternalState $internalState, AnnounceDealer $command): \Traversable
    {
        $internalState->guardValidGameNumber($command->gameNumber);
        $internalState->guardNumberOfPlayers();
        $internalState->guardPlayerIsPresent($command->playerIdentifier);

        if ($command->playerIdentifier == $internalState->dealerIdentifier) {
            return;
        }

        yield new DealerAnnounced(
            $command->tableIdentifier,
            $command->playerIdentifier,
            $command->gameNumber,
        );
    }

    /**
     * @return \Traversable<Event>
     */
    private function passAround(TableInternalState $internalState, PassAround $command): \Traversable
    {
        $internalState->guardNumberOfPlayers();
        $internalState->guardValidGameNumber($command->gameNumber);
        $internalState->guardTraditionalPassAround();

        $currentDealer = $internalState->getKnownDealer();
        yield from $this->ensureTableBusy($internalState, $command->tableIdentifier);

        $passedAroundCount = $internalState->passedAroundCount + 1;
        yield new PassedAround(
            $command->tableIdentifier,
            $command->gameNumber,
            $passedAroundCount,
            $internalState->getPlayersInvolved(),
        );

        if (0 === $passedAroundCount % Whist::MAX_DEALER_ATTEMPTS) {
            yield new DealerFailed(
                $command->tableIdentifier,
                $command->gameNumber,
                $currentDealer,
            );
            yield from $this->announceNextDealer($internalState, $command->gameNumber, $command->tableIdentifier);
        }
    }

    private function afterTableStartedPlaying(TableInternalState $internalState, TableStartedPlaying $event): TableInternalState
    {
        return $internalState->with(tableStatus: TableStatus::BUSY);
    }

    private function afterPassedAround(TableInternalState $internalState, PassedAround $event): TableInternalState
    {
        return $internalState->with(passedAroundCount: $event->count)
            ->withUndoEvent(new PassAroundUndone(
                $event->tableIdentifier,
                $event->gameNumber,
                $event->playersInvolved,
                $event->count,
            ));
    }

    /**
     * @return \Traversable<Event>
     */
    private function ensureTableBusy(TableInternalState $internalState, TableIdentifier $tableIdentifier): \Traversable
    {
        $internalState->guardNumberOfPlayers();
        $internalState->getKnownDealer();
        if (TableStatus::BUSY === $internalState->tableStatus) {
            return;
        }

        yield new TableStartedPlaying($tableIdentifier);
    }

    /**
     * @return \Traversable<Event>
     */
    private function announceNextDealer(
        TableInternalState $internalState,
        GameNumber $gameNumber,
        TableIdentifier $tableIdentifier,
    ): \Traversable {
        $internalState->guardNumberOfPlayers();
        $currentDealer = $internalState->getKnownDealer();

        if ($internalState->currentGame == $gameNumber || $internalState->currentGame->getNext() == $gameNumber) {
            yield new DealerAnnounced(
                $tableIdentifier,
                $internalState->seatedPlayers->getPlayerNextTo($currentDealer),
                $gameNumber,
            );

            return;
        }

        throw new IncorrectGameNumber();
    }

    /**
     * @return \Traversable<Event>
     */
    private function proposeWithNobodyAlong(TableInternalState $internalState, ProposeAndNobodyGoesAlong $command): \Traversable
    {
        $internalState->guardNumberOfPlayers();
        $internalState->guardValidGameNumber($command->gameNumber);
        $internalState->getKnownDealer();
        yield from $this->ensureTableBusy($internalState, $command->tableIdentifier);
        yield new ProposedWithNobodyAlong(
            $command->tableIdentifier,
            $command->gameNumber,
        );
        yield from $this->announceNextDealer(
            $internalState,
            $command->gameNumber,
            $command->tableIdentifier,
        );
    }

    private function afterProposedWithNobodyAlong(TableInternalState $internalState, ProposedWithNobodyAlong $event): TableInternalState
    {
        return $internalState->with(passedAroundCount: 0)
            ->withUndoEvent(new PropositionWithNobodyAlongUndone(
                $event->tableIdentifier,
                $event->gameNumber,
                $internalState->getKnownDealer(),
                $internalState->passedAroundCount,
            ));
    }

    /**
     * @return \Traversable<Event>
     */
    private function logGame(TableInternalState $internalState, LogGame $command): \Traversable
    {
        $internalState->guardNumberOfPlayers();
        $internalState->guardValidGameNumber($command->gameNumber);

        $gameSpecification = $command->gameSpecification;

        if (!$internalState->allowedGames->hasBySpecificationClass($gameSpecification::class)) {
            throw UnsupportedByRules::byGameType($gameSpecification->getName());
        }

        if (!$this->announcementValidator->isValidAnnouncement(
            $command->gameAnnouncement,
            $internalState->getPlayersInvolved(),
        )) {
            throw new InvalidAnnouncement();
        }

        if (!(
            is_a($command->gameAnnouncement, $gameSpecification->getAnnouncementType())
            && is_a($command->gameOutcome, $gameSpecification->getOutcomeType())
        )) {
            throw new \InvalidArgumentException('Announcement or outcome does not match with game specification');
        }

        yield from $this->ensureTableBusy($internalState, $command->tableIdentifier);

        $reflectionMethod = new \ReflectionMethod($gameSpecification->getEventType(), 'create');
        /** @var GamePlayed<GameAnnouncement, GameOutcome> $event */
        $event = $reflectionMethod->invoke(
            null,
            $command->tableIdentifier,
            $command->gameNumber,
            $command->gameAnnouncement,
            $command->gameOutcome,
            $internalState->getPlayersInvolved(),
            $command->loggedAt,
        );

        yield $event;
        yield from $this->announceNextDealer(
            $internalState,
            $command->gameNumber->getNext(),
            $command->tableIdentifier,
        );
    }

    /**
     * @return \Traversable<Event>
     */
    private function starPlayer(TableInternalState $internalState, StarPlayer $command): \Traversable
    {
        $internalState->guardKnownPlayer($command->playerIdentifier);
        $internalState->guardValidGameNumber($command->gameNumber);

        $currentStars = $internalState->getPlayerStars($command->playerIdentifier);

        // Starring a player is undoable, but for to undo, we need to know the dealer. So
        // make sure the table is busy, and so enforce that a dealer is announced.
        yield from $this->ensureTableBusy($internalState, $command->tableIdentifier);

        yield new PlayerStarred(
            $command->tableIdentifier,
            $command->gameNumber,
            $command->playerIdentifier,
            $currentStars + 1,
        );
    }

    private function afterPlayerStarred(TableInternalState $internalState, PlayerStarred $event): TableInternalState
    {
        Assert::isInstanceOf($internalState->dealerIdentifier, PlayerIdentifier::class);

        return $internalState->withUndoEvent(new PlayerStarUndone(
            $event->tableIdentifier,
            $event->gameNumber,
            $event->playerIdentifier,
            $event->numberOfStars,
            $internalState->dealerIdentifier,
            $internalState->passedAroundCount,
        ))->withPlayerStars($event->playerIdentifier, $event->numberOfStars);
    }

    /**
     * @return \Traversable<Event>
     */
    private function undo(TableInternalState $internalState, Undo $command): \Traversable
    {
        $internalState->guardValidGameNumber($command->currentGameNumber);
        $numberOfUndoEvents = count($internalState->undoEvents);

        if (0 === $numberOfUndoEvents) {
            throw NothingToUndo::atTable($command->tableIdentifier);
        }
        $undoEvent = array_values($internalState->undoEvents)[$numberOfUndoEvents - 1];

        yield $undoEvent;

        if (1 === $numberOfUndoEvents) {
            yield new AllUndone($command->tableIdentifier);
        }

        if ($undoEvent->getDealer() == $internalState->dealerIdentifier) {
            return;
        }

        yield new DealerAnnounced(
            $command->tableIdentifier,
            $undoEvent->getDealer(),
            $undoEvent->getGameNumber(),
        );
    }

    private function afterAllUndone(TableInternalState $internalState): TableInternalState
    {
        return $internalState->with(tableStatus: TableStatus::ON_HOLD);
    }

    /** @return \Traversable<Event> */
    private function reorderPlayers(TableInternalState $internalState, ReorderPlayers $command): \Traversable
    {
        $internalState->guardValidGameNumber($command->gameNumber);
        $internalState->guardPlayers($command->orderedPlayers);

        $reorderedPlayers = $internalState->seatedPlayers->reordered($command->orderedPlayers);
        if ($reorderedPlayers == $internalState->seatedPlayers) {
            yield from [];

            return;
        }

        yield new PlayersReordered(
            $command->tableIdentifier,
            $command->gameNumber,
            $reorderedPlayers,
        );
    }

    private function afterPlayersReordered(TableInternalState $internalState, PlayersReordered $event): TableInternalState
    {
        return $internalState->with(
            tableStatus: TableStatus::BUSY === $internalState->tableStatus ? TableStatus::ON_HOLD : $internalState->tableStatus,
            seatedPlayers: $event->newPlayerOrder,
        );
    }

    /**
     * @return \Traversable<Event>
     */
    private function announceFinalRound(TableInternalState $internalState, AnnounceFinalRound $command): \Traversable
    {
        $internalState->guardValidGameNumber($command->gameNumber);
        $internalState->guardNumberOfPlayers();
        $internalState->guardFinalRoundNotAnnounced();

        // FinalRoundAnnounced is an undoable event, but an undo-event needs
        // a dealer. So let's just throw an exception if the dealer is unknown

        $internalState->getKnownDealer();

        yield from $this->ensureTableBusy($internalState, $command->tableIdentifier);
        yield new FinalRoundAnnounced(
            $command->tableIdentifier,
            $command->gameNumber,
            $command->gameNumber->increased($internalState->getNumberOfPlayers() - 1),
        );
    }

    private function afterFinalRoundAnnounced(
        TableInternalState $internalState,
        FinalRoundAnnounced $event
    ): TableInternalState {
        return $internalState->with(finalGame: $event->end)
            ->withUndoEvent(
                new FinalRoundAnnouncementUndone(
                    $event->tableIdentifier,
                    $event->start,
                    $event->end,
                    $internalState->getKnownDealer(),
                    $internalState->passedAroundCount,
                ),
            );
    }

    /**
     * @return \Traversable<Event>
     */
    private function configureTable(TableInternalState $internalState, ConfigureTable $command): \Traversable
    {
        if ($internalState->currentGame == GameNumber::first()) {
            yield new TableConfigured(
                $command->tableIdentifier,
                $command->ruleSetName,
            );

            return;
        }

        throw TableBusy::byTableIdentifier($command->tableIdentifier);
    }

    private function afterTableConfigured(TableInternalState $internalState, TableConfigured $event): TableInternalState
    {
        return $internalState->with(
            allowedGames: $this->rulesByConvention->getPermissibleGames($event->ruleSetName),
            passAroundRules: $this->rulesByConvention->getPassAroundRules($event->ruleSetName),
        );
    }

    /**
     * @return \Traversable<Event>
     */
    private function rejoinPlayer(TableInternalState $internalState, RejoinKnownPlayer $command): \Traversable
    {
        $internalState->guardValidGameNumber($command->gameNumber);
        $internalState->guardFinalRoundNotAnnounced();
        $internalState->guardKnownPlayer($command->playerIdentifier);

        if ($internalState->hasSeatedPlayer($command->playerIdentifier)) {
            return;
        }

        $seat = $internalState->getFreeSeat();

        yield new PlayerJoined(
            $command->tableIdentifier,
            $command->playerIdentifier,
            $command->gameNumber,
            $seat,
            $internalState->getPlayerName($command->playerIdentifier) ?: '(???)',
        );
    }
}
