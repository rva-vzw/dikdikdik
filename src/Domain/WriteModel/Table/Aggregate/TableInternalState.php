<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Aggregate;

use App\Domain\Table\Event\Undo\UndoEvent;
use App\Domain\Table\Exception\DealerUnknown;
use App\Domain\Table\Exception\IncorrectGameNumber;
use App\Domain\Table\Exception\ItTakesFourToPlay;
use App\Domain\Table\Exception\UnknownPlayer;
use App\Domain\Table\Exception\UnsupportedByRules;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\GameSpecification\AllowedGamesTrait;
use App\Domain\Table\Loggable\GameSpecification\GameSpecifications;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Rules\PassAroundRules;
use App\Domain\Table\Rules\Whist;
use App\Domain\Table\Seat;
use App\Domain\Table\TableStatus;
use App\Domain\WriteModel\Table\Exception\GameOver;
use App\Domain\WriteModel\Table\Exception\NotAllowedInFinalRound;
use RvaVzw\KrakBoem\Cqrs\Decider\AggregateInternalState;
use Spatie\Cloneable\Cloneable;

// TODO: Figure out how to keep the internal state really internal.
// Maybe enforce it with deptrac? There's also an @internal annotation, I'm not sure
// how that works.

final readonly class TableInternalState implements AggregateInternalState
{
    use Cloneable;

    /**
     * @param array<non-empty-string, int>              $playerStars maps player id to number of stars
     * @param array<non-empty-string, non-empty-string> $playerNames maps player id to player name
     * @param UndoEvent[]                               $undoEvents  events we need to apply in order to undo
     */
    public function __construct(
        public SeatedPlayers $seatedPlayers,
        public array $playerNames,
        public ?PlayerIdentifier $dealerIdentifier,
        public GameNumber $currentGame,
        public int $passedAroundCount,
        public ?GameNumber $finalGame,
        public array $playerStars,
        public TableStatus $tableStatus,
        public array $undoEvents,
        // Below are the table rules, which could have been grouped in a single value object:
        public GameSpecifications $allowedGames,
        public PassAroundRules $passAroundRules,
    ) {
    }

    public static function createEmpty(): self
    {
        return new TableInternalState(
            seatedPlayers: SeatedPlayers::none(),
            playerNames: [],
            dealerIdentifier: null,
            currentGame: GameNumber::first(),
            passedAroundCount: 0,
            finalGame: null,
            playerStars: [],
            tableStatus: TableStatus::NEW,
            undoEvents: [],
            // Before configuration, no games are allowed yet.
            allowedGames: self::noAllowedGames(),
            passAroundRules: PassAroundRules::Traditional,
        );
    }

    public static function noAllowedGames(): GameSpecifications
    {
        return new class() implements GameSpecifications {
            use AllowedGamesTrait;

            #[\Override]
            public function getAll(): \Traversable
            {
                return new \ArrayIterator([]);
            }
        };
    }

    public function guardValidGameNumber(GameNumber $gameNumber): void
    {
        if ($this->finalGame instanceof GameNumber && $this->finalGame->isSmallerThan($gameNumber)) {
            throw new GameOver();
        }

        if ($this->currentGame == $gameNumber) {
            return;
        }

        throw new IncorrectGameNumber();
    }

    public function guardFinalRoundNotAnnounced(): void
    {
        if ($this->finalGame instanceof GameNumber) {
            throw new NotAllowedInFinalRound();
        }
    }

    public function withSeatedPlayer(
        Seat $seat,
        PlayerIdentifier $playerIdentifier,
        string $playerName,
    ): self {
        $seatedPlayers = $this->seatedPlayers->withSeatedPlayer($seat, $playerIdentifier);
        $playerNames = [(string) $playerIdentifier => $playerName] + $this->playerNames;

        // FIXME: This, in combination with auto-selecting a dealer in joinPlayer, causes a bug:
        // the dealer is always wrong when a new player joins.
        $newStatus = TableStatus::BUSY === $this->tableStatus
            ? TableStatus::ON_HOLD
            : $this->tableStatus;

        return $this->with(
            seatedPlayers: $seatedPlayers,
            playerNames: $playerNames,
            tableStatus: $newStatus,
        );
    }

    public function readyForGame(GameNumber $gameNumber, PlayerIdentifier $dealerIdentifier): self
    {
        $passedAroundCount = $this->currentGame->getNext() == $gameNumber ? 0 : $this->passedAroundCount;

        return $this->with(
            currentGame: $gameNumber,
            dealerIdentifier: $dealerIdentifier,
            passedAroundCount: $passedAroundCount,
        );
    }

    public function guardPlayerIsPresent(PlayerIdentifier $playerIdentifier): void
    {
        if ($this->seatedPlayers->has($playerIdentifier)) {
            return;
        }

        throw UnknownPlayer::withIdentifier($playerIdentifier);
    }

    public function withPlayerLeft(PlayerIdentifier $playerIdentifier): self
    {
        return $this->with(
            seatedPlayers: $this->seatedPlayers->withoutSeatedPlayer($playerIdentifier),
            // FIXME: Changing the state to ON_HOLD seems to conflict with changing the dealer if there's enough players
            // (See TableDecider::kickPlayer())
            status: TableStatus::BUSY === $this->tableStatus
                ? TableStatus::ON_HOLD
                : $this->tableStatus,
        );
    }

    public function guardNumberOfPlayers(): void
    {
        if (Whist::PLAYERS_NEEDED_TO_PLAY > $this->seatedPlayers->getCount()) {
            throw ItTakesFourToPlay::got($this->seatedPlayers->getCount());
        }
    }

    public function withUndoEvent(UndoEvent $event): self
    {
        $undoEvents = $this->undoEvents;
        array_push($undoEvents, $event);

        return $this->with(undoEvents: $undoEvents);
    }

    public function getPlayersInvolved(): PlayersInvolved
    {
        return PlayersInvolved::from(
            $this->seatedPlayers,
            $this->getKnownDealer(),
        );
    }

    public function getKnownDealer(): PlayerIdentifier
    {
        if ($this->dealerIdentifier instanceof PlayerIdentifier) {
            return $this->dealerIdentifier;
        }

        throw DealerUnknown::noDealerAnnounced();
    }

    public function guardKnownPlayer(PlayerIdentifier $playerIdentifier): void
    {
        if (array_key_exists($playerIdentifier->toString(), $this->playerNames)) {
            return;
        }

        throw UnknownPlayer::withIdentifier($playerIdentifier);
    }

    public function getPlayerStars(PlayerIdentifier $playerIdentifier): int
    {
        $playerId = $playerIdentifier->toString();

        return $this->playerStars[$playerId] ?? 0;
    }

    public function withPlayerStars(PlayerIdentifier $playerIdentifier, int $stars): self
    {
        $newPlayerStars = $this->playerStars;
        $newPlayerStars[$playerIdentifier->toString()] = $stars;

        return $this->with(playerStars: $newPlayerStars);
    }

    /**
     * @param \Traversable<PlayerIdentifier> $orderedPlayers
     */
    public function guardPlayers(\Traversable $orderedPlayers): void
    {
        foreach ($orderedPlayers as $playerIdentifier) {
            if ($this->seatedPlayers->has($playerIdentifier)) {
                continue;
            }

            throw UnknownPlayer::withIdentifier($playerIdentifier);
        }
    }

    public function getNumberOfPlayers(): int
    {
        return $this->seatedPlayers->getCount();
    }

    public function hasSeatedPlayer(PlayerIdentifier $playerIdentifier): bool
    {
        return $this->seatedPlayers->has($playerIdentifier);
    }

    public function getFreeSeat(): Seat
    {
        return $this->seatedPlayers->getFreeSeat();
    }

    public function getPlayerName(PlayerIdentifier $playerIdentifier): string
    {
        return $this->playerNames[(string) $playerIdentifier];
    }

    public function guardTraditionalPassAround(): void
    {
        if (PassAroundRules::Traditional === $this->passAroundRules) {
            return;
        }

        throw UnsupportedByRules::traditionalPassAroundNotSupported();
    }
}
