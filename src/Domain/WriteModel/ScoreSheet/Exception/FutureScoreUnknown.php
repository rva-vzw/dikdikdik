<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Exception;

final class FutureScoreUnknown extends \Exception
{
}
