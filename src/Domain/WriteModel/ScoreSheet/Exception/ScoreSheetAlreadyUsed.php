<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Exception;

final class ScoreSheetAlreadyUsed extends \Exception
{
}
