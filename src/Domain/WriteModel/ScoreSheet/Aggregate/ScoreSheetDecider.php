<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Aggregate;

use App\Domain\ScoreSheet\Event\PaidOut;
use App\Domain\ScoreSheet\Event\ScoreFactorDecreased;
use App\Domain\ScoreSheet\Event\ScoreFactorIncreased;
use App\Domain\ScoreSheet\Event\ScoresCorrected;
use App\Domain\ScoreSheet\Event\ScoresLogged;
use App\Domain\ScoreSheet\Event\UndoLogged;
use App\Domain\ScoreSheet\Score\MultipliedWinnings;
use App\Domain\WriteModel\ScoreSheet\Command\DecreaseScoreFactor;
use App\Domain\WriteModel\ScoreSheet\Command\IncreaseScoreFactor;
use App\Domain\WriteModel\ScoreSheet\Command\LogNoGame;
use App\Domain\WriteModel\ScoreSheet\Command\LogPlayedGame;
use App\Domain\WriteModel\ScoreSheet\Command\LogUndoneGame;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Cqrs\Decider\AggregateInternalState;
use RvaVzw\KrakBoem\Cqrs\Decider\Decider;
use RvaVzw\KrakBoem\EventSourcing\Event;

/**
 * @implements Decider<ScoreSheetInternalState>
 */
final class ScoreSheetDecider implements Decider
{
    public function decide(AggregateInternalState $internalState, Command $command): \Traversable
    {
        yield from match ($command::class) {
            LogPlayedGame::class => $this->logPlayedGame($internalState, $command),
            IncreaseScoreFactor::class => $this->increaseScoreFactor($internalState, $command),
            DecreaseScoreFactor::class => $this->decreaseScoreFactor($internalState, $command),
            LogNoGame::class => $this->logNoGame($internalState, $command),
            LogUndoneGame::class => $this->logUndo($internalState, $command),
            default => [],
        };
    }

    public function evolve(AggregateInternalState $internalState, Event $event): AggregateInternalState
    {
        return match ($event::class) {
            ScoresLogged::class => $this->afterScoresLogged($internalState, $event),
            ScoreFactorIncreased::class => $this->afterScoreFactorIncreased($internalState, $event),
            ScoreFactorDecreased::class => $this->afterScoreFactorDecreased($internalState, $event),
            ScoresCorrected::class => $this->afterScoresCorrected($internalState, $event),
            UndoLogged::class => $this->afterUndoLogged($internalState, $event),
            default => $internalState,
        };
    }

    public function getInitialState(): AggregateInternalState
    {
        return ScoreSheetInternalState::createEmpty();
    }

    public function isTerminal(AggregateInternalState $internalState): bool
    {
        return false;
    }

    /**
     * @return \Traversable<Event>
     */
    private function logPlayedGame(ScoreSheetInternalState $internalState, LogPlayedGame $command): \Traversable
    {
        $scoreFactor = $internalState->scoreFactors->getScoreFactor(
            $command->getGameNumber(),
        );

        $multipliedWinnings = new MultipliedWinnings(
            $command->getPlainWinnings(),
            $scoreFactor,
        );

        $newScores = $internalState->currentScores->getNext(
            $multipliedWinnings,
            $command->getNote(),
        );

        // the PaidOut event is less relevant, but could be useful for some listeners.
        yield new PaidOut(
            $command->getScoreSheetIdentifier(),
            $command->getGameNumber(),
            $multipliedWinnings,
            $newScores->getNote(),
            $newScores->getScoreLineNumber()
        );

        // The ScoresLogged event actually contains the new scores the aggregate will be using.
        yield new ScoresLogged(
            $command->getScoreSheetIdentifier(),
            $command->getGameNumber(),
            $newScores,
            $scoreFactor
        );

        // It would be nice to consider using private and public events.
    }

    private function afterScoresLogged(
        ScoreSheetInternalState $internalState,
        ScoresLogged $event,
    ): ScoreSheetInternalState {
        // It's a little strange that about the whole internal state is
        // included in the event... 🤔
        return $internalState->withScoreLine($event->scoreLine);
    }

    /**
     * @return \Traversable<Event>
     */
    private function increaseScoreFactor(
        ScoreSheetInternalState $internalState,
        IncreaseScoreFactor $command,
    ): \Traversable {
        $currentFactor = $internalState->scoreFactors->getScoreFactor($command->getGameNumber());

        yield new ScoreFactorIncreased(
            $command->getScoreSheetIdentifier(),
            $command->getGameNumber(),
            $currentFactor->increased(),
        );
    }

    private function afterScoreFactorIncreased(
        ScoreSheetInternalState $internalState,
        ScoreFactorIncreased $event,
    ): ScoreSheetInternalState {
        return $internalState->withScoreFactor(
            $event->getGameNumber(),
            $event->getNewScoreFactor(),
        );
    }

    /**
     * @return \Traversable<Event>
     */
    private function decreaseScoreFactor(
        ScoreSheetInternalState $internalState,
        DecreaseScoreFactor $command,
    ): \Traversable {
        $currentFactor = $internalState->scoreFactors->getScoreFactor($command->gameNumber);

        yield new ScoreFactorDecreased(
            $command->scoreSheetIdentifier,
            $command->gameNumber,
            $currentFactor->decreased(),
        );
    }

    private function afterScoreFactorDecreased(
        ScoreSheetInternalState $internalState,
        ScoreFactorDecreased $event,
    ): ScoreSheetInternalState {
        return $internalState->withScoreFactor(
            $event->gameNumber,
            $event->newScoreFactor,
        );
    }

    /**
     * @return \Traversable<Event>
     */
    private function logNoGame(ScoreSheetInternalState $internalState, LogNoGame $command): \Traversable
    {
        $multipiedWinnings = MultipliedWinnings::plain($command->getPlainWinnings());

        $newScores = $internalState->currentScores->getNext(
            $multipiedWinnings,
            $command->getNote(),
        );

        yield new PaidOut(
            $command->getScoreSheetIdentifier(),
            $command->getGameNumber(),
            $multipiedWinnings,
            $command->getNote(),
            $newScores->getScoreLineNumber(),
        );

        yield new ScoresCorrected(
            $command->getScoreSheetIdentifier(),
            $command->getGameNumber(),
            $newScores,
        );
    }

    private function afterScoresCorrected(ScoreSheetInternalState $internalState, ScoresCorrected $event): ScoreSheetInternalState
    {
        return $internalState->withScoreLine($event->scoreLine);
    }

    /**
     * @return \Traversable<Event>
     */
    private function logUndo(ScoreSheetInternalState $internalState, LogUndoneGame $command): \Traversable
    {
        $scoreFactor = $internalState->scoreFactors->getScoreFactor($command->gameNumber);
        $multipliedWinnings = new MultipliedWinnings($command->plainWinnings, $scoreFactor);
        $newScores = $internalState->currentScores->getNext($multipliedWinnings, $command->note);

        yield new PaidOut(
            $command->scoreSheetIdentifier,
            $command->gameNumber,
            $multipliedWinnings,
            $command->note,
            $newScores->getScoreLineNumber(),
        );

        yield new UndoLogged(
            $command->scoreSheetIdentifier,
            $command->gameNumber,
            $newScores,
        );
    }

    private function afterUndoLogged(ScoreSheetInternalState $internalState, UndoLogged $event): ScoreSheetInternalState
    {
        return $internalState->withScoreLine($event->scoreLine);
    }
}
