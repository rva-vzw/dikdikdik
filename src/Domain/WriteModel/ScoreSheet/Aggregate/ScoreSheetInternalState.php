<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Aggregate;

use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreFactorsByGame;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\Table\GameNumber;
use RvaVzw\KrakBoem\Cqrs\Decider\AggregateInternalState;

final readonly class ScoreSheetInternalState implements AggregateInternalState
{
    private function __construct(
        public ScoreLine $currentScores,
        public ScoreFactorsByGame $scoreFactors,
    ) {
    }

    public static function createEmpty(): self
    {
        return new self(
            ScoreLine::empty(ScoreLineNumber::fromInteger(0)),
            ScoreFactorsByGame::create(),
        );
    }

    public function withScoreLine(ScoreLine $scoreLine): self
    {
        return new self($scoreLine, $this->scoreFactors);
    }

    public function withScoreFactor(GameNumber $gameNumber, ScoreFactor $scoreFactor): self
    {
        return new self(
            $this->currentScores,
            $this->scoreFactors->withScoreFactor($gameNumber, $scoreFactor),
        );
    }
}
