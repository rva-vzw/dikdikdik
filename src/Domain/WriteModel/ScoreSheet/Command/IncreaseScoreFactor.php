<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Command;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;

final class IncreaseScoreFactor implements ScoreSheetCommand
{
    use ScoreSheetCommandTrait;

    /** @var ScoreSheetIdentifier */
    private $scoreSheetIdentifier;

    /** @var GameNumber */
    private $gameNumber;

    public function __construct(ScoreSheetIdentifier $scoreSheetIdentifier, GameNumber $gameNumber)
    {
        $this->scoreSheetIdentifier = $scoreSheetIdentifier;
        $this->gameNumber = $gameNumber;
    }

    public function getScoreSheetIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }

    public function getGameNumber(): GameNumber
    {
        return $this->gameNumber;
    }
}
