<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Command;

use App\Domain\WriteModel\ScoreSheet\Aggregate\ScoreSheetDecider;
use App\Domain\WriteModel\ScoreSheet\Aggregate\ScoreSheetInternalState;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandHandler;
use RvaVzw\KrakBoem\Cqrs\Decider\EventSourcedCommandHandler;
use RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;

/**
 * @extends EventSourcedCommandHandler<ScoreSheetInternalState, ScoreSheetCommand>
 */
final readonly class ScoreSheetCommandHandler extends EventSourcedCommandHandler implements CommandHandler
{
    public function __construct(EventStore $eventStore, EventBus $eventBus, ScoreSheetDecider $decider)
    {
        parent::__construct($eventStore, $eventBus, $decider);
    }

    public function __invoke(ScoreSheetCommand $command): void
    {
        $this->handle($command);
    }
}
