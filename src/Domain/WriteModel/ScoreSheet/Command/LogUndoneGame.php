<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Command;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;

final class LogUndoneGame implements ScoreSheetCommand
{
    use ScoreSheetCommandTrait;

    public function __construct(
        public readonly ScoreSheetIdentifier $scoreSheetIdentifier,
        public readonly GameNumber $gameNumber,
        public readonly PlainWinnings $plainWinnings,
        public readonly string $note,
    ) {
    }
}
