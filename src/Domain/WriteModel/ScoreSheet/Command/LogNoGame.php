<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Command;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;

final class LogNoGame implements ScoreSheetCommand
{
    use ScoreSheetCommandTrait;

    /** @var ScoreSheetIdentifier */
    private $scoreSheetIdentifier;
    /** @var GameNumber */
    private $gameNumber;
    /** @var PlainWinnings */
    private $plainWinnings;
    /** @var string */
    private $note;

    public function __construct(
        ScoreSheetIdentifier $scoreSheetIdentifier,
        GameNumber $gameNumber,
        PlainWinnings $plainWinnings,
        string $note
    ) {
        $this->scoreSheetIdentifier = $scoreSheetIdentifier;
        $this->gameNumber = $gameNumber;
        $this->plainWinnings = $plainWinnings;
        $this->note = $note;
    }

    public function getScoreSheetIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }

    public function getGameNumber(): GameNumber
    {
        return $this->gameNumber;
    }

    public function getPlainWinnings(): PlainWinnings
    {
        return $this->plainWinnings;
    }

    public function getNote(): string
    {
        return $this->note;
    }
}
