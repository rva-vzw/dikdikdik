<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Command;

use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;

interface ScoreSheetCommand extends Command
{
}
