<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ScoreSheet\Command;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;

final class CreateNewScoreSheet implements ScoreSheetCommand
{
    use ScoreSheetCommandTrait;

    /**
     * @var ScoreSheetIdentifier
     */
    private $scoreSheetIdentifier;

    public function __construct(ScoreSheetIdentifier $scoreSheetIdentifier)
    {
        $this->scoreSheetIdentifier = $scoreSheetIdentifier;
    }

    public function getScoreSheetIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }
}
