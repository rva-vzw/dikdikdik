<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\ProcessManager;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\ScoreSheet\Tariffs\TariffsByTable;
use App\Domain\Table\Event\FinalRoundAnnounced;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Event\PassedAround;
use App\Domain\Table\Event\Undo\FinalRoundAnnouncementUndone;
use App\Domain\Table\Event\Undo\GameUndone;
use App\Domain\Table\Event\Undo\PassAroundUndone;
use App\Domain\Table\FinalGames;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Loggable\Outcome\GameOutcome;
use App\Domain\Table\Rules\Whist;
use App\Domain\WriteModel\ScoreSheet\Command\DecreaseScoreFactor;
use App\Domain\WriteModel\ScoreSheet\Command\IncreaseScoreFactor;
use App\Domain\WriteModel\ScoreSheet\Command\LogNoGame;
use App\Domain\WriteModel\ScoreSheet\Command\LogPlayedGame;
use App\Domain\WriteModel\ScoreSheet\Command\LogUndoneGame;
use App\Domain\WriteModel\Table\Undo\UndoEventConversion;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use RvaVzw\KrakBoem\Cqrs\ProcessManager\AbstractProcessManager;
use RvaVzw\KrakBoem\EventSourcing\Event;

readonly final class ScoreSheetProcessManager extends AbstractProcessManager
{
    public function __construct(
        CommandBus $commandBus,
        private FinalGames $finalGames,
        private UndoEventConversion $undoEventConversion,
        private AllSpecifiedGames $specifiedGames,
        private TariffsByTable $tableTariffs,
    ) {
        parent::__construct($commandBus);
    }

    public function __invoke(Event $event): void
    {
        if ($event instanceof GamePlayed) {
            // For GamePlayed events, we can use the general winnings calculator
            $this->applyGamePlayed($event);

            return;
        }

        if ($event instanceof GameUndone) {
            // For GameUndone as well
            $this->applyGameUndone($event);

            return;
        }

        // The parent method calls the apply function for the specific event:
        parent::__invoke($event);
    }

    public function applyPassedAround(PassedAround $event): void
    {
        $tableIdentifier = $event->tableIdentifier;

        // Increase score factor of correct game,
        // see https://www.rijkvanafdronk.be/spelregels/scorefactor/
        $gameNumber = $event->gameNumber->increased($event->count - 1);

        if ($this->finalGames->isEnding($tableIdentifier)) {
            $finalGame = $this->finalGames->getFinalGame($tableIdentifier);
            if ($finalGame->isSmallerThan($gameNumber)) {
                // It makes no sense to increase a score factor after the final game.
                // So let's increase the score factor of the final game.
                // See https://gitlab.com/rva-vzw/dikdikdik/issues/77
                $gameNumber = $finalGame;
            }
        }

        $this->commandBus->dispatch(
            new IncreaseScoreFactor(
                ScoreSheetIdentifier::forTable($tableIdentifier),
                $gameNumber
            )
        );

        if ($event->playersInvolved->dealerIsPlaying()) {
            // In case of 4 players at the table, we're done.
            return;
        }
        // On a 5 or 6 player table, passing around first or second time for the same dealer
        // makes the dealer receive 4 francs, paid by the players that pass around.
        // The way I check this, is convoluted, and depends too much on how the table aggregate
        // works. But it will do, I guess.

        if (0 == $event->count % Whist::MAX_DEALER_ATTEMPTS) {
            return;
        }

        $this->commandBus->dispatch(
            new LogNoGame(
                ScoreSheetIdentifier::forTable($tableIdentifier),
                $event->gameNumber,
                PlainWinnings::dealerWins(
                    1,
                    $event->playersInvolved
                ),
                'compensation_passed_around'
            )
        );
    }

    /**
     * @param GamePlayed<GameAnnouncement, GameOutcome> $event
     */
    public function applyGamePlayed(GamePlayed $event): void
    {
        $plainWinnings = $this->tableTariffs
            ->getTariffsForTable($event->getTableIdentifier())
            ->calculateWinnings($event);

        $this->commandBus->dispatch(
            new LogPlayedGame(
                ScoreSheetIdentifier::forTable(
                    $event->getTableIdentifier()),
                $event->getGameNumber(),
                $plainWinnings,
                $this->getNoteForGamePlayedEvent($event),
            )
        );
    }

    /**
     * @param GameUndone<GameAnnouncement, GameOutcome> $event
     */
    public function applyGameUndone(GameUndone $event): void
    {
        $correspondingGameEvent = $this->undoEventConversion->getCorrespondingEventForUndo($event);

        /** @psalm-suppress InvalidArgument */
        $plainWinnings = $this->tableTariffs
            ->getTariffsForTable($event->getTableIdentifier())
            ->calculateWinnings($correspondingGameEvent)
            ->reversed();

        $this->commandBus->dispatch(
            new LogUndoneGame(
                ScoreSheetIdentifier::forTable(
                    $event->getTableIdentifier()),
                $event->getGameNumber(),
                $plainWinnings,
                $this->getNoteForGameUndoneEvent($event),
            )
        );
    }

    public function applyFinalRoundAnnounced(FinalRoundAnnounced $event): void
    {
        for ($gameNumber = $event->start; $gameNumber->isNotBiggerThan($event->end); $gameNumber = $gameNumber->getNext()) {
            $this->commandBus->dispatch(
                new IncreaseScoreFactor(
                    ScoreSheetIdentifier::forTable($event->tableIdentifier),
                    $gameNumber
                )
            );
        }
    }

    public function applyPassAroundUndone(PassAroundUndone $event): void
    {
        // Decrease score factor of correct game
        // see https://www.rijkvanafdronk.be/spelregels/scorefactor/

        $gameNumber = $event->gameNumber->increased($event->count - 1);

        if ($this->finalGames->isEnding($event->tableIdentifier)) {
            $finalGame = $this->finalGames->getFinalGame($event->tableIdentifier);
            if ($finalGame->isSmallerThan($gameNumber)) {
                $gameNumber = $finalGame;
            }
        }

        $this->commandBus->dispatch(
            new DecreaseScoreFactor(
                ScoreSheetIdentifier::forTable($event->tableIdentifier),
                $gameNumber
            )
        );

        if ($event->playersInvolved->dealerIsPlaying()) {
            // In case of 4 players at the table, we're done.
            return;
        }

        if (0 == $event->count % Whist::MAX_DEALER_ATTEMPTS) {
            return;
        }

        // On a 5 or 6 player table, passing around first or second time for the same dealer
        // makes the dealer receive 4 francs, paid by the players that pass around.
        // Let's pay that back

        $this->commandBus->dispatch(
            new LogNoGame(
                ScoreSheetIdentifier::forTable($event->tableIdentifier),
                // !! this can make the game numbers in the score log decreasing, which can have unexpected
                // side effects !!
                $event->gameNumber,
                PlainWinnings::dealerLooses(
                    1,
                    $event->playersInvolved,
                ),
                'undo_compensation_passed_around',
            ),
        );
    }

    public function applyFinalRoundAnnouncementUndone(FinalRoundAnnouncementUndone $event): void
    {
        for ($gameNumber = $event->start; $gameNumber->isNotBiggerThan($event->end); $gameNumber = $gameNumber->getNext()) {
            $this->commandBus->dispatch(
                new DecreaseScoreFactor(
                    ScoreSheetIdentifier::forTable($event->tableIdentifier),
                    $gameNumber
                )
            );
        }
    }

    /**
     * @param GamePlayed<GameAnnouncement, GameOutcome> $event
     */
    protected function getNoteForGamePlayedEvent(GamePlayed $event): string
    {
        $specifiedGame = $this->specifiedGames->getByEvent($event);

        return $specifiedGame->getName();
    }

    /**
     * @param GameUndone<GameAnnouncement, GameOutcome> $event
     */
    protected function getNoteForGameUndoneEvent(GameUndone $event): string
    {
        $specifiedGame = $this->specifiedGames->getByUndoEvent($event);

        return 'undo_'.$specifiedGame->getName();
    }
}
