<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Event;

use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class ScoreFactorIncreased implements Event
{
    public function __construct(
        public ScoreSheetIdentifier $scoreSheetIdentifier,
        public GameNumber $gameNumber,
        public ScoreFactor $newScoreFactor,
    ) {
    }

    public function getGameNumber(): GameNumber
    {
        return $this->gameNumber;
    }

    public function getNewScoreFactor(): ScoreFactor
    {
        return $this->newScoreFactor;
    }

    public function getAggregateRootIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }
}
