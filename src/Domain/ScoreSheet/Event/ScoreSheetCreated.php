<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Event;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

final class ScoreSheetCreated implements Event
{
    /**
     * @var ScoreSheetIdentifier
     */
    private $scoreSheetIdentifier;

    public function __construct(ScoreSheetIdentifier $scoreSheetIdentifier)
    {
        $this->scoreSheetIdentifier = $scoreSheetIdentifier;
    }

    public function getScoreSheetIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }

    public function getAggregateRootIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }
}
