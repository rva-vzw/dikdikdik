<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Event;

use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use RvaVzw\KrakBoem\EventSourcing\Event;

final class ScoreFactorDecreased implements Event
{
    public function __construct(
        public readonly ScoreSheetIdentifier $scoreSheetIdentifier,
        public readonly GameNumber $gameNumber,
        public readonly ScoreFactor $newScoreFactor,
    ) {
    }

    public function getAggregateRootIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }
}
