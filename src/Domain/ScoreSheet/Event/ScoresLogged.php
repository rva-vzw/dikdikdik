<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Event;

use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class ScoresLogged implements Event
{
    public function __construct(
        public ScoreSheetIdentifier $scoreSheetIdentifier,
        public GameNumber $gameNumber,
        public ScoreLine $scoreLine,
        public ScoreFactor $scoreFactor
    ) {
    }

    public function getAggregateRootIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }

    public function getNote(): string
    {
        return $this->scoreLine->getNote();
    }
}
