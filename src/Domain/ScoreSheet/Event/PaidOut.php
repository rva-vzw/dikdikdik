<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Event;

use App\Domain\ScoreSheet\Score\MultipliedWinnings;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifiers;
use RvaVzw\KrakBoem\EventSourcing\Event;

final readonly class PaidOut implements Event
{
    public function __construct(
        public ScoreSheetIdentifier $scoreSheetIdentifier,
        public GameNumber $gameNumber,
        public MultipliedWinnings $multipliedWinnings,
        public string $note,
        public ScoreLineNumber $scoreLineNumber
    ) {
    }

    public function getAggregateRootIdentifier(): ScoreSheetIdentifier
    {
        return $this->scoreSheetIdentifier;
    }

    public function getAffectedPlayers(): PlayerIdentifiers
    {
        return $this->multipliedWinnings->getAffectedPlayers();
    }
}
