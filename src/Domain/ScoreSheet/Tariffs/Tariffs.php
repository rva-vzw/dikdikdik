<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Tariffs;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\Table\Event\Game\GamePlayed;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Outcome\GameOutcome;

interface Tariffs
{
    public function getName(): string;

    /**
     * @template Announcement of GameAnnouncement
     * @template Outcome of GameOutcome
     *
     * @param GamePlayed<Announcement, Outcome> $event
     */
    public function calculateWinnings(GamePlayed $event): PlainWinnings;
}
