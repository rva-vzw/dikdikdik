<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Tariffs;

use App\Domain\Table\TableIdentifier;

// Hmm, maybe I should have created 'TariffsBySheet', because we're in
// the context of the score sheet.

interface TariffsByTable
{
    public function getTariffsForTable(TableIdentifier $tableIdentifier): Tariffs;
}
