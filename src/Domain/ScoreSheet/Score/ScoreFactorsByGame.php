<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Score;

use App\Domain\Table\GameNumber;

final class ScoreFactorsByGame
{
    /** @var ScoreFactor[] */
    private $scoreFactors;

    /**
     * @param ScoreFactor[] $scoreFactors
     */
    private function __construct(array $scoreFactors)
    {
        $this->scoreFactors = $scoreFactors;
    }

    public static function create(): self
    {
        return new self([]);
    }

    public function withScoreFactor(GameNumber $gameNumber, ScoreFactor $scoreFactor): self
    {
        $result = clone $this;
        $result->scoreFactors[$gameNumber->toInteger()] = $scoreFactor;

        return $result;
    }

    public function getScoreFactor(GameNumber $gameNumber): ScoreFactor
    {
        $key = $gameNumber->toInteger();

        if (array_key_exists($key, $this->scoreFactors)) {
            return $this->scoreFactors[$key];
        }

        return ScoreFactor::single();
    }

    public function withIncreasedScoreFactor(GameNumber $gameNumber): self
    {
        return $this->withScoreFactor(
            $gameNumber,
            $this->getScoreFactor($gameNumber)->increased()
        );
    }

    public function getLastGameWithExplicitFactor(): GameNumber
    {
        if (empty($this->scoreFactors)) {
            return GameNumber::first();
        }

        return GameNumber::fromInteger((int) max(array_keys($this->scoreFactors)));
    }
}
