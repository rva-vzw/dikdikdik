<?php

namespace App\Domain\ScoreSheet\Score;

use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;

interface FrancsForPlayers
{
    /**
     * Return the number of francs assigned to the player with the given identifier.
     *
     * Unknown players are assumed to have 0 francs.
     */
    public function getFrancsForPlayer(PlayerIdentifier $playerIdentifier): int;

    public function getKnownPlayerIdentifiers(): PlayerIdentifiers;
}
