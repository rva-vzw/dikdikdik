<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Score;

use App\Domain\Table\Player\PlayerIdentifier;

final class PlainWinningsBuilder
{
    /**
     * @var int[] associative array mapping player id to francs
     */
    private $francs;

    private function __construct()
    {
        $this->francs = [];
    }

    public static function create(): self
    {
        return new self();
    }

    public static function fromPlainWinnigs(PlainWinnings $plainWinnings): self
    {
        $result = new self();

        $result->francs = array_reduce(
            $plainWinnings->getKnownPlayerIdentifiers()->toArray(),
            /**
             * @param int[] $francs,
             * @result int[]
             */
            function (array $francs, PlayerIdentifier $playerIdentifier) use ($plainWinnings): array {
                $francs[$playerIdentifier->toString()] = $plainWinnings->getFrancsForPlayer($playerIdentifier);

                return $francs;
            },
            []
        );

        return $result;
    }

    public function withWinner(PlayerIdentifier $playerIdentifier, int $francs): self
    {
        $this->assertStrictlyPositive($francs);
        $result = clone $this;
        $result->francs[$playerIdentifier->toString()] = $francs;

        return $result;
    }

    private function assertStrictlyPositive(int $francs): void
    {
        if ($francs <= 0) {
            throw new \InvalidArgumentException('Positive value expected.');
        }
    }

    public function withLoser(PlayerIdentifier $playerIdentifier, int $francs): self
    {
        $this->assertStrictlyPositive($francs);
        $result = clone $this;
        $result->francs[$playerIdentifier->toString()] = -$francs;

        return $result;
    }

    public function build(): PlainWinnings
    {
        return new PlainWinnings($this->francs);
    }

    public function addedWith(PlainWinnings $plainWinnings): self
    {
        $result = array_reduce(
            $plainWinnings->getKnownPlayerIdentifiers()->toArray(),
            function (self $result, PlayerIdentifier $playerIdentifier) use ($plainWinnings): self {
                return $result->withFrancsForPlayer(
                    $playerIdentifier,
                    $result->getFrancsForPlayer($playerIdentifier) + $plainWinnings->getFrancsForPlayer($playerIdentifier)
                );
            },
            $this
        );

        return $result;
    }

    private function getFrancsForPlayer(PlayerIdentifier $playerIdentifier): int
    {
        if (array_key_exists($playerIdentifier->toString(), $this->francs)) {
            return $this->francs[$playerIdentifier->toString()];
        }

        return 0;
    }

    private function withFrancsForPlayer(PlayerIdentifier $playerIdentifier, int $francs): self
    {
        $result = clone $this;
        $result->francs[$playerIdentifier->toString()] = $francs;

        return $result;
    }
}
