<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Score;

use App\Domain\ScoreSheet\Exception\ScoresDontAddUpToZero;
use App\Domain\Table\Player\PlayerIdentifier;

final class ScoreLineBuilder
{
    /**
     * @var int[] associative array mapping player id to francs
     */
    private $francs;

    /** @var string note */
    private $note;
    /** @var ScoreLineNumber */
    private $scoreLineNumber;

    private function __construct(string $note, ScoreLineNumber $scoreLineNumber)
    {
        $this->francs = [];
        $this->note = $note;
        $this->scoreLineNumber = $scoreLineNumber;
    }

    public static function fromPrecedingScoreLine(ScoreLine $scoreLine): self
    {
        $result = new self(
            $scoreLine->getNote(),
            $scoreLine->getScoreLineNumber()->getNext()
        );
        $scoreLine->getKnownPlayerIdentifiers()->each(
            function (PlayerIdentifier $playerIdentifier) use (&$result, $scoreLine): void {
                $result = $result->withFrancsForPlayer(
                    $playerIdentifier,
                    $scoreLine->getFrancsForPlayer($playerIdentifier)
                );
            }
        );

        return $result;
    }

    public function withFrancsForPlayer(PlayerIdentifier $playerIdentifier, int $francs): self
    {
        $result = clone $this;
        $result->francs[$playerIdentifier->toString()] = $francs;

        return $result;
    }

    public static function create(string $note, ScoreLineNumber $scoreLineNumber): self
    {
        return new self($note, $scoreLineNumber);
    }

    public function withAdditionalFrancsForPlayer(PlayerIdentifier $playerIdentifier, int $francs): self
    {
        $result = clone $this;

        if (!array_key_exists($playerIdentifier->toString(), $result->francs)) {
            $result->francs[$playerIdentifier->toString()] = 0;
        }

        $result->francs[$playerIdentifier->toString()] += $francs;

        return $result;
    }

    /**
     * @throws ScoresDontAddUpToZero
     */
    public function build(): ScoreLine
    {
        return new ScoreLine($this->francs, $this->note, $this->scoreLineNumber);
    }
}
