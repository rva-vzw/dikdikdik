<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Score;

use RvaVzw\KrakBoem\Id\IntWrapper;

final class ScoreLineNumber implements IntWrapper
{
    /** @var int */
    private $value;

    private function __construct(int $value)
    {
        $this->value = $value;
    }

    public static function fromInteger(int $value): self
    {
        return new self($value);
    }

    public static function first(): self
    {
        return new self(1);
    }

    public static function second(): self
    {
        return new self(2);
    }

    public static function third(): self
    {
        return new self(3);
    }

    public function toInteger(): int
    {
        return $this->value;
    }

    public function getNext(): self
    {
        return new self($this->value + 1);
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    public function diff(ScoreLineNumber $scoreLineNumber): int
    {
        return $this->value - $scoreLineNumber->value;
    }
}
