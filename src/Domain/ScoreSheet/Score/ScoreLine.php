<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Score;

use App\Domain\ScoreSheet\Exception\ScoresDontAddUpToZero;
use App\Domain\Table\Player\PlayerIdentifier;

/**
 * A score line shows the cumulative score of the players on a table for a certain game.
 */
final class ScoreLine extends AbstractFrancsTransaction
{
    /** @var string */
    private $note;
    /** @var ScoreLineNumber */
    private $scoreLineNumber;

    /**
     * @param int[] $francs
     *
     * @throws ScoresDontAddUpToZero
     */
    public function __construct(
        array $francs,
        string $note,
        ScoreLineNumber $scoreLineNumber
    ) {
        parent::__construct($francs);
        $this->note = $note;
        $this->scoreLineNumber = $scoreLineNumber;
    }

    public static function empty(ScoreLineNumber $scoreLineNumber): self
    {
        return new self([], '', $scoreLineNumber);
    }

    public static function fromMultipliedWinnings(
        MultipliedWinnings $multipliedWinnings,
        string $note,
        ScoreLineNumber $scoreLineNumber
    ): self {
        $builder = ScoreLineBuilder::create($note, $scoreLineNumber);

        $multipliedWinnings->getPlayerIdentifiers()->each(
            function (PlayerIdentifier $playerIdentifier) use (&$builder, $multipliedWinnings): void {
                $builder = $builder->withFrancsForPlayer(
                    $playerIdentifier,
                    $multipliedWinnings->getFrancsForPlayer($playerIdentifier)
                );
            }
        );

        return $builder->build();
    }

    public static function fromPlainWinnings(
        PlainWinnings $winnings,
        string $note,
        ScoreLineNumber $scoreLineNumber,
    ): self {
        $builder = ScoreLineBuilder::create($note, $scoreLineNumber);

        $winnings->getKnownPlayerIdentifiers()->each(
            function (PlayerIdentifier $playerIdentifier) use (&$builder, $winnings): void {
                $builder = $builder->withFrancsForPlayer(
                    $playerIdentifier,
                    $winnings->getFrancsForPlayer($playerIdentifier)
                );
            }
        );

        return $builder->build();
    }

    public function getNext(MultipliedWinnings $multipliedWinnings, string $note): self
    {
        $builder = ScoreLineBuilder::fromPrecedingScoreLine($this);
        $multipliedWinnings->getPlayerIdentifiers()->each(
            function (PlayerIdentifier $playerIdentifier) use (&$builder, $multipliedWinnings): void {
                $builder = $builder->withAdditionalFrancsForPlayer(
                    $playerIdentifier,
                    $multipliedWinnings->getFrancsForPlayer($playerIdentifier)
                );
            }
        );

        // The cumulative score always shows the latest note.
        return $builder->build()->withNote($note);
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function withNote(string $note): self
    {
        $result = clone $this;
        $result->note = $note;

        return $result;
    }

    public function getScoreLineNumber(): ScoreLineNumber
    {
        return $this->scoreLineNumber;
    }

    public function shiftedDown(): self
    {
        $result = clone $this;
        $result->scoreLineNumber = $this->scoreLineNumber->getNext();

        return $result;
    }
}
