<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Score;

use App\Domain\ScoreSheet\Exception\ScoresDontAddUpToZero;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;

abstract class AbstractFrancsTransaction implements FrancsTransaction
{
    /**
     * @var array<non-empty-string, int> associative array mapping player id to francs
     */
    protected $francs;

    /**
     * @param int[] $francs associative array mapping player id to francs
     *
     * Can't make this protected, because this is used by {@see PlainWinningsBuilder}
     *
     * @throws ScoresDontAddUpToZero
     */
    public function __construct(array $francs)
    {
        $this->francs = $francs;
        $this->guardZeroSum();
    }

    public function getKnownPlayerIdentifiers(): PlayerIdentifiers
    {
        $playerIds = array_keys($this->francs);

        return PlayerIdentifiers::fromArray(array_map(
            function (string $playerId): PlayerIdentifier {
                return PlayerIdentifier::fromString($playerId);
            },
            $playerIds
        ));
    }

    /**
     * Return the number of francs assigned to the player with the given identifier.
     *
     * Unknown players are assumed to have 0 francs.
     */
    public function getFrancsForPlayer(PlayerIdentifier $playerIdentifier): int
    {
        return array_key_exists(
            $playerIdentifier->toString(),
            $this->francs
        ) ? $this->francs[$playerIdentifier->toString()] : 0;
    }

    /**
     * @throws ScoresDontAddUpToZero
     */
    public function guardZeroSum(): void
    {
        if (0 == array_sum($this->francs)) {
            return;
        }

        throw new ScoresDontAddUpToZero();
    }

    public function knowsAbout(PlayerIdentifier $playerIdentifier): bool
    {
        return array_key_exists($playerIdentifier->toString(), $this->francs);
    }

    public function hasFrancs(): bool
    {
        $result = !empty($this->francs);

        return $result;
    }

    public function getAffectedPlayers(): PlayerIdentifiers
    {
        return PlayerIdentifiers::fromArray(array_map(
            fn (string $playerId) => PlayerIdentifier::fromString($playerId),
            array_keys(array_filter($this->francs)),
        ));
    }
}
