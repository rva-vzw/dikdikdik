<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Score;

use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Player\PlayersInvolved;
use Webmozart\Assert\Assert;

/**
 * Value object to describe how many francs each player wins, disregarding the score factor.
 */
final class PlainWinnings extends AbstractFrancsTransaction
{
    public static function singlePlayerPays(
        PlayerIdentifier $loser,
        PlayerIdentifiers $winners,
        int $francsPerPlayer,
    ): self {
        return self::singlePlayerWins(
            $loser,
            $winners,
            -$francsPerPlayer,
        );
    }

    public static function singlePlayerWins(
        PlayerIdentifier $winner,
        PlayerIdentifiers $losers,
        int $francsPerPlayer,
    ): self {
        $francsForPlayers = array_reduce(
            $losers->toArray(),
            function (array $francsForPlayer, PlayerIdentifier $playerIdentifier) use ($francsPerPlayer): array {
                $francsForPlayer[$playerIdentifier->toString()] = -$francsPerPlayer;

                return $francsForPlayer;
            },
            [$winner->toString() => $francsPerPlayer * 3]
        );

        return new self($francsForPlayers);
    }

    public static function twoPlayersWin(
        PlayerIdentifiers $winners,
        PlayerIdentifiers $losers,
        int $francsPerPlayer,
    ): self {
        Assert::eq($winners->getCount(), 2);
        Assert::eq($losers->getCount(), 2);

        return new self([
            (string) $winners->toArray()[0] => $francsPerPlayer,
            (string) $winners->toArray()[1] => $francsPerPlayer,
            (string) $losers->toArray()[0] => -$francsPerPlayer,
            (string) $losers->toArray()[1] => -$francsPerPlayer,
        ]);
    }

    public static function twoPlayersPay(
        PlayerIdentifiers $winners,
        PlayerIdentifiers $losers,
        int $francsPerPlayer,
    ): self {
        return self::twoPlayersWin(
            $losers,
            $winners,
            $francsPerPlayer,
        );
    }

    public static function none(): self
    {
        return new self([]);
    }

    public static function dealerLooses(int $francsPerPlayer, PlayersInvolved $playersInvolved): self
    {
        return self::dealerWins(-$francsPerPlayer, $playersInvolved);
    }

    public static function create(PlayerIdentifiers $winners, PlayerIdentifiers $losers, int $francsPerPlayer): self
    {
        if (1 === $winners->getCount()) {
            return self::singlePlayerWins(
                $winners->getSingle(),
                $losers,
                $francsPerPlayer,
            );
        }

        if (1 === $losers->getCount()) {
            return self::singlePlayerPays(
                $losers->getSingle(),
                $winners,
                $francsPerPlayer,
            );
        }

        if (2 === $winners->getCount() && 2 === $losers->getCount()) {
            return self::twoPlayersWin(
                $winners,
                $losers,
                $francsPerPlayer,
            );
        }

        throw new \InvalidArgumentException('PlainWinnings: unexpected number of winners or losers', );
    }

    public function addedWith(PlainWinnings $winnings): self
    {
        return PlainWinningsBuilder::fromPlainWinnigs($this)
            ->addedWith($winnings)
            ->build();
    }

    public static function dealerWins(
        int $francsPerPlayer,
        PlayersInvolved $playersInvolved
    ): self {
        $dealer = $playersInvolved->getDealerIdentifier();

        $payingPlayers = $playersInvolved->getPlayingPlayers()
            ->without($dealer);

        $francsForPlayers = array_reduce(
            $payingPlayers->toArray(),
            function (array $francsForPlayer, PlayerIdentifier $playerIdentifier) use ($francsPerPlayer): array {
                $francsForPlayer[$playerIdentifier->toString()] = -$francsPerPlayer;

                return $francsForPlayer;
            },
            [
                $dealer->toString() => $francsPerPlayer * $payingPlayers->getCount(),
            ]
        );

        return new self($francsForPlayers);
    }

    public function reversed(): self
    {
        return new self(
            array_map(
                fn (int $francs) => -$francs,
                $this->francs,
            ),
        );
    }

    public function doubled(): self
    {
        return new self(
            array_map(
                fn (int $francs) => 2 * $francs,
                $this->francs,
            ),
        );
    }
}
