<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Score;

use RvaVzw\KrakBoem\Id\IntWrapper;

final class ScoreFactor implements IntWrapper
{
    /** @var int */
    private $value;

    private function __construct(int $value)
    {
        $this->value = $value;
        $this->guardStrictlyPositive();
    }

    public static function single(): self
    {
        return new self(1);
    }

    /**
     * @return self
     */
    public static function fromInteger(int $value): IntWrapper
    {
        return new self($value);
    }

    public static function double(): self
    {
        return new self(2);
    }

    public function toInteger(): int
    {
        return $this->value;
    }

    public function increased(): self
    {
        return new self($this->value + 1);
    }

    private function guardStrictlyPositive(): void
    {
        if ($this->value > 0) {
            return;
        }

        throw new \InvalidArgumentException();
    }

    public function decreased(): self
    {
        return new self($this->value - 1);
    }
}
