<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Score;

use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;

final class MultipliedWinnings
{
    /** @var PlainWinnings */
    private $plainWinnings;
    /** @var ScoreFactor */
    private $scoreFactor;

    public function __construct(PlainWinnings $plainWinnings, ScoreFactor $scoreFactor)
    {
        $this->plainWinnings = $plainWinnings;
        $this->scoreFactor = $scoreFactor;
    }

    public static function plain(PlainWinnings $winnings): self
    {
        return new self($winnings, ScoreFactor::single());
    }

    public function getPlainWinnings(): PlainWinnings
    {
        return $this->plainWinnings;
    }

    public function getScoreFactor(): ScoreFactor
    {
        return $this->scoreFactor;
    }

    public function getPlayerIdentifiers(): PlayerIdentifiers
    {
        return $this->plainWinnings->getKnownPlayerIdentifiers();
    }

    public function getFrancsForPlayer(PlayerIdentifier $playerIdentifier): int
    {
        return $this->scoreFactor->toInteger() * $this->plainWinnings->getFrancsForPlayer($playerIdentifier);
    }

    public function getAffectedPlayers(): PlayerIdentifiers
    {
        return $this->plainWinnings->getAffectedPlayers();
    }
}
