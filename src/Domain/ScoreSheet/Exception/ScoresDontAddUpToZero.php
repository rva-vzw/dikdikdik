<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet\Exception;

final class ScoresDontAddUpToZero extends \Exception
{
}
