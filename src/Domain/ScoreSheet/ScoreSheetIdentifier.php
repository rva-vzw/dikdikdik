<?php

declare(strict_types=1);

namespace App\Domain\ScoreSheet;

use App\Domain\Table\TableIdentifier;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;
use RvaVzw\KrakBoem\Id\Uuid5Identifier;

final readonly class ScoreSheetIdentifier extends Uuid5Identifier implements AggregateRootIdentifier
{
    protected static function getNamespace(): string
    {
        return '3ff4f493-dff0-46c3-acc9-3dbf1ffaab68';
    }

    public static function forTable(TableIdentifier $tableIdentifier): self
    {
        return self::fromName($tableIdentifier->toString());
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
