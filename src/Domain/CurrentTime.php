<?php

declare(strict_types=1);

namespace App\Domain;

interface CurrentTime
{
    public function getCurrentTime(): \DateTimeImmutable;
}
