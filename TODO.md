# TODO

(voorlopig all done)

# Fixup config-scherm

Na #357, doe ik #298.



# Terminologie

Na al die jaren heb ik mijn 'ubiquitous language nog steeds niet op punt.

* TableConventions: afspraken, ruimer dan spelregels
  * allowed games (valt onder spelregels)
  * tarieven (dat zijn geen spelregels)
  * advanced passed around (spelregels)
  * last round score factor (geen spelregels)
  * strafpunten (geen spelregels)

Deze conventions zijn eigenlijk configuratie, ze leven in een aparte namespace.

GameRules zijn table-specifieke spelregels. Die leven in de Table-namespace. PermissibleGames zijn hiervan een
voorbeeld.

De tarieven zitten normaal gesproken ergens onder de ScoreSheets.