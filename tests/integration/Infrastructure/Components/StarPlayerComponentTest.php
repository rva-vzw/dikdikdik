<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure\Components;

use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\KickPlayer;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Seat;
use App\Infrastructure\Components\StarPlayerComponent;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class StarPlayerComponentTest extends AbstractEventSourcingIntegrationTest
{
    private readonly StarPlayerComponent $starPlayerComponent;

    protected function setUp(): void
    {
        parent::setUp();
        /** @var StarPlayerComponent $component */
        $component = $this->getContainer()->get(StarPlayerComponent::class);
        $component->tableIdentifier = $this->tableIdentifier;
        $this->starPlayerComponent = $component;
    }

    /** @test */
    public function itAllowsStarringAbsentPlayer(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'penn', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', Seat::four()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dvl(), GameNumber::first(), 'dvl', Seat::five()),
            new AnnounceDealer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
            // Pass around, so that all players become active
            new PassAround($this->tableIdentifier, GameNumber::first()),
            // Kick a player
            new KickPlayer($this->tableIdentifier, TestPlayer::dvl(), GameNumber::first()),
        );

        $this->starPlayerComponent->gameNumber = GameNumber::first();

        // Should be able to star the kicked player:
        $this->assertTrue(
            $this->starPlayerComponent->getKnownPlayers()->has(TestPlayer::dvl()),
        );
    }
}
