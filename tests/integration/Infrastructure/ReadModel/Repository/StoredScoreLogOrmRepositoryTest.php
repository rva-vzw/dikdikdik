<?php

declare(strict_types=1);

namespace App\Tests\integration\Infrastructure\ReadModel\Repository;

use App\Domain\ScoreSheet\Score\MultipliedWinnings;
use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\ReadModel\Entity\OrmStoredLogEntry;
use App\Infrastructure\ReadModel\Repository\StoredScoreLogOrmRepository;
use App\TestData\TestDataBuilders\TestPlayer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class StoredScoreLogOrmRepositoryTest extends KernelTestCase
{
    /** @var StoredScoreLogOrmRepository */
    private $repository;

    /** @var ScoreSheetIdentifier */
    private $scoreSheetIdentifier;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        /** @var StoredScoreLogOrmRepository $repository */
        $repository = self::getContainer()->get(StoredScoreLogOrmRepository::class);
        $this->repository = $repository;

        $this->scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
            TableIdentifier::fromString('77E2F21C-BA5F-49EC-B787-A31EC62065F4')
        );

        $this->repository->deleteForScoreSheet($this->scoreSheetIdentifier);
    }

    /** @test */
    public function itRetrievesTheEntriesForAScoreSheet(): void
    {
        $someEntry = new OrmStoredLogEntry(
            GameNumber::first(),
            $this->scoreSheetIdentifier,
            TestPlayer::diu(),
            ScoreFactor::single(),
            ScoreLine::fromMultipliedWinnings(
                new MultipliedWinnings(
                    PlainWinnings::none(),
                    ScoreFactor::single()
                ),
                'test',
                ScoreLineNumber::first()
            )
        );

        $this->repository->save($someEntry);

        $actual = iterator_to_array($this->repository->generateForScoreSheet($this->scoreSheetIdentifier));
        $expected = [1 => $someEntry];

        $this->assertEquals($expected, $actual);
    }
}
