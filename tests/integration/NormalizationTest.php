<?php

declare(strict_types=1);

/** @noinspection PhpUnhandledExceptionInspection */

namespace App\Tests\integration;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\Player\Model\PresentPlayers;
use App\Domain\ReadModel\ScoreLog\Model\SimpleScoreLogEntry;
use App\Domain\ScoreSheet\Event\ScoresLogged;
use App\Domain\ScoreSheet\Score\MultipliedWinnings;
use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Event\Game\Alone;
use App\Domain\Table\Event\Game\MisereOnTheTable;
use App\Domain\Table\Event\Game\PropAndCop;
use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\Event\TableCleared;
use App\Domain\Table\Event\Undo\PropAndCopUndone;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TroelAwareMultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Loggable\Outcome\Winners;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestPlayer;

/**
 * Normalization of events seems hard. Here are some tests.
 *
 * I got event normalization working using the PropertyNormalizer; this is configured in services.yaml.
 */
final class NormalizationTest extends AbstractEventSourcingIntegrationTest
{
    /** @test */
    public function itNormalizesPlayerClaimedTable(): void
    {
        $event = new TableCleared(
            TableIdentifier::fromString('a3e71bd3-cd15-4dc5-88d5-f2a7f42aea66')
        );

        $normalized = $this->getNormalizer()->normalize($event);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, TableCleared::class);

        $this->assertEquals($event, $denormalized);
    }

    /** @test */
    public function itNormalizesATableIdentifier(): void
    {
        $id = TableIdentifier::fromString('48d44d6a-78cc-44b5-a9cf-110a340abe76');

        $normalized = $this->getNormalizer()->normalize($id);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, TableIdentifier::class);

        $this->assertEquals($id, $denormalized);
    }

    /** @test */
    public function itNormalizesPlayerJoined(): void
    {
        $event = new PlayerJoined(
            TableIdentifier::fromString('a3e71bd3-cd15-4dc5-88d5-f2a7f42aea66'),
            PlayerIdentifier::fromString('88f2f9fe-ff80-4001-8630-9286895bda50'),
            GameNumber::first(),
            Seat::three(),
            'dcb',
        );

        $normalized = $this->getNormalizer()->normalize($event);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, PlayerJoined::class);

        $this->assertEquals($event, $denormalized);
    }

    /**
     * @test
     *
     * This test works because of the IdentifierNormalizer, also configured in services.yaml.
     */
    public function itNormalizesArrayOfPlayerIdentifiers(): void
    {
        $array = [
            PlayerIdentifier::fromString('86887d20-4566-4842-bf21-f00fc9a296b3'),
            PlayerIdentifier::fromString('2ffe70b6-d050-47fa-8d54-1136264ed618'),
        ];

        $normalized = $this->getNormalizer()->normalize($array);

        $expected = [
            '86887d20-4566-4842-bf21-f00fc9a296b3',
            '2ffe70b6-d050-47fa-8d54-1136264ed618',
        ];

        $this->assertEquals($expected, $normalized);
    }

    /** @test */
    public function itNormalizesGameNumber(): void
    {
        $gameNumber = GameNumber::first();

        $normalized = $this->getNormalizer()->normalize($gameNumber);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, GameNumber::class);

        $this->assertEquals($gameNumber, $denormalized);
    }

    /** @test */
    public function itNormalizesAlone(): void
    {
        $loggedAt = new \DateTimeImmutable('2021-04-03');
        $event = new Alone(
            $this->tableIdentifier,
            GameNumber::first(),
            new SinglePlayerAnnouncement(
                TestPlayer::secretaris()
            ),
            NumberOfTricks::fromNumber(6),
            $this->get4InvolvedPlayers(),
            $loggedAt,
        );

        $expectedNormalized = [
            // I think the __gameName property should not be needed anymore,
            // but it is still used in GamePlayedNormalizer.
            '__gameName' => 'alone',
            'tableIdentifier' => $this->tableIdentifier->toString(),
            'gameNumber' => 1,
            'announcement' => [
                'player' => TestIds::PLAYER_SECRETARIS,
            ],
            'outcome' => [
                'number' => 6,
            ],
            'playersInvolved' => [
                'playingPlayers' => [
                    TestIds::PLAYER_PENNINGMEESTER,
                    TestIds::PLAYER_SECRETARIS,
                    TestIds::PLAYER_DLB,
                    TestIds::PLAYER_DVL,
                ],
                'dealerIdentifier' => TestIds::PLAYER_PENNINGMEESTER,
            ],
            // DateTime normalization seems to depend on server setup, which is not ok,
            // but let's just work around this problem right now.
            'loggedAt' => $this->getNormalizer()->normalize($loggedAt),
        ];
        $normalized = $this->getNormalizer()->normalize($event);
        $this->assertIsArray($normalized);

        $this->assertEquals($expectedNormalized, $normalized);

        $denormalized = $this->getDenormalizer()->denormalize($normalized, Alone::class);
        $this->assertEquals($event, $denormalized);
    }

    /** @test */
    public function itNormalizesSeatedPlayers(): void
    {
        $seatedPlayers = SeatedPlayers::fromArray([
            0 => TestPlayer::dtl(),
            3 => TestPlayer::penningmeester(),
        ]);

        $normalized = $this->getNormalizer()->normalize($seatedPlayers);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, SeatedPlayers::class);

        $this->assertEquals($seatedPlayers, $denormalized);
    }

    /** @test */
    public function itNormalizesScoresLogged(): void
    {
        $event = new ScoresLogged(
            $this->scoreSheetIdentifier,
            GameNumber::first(),
            ScoreLine::fromMultipliedWinnings(
                new MultipliedWinnings(
                    new PlainWinnings(
                        [
                            TestIds::PLAYER_DBFC => 3,
                            TestIds::PLAYER_DVL => 3,
                            TestIds::PLAYER_DLB => 3,
                            TestIds::PLAYER_SECRETARIS => -9,
                        ]
                    ),
                    ScoreFactor::single()
                ),
                AloneSpecification::NAME,
                ScoreLineNumber::first()
            ),
            ScoreFactor::single()
        );

        $normalized = $this->getNormalizer()->normalize($event);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, ScoresLogged::class);

        $this->assertEquals($event, $denormalized);
    }

    /** @test */
    public function itNormalizesPlayersInvolved(): void
    {
        $playersInvolved = $this->get4InvolvedPlayers();

        $normalized = $this->getNormalizer()->normalize($playersInvolved);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, PlayersInvolved::class);

        $this->assertEquals($playersInvolved, $denormalized);
    }

    private function get4InvolvedPlayers(): PlayersInvolved
    {
        return PlayersInvolved::from(
            SeatedPlayers::fromArray([
                0 => TestPlayer::penningmeester(),
                1 => TestPlayer::secretaris(),
                2 => TestPlayer::dlb(),
                3 => TestPlayer::dvl(),
            ]),
            TestPlayer::penningmeester()
        );
    }

    /** @test */
    public function itNormalizesTroelAwareMultiPlayerAnnouncement(): void
    {
        $announcement = new TroelAwareMultiPlayerAnnouncement(
            PlayerIdentifiers::fromArray([
                TestPlayer::dtl(),
            ]),
            true
        );

        $normalized = $this->getNormalizer()->normalize($announcement);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, TroelAwareMultiPlayerAnnouncement::class);

        $this->assertEquals($announcement, $denormalized);
    }

    /** @test */
    public function itNormalizesMisereOnTheTable(): void
    {
        $announcement = new TroelAwareMultiPlayerAnnouncement(
            PlayerIdentifiers::fromArray([
                TestPlayer::dtl(),
            ]),
            true
        );

        $event = new MisereOnTheTable(
            $this->tableIdentifier,
            GameNumber::first(),
            $announcement,
            new Winners(PlayerIdentifiers::fromArray([TestPlayer::dtl()])),
            $this->get4InvolvedPlayers(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $normalized = $this->getNormalizer()->normalize($event);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, MisereOnTheTable::class);

        $this->assertEquals($event, $denormalized);
    }

    /**
     * @test
     */
    public function itDenormalizesEmptyArrayToSeatedPlayers(): void
    {
        /** @var SeatedPlayers $seatedPlayers */
        $seatedPlayers = $this->getDenormalizer()->denormalize([], SeatedPlayers::class);

        $this->assertEquals(SeatedPlayers::none(), $seatedPlayers);
    }

    /** @test */
    public function itNormalizesPlayerDetailsSet(): void
    {
        $playerDetailsSet = PlayersGroup::none()
            ->withPlayer(
                    PlayerIdentifier::fromString('62c3983e-cab2-4738-a917-5d13301e0ef9'),
                    Seat::one(),
                    'dtl',
            )->withPlayerActivated(
                PlayerIdentifier::fromString('62c3983e-cab2-4738-a917-5d13301e0ef9'),
            );

        $normalized = $this->getNormalizer()->normalize($playerDetailsSet);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, PlayersGroup::class);

        $this->assertEquals($playerDetailsSet, $denormalized);
    }

    /** @test */
    public function itNormalizesPlainWinnings(): void
    {
        $winnings = PlainWinnings::dealerWins(
            1,
            PlayersInvolved::from(
                SeatedPlayers::fromArray([
                    TestPlayer::diu(),
                    TestPlayer::dpb(),
                    TestPlayer::secretaris(),
                    TestPlayer::dtl(),
                    TestPlayer::penningmeester(),
                ]),
                TestPlayer::diu()
            )
         );

        $normalized = $this->getNormalizer()->normalize($winnings);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, PlainWinnings::class);

        $this->assertEquals($winnings, $denormalized);
    }

    /** @test */
    public function itNormalizesSimpleScoreLogEntry(): void
    {
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable(TableIdentifier::fromString('bed8d7b0-df43-4094-8f61-c4b56a6e1e70'));
        $scoreLogEntry = new SimpleScoreLogEntry(
            TestPlayer::diu(),
            ScoreFactor::single(),
            GameNumber::first(),
            ScoreLine::fromMultipliedWinnings(
                new MultipliedWinnings(
                    PlainWinnings::none(),
                    ScoreFactor::single()
                ),
                'test',
                ScoreLineNumber::first()
            ),
            $scoreSheetIdentifier,
        );

        /** @var mixed[] */
        $normalized = $this->getNormalizer()->normalize($scoreLogEntry);

        // Check some array keys that should not change, because
        // they're used by the normalization for the API.
        $this->assertIsArray($normalized['scores']);
        $this->assertEquals(1, $normalized['line']);

        /** @var SimpleScoreLogEntry */
        $denormalized = $this->getDenormalizer()->denormalize($normalized, SimpleScoreLogEntry::class);

        $this->assertEquals($scoreLogEntry, $denormalized);
    }

    /** @test */
    public function itNormalizesPresentPlayers(): void
    {
        $presentPlayers = PresentPlayers::none();
        $normalized = $this->getNormalizer()->normalize($presentPlayers);
        $denormalized = $this->getDenormalizer()->denormalize($normalized, PresentPlayers::class);

        $this->assertEquals($presentPlayers, $denormalized);
    }

    /** @test */
    public function itNormalizesUndoEvent(): void
    {
        $propAndCopUndone = new PropAndCopUndone(
            TableIdentifier::fromString('e2dbd362-ff43-4390-8910-1203db17032a'),
            GameNumber::first(),
            new TwoPlayerAnnouncement(TestPlayer::dlb(), TestPlayer::dvl()),
            new NumberOfTricks(7),
            $this->get4InvolvedPlayers(),
            1,
        );

        $normalized = $this->getNormalizer()->normalize($propAndCopUndone);
        $actual = $this->getDenormalizer()->denormalize($normalized, PropAndCopUndone::class);

        $this->assertEquals($propAndCopUndone, $actual);
    }

    /** @test */
    public function itNormalizesPlayersGroup(): void
    {
        $playersGroup = PlayersGroup::fromArray([
            (Player::createGone(TestPlayer::secretaris(), Seat::zero(), 'sec')),
            (Player::createActive(TestPlayer::penningmeester(), Seat::one(), 'pen')),
            (Player::createActive(TestPlayer::dpb(), Seat::two(), 'dpb')),
            (Player::createActive(TestPlayer::dpb(), Seat::three(), 'dpb')),
            (Player::createActive(TestPlayer::ddl(), Seat::four(), 'ddl')->dealing()),
        ]);

        $normalized = $this->getNormalizer()->normalize($playersGroup);
        $actual = $this->getDenormalizer()->denormalize($normalized, PlayersGroup::class);

        $this->assertEquals($playersGroup, $actual);
    }

    /** @test */
    public function itNormalizesTwoPlayerAnnouncement(): void
    {
        $announcement = new TwoPlayerAnnouncement(
            TestPlayer::dtl(),
            TestPlayer::ddl(),
        );

        $actual = $this->getNormalizer()->normalize($announcement);
        $expected = [
            'player1' => TestIds::PLAYER_DTL,
            'player2' => TestIds::PLAYER_DDL,
        ];

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itNormalizesPropAndCop(): void
    {
        $loggedAt = new \DateTimeImmutable('2024-03-29');
        $event = new PropAndCop(
            $this->tableIdentifier,
            GameNumber::first(),
            new TwoPlayerAnnouncement(
                TestPlayer::secretaris(),
                TestPlayer::penningmeester(),
            ),
            NumberOfTricks::fromNumber(7),
            $this->get4InvolvedPlayers(),
            $loggedAt,
        );

        $expectedNormalized = [
            // I think the __gameName property should not be needed anymore,
            // but it is still used in GamePlayedNormalizer.
            '__gameName' => 'prop_and_cop',
            'tableIdentifier' => $this->tableIdentifier->toString(),
            'gameNumber' => 1,
            'announcement' => [
                'player1' => TestIds::PLAYER_SECRETARIS,
                'player2' => TestIds::PLAYER_PENNINGMEESTER,
            ],
            'outcome' => [
                'number' => 7,
            ],
            'playersInvolved' => [
                'playingPlayers' => [
                    TestIds::PLAYER_PENNINGMEESTER,
                    TestIds::PLAYER_SECRETARIS,
                    TestIds::PLAYER_DLB,
                    TestIds::PLAYER_DVL,
                ],
                'dealerIdentifier' => TestIds::PLAYER_PENNINGMEESTER,
            ],
            // DateTime normalization seems to depend on server setup, which is not ok,
            // but let's just work around this problem right now.
            'loggedAt' => $this->getNormalizer()->normalize($loggedAt),
        ];
        $normalized = $this->getNormalizer()->normalize($event);
        $this->assertIsArray($normalized);

        $this->assertEquals($expectedNormalized, $normalized);
    }
}
