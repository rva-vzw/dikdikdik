<?php

declare(strict_types=1);

namespace App\Tests\integration\Controller\Helper;

use App\Domain\CurrentTime;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\MisereSpecification;
use App\Domain\Table\Loggable\Outcome\Winners;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\Controller\Helper\LogGameCommandFactory;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestPlayer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class LogGameCommandFactoryTest extends KernelTestCase
{
    /** @var LogGameCommandFactory */
    private $logGameCommandFactory;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        /** @var LogGameCommandFactory */
        $logGameCommandFactory = self::getContainer()->get(LogGameCommandFactory::class);
        $logGameCommandFactory->setCurrentTime(
            new class() implements CurrentTime {
                public function getCurrentTime(): \DateTimeImmutable
                {
                    return new \DateTimeImmutable('2021-04-03');
                }
            }
        );
        $this->logGameCommandFactory = $logGameCommandFactory;
    }

    /** @test */
    public function itAssemblesLogGameForMisere(): void
    {
        $tableIdentifier = TableIdentifier::fromString('AF02B73A-2DD1-4D7A-89AC-822DA7032A09');
        $misereSpecification = new MisereSpecification();

        $params = [
            'announcement' => [
                'playerSet' => [TestIds::PLAYER_DTL],
            ],
            'outcome' => [
                'winners' => [],
            ],
        ];

        $actual = $this->logGameCommandFactory->createCommandFromArray(
            $tableIdentifier,
            GameNumber::first(),
            $misereSpecification,
            $params
        );

        $expected = new LogGame(
            $tableIdentifier,
            GameNumber::first(),
            $misereSpecification,
            new MultiPlayerAnnouncement(
                PlayerIdentifiers::fromArray([TestPlayer::dtl()])
            ),
            Winners::none(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $this->assertEquals($expected, $actual);
    }
}
