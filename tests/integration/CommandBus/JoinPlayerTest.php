<?php

declare(strict_types=1);

namespace App\Tests\integration\CommandBus;

use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Seat;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class JoinPlayerTest extends AbstractEventSourcingIntegrationTest
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function commandInducesEvent(): void
    {
        $newPlayerIdentifier = PlayerIdentifier::fromString('1e211ba2-68ad-408b-81d9-ff273ec4c5c9');
        $command = new JoinPlayer(
            $this->tableIdentifier,
            $newPlayerIdentifier,
            GameNumber::first(),
            'dcb',
            Seat::three(),
        );

        $this->getCommandBus()->dispatch($command);

        $actualEvents = iterator_to_array($this->getEventStore()->getStreamForAggregate($this->tableIdentifier));
        $expectedEvent = new PlayerJoined($this->tableIdentifier, $newPlayerIdentifier, GameNumber::first(), Seat::three(), 'dcb');

        $this->assertContainsEquals($expectedEvent, $actualEvents);
    }
}
