<?php

declare(strict_types=1);

namespace App\Tests\integration\CommandBus;

use App\Domain\ReadModel\ScoreLog\Service\ScoreLogDeleter;
use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class PassAroundTwiceOnTableOf5Test extends AbstractEventSourcingIntegrationTest
{
    /** @var ScoreLogDeleter */
    private $scoreLogReadModel;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var ScoreLogDeleter $scoreLogReadModel */
        $scoreLogReadModel = self::getContainer()->get(ScoreLogDeleter::class);
        $this->scoreLogReadModel = $scoreLogReadModel;
        $this->scoreLogReadModel->deleteScoreLog($this->scoreSheetIdentifier);
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/.
     *
     * @test
     */
    public function itPassesAroundTwice(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'penn', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', Seat::four()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dvl(), GameNumber::first(), 'dvl', Seat::five()),
            new AnnounceDealer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
            // Since #129, passing around doesn't increase the game number, even on a table of 5.
            // See https://gitlab.com/rva-vzw/dikdikdik/issues/129
            new PassAround($this->tableIdentifier, GameNumber::first()),
            new PassAround($this->tableIdentifier, GameNumber::first())
        );
    }
}
