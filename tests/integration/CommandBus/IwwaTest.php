<?php

declare(strict_types=1);

namespace App\Tests\integration\CommandBus;

use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\ConfigureTable;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class IwwaTest extends AbstractEventSourcingIntegrationTest
{
    private AllSpecifiedGames $allSpecifiedGames;

    protected function setUp(): void
    {
        parent::setUp();
        /** @var AllSpecifiedGames $specifiedGames */
        $specifiedGames = $this->getContainer()->get(AllSpecifiedGames::class);
        $this->allSpecifiedGames = $specifiedGames;
    }

    /** @test */
    public function itAllowsLoggingAbundance12(): void
    {
        $this->prepareFourPlayerTable();
        $this->getCommandBus()->dispatch(
            new ConfigureTable(
                $this->tableIdentifier,
                'conventions.iwwa',
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                $this->allSpecifiedGames->getByGameName('abundance12'),
                new SinglePlayerAnnouncement(TestPlayer::dvt()),
                AllOrNothing::asSuccess(),
                new \DateTimeImmutable('2024-04-03'),
            ),
        );
    }

    private function prepareFourPlayerTable(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dvt(), GameNumber::first(), 'penn', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', Seat::four()),
            new AnnounceDealer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first())
        );
    }
}
