<?php

declare(strict_types=1);

namespace App\Tests\integration\CommandBus;

use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\Exception\InvalidAnnouncement;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TroelAwareMultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\MisereOnTheTableSpecification;
use App\Domain\Table\Loggable\GameSpecification\MisereSpecification;
use App\Domain\Table\Loggable\GameSpecification\PropAndCopSpecification;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;
use Symfony\Component\Messenger\Exception\HandlerFailedException;

// FIXME: I catch HandlerFailed exceptions in these tests, but I should check whether the inner exception is an InvalidAnnouncement.
final class AnnouncementValidationTest extends AbstractEventSourcingIntegrationTest
{
    /** @test */
    public function itValidatesTwoPlayerAnnouncement(): void
    {
        $this->prepareFivePlayerTable();

        $this->expectException(HandlerFailedException::class);
        $this->getCommandBus()->dispatch(
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(
                    TestPlayer::secretaris(),
                    // DTL is dealer and shouldn't be allowed to announce prop&cop.
                    TestPlayer::dtl()
                ),
                NumberOfTricks::fromNumber(10),
                new \DateTimeImmutable('2021-04-03'),
            )
        );
    }

    /** @test */
    public function itValidatesMultiPlayerAnnouncement(): void
    {
        $this->prepareFivePlayerTable();
        $this->expectException(HandlerFailedException::class);
        $this->getCommandBus()->dispatch(
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new MisereSpecification(),
                new MultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::dtl())),
                AllOrNothing::asSuccess(),
                new \DateTimeImmutable('2021-04-03'),
            )
        );
    }

    /** @test */
    public function itValidatesTroelAwareMultiPlayerAnnouncement(): void
    {
        $this->prepareFivePlayerTable();
        $this->expectException(HandlerFailedException::class);
        $this->getCommandBus()->dispatch(
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new MisereOnTheTableSpecification(),
                new TroelAwareMultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::dtl()), false),
                AllOrNothing::asSuccess(),
                new \DateTimeImmutable('2021-04-03'),
            )
        );
    }

    private function prepareFivePlayerTable(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'penn', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', Seat::four()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dvl(), GameNumber::first(), 'dvl', Seat::five()),
            new AnnounceDealer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first())
        );
    }
}
