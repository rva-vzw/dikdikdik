<?php

// Cluelessly copied (and adapted) this from
// https://github.com/Codeception/module-symfony/issues/99#issuecomment-735415461

$_ENV['APP_ENV'] = 'test';
(new Symfony\Component\Dotenv\Dotenv())->bootEnv(dirname(__DIR__).'/../.env.test');
