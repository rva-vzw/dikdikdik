<?php

namespace App\Tests\integration\Domain;

use App\Domain\Table\Loggable\Announcement\AnnouncementValidator;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\Player\SeatedPlayers;
use App\TestData\TestDataBuilders\TestPlayer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class AnnouncementValidatorTest extends KernelTestCase
{
    /** @var AnnouncementValidator */
    private $validator;

    /** @var PlayersInvolved */
    private $playersInvolved;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        /** @var AnnouncementValidator $validator */
        $validator = self::getContainer()->get(AnnouncementValidator::class);
        $this->validator = $validator;
        $this->playersInvolved = PlayersInvolved::from(
            SeatedPlayers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::secretaris(),
                TestPlayer::ddb(),
                TestPlayer::penningmeester(),
            ]),
            TestPlayer::dtl()
        );
    }

    /** @test */
    public function itAcceptsSinglePlayerAnnouncementWithKnownPlayer(): void
    {
        $announcement = new SinglePlayerAnnouncement(
            TestPlayer::ddb()
        );

        $result = $this->validator->isValidAnnouncement(
            $announcement, $this->playersInvolved
        );

        $this->assertTrue($result);
    }

    /** @test */
    public function itRejectsSinglePlayerAnnouncementWithUnknownPlayer(): void
    {
        $announcement = new SinglePlayerAnnouncement(
            TestPlayer::dbfc()
        );

        $result = $this->validator->isValidAnnouncement(
            $announcement, $this->playersInvolved
        );

        $this->assertFalse($result);
    }
}
