<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain;

use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;
use Symfony\Component\Messenger\Exception\HandlerFailedException;

final class AnnouncementValidationViaTableTest extends AbstractEventSourcingIntegrationTest
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testDealerCantAnnounceGameOn5PlayerTable(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer(
                $this->tableIdentifier,
                TestPlayer::dtl(),
                GameNumber::first(),
                'dtl',
                Seat::three()
            ),
            new JoinPlayer(
                $this->tableIdentifier,
                TestPlayer::dvl(),
                GameNumber::first(),
                'dvl',
                Seat::zero()
            ),
            new JoinPlayer(
                $this->tableIdentifier,
                TestPlayer::penningmeester(),
                GameNumber::first(),
                'penn',
                Seat::one()
            ),
            new JoinPlayer(
                $this->tableIdentifier,
                TestPlayer::secretaris(),
                GameNumber::first(),
                'sec',
                Seat::two()
            ),
            new JoinPlayer(
                $this->tableIdentifier,
                TestPlayer::ddb(),
                GameNumber::first(),
                'ddb',
                Seat::four()
            ),
            new AnnounceDealer(
                $this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()
            )
        );

        $this->expectException(HandlerFailedException::class);

        $this->getCommandBus()->dispatch(
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::dtl()),
                NumberOfTricks::fromNumber(6),
                new \DateTimeImmutable('2021-04-03')
            )
        );
    }
}
