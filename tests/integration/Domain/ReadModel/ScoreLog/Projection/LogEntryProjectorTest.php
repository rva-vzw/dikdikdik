<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\ScoreLog\Projection;

use App\Domain\ReadModel\ScoreLog\Projection\LogEntryProjector;
use App\Domain\ReadModel\ScoreLog\Service\LogEntryRepository;
use App\Domain\ScoreSheet\Event\PaidOut;
use App\Domain\ScoreSheet\Event\ScoreFactorIncreased;
use App\Domain\ScoreSheet\Event\ScoresCorrected;
use App\Domain\ScoreSheet\Event\ScoreSheetCreated;
use App\Domain\ScoreSheet\Event\ScoresLogged;
use App\Domain\ScoreSheet\Score\MultipliedWinnings;
use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestPlayer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class LogEntryProjectorTest extends KernelTestCase
{
    private TableIdentifier $tableIdentifier;
    private ScoreSheetIdentifier $scoreSheetIdentifier;

    private LogEntryProjector $projector;
    private LogEntryRepository $repository;

    protected function setUp(): void
    {
        self::bootKernel();

        /** @var LogEntryProjector $projector */
        $projector = $this->getContainer()->get(LogEntryProjector::class);
        $this->projector = $projector;

        /** @var LogEntryRepository $repository */
        $repository = $this->getContainer()->get(LogEntryRepository::class);
        $this->repository = $repository;

        $this->tableIdentifier = TableIdentifier::fromString('49d02cfd-d550-4cdf-b509-fad293e52a4e');
        // 5c1e9dec-4953-5a08-a66e-470698d38082
        $this->scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
            $this->tableIdentifier,
        );

        $this->repository->deleteForScoreSheet($this->scoreSheetIdentifier);
    }

    /** @test */
    public function itStillHandlesOldCorrectionLines(): void
    {
        $firstScoreLine = new ScoreLine(
            [
                (string) TestPlayer::dtl() => 2,
                (string) TestPlayer::penningmeester() => 2,
                (string) TestPlayer::dpb() => -2,
                (string) TestPlayer::secretaris() => -2,
            ],
            'prop_and_cop',
            ScoreLineNumber::first()
        );

        $correctedScoreLine = new ScoreLine(
            [
                (string) TestPlayer::dtl() => 3,
                (string) TestPlayer::penningmeester() => 3,
                (string) TestPlayer::dpb() => -3,
                (string) TestPlayer::secretaris() => -3,
            ],
            'correction',
            ScoreLineNumber::second(),
        );

        $anotherScoreLine = new ScoreLine(
            [
                (string) TestPlayer::dtl() => 1,
                (string) TestPlayer::penningmeester() => 5,
                (string) TestPlayer::dpb() => -1,
                (string) TestPlayer::secretaris() => -5,
            ],
            'prop_and_cop',
            ScoreLineNumber::third(),
        );

        $events = [
            new ScoreSheetCreated($this->scoreSheetIdentifier),
            new DealerAnnounced($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first()),
            new PaidOut(
                $this->scoreSheetIdentifier,
                GameNumber::first(),
                MultipliedWinnings::plain(new PlainWinnings([
                    (string) TestPlayer::dtl() => 2,
                    (string) TestPlayer::penningmeester() => 2,
                    (string) TestPlayer::dpb() => -2,
                    (string) TestPlayer::secretaris() => -2,
                ])),
                'prop_and_cop',
                ScoreLineNumber::first(),
            ),
            new ScoresLogged(
                $this->scoreSheetIdentifier,
                GameNumber::first(),
                $firstScoreLine,
                ScoreFactor::single(),
            ),
            new PaidOut(
                $this->scoreSheetIdentifier,
                GameNumber::first(),
                MultipliedWinnings::plain(new PlainWinnings([
                    (string) TestPlayer::dtl() => 1,
                    (string) TestPlayer::penningmeester() => 1,
                    (string) TestPlayer::dpb() => -1,
                    (string) TestPlayer::secretaris() => -1,
                ])),
                'correction',
                ScoreLineNumber::second(),
            ),
            new ScoresCorrected(
                $this->scoreSheetIdentifier,
                GameNumber::first(),
                $correctedScoreLine
            ),
            new ScoreFactorIncreased($this->scoreSheetIdentifier, GameNumber::second(), ScoreFactor::double()),
            new ScoreFactorIncreased($this->scoreSheetIdentifier, GameNumber::third(), ScoreFactor::double()),
            new PaidOut(
                $this->scoreSheetIdentifier,
                GameNumber::second(),
                new MultipliedWinnings(
                    new PlainWinnings([
                        (string) TestPlayer::dtl() => -2,
                        (string) TestPlayer::penningmeester() => 2,
                        (string) TestPlayer::dpb() => 2,
                        (string) TestPlayer::secretaris() => -2,
                    ]),
                    ScoreFactor::double()
                ),
                'prop_and_cop',
                ScoreLineNumber::third(),
            ),
            new ScoresLogged(
                $this->scoreSheetIdentifier,
                GameNumber::second(),
                $anotherScoreLine,
                ScoreFactor::double(),
            ),
        ];

        foreach ($events as $event) {
            $this->projector->__invoke($event);
        }

        // Initial logged prop & cop
        $entry1 = $this->repository->getOrCreateEntryByGameAndLine(
            $this->scoreSheetIdentifier,
            GameNumber::first(),
            ScoreLineNumber::first(),
        );
        $this->assertEquals($firstScoreLine, $entry1->getScoreLine());
        $this->assertEquals(ScoreFactor::single(), $entry1->getScoreFactor());

        // Corrected prop & cop (this is a correction, not an undo)
        $entry2 = $this->repository->getOrCreateEntryByGameAndLine(
            $this->scoreSheetIdentifier,
            GameNumber::first(),
            ScoreLineNumber::second(),
        );
        $this->assertEquals($correctedScoreLine, $entry2->getScoreLine());
        $this->assertEquals(ScoreFactor::single(), $entry2->getScoreFactor());

        // Another prop & cop
        $entry3 = $this->repository->getOrCreateEntryByGameAndLine(
            $this->scoreSheetIdentifier,
            GameNumber::second(),
            ScoreLineNumber::third(),
        );
        $this->assertEquals($anotherScoreLine, $entry3->getScoreLine());
        // Game 2 and 3 have score factor 2.
        $this->assertEquals(ScoreFactor::double(), $entry3->getScoreFactor());

        $entry4 = $this->repository->getOrCreateEntryByGameAndLine(
            $this->scoreSheetIdentifier,
            GameNumber::third(),
            ScoreLineNumber::fromInteger(4),
        );
        $this->assertFalse($entry4->hasScores());
        $this->assertEquals(ScoreFactor::double(), $entry4->getScoreFactor());
    }
}
