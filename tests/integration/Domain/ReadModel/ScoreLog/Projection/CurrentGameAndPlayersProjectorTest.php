<?php

declare(strict_types=1);

/** @noinspection PhpUnhandledExceptionInspection */

namespace App\Tests\integration\Domain\ReadModel\ScoreLog\Projection;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\ReadModelUpdateNotifier;
use App\Domain\ReadModel\ScoreLog\Model\CurrentGameAndPlayers;
use App\Domain\ReadModel\ScoreLog\Projection\CurrentGameAndPlayersProjector;
use App\Domain\ReadModel\ScoreLog\Service\CurrentGameAndPlayersRepository;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\Event\PlayerLeft;
use App\Domain\Table\Event\PlayersReordered;
use App\Domain\Table\Event\TableConfigured;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestPlayer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class CurrentGameAndPlayersProjectorTest extends KernelTestCase
{
    private TableIdentifier $tableIdentifier;
    private ScoreSheetIdentifier $scoreSheetIdentifier;
    private CurrentGameAndPlayersRepository $repository;
    private CurrentGameAndPlayersProjector $projector;

    protected function setUp(): void
    {
        self::bootKernel();

        /** @var CurrentGameAndPlayersRepository $currentGameAndPlayersOrmRepository */
        $currentGameAndPlayersOrmRepository = $this->getContainer()->get(CurrentGameAndPlayersRepository::class);
        $this->repository = $currentGameAndPlayersOrmRepository;
        /** @var CurrentGameAndPlayersProjector $currentGameAndPlayersProjector */
        $currentGameAndPlayersProjector = $this->getContainer()->get(CurrentGameAndPlayersProjector::class);
        $this->projector = $currentGameAndPlayersProjector;

        $this->tableIdentifier = TableIdentifier::fromString('16c42bc8-54b1-4b73-a78d-d79d67f7466e');
        $this->scoreSheetIdentifier = ScoreSheetIdentifier::forTable($this->tableIdentifier);

        $this->repository->removeCurrentGameAndPlayers($this->scoreSheetIdentifier);
    }

    /** @test */
    public function itProjectsANewPlayer(): void
    {
        $expectedPlayersGroup = PlayersGroup::none()
            ->withPlayer(TestPlayer::dlb(), Seat::three(), 'dlb');

        $this->projector->__invoke(
            new PlayerJoined(
                $this->tableIdentifier,
                TestPlayer::dlb(),
                GameNumber::first(),
                Seat::three(),
                'dlb',
            )
        );

        $actualPlayersGroup = $this->getActualPlayersGroup();

        $this->assertEquals($expectedPlayersGroup, $actualPlayersGroup);
    }

    /** @test */
    public function itProjectsLeavingPlayer(): void
    {
        $playersGroup = PlayersGroup::fromArray([
            (Player::createActive(TestPlayer::dtl(), Seat::zero(), 'dtl')),
        ]);
        $this->repository->saveGameAndPlayers(
            $this->scoreSheetIdentifier,
            new CurrentGameAndPlayers(GameNumber::first(), $playersGroup),
        );

        $this->projector->__invoke(
            new PlayerLeft($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first())
        );

        $expected = PlayersGroup::fromArray([
            (Player::createGone(TestPlayer::dtl(), Seat::zero(), 'dtl')),
        ]);

        $this->assertEquals($expected, $this->getActualPlayersGroup());
    }

    /** @test */
    public function itSetsNewDealerWhenDealerAnnounced(): void
    {
        $playersGroup = PlayersGroup::fromArray([
            (Player::createActive(TestPlayer::dtl(), Seat::zero(), 'dtl')),
            (Player::createActive(TestPlayer::secretaris(), Seat::one(), 'sec')),
            (Player::createActive(TestPlayer::dpb(), Seat::two(), 'dpb')),
            (Player::createActive(TestPlayer::diu(), Seat::three(), 'diu')),
        ])->withDealer(TestPlayer::secretaris());

        $this->repository->saveGameAndPlayers(
            $this->scoreSheetIdentifier,
            new CurrentGameAndPlayers(GameNumber::first(), $playersGroup),
        );

        $this->projector->applyDealerAnnounced(new DealerAnnounced(
            $this->tableIdentifier,
            TestPlayer::dpb(),
            GameNumber::second(),
        ));

        $expected = new CurrentGameAndPlayers(
            GameNumber::second(),
            $playersGroup->withDealer(TestPlayer::dpb()),
        );

        $actual = $this->repository->getGameAndPlayers($this->scoreSheetIdentifier);

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itReordersPlayers(): void
    {
        $playersGroup = PlayersGroup::fromArray([
            (Player::createActive(TestPlayer::dtl(), Seat::zero(), 'dtl')),
            (Player::createActive(TestPlayer::secretaris(), Seat::one(), 'sec')),
            (Player::createActive(TestPlayer::dpb(), Seat::two(), 'dpb')),
            (Player::createActive(TestPlayer::ddb(), Seat::three(), 'ddb')),
        ])->withDealer(TestPlayer::secretaris());

        $this->repository->saveGameAndPlayers(
            $this->scoreSheetIdentifier,
            new CurrentGameAndPlayers(GameNumber::first(), $playersGroup),
        );

        $this->projector->__invoke(
            new PlayersReordered(
                $this->tableIdentifier,
                GameNumber::first(),
                SeatedPlayers::none()
                    ->withSeatedPlayer(Seat::one(), TestPlayer::dtl())
                    ->withSeatedPlayer(Seat::two(), TestPlayer::dpb())
                    ->withSeatedPlayer(Seat::three(), TestPlayer::secretaris())
                    ->withSeatedPlayer(Seat::four(), TestPlayer::ddb())
            )
        );

        $expectedPlayersGroup = PlayersGroup::fromArray([
            (Player::createActive(TestPlayer::dtl(), Seat::one(), 'dtl')),
            (Player::createActive(TestPlayer::dpb(), Seat::two(), 'dpb')),
            (Player::createActive(TestPlayer::secretaris(), Seat::three(), 'sec')),
            (Player::createActive(TestPlayer::ddb(), Seat::four(), 'ddb')),
        ])->withDealer(TestPlayer::secretaris());

        $this->assertEquals($expectedPlayersGroup, $this->getActualPlayersGroup());
    }

    /** @test */
    public function itDoesNotNotifyOnIrrelevantEvent(): void
    {
        $notifierMock = $this->createMock(ReadModelUpdateNotifier::class);
        $notifierMock->expects($this->never())->method('notify');

        $projector = new CurrentGameAndPlayersProjector(
            $this->repository,
            $notifierMock,
        );

        $projector->__invoke(new TableConfigured(
            TableIdentifier::fromString('820a4d9c-d81e-4f7d-a7d3-6d98b757bcfc'),
            'conventions.traditional',
        ));
    }

    /** @test */
    public function itReordersPlayersWithoutDealer(): void
    {
        $playersGroup = PlayersGroup::fromArray([
            (Player::createActive(TestPlayer::dtl(), Seat::zero(), 'dtl')),
            (Player::createActive(TestPlayer::secretaris(), Seat::one(), 'sec')),
            (Player::createActive(TestPlayer::dpb(), Seat::two(), 'dpb')),
            (Player::createActive(TestPlayer::ddb(), Seat::three(), 'ddb')),
        ]);

        $this->repository->saveGameAndPlayers(
            $this->scoreSheetIdentifier,
            new CurrentGameAndPlayers(GameNumber::first(), $playersGroup),
        );

        $this->projector->__invoke(
            new PlayersReordered(
                $this->tableIdentifier,
                GameNumber::first(),
                SeatedPlayers::none()
                    ->withSeatedPlayer(Seat::one(), TestPlayer::dtl())
                    ->withSeatedPlayer(Seat::two(), TestPlayer::dpb())
                    ->withSeatedPlayer(Seat::three(), TestPlayer::secretaris())
                    ->withSeatedPlayer(Seat::four(), TestPlayer::ddb())
            )
        );

        $expectedPlayersGroup = PlayersGroup::fromArray([
            (Player::createActive(TestPlayer::dtl(), Seat::one(), 'dtl')),
            (Player::createActive(TestPlayer::dpb(), Seat::two(), 'dpb')),
            (Player::createActive(TestPlayer::secretaris(), Seat::three(), 'sec')),
            (Player::createActive(TestPlayer::ddb(), Seat::four(), 'ddb')),
        ]);

        $this->assertEquals($expectedPlayersGroup, $this->getActualPlayersGroup());
    }

    private function getActualPlayersGroup(): PlayersGroup
    {
        return $this->repository->getGameAndPlayers($this->scoreSheetIdentifier)->playersGroup;
    }
}
