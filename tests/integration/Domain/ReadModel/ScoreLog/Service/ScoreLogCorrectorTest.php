<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ReadModel\ScoreLog\Model\ScoreLogEntry;
use App\Domain\ReadModel\ScoreLog\Model\SimpleScoreLogEntry;
use App\Domain\ReadModel\ScoreLog\Service\LogEntryRepository;
use App\Domain\ReadModel\ScoreLog\Service\ScoreLogCorrector;
use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestPlayer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class ScoreLogCorrectorTest extends KernelTestCase
{
    private ScoreSheetIdentifier $scoreSheetIdentifier;
    private LogEntryRepository $logEntryRepository;
    private ScoreLogCorrector $scoreLogCorrector;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
            TableIdentifier::fromString('4b7e9a11-fbac-471d-ba8a-0154758769d7'),
        );

        /** @var LogEntryRepository $logEntryRepository */
        $logEntryRepository = self::getContainer()->get(LogEntryRepository::class);
        $this->logEntryRepository = $logEntryRepository;
        $this->logEntryRepository->deleteForScoreSheet($this->scoreSheetIdentifier);

        /** @var ScoreLogCorrector $scoreLogCorrector */
        $scoreLogCorrector = self::getContainer()->get(ScoreLogCorrector::class);
        $this->scoreLogCorrector = $scoreLogCorrector;
    }

    /** @test */
    public function itInsertsPassedAroundEntry(): void
    {
        $originalLogEntry = SimpleScoreLogEntry::create(
            GameNumber::first(),
            ScoreLineNumber::first(),
            $this->scoreSheetIdentifier,
        )->withDealer(TestPlayer::penningmeester());

        $entryToInsert = SimpleScoreLogEntry::create(
            GameNumber::first(),
            ScoreLineNumber::first(),
            $this->scoreSheetIdentifier
        )->withScoreLine(
            ScoreLine::fromPlainWinnings(
                PlainWinnings::dealerWins(
                    1,
                    PlayersInvolved::from(
                        SeatedPlayers::none()->withSeatedPlayer(Seat::zero(), TestPlayer::dtl())
                            ->withSeatedPlayer(Seat::one(), TestPlayer::diu())
                            ->withSeatedPlayer(Seat::two(), TestPlayer::dcb())
                            ->withSeatedPlayer(Seat::three(), TestPlayer::dpb())
                            ->withSeatedPlayer(Seat::four(), TestPlayer::penningmeester()),
                        TestPlayer::penningmeester()
                    ),
                ),
                'compensation_passed_around',
                ScoreLineNumber::first(),
            ),
        );

        $this->logEntryRepository->saveLogEntry($originalLogEntry);
        $this->scoreLogCorrector->insertCorrectionEntry(
            $entryToInsert,
        );

        $actualEntries = array_map(
            fn (ScoreLogEntry $entry) => SimpleScoreLogEntry::fromLogEntry($entry),
            iterator_to_array($this->logEntryRepository->getLogEntriesForSheet($this->scoreSheetIdentifier)),
        );

        $expectedEntries = [
            1 => $entryToInsert->withDealer(TestPlayer::penningmeester()),
            2 => $originalLogEntry->shiftedDown(),
        ];

        $this->assertEquals($expectedEntries, $actualEntries);
    }

    /** @test */
    public function itInsertsUndoEntry(): void
    {
        $originalFirstEntry = SimpleScoreLogEntry::create(
            GameNumber::first(),
            ScoreLineNumber::first(),
            $this->scoreSheetIdentifier
        )->withScoreLine(
            ScoreLine::fromPlainWinnings(
                PlainWinnings::twoPlayersWin(
                    PlayerIdentifiers::fromArray([TestPlayer::dtl(), TestPlayer::dpb()]),
                    PlayerIdentifiers::fromArray([TestPlayer::diu(), TestPlayer::dcb()]),
                    2,
                ),
                'prop_and_cop',
                ScoreLineNumber::first(),
            ),
        )->withScoreFactor(ScoreFactor::double())->withDealer(TestPlayer::diu());

        $originalSecondEntry = SimpleScoreLogEntry::create(
            GameNumber::second(),
            ScoreLineNumber::second(),
            $this->scoreSheetIdentifier,
        )->withDealer(TestPlayer::dcb())->withScoreFactor(ScoreFactor::double());

        $this->logEntryRepository->saveLogEntry($originalFirstEntry);
        $this->logEntryRepository->saveLogEntry($originalSecondEntry);

        $undoEntry = SimpleScoreLogEntry::create(
            GameNumber::first(),
            ScoreLineNumber::second(),
            $this->scoreSheetIdentifier
        )->withScoreLine(
            ScoreLine::fromPlainWinnings(
                // convoluted way to generate a line with all zeroes:
                PlainWinnings::twoPlayersWin(
                    PlayerIdentifiers::fromArray([TestPlayer::dtl(), TestPlayer::dpb()]),
                    PlayerIdentifiers::fromArray([TestPlayer::diu(), TestPlayer::dcb()]),
                    0,
                ),
                'undo_prop_and_cop',
                ScoreLineNumber::second(),
            ),
        )->withScoreFactor(ScoreFactor::double());

        $this->scoreLogCorrector->insertUndoEntry($undoEntry);

        $actualGame1Log = SimpleScoreLogEntry::fromLogEntry(
            $this->logEntryRepository->getOrCreateEntryForGame(
                $this->scoreSheetIdentifier,
                GameNumber::first(),
            ),
        );
        $actualGame2Log = SimpleScoreLogEntry::fromLogEntry(
            $this->logEntryRepository->getOrCreateEntryForGame(
                $this->scoreSheetIdentifier,
                GameNumber::second(),
            ),
        );

        $expectedGame1Log = SimpleScoreLogEntry::create(
            GameNumber::first(),
            ScoreLineNumber::fromInteger(3),
            $this->scoreSheetIdentifier
        )->withScoreFactor(ScoreFactor::double())
            ->withDealer(TestPlayer::diu());

        $expectedGame2Log = SimpleScoreLogEntry::create(
            GameNumber::second(),
            ScoreLineNumber::fromInteger(4),
            $this->scoreSheetIdentifier
        )->withScoreFactor(ScoreFactor::double())
            ->withDealer(TestPlayer::dcb()); // this is debatable

        $this->assertEquals($expectedGame1Log, $actualGame1Log);
        $this->assertEquals($expectedGame2Log, $actualGame2Log);
    }
}
