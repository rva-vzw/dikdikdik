<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ReadModel\ScoreLog\Model\SimpleScoreLogEntry;
use App\Domain\ReadModel\ScoreLog\Service\LogEntryRepository;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class LogEntryRepositoryTest extends KernelTestCase
{
    private LogEntryRepository $logEntryRepository;
    private ScoreSheetIdentifier $scoreSheetIdentifier;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
            TableIdentifier::fromString('c33d92e6-0555-4dfd-9685-d2781f346210')
        );

        /** @var LogEntryRepository $scoreLogEntries */
        $scoreLogEntries = self::getContainer()->get(LogEntryRepository::class);
        $this->logEntryRepository = $scoreLogEntries;
    }

    /** @test */
    public function itReturnsDefaultScoreFactorIfNotYetSaved(): void
    {
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
                TableIdentifier::fromString('5F155B02-5E95-4326-BE4D-7E84754643C0')
        );

        $scoreLogEntry = $this->logEntryRepository->getOrCreateEntryForGame($scoreSheetIdentifier, GameNumber::first());

        $this->assertEquals(ScoreFactor::single(), $scoreLogEntry->getScoreFactor());
    }

    /** @test */
    public function itCreatesEntryIfNotFound(): void
    {
        $logEntry = SimpleScoreLogEntry::create(
            GameNumber::first(),
            ScoreLineNumber::first(),
            $this->scoreSheetIdentifier,
        );

        $this->logEntryRepository->saveLogEntry($logEntry);

        $actual = SimpleScoreLogEntry::fromLogEntry(
            $this->logEntryRepository->getOrCreateEntryForGame($this->scoreSheetIdentifier, GameNumber::second()),
        );
        $expected = SimpleScoreLogEntry::create(
            GameNumber::second(),
            ScoreLineNumber::fromInteger(2),
            $this->scoreSheetIdentifier,
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itSavesScoreLogEntry(): void
    {
        $logEntry = SimpleScoreLogEntry::create(
            GameNumber::first(),
            ScoreLineNumber::first(),
            $this->scoreSheetIdentifier,
        );

        $this->logEntryRepository->saveLogEntry($logEntry);

        // Ok if no exception occured.
        $this->assertTrue(true);
    }
}
