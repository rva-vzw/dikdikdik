<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ReadModel\GameCounter\GameCounterDeleter;
use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\Player\Model\PlayerStatus;
use App\Domain\ReadModel\ScoreLog\Model\ScoreLog;
use App\Domain\ReadModel\ScoreLog\Model\ScoreLogEntry;
use App\Domain\ReadModel\ScoreLog\Model\SimpleScoreLogEntry;
use App\Domain\ReadModel\ScoreLog\Service\ScoreLogDeleter;
use App\Domain\ReadModel\ScoreLog\Service\ScoreLogs;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineBuilder;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\KickPlayer;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\Command\ProposeAndNobodyGoesAlong;
use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TroelAwareMultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AbundanceSpecification;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\GameSpecification\MisereOnTheTableSpecification;
use App\Domain\Table\Loggable\GameSpecification\MisereSpecification;
use App\Domain\Table\Loggable\GameSpecification\PropAndCopSpecification;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Loggable\Outcome\Winners;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestPlayer;
use App\TestData\TestDataBuilders\TestTableBuilder;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class ScoreLogsTest extends AbstractEventSourcingIntegrationTest
{
    private ScoreLogs $scoreLogs;
    private TestTableBuilder $testTableBuilder;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var ScoreLogs $scoreLogs */
        $scoreLogs = self::getContainer()->get(ScoreLogs::class);
        $this->scoreLogs = $scoreLogs;

        /** @var TestTableBuilder $testTableBuilder */
        $testTableBuilder = self::getContainer()->get(TestTableBuilder::class);
        $this->testTableBuilder = $testTableBuilder;

        // FIXME: Using the TestTableBuilder, I think I could do without
        // deleter and gameCounterDeleter.

        /** @var ScoreLogDeleter $deleter */
        $deleter = self::getContainer()->get(ScoreLogDeleter::class);
        $deleter->deleteScoreLog($this->scoreSheetIdentifier);

        /** @var GameCounterDeleter $gameCounterDeleter */
        $gameCounterDeleter = self::getContainer()->get(GameCounterDeleter::class);
        $gameCounterDeleter->removeForTable($this->tableIdentifier);
    }

    /** @test */
    public function itReturnsAnEmptyScoreLog(): void
    {
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
            TableIdentifier::fromString('4F70DB2A-521D-46F4-B6A6-4CEA62125731')
        );

        $actual = $this->scoreLogs->getScoreLog($scoreSheetIdentifier);

        // This test used to expect a single empty entry, but I think it's also ok
        // if no entries are returned.

        $expected = ScoreLog::forPlayers(
            PlayersGroup::none(),
            $scoreSheetIdentifier,
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itRetrievesPlayerScores(): void
    {
        $this->logTestGames();
        $log = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);
        $lastLoggedLine = $log->getLastLoggedLine();

        $this->assertEquals(-51, $lastLoggedLine->getFrancsForPlayer(TestPlayer::secretaris()));
        $this->assertEquals(-51, $lastLoggedLine->getFrancsForPlayer(TestPlayer::dtl()));
        $this->assertEquals(-47, $lastLoggedLine->getFrancsForPlayer(TestPlayer::ddb()));
        $this->assertEquals(149, $lastLoggedLine->getFrancsForPlayer(TestPlayer::penningmeester()));
    }

    private function logTestGames(): void
    {
        $this->testTableBuilder->buildTableWithPlayers($this->tableIdentifier, 4);
        $this->getCommandBus()->dispatch(
            new AnnounceDealer(
                $this->tableIdentifier,
                TestPlayer::secretaris(),
                GameNumber::first()
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new MisereSpecification(),
                new MultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::ddb())),
                new Winners(PlayerIdentifiers::fromSingle(TestPlayer::ddb())),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::second(),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::secretaris(), TestPlayer::penningmeester()),
                new NumberOfTricks(8),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(3),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::penningmeester()),
                new NumberOfTricks(9),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(4),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::ddb(), TestPlayer::secretaris()),
                new NumberOfTricks(11),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new PassAround(
                $this->tableIdentifier,
                GameNumber::fromInteger(5)
            ),
            new PassAround(
                $this->tableIdentifier,
                GameNumber::fromInteger(5)
            ),
            new PassAround(
                $this->tableIdentifier,
                GameNumber::fromInteger(5)
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(5),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::ddb(), TestPlayer::penningmeester()),
                new NumberOfTricks(10),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new PassAround(
                $this->tableIdentifier,
                GameNumber::fromInteger(6)
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(6),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::ddb(), TestPlayer::secretaris()),
                new NumberOfTricks(11),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(7),
                new MisereSpecification(),
                new MultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::dtl())),
                new Winners(PlayerIdentifiers::fromSingle(TestPlayer::dtl())),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(8),
                new MisereSpecification(),
                new MultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::dtl())),
                Winners::none(),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(9),
                new AbundanceSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::secretaris()),
                AllOrNothing::asSuccess(),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new PassAround(
                $this->tableIdentifier,
                GameNumber::fromInteger(10)
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(10),
                new MisereOnTheTableSpecification(),
                new TroelAwareMultiPlayerAnnouncement(PlayerIdentifiers::fromSingle(TestPlayer::penningmeester()), true),
                new Winners(PlayerIdentifiers::fromSingle(TestPlayer::penningmeester())),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(11),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::secretaris()),
                new NumberOfTricks(10),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(12),
                new PropAndCopSpecification(),
                new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::penningmeester()),
                new NumberOfTricks(9),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new PassAround(
                $this->tableIdentifier,
                GameNumber::fromInteger(13)
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::fromInteger(13),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::dtl()),
                NumberOfTricks::fromNumber(7),
                new \DateTimeImmutable('2021-04-03'),
            )
        );
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#ronde-pas.
     *
     * @test
     */
    public function itIncreaseScoreFactorWhenPassingAround(): void
    {
        $this->prepareAnotherTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
            new PassAround($this->tableIdentifier, GameNumber::first()),
            new PassAround($this->tableIdentifier, GameNumber::first()),
            new ProposeAndNobodyGoesAlong($this->tableIdentifier, GameNumber::first()),
            new PassAround($this->tableIdentifier, GameNumber::first())
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $scoreLog = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);

        $entries = iterator_to_array($scoreLog);

        $this->assertEquals(ScoreFactor::fromInteger(3), $entries[1]->getScoreFactor());
        $this->assertEquals(ScoreFactor::fromInteger(2), $entries[2]->getScoreFactor());
    }

    /**
     * Proposing with nobody going along doesn't increase game number.
     *
     * See https://gitlab.com/rva-vzw/dikdikdik/issues/36
     *
     * @test
     */
    public function itDoesNotIncreaseGameNumberWhenNobodyGoesAlong(): void
    {
        $this->prepareAnotherTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
            new ProposeAndNobodyGoesAlong($this->tableIdentifier, GameNumber::first())
        );

        $scoreLog = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);

        $this->assertEquals(GameNumber::first(), $scoreLog->getCurrentGameNumber());
    }

    /** @test */
    public function itDoesNotIncreaseGameNumberWhenPassingAround(): void
    {
        $this->prepareAnotherTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
            new PassAround($this->tableIdentifier, GameNumber::first())
        );

        $scoreLog = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);

        $this->assertEquals(GameNumber::first(), $scoreLog->getCurrentGameNumber());
    }

    /**
     * Proposing with nobody going along resets pass-around counter.
     *
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#ronde-pas
     * See https://gitlab.com/rva-vzw/dikdikdik/issues/36
     *
     * @test
     */
    public function itResetsPassAroundCountWhenNobodyGoesAlong(): void
    {
        $this->prepareAnotherTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
        // Pass around; score factor of 1st game will increase
            new PassAround($this->tableIdentifier, GameNumber::first()),
            // Pass around; score factor of 2nd game will increase
            new PassAround($this->tableIdentifier, GameNumber::first()),
            // Pass around; score factor of 3rd game will increase
            new PassAround($this->tableIdentifier, GameNumber::first()),
            // Proposed; nobody went along.
            new ProposeAndNobodyGoesAlong($this->tableIdentifier, GameNumber::first()),
            // New dealer, can pass around again, score factor of 1st game will increase once more.
            new PassAround($this->tableIdentifier, GameNumber::first())
        );

        $scoreLog = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);

        $entries = iterator_to_array($scoreLog);

        $this->assertEquals(ScoreFactor::fromInteger(3), $entries[1]->getScoreFactor());
        $this->assertEquals(ScoreFactor::fromInteger(2), $entries[2]->getScoreFactor());
        $this->assertEquals(ScoreFactor::fromInteger(2), $entries[3]->getScoreFactor());
    }

    /** @test */
    public function itDoesNotResetPassAroundCountWhenDealerFails(): void
    {
        $this->prepareAnotherTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
            new PassAround($this->tableIdentifier, GameNumber::first()),
            new PassAround($this->tableIdentifier, GameNumber::first()),
            new PassAround($this->tableIdentifier, GameNumber::first()),

            new PassAround($this->tableIdentifier, GameNumber::first()),
            new PassAround($this->tableIdentifier, GameNumber::first()),
            new PassAround($this->tableIdentifier, GameNumber::first())
        );

        $scoreLog = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);

        foreach ($scoreLog as $i => $entry) {
            $this->assertEquals(ScoreFactor::fromInteger(2), $entry->getScoreFactor());

            // Since no game has been played, only for the first line
            // the dealer is set. We expect dtl, because penningmeester and
            // dbfc have failed as dealer.
            $expectedDealer = 1 == $i ? TestPlayer::dtl() : null;
            $this->assertEquals($expectedDealer, $entry->getDealer());
        }
    }

    /** @test */
    public function itCorrectlyPassesAroundTwiceOnTableOf5(): void
    {
        $this->prepareAnotherTable($this->tableIdentifier);

        // Passing around twice is passing around in the same game, but multiple
        // score lines will be generated.
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', null),
            new AnnounceDealer($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first()),
            new PassAround($this->tableIdentifier, GameNumber::first()),
            new PassAround($this->tableIdentifier, GameNumber::first())
        );

        $scoreLog = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);

        $expected = ScoreLog::fromEntries(
            $this->generateEntries([
                1 => new SimpleScoreLogEntry(
                    TestPlayer::dbfc(),
                    ScoreFactor::single(),
                    GameNumber::first(),
                    ScoreLineBuilder::create(
                        'compensation_passed_around',
                        ScoreLineNumber::first()
                    )->withFrancsForPlayer(TestPlayer::penningmeester(), -1)
                        ->withFrancsForPlayer(TestPlayer::dtl(), -1)
                        ->withFrancsForPlayer(TestPlayer::ddb(), -1)
                        ->withFrancsForPlayer(TestPlayer::secretaris(), -1)
                        ->withFrancsForPlayer(TestPlayer::dbfc(), 4)
                        ->build(),
                    $this->scoreSheetIdentifier,
                ),
                2 => new SimpleScoreLogEntry(
                    TestPlayer::dbfc(),
                    ScoreFactor::single(),
                    GameNumber::first(),
                    ScoreLineBuilder::create(
                        'compensation_passed_around',
                        ScoreLineNumber::fromInteger(2)
                    )->withFrancsForPlayer(TestPlayer::penningmeester(), -2)
                        ->withFrancsForPlayer(TestPlayer::dtl(), -2)
                        ->withFrancsForPlayer(TestPlayer::ddb(), -2)
                        ->withFrancsForPlayer(TestPlayer::secretaris(), -2)
                        ->withFrancsForPlayer(TestPlayer::dbfc(), 8)
                        ->build(),
                    $this->scoreSheetIdentifier,
                ),
                3 => new SimpleScoreLogEntry(
                    TestPlayer::dbfc(),
                    ScoreFactor::double(),
                    GameNumber::first(),
                    ScoreLineBuilder::create(
                        '',
                        ScoreLineNumber::fromInteger(3)
                    )->build(),
                    $this->scoreSheetIdentifier,
                ),
                4 => new SimpleScoreLogEntry(
                    null,
                    ScoreFactor::double(),
                    GameNumber::second(),
                    ScoreLineBuilder::create(
                        '',
                        ScoreLineNumber::fromInteger(4)
                    )->build(),
                    $this->scoreSheetIdentifier,
                ),
            ]),
            GameNumber::first(),
            PlayersGroup::fromArray([
                (Player::createActive(TestPlayer::penningmeester(), Seat::three(), 'pen')),
                (Player::createActive(TestPlayer::dtl(), Seat::zero(), 'dtl')),
                (Player::createActive(TestPlayer::ddb(), Seat::one(), 'ddb')),
                (Player::createActive(TestPlayer::dbfc(), Seat::two(), 'dbfc')),
                (Player::createActive(TestPlayer::secretaris(), Seat::four(), 'sec')),
            ])->withDealer(TestPlayer::dbfc()),
            $this->scoreSheetIdentifier,
        );
        $this->assertEquals($expected, $scoreLog);
    }

    /** @test */
    public function itProjectsTheDealer(): void
    {
        $tableIdentifier = $this->tableIdentifier;
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
            $tableIdentifier
        );

        $this->prepareTable($tableIdentifier);

        $actual = $this->scoreLogs->getScoreLog($scoreSheetIdentifier);
        $expected = ScoreLog::fromEntries(
            $this->generateEntries([
                1 => SimpleScoreLogEntry::create(GameNumber::first(), ScoreLineNumber::first(), $scoreSheetIdentifier)
                    ->withDealer(TestPlayer::penningmeester()),
            ]),
            GameNumber::first(),
            $this->getNewPlayersGroup()->withDealer(TestPlayer::penningmeester()),
            $scoreSheetIdentifier,
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test  */
    public function itCorrectlyLogsPassingAroundOnTableOf5(): void
    {
        $this->prepareTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', null),
            // JoinPlayer now changes the dealer, see #172
            // So let's reset it to penningmeester
            new AnnounceDealer(
                $this->tableIdentifier,
                TestPlayer::penningmeester(),
                GameNumber::first()
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::diu()),
                new NumberOfTricks(6),
                new \DateTimeImmutable('2021-04-03'),
            ),
            new PassAround(
                $this->tableIdentifier,
                GameNumber::second()
            )
        );

        $actual = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);

        $expected = ScoreLog::fromEntries(
            $this->generateEntries([
                1 => new SimpleScoreLogEntry(
                    TestPlayer::penningmeester(),
                    ScoreFactor::single(),
                    GameNumber::first(),
                    new ScoreLine(
                        [
                            TestIds::PLAYER_DIU => 9,
                            TestIds::PLAYER_DPB => -3,
                            TestIds::PLAYER_DTL => -3,
                            TestIds::PLAYER_DLB => -3,
                        ],
                        AloneSpecification::NAME,
                        ScoreLineNumber::first()
                    ),
                    $this->scoreSheetIdentifier,
                ),
                2 => new SimpleScoreLogEntry(
                    TestPlayer::diu(),
                    ScoreFactor::single(),
                    GameNumber::second(),
                    new ScoreLine(
                        [
                            TestIds::PLAYER_PENNINGMEESTER => -1,
                            TestIds::PLAYER_DIU => 13,
                            TestIds::PLAYER_DPB => -4,
                            TestIds::PLAYER_DTL => -4,
                            TestIds::PLAYER_DLB => -4,
                        ],
                        'compensation_passed_around',
                        ScoreLineNumber::fromInteger(2)
                    ),
                    $this->scoreSheetIdentifier,
                ),
                3 => new SimpleScoreLogEntry(
                    TestPlayer::diu(),
                    ScoreFactor::double(),
                    GameNumber::second(),
                    ScoreLine::empty(ScoreLineNumber::fromInteger(3)),
                    $this->scoreSheetIdentifier,
                ),
            ]),
            GameNumber::fromInteger(2),
            $this->getActivePlayersGroup()
                ->withPlayer(TestPlayer::dlb(), Seat::four(), 'dlb')
                ->withPlayerActivated(TestPlayer::dlb())
                ->withDealer(TestPlayer::diu()),
            $this->scoreSheetIdentifier,
        );

        $this->assertEquals($expected, $actual);
    }

    public function itSelectsNewDealerIfDealerLeavesDuringGame(): void
    {
        // This could happen on a table of 5, when after passing around (dealer gets 1 fr/player) the
        // dealer leaves.

        $this->prepareTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', null),
            // JoinPlayer now changes the dealer, see #172
            // So let's reset it to penningmeester
            new AnnounceDealer(
                $this->tableIdentifier,
                TestPlayer::penningmeester(),
                GameNumber::first()
            ),
            new PassAround(
                $this->tableIdentifier,
                GameNumber::first()
            ),
            new KickPlayer(
                $this->tableIdentifier,
                TestPlayer::penningmeester(),
                GameNumber::first(),
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::diu()),
                new NumberOfTricks(6),
                new \DateTimeImmutable('2021-04-03'),
            ),
        );

        $actual = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);

        $expected = ScoreLog::fromEntries(
            $this->generateEntries([
                1 => new SimpleScoreLogEntry(
                    TestPlayer::penningmeester(),
                    ScoreFactor::single(),
                    GameNumber::first(),
                    new ScoreLine(
                        [
                            TestIds::PLAYER_PENNINGMEESTER => 4,
                            TestIds::PLAYER_DIU => -1,
                            TestIds::PLAYER_DPB => -1,
                            TestIds::PLAYER_DTL => -1,
                            TestIds::PLAYER_DLB => -1,
                        ],
                        'compensation_passed_around',
                        ScoreLineNumber::fromInteger(1)
                    ),
                    $this->scoreSheetIdentifier,
                ),
                2 => new SimpleScoreLogEntry(
                    TestPlayer::penningmeester(),
                    ScoreFactor::single(),
                    GameNumber::first(),
                    new ScoreLine(
                        [
                            TestIds::PLAYER_PENNINGMEESTER => 4,
                            TestIds::PLAYER_DIU => 8,
                            TestIds::PLAYER_DPB => -4,
                            TestIds::PLAYER_DTL => -4,
                            TestIds::PLAYER_DLB => -4,
                        ],
                        AloneSpecification::NAME,
                        ScoreLineNumber::first()
                    ),
                    $this->scoreSheetIdentifier,
                ),
            ]),
            GameNumber::fromInteger(2),
            $this->getNewPlayersGroup()
                ->withPlayer(TestPlayer::dlb(), Seat::four(), 'dlb')
                ->withPlayerActivated(TestPlayer::dlb())
                ->withDealer(TestPlayer::diu()),
            $this->scoreSheetIdentifier,
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itCorrectlyUpdatesTheDealer(): void
    {
        $this->prepareTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
            new PassAround(
                $this->tableIdentifier,
                GameNumber::first()
            ),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::diu()),
                new NumberOfTricks(6),
                new \DateTimeImmutable('2021-04-03'),
            ),
        );

        $scoreLog = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);
        $this->assertEquals(GameNumber::second(), $scoreLog->getCurrentGameNumber());
    }

    /** @test */
    public function itProjectsNewPlayerName(): void
    {
        $playerIdentifier = PlayerIdentifier::fromString('af2ae374-b0bd-4dc5-a6b7-571519ca0c38');

        $event = new PlayerJoined(
            $this->tableIdentifier,
            $playerIdentifier,
            GameNumber::first(),
            Seat::zero(),
            'Voorzitter'
        );

        $this->getEventBus()->publish($event, 3);

        $details = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier)
            ->getPlayersGroup();
        $actualName = $details->getPlayer($playerIdentifier)->getName();

        $this->assertEquals(
            'Voorzitter',
            $actualName
        );
    }

    /** @test */
    public function itProjectsThatAPlayerIsGone(): void
    {
        $this->prepareTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
            // log a game, so that every new player becomes active
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::diu()),
                new NumberOfTricks(6),
                new \DateTimeImmutable('2022-05-20'),
            ),
            new KickPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::second()),
        );

        $scoreLog = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);
        $player = $scoreLog->getPlayer(TestPlayer::penningmeester());

        $this->assertEquals(
            PlayerStatus::GONE,
            $player->getStatus(),
        );
    }

    private function prepareTable(TableIdentifier $tableIdentifier): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'pen', null),
            new JoinPlayer($tableIdentifier, TestPlayer::diu(), GameNumber::first(), 'diu', null),
            new JoinPlayer($tableIdentifier, TestPlayer::dpb(), GameNumber::first(), 'dpb', null),
            new JoinPlayer($tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', null),
            new AnnounceDealer(
                $tableIdentifier,
                TestPlayer::penningmeester(),
                GameNumber::first()
            )
        );
    }

    private function getNewPlayersGroup(): PlayersGroup
    {
        return PlayersGroup::none()
            ->withPlayer(TestPlayer::penningmeester(), Seat::zero(), 'pen')
            ->withPlayer(TestPlayer::diu(), Seat::one(), 'diu')
            ->withPlayer(TestPlayer::dpb(), Seat::two(), 'dpb')
            ->withPlayer(TestPlayer::dtl(), Seat::three(), 'dtl');
    }

    private function prepareAnotherTable(TableIdentifier $tableIdentifier): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'pen', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::zero()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::ddb(), GameNumber::first(), 'ddb', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first(), 'dbfc', Seat::two()),
            new AnnounceDealer($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first())
        );
    }

    /**
     * @param array<ScoreLogEntry> $array
     *
     * @return \Traversable<ScoreLogEntry>
     */
    private function generateEntries(array $array): \Traversable
    {
        foreach ($array as $entry) {
            yield $entry;
        }
    }

    private function getActivePlayersGroup(): PlayersGroup
    {
        return PlayersGroup::fromArray([
            (Player::createActive(TestPlayer::penningmeester(), Seat::zero(), 'pen')),
            (Player::createActive(TestPlayer::diu(), Seat::one(), 'diu')),
            (Player::createActive(TestPlayer::dpb(), Seat::two(), 'dpb')),
            (Player::createActive(TestPlayer::dtl(), Seat::three(), 'dtl')),
        ]);
    }

    /** @test */
    public function itRemovesANewPlayerLeaving(): void
    {
        $this->prepareTable($this->tableIdentifier);

        $this->getCommandBus()->dispatch(
            new KickPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first()),
        );

        $scoreLog = $this->scoreLogs->getScoreLog($this->scoreSheetIdentifier);
        $this->assertFalse($scoreLog->getPlayersGroup()->has(
            TestPlayer::penningmeester(),
        ));
    }
}
