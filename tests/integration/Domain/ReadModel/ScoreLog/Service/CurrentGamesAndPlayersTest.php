<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\ScoreLog\Service;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\ScoreLog\Service\CurrentGamesAndPlayers;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\ReorderPlayers;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\OrderedPlayers;
use App\Domain\Table\Seat;
use App\Infrastructure\ReadModel\Repository\CurrentGameAndPlayersOrmRepository;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class CurrentGamesAndPlayersTest extends AbstractEventSourcingIntegrationTest
{
    private CurrentGamesAndPlayers $currentGamesAndPlayers;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var CurrentGamesAndPlayers $currentGamesAndPlayers */
        $currentGamesAndPlayers = $this->getContainer()->get(CurrentGamesAndPlayers::class);
        $this->currentGamesAndPlayers = $currentGamesAndPlayers;

        /** @var CurrentGameAndPlayersOrmRepository $currentGameAndPlayersOrmRepository */
        $currentGameAndPlayersOrmRepository = $this->getContainer()->get(CurrentGameAndPlayersOrmRepository::class);
        $currentGameAndPlayersOrmRepository->removeCurrentGameAndPlayers($this->scoreSheetIdentifier);
    }

    /** @test */
    public function itProjectsNewPlayers(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::zero()),
        );

        $currentGameAndPlayers = $this->currentGamesAndPlayers->getGameAndPlayers($this->scoreSheetIdentifier);
        $actual = $currentGameAndPlayers->playersGroup->getPlayer(TestPlayer::dtl());
        $expected = Player::createNew(TestPlayer::dtl(), Seat::zero(), 'dtl');

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itReordersPlayes(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::zero()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'penn', Seat::one()),
            new ReorderPlayers($this->tableIdentifier, GameNumber::first(), OrderedPlayers::fromArray([
                0 => TestPlayer::penningmeester(),
                1 => TestPlayer::dtl(),
            ])),
        );

        $currentGameAndPlayers = $this->currentGamesAndPlayers->getGameAndPlayers($this->scoreSheetIdentifier);
        $actual = $currentGameAndPlayers->playersGroup;

        $expected = PlayersGroup::none()
            ->withPlayer(TestPlayer::penningmeester(), Seat::zero(), 'penn')
            ->withPlayer(TestPlayer::dtl(), Seat::one(), 'dtl');

        $this->assertEquals($expected, $actual);
    }
}
