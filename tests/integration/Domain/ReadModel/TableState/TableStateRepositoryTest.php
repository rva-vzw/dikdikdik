<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\TableState;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ReadModel\TableState\TableStateRepository;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Rules\Whist;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestPlayer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class TableStateRepositoryTest extends KernelTestCase
{
    private TableStateRepository $tableStateRepository;

    protected function setUp(): void
    {
        /** @var TableStateRepository $tableStateRepository */
        $tableStateRepository = $this->getContainer()->get(TableStateRepository::class);
        $this->tableStateRepository = $tableStateRepository;
    }

    /** @test */
    public function itUsesDefaultRuleSetForNewTableState(): void
    {
        $tableState = $this->tableStateRepository->getTableState(
            TableIdentifier::fromString('91b4fb79-db5d-4a10-8e6a-d151de3922b8'),
        );

        $this->assertEquals(
            Whist::DEFAULT_CONVENTIONS_NAME,
            $tableState->conventionsName,
        );
    }

    /** @void */
    public function itSavesAndRetrievesTableState(): void
    {
        $tableIdentifier = TableIdentifier::fromString('dd38a84b-4081-432d-8af4-309227af9430');
        $tableState = new TableState(
            $tableIdentifier,
            TestPlayer::dvt(),
            GameNumber::first(),
            canChangeRuleSet: true,
            dealerChangeable: true,
            numberOfPresentPlayers: 4,
            finalGameNumber: null,
            conventionsName: 'ruleset.alone5',
        );

        $this->tableStateRepository->saveTableState($tableState);
        $actual = $this->tableStateRepository->getTableState($tableIdentifier);

        $this->assertEquals($tableState, $actual);
    }
}
