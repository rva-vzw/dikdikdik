<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\TableState;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ReadModel\TableState\TableStateRepository;
use App\Domain\ReadModel\TableState\TableStates;
use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\AnnounceFinalRound;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\KickPlayer;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\Command\ReorderPlayers;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AbundanceSpecification;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;
use App\Domain\Table\Player\OrderedPlayers;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class TableStatesTest extends AbstractEventSourcingIntegrationTest
{
    private TableStates $tableStates;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var TableStates $tableStates */
        $tableStates = self::getContainer()->get(TableStates::class);
        $this->tableStates = $tableStates;

        /** @var TableStateRepository $tableStateRepository */
        $tableStateRepository = self::getContainer()->get(TableStateRepository::class);
        $tableStateRepository->deleteTableState($this->tableIdentifier);
    }

    /** @test */
    public function itUnlocksTheDealerAfterReorderingPlayers(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'pen', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::zero()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::ddb(), GameNumber::first(), 'ddb', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first(), 'dbfc', Seat::two()),
            new PassAround($this->tableIdentifier, GameNumber::first()),
            new ReorderPlayers(
                $this->tableIdentifier,
                GameNumber::first(),
                OrderedPlayers::fromArray([
                    TestPlayer::dtl(),
                    TestPlayer::penningmeester(),
                    TestPlayer::ddb(),
                    TestPlayer::dbfc(),
                ]),
            ),
        );

        $tableState = $this->tableStates->getTableState($this->tableIdentifier);

        $this->assertTrue($tableState->dealerChangeable);
    }

    /** @test */
    public function itDoesNotUnlockTheDealerIfTheresNotEnoughPlayers(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'pen', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::zero()),
            new ReorderPlayers(
                $this->tableIdentifier,
                GameNumber::first(),
                OrderedPlayers::fromArray([
                    TestPlayer::dtl(),
                    TestPlayer::penningmeester(),
                ]),
            ),
        );

        $tableState = $this->tableStates->getTableState($this->tableIdentifier);

        $this->assertFalse($tableState->dealerChangeable);
    }

    /** @test */
    public function itAlwaysReturnsATableState(): void
    {
        $tableIdentifier = TableIdentifier::fromString('8c0bd1e6-93dc-4d82-8df7-31b1e1743430');
        $actualTableState = $this->tableStates->getTableState(
            $tableIdentifier,
        );
        $expectedTableState = new TableState(
            $tableIdentifier,
            null,
            GameNumber::first(),
            true,
            false,
            0,
            null,
            'conventions.traditional',
        );
        $this->assertEquals($expectedTableState, $actualTableState);
    }

    /** @test */
    public function itIsAwareOfTheFinalRound(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'penn', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', Seat::four()),
            new AnnounceFinalRound($this->tableIdentifier, GameNumber::first()),
        );

        $tableState = $this->tableStates->getTableState($this->tableIdentifier);
        $this->assertTrue($tableState->isFinalRound());
    }

    /** @test */
    public function itJoinsAPlayer(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
        );

        $tableState = $this->tableStates->getTableState($this->tableIdentifier);
        $this->assertEquals(
            1,
            $tableState->numberOfPresentPlayers,
        );
    }

    /** @test */
    public function itKicksAPlayer(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new KickPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
        );

        $tableState = $this->tableStates->getTableState($this->tableIdentifier);
        $this->assertEquals(
            0,
            $tableState->numberOfPresentPlayers,
        );
    }

    /** @test */
    public function itLocksTheDealerAfterApplyingAGameEvent(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'penn', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', Seat::four()),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new AbundanceSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::dlb()),
                AllOrNothing::asSuccess(),
                new \DateTimeImmutable('2022-08-20'),
            ),
        );

        $tableState = $this->tableStates->getTableState($this->tableIdentifier);
        $this->assertFalse($tableState->dealerChangeable);
    }

    /** @test */
    public function itAnnouncesTheDealer(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'penn', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', Seat::four()),
            new AnnounceDealer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first()),
        );

        $tableState = $this->tableStates->getTableState($this->tableIdentifier);
        $this->assertEquals(TestPlayer::secretaris(), $tableState->dealer);
    }

    /** @test */
    public function itAnnouncesNextDealerAndIncreasesGameNumber(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'penn', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', Seat::four()),
            new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new AbundanceSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::dlb()),
                AllOrNothing::asSuccess(),
                new \DateTimeImmutable('2022-08-20'),
            ),
         );

        $tableState = $this->tableStates->getTableState($this->tableIdentifier);
        $this->assertEquals(TestPlayer::dtl(), $tableState->dealer);
        $this->assertEquals(GameNumber::second(), $tableState->currentGameNumber);
    }

    /** @test */
    public function itDoesNotIncreaseGameNumberWhenPassingAround(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'penn', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), 'dlb', Seat::four()),
            new PassAround($this->tableIdentifier, GameNumber::first()),
        );

        $tableState = $this->tableStates->getTableState($this->tableIdentifier);
        $this->assertEquals(GameNumber::first(), $tableState->currentGameNumber);
    }
}
