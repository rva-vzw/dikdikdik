<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\TableState;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ReadModel\TableState\TableStateProjector;
use App\Domain\ReadModel\TableState\TableStateRepository;
use App\Domain\Table\Command\AnnounceFinalRound;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class TableStateProjectorTest extends AbstractEventSourcingIntegrationTest
{
    private TableStateRepository $tableStateRepository;
    private TableStateProjector $tableStateProjector;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var TableStateRepository $tableStateRepository */
        $tableStateRepository = self::getContainer()->get(TableStateRepository::class);
        $this->tableStateRepository = $tableStateRepository;

        /** @var TableStateProjector $tableStateProjector */
        $tableStateProjector = self::getContainer()->get(TableStateProjector::class);
        $this->tableStateProjector = $tableStateProjector;

        $this->tableStateRepository->deleteTableState($this->tableIdentifier);
    }

    /** @test */
    public function itEnablesDealerSelectionWhenEnoughPlayersAndNoDealer(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'pen', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::zero()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::ddb(), GameNumber::first(), 'ddb', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first(), 'dbfc', Seat::two()),
        );

        $tableState = $this->tableStateRepository->getTableState($this->tableIdentifier);

        $this->assertTrue($tableState->dealerChangeable);
    }

    /** @test */
    public function itRefusesNewPlayersInFinalRound(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'pen', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::zero()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::ddb(), GameNumber::first(), 'ddb', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first(), 'dbfc', Seat::two()),
            new AnnounceFinalRound($this->tableIdentifier, GameNumber::first()),
        );

        $tableState = $this->tableStateRepository->getTableState($this->tableIdentifier);

        $this->assertFalse($tableState->hasFreePlaces());
    }

    /** @test */
    public function itUpdatesTableStateWhenDealerIsAnnounced(): void
    {
        $originalTableState = new TableState(
            $this->tableIdentifier,
            TestPlayer::dvl(),
            GameNumber::first(),
            false,
            false,
            4,
            null,
            'conventions.traditional',
        );

        $this->tableStateRepository->saveTableState(
            $originalTableState,
        );

        $this->tableStateProjector->__invoke(new DealerAnnounced(
            $this->tableIdentifier,
            TestPlayer::diu(),
            GameNumber::second(),
        ));

        $actualTableState = $this->tableStateRepository->getTableState($this->tableIdentifier);
        $expectedTableState = $originalTableState->forGame(GameNumber::second())
            ->withDealer(TestPlayer::diu());

        $this->assertEquals($expectedTableState, $actualTableState);
    }
}
