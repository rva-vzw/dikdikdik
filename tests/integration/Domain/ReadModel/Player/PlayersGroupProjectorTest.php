<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\Player;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayerStatus;
use App\Domain\ReadModel\Player\Service\PlayersGroups;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\KickPlayer;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class PlayersGroupProjectorTest extends AbstractEventSourcingIntegrationTest
{
    private PlayersGroups $playersGroups;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var PlayersGroups $playersGroups */
        $playersGroups = self::getContainer()->get(PlayersGroups::class);
        $this->playersGroups = $playersGroups;
    }

    /** @test */
    public function itActivatesAPlayer(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dpb(), GameNumber::first(), 'dpb', Seat::three()),
        );

        $actual = $this->playersGroups->getPlayersGroup($this->scoreSheetIdentifier)
            ->getSeatedPlayers();

        $expected = SeatedPlayers::none()->withSeatedPlayer(
            Seat::three(),
            TestPlayer::dpb()
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itUpdatesStatusOfReactivatedPlayer(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::dpb(), GameNumber::first(), 'dpb', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::diu(), GameNumber::first(), 'diu', Seat::four()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::ddl(), GameNumber::first(), 'ddl', Seat::zero()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::two()),
            // Pass around, so that player gets paid, and will actually be deactivated (instead of gone)
            new PassAround($this->tableIdentifier, GameNumber::first()),
            new KickPlayer($this->tableIdentifier, TestPlayer::dpb(), GameNumber::first()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dpb(), GameNumber::first(), 'dpb', Seat::three()),
        );

        $playersGroup = $this->playersGroups->getPlayersGroup($this->scoreSheetIdentifier);
        $actualStatus = $playersGroup->getPlayer(TestPlayer::dpb())->getStatus();

        $this->assertEquals(PlayerStatus::ACTIVE, $actualStatus);
    }

    /**
     * See https://gitlab.com/rva-vzw/dikdikdik/issues/74.
     *
     * @test
     */
    public function itStoresPlayersAtCorrectSeats(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'pen', Seat::two()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', Seat::one()),
        );

        $actual = $this->playersGroups->getPlayersGroup($this->scoreSheetIdentifier)
            ->getSeatedPlayers();

        $expected = SeatedPlayers::none()
            ->withSeatedPlayer(Seat::one(), TestPlayer::secretaris())
            ->withSeatedPlayer(Seat::two(), TestPlayer::penningmeester()
        );

        $this->assertEquals($expected, $actual);
    }
}
