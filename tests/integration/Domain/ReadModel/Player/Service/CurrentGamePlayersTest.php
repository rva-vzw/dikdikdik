<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\Player\Service;

use App\Domain\ReadModel\Player\Service\CurrentGamePlayers;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\TableIdentifier;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class CurrentGamePlayersTest extends KernelTestCase
{
    private CurrentGamePlayers $currentGamePlayers;

    protected function setUp(): void
    {
        self::bootKernel();

        /** @var CurrentGamePlayers $currentGamePlayer */
        $currentGamePlayer = self::getContainer()->get(CurrentGamePlayers::class);
        $this->currentGamePlayers = $currentGamePlayer;
    }

    /** @test */
    public function itReturnsNoPlayersInvolvedForNewTable(): void
    {
        $actual = $this->currentGamePlayers->findPlayersInvolved(
            ScoreSheetIdentifier::forTable(TableIdentifier::fromString('4D0B9A04-F366-459C-AF71-AEB2B1C20E0D'))
        );

        $this->assertNull($actual);
    }
}
