<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\Player\Service;

use App\Domain\ReadModel\Player\Service\PlayersGroups;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\TableIdentifier;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class PlayersGroupsTest extends KernelTestCase
{
    private PlayersGroups $playersGroups;

    protected function setUp(): void
    {
        self::bootKernel();

        /** @var PlayersGroups $playersGroups */
        $playersGroups = self::getContainer()->get(PlayersGroups::class);
        $this->playersGroups = $playersGroups;
    }

    /** @test */
    public function itGetsSeatedPlayersOnANewTable(): void
    {
        $actual = $this->playersGroups->getPlayersGroup(
            ScoreSheetIdentifier::forTable(TableIdentifier::fromString('ACB06E44-9BC8-4433-BF31-99DB1E5EE38C'))
        )->getSeatedPlayers();
        $expected = SeatedPlayers::none();

        $this->assertEquals($expected, $actual);
    }
}
