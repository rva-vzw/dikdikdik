<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\TablePermissions;

use App\Domain\ReadModel\TablePermissions\PermissionsByTable;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\ProposeAndNobodyGoesAlong;
use App\Domain\Table\Command\Undo;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Seat;
use App\Infrastructure\ReadModel\Repository\PermissionParametersOrmRepository;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

final class PermissionsByTableTest extends AbstractEventSourcingIntegrationTest
{
    // TODO: Check if undo is enabled when someone proposed without anybody along.
    // And disabled again when that's been undone

    private PermissionsByTable $permissionsByTable;

    protected function setUp(): void
    {
        parent::setUp();
        /** @var PermissionParametersOrmRepository $permissionParametersOrmRepository */
        $permissionParametersOrmRepository = self::getContainer()->get(PermissionParametersOrmRepository::class);
        $permissionParametersOrmRepository->deletePermissionParameters($this->tableIdentifier);

        /** @var PermissionsByTable $permissionsByTable */
        $permissionsByTable = self::getContainer()->get(PermissionsByTable::class);
        $this->permissionsByTable = $permissionsByTable;
    }

    /** @test */
    public function itKeepsTrackOfCanUndo(): void
    {
        $this->getCommandBus()->dispatch(
            new JoinPlayer($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), 'pen', Seat::three()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', Seat::zero()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::ddb(), GameNumber::first(), 'ddb', Seat::one()),
            new JoinPlayer($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first(), 'dbfc', Seat::two()),
        );

        $permissions = $this->permissionsByTable->getPermissionsByTable($this->tableIdentifier);
        $this->assertFalse($permissions->canUndo());

        $this->getCommandBus()->dispatch(
            new ProposeAndNobodyGoesAlong($this->tableIdentifier, GameNumber::first()),
        );

        $permissions = $this->permissionsByTable->getPermissionsByTable($this->tableIdentifier);
        $this->assertTrue($permissions->canUndo());

        $this->getCommandBus()->dispatch(
            new Undo($this->tableIdentifier, GameNumber::first()),
        );

        $permissions = $this->permissionsByTable->getPermissionsByTable($this->tableIdentifier);
        $this->assertFalse($permissions->canUndo());
    }
}
