<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\Game\Service;

use App\Domain\ReadModel\Game\Service\CurrentGames;
use App\Domain\ReadModel\GameCounter\GameCounterDeleter;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\Command\Undo;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;

/**
 * FIXME: Move this test to another class (CurrentGamesAndPlayersTest?).
 */
final class CurrentGamesTest extends AbstractEventSourcingIntegrationTest
{
    private CurrentGames $currentGames;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var CurrentGames $currentGames */
        $currentGames = $this->getContainer()->get(CurrentGames::class);
        $this->currentGames = $currentGames;

        /** @var GameCounterDeleter $gameCounterDeleter */
        $gameCounterDeleter = $this->getContainer()->get(GameCounterDeleter::class);
        $gameCounterDeleter->removeForTable($this->tableIdentifier);
    }

    /** @test */
    public function itKeepsTrackOfGameNumberWhenUndoing(): void
    {
        $tableIdentifier = $this->tableIdentifier;

        $this->getCommandBus()->dispatch(
            new JoinPlayer($tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), 'sec', null),
            new JoinPlayer($tableIdentifier, TestPlayer::ddl(), GameNumber::first(), 'ddl', null),
            new JoinPlayer($tableIdentifier, TestPlayer::dtl(), GameNumber::first(), 'dtl', null),
            new JoinPlayer($tableIdentifier, TestPlayer::ddb(), GameNumber::first(), 'ddb', null),
            new LogGame(
                $tableIdentifier,
                GameNumber::first(),
                new AloneSpecification(),
                new SinglePlayerAnnouncement(TestPlayer::ddl()),
                NumberOfTricks::fromNumber(6),
                new \DateTimeImmutable('2022-01-30'),
            ),
            new Undo($tableIdentifier, GameNumber::second()),
        );

        $actualGameNumber = $this->currentGames->getCurrentGameNumber($this->tableIdentifier);

        $this->assertEquals(
            GameNumber::first(),
            $actualGameNumber,
        );
    }
}
