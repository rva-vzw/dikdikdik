<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\GameCounter;

use App\Domain\ReadModel\GameCounter\ActiveTablesCounter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class ActiveTablesCounterTest extends KernelTestCase
{
    private ActiveTablesCounter $activeTablesCounter;

    protected function setUp(): void
    {
        self::bootKernel();

        /** @var ActiveTablesCounter $activeTablesCounter */
        $activeTablesCounter = $this->getContainer()->get(ActiveTablesCounter::class);
        $this->activeTablesCounter = $activeTablesCounter;
    }

    /** @test */
    public function itCountsActiveTables(): void
    {
        $count = $this->activeTablesCounter->getActiveTablesCount(
            new \DateTimeImmutable('2021-12-29 21:00'),
            new \DateTimeImmutable('2021-12-29 22:00'),
            4
        );

        // It's ok if we don't get an exception.
        $this->assertTrue($count >= 0);
    }
}
