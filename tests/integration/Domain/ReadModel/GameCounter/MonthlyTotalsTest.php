<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\ReadModel\GameCounter;

use App\Domain\ReadModel\GameCounter\GameCounter;
use App\Domain\ReadModel\GameCounter\GameCounterRepository;
use App\Domain\ReadModel\GameCounter\Month;
use App\Domain\ReadModel\GameCounter\MonthlyTotal;
use App\Domain\ReadModel\GameCounter\MonthlyTotals;
use App\Domain\Table\TableIdentifier;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class MonthlyTotalsTest extends KernelTestCase
{
    private MonthlyTotals $monthlyTotals;
    private GameCounterRepository $gameCounterRepository;

    protected function setUp(): void
    {
        self::bootKernel();

        /** @var MonthlyTotals $monthlyTotals */
        $monthlyTotals = $this->getContainer()->get(MonthlyTotals::class);
        $this->monthlyTotals = $monthlyTotals;

        /** @var GameCounterRepository $gameCounterRepository */
        $gameCounterRepository = $this->getContainer()->get(GameCounterRepository::class);
        $this->gameCounterRepository = $gameCounterRepository;

        $this->gameCounterRepository->saveGameCounter(
            GameCounter::createNew(
                TableIdentifier::fromString('e4ca6e3f-a5a1-41d5-9af6-44d64dff6b17'),
                new \DateTimeImmutable('2021-11-11')
            )->withGamesCounted(20, new \DateTimeImmutable('2021-11-11'))
        );
    }

    /** @test */
    public function itReturnsMonthlyTotals(): void
    {
        $totals = $this->monthlyTotals->getByMonth(
            Month::fromYearAndDay(2021, 11),
            Month::fromYearAndDay(2021, 12),
            4
        );

        foreach ($totals as $monthlyTotal) {
            // Do a foreach, to make sure the function is actually called
            $this->assertInstanceOf(MonthlyTotal::class, $monthlyTotal);
            break;
        }

        $this->assertTrue(true);
    }
}
