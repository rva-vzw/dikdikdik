<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\Table\Rules;

use App\Domain\Table\Command\ConfigureTable;
use App\Domain\Table\Exception\UnknownLoggable;
use App\Domain\Table\Rules\RulesByTable;
use App\Tests\integration\AbstractEventSourcingIntegrationTest;
use Webmozart\Assert\Assert;

final class RulesByTableTest extends AbstractEventSourcingIntegrationTest
{
    private RulesByTable $rulesByTable;

    protected function setUp(): void
    {
        parent::setUp();
        $rulesByTable = $this->getContainer()->get(RulesByTable::class);

        Assert::isInstanceOf($rulesByTable, RulesByTable::class);
        $this->rulesByTable = $rulesByTable;
    }

    /** @test */
    public function itDoesNotIncludeNoBidForSimplifiedPassAround(): void
    {
        $this->getCommandBus()->dispatch(
            new ConfigureTable(
                $this->tableIdentifier,
                'conventions.iwwa',
            ),
        );

        $loggables = $this->rulesByTable->getLoggables($this->tableIdentifier);

        $this->expectException(UnknownLoggable::class);
        $noBid = $loggables->getByName('no_bid_accepted');
    }
}
