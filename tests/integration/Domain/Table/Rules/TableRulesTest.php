<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\Table\Rules;

use App\Domain\Table\Rules\RulesByConvention;
use App\Domain\Table\Rules\Whist;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class TableRulesTest extends KernelTestCase
{
    /** @test */
    public function itRetrievesGamesByRuleSet(): void
    {
        $tableRules = $this->getContainer()->get(RulesByConvention::class);
        $this->assertInstanceOf(RulesByConvention::class, $tableRules);

        $defaultAllowedGames = $tableRules->getPermissibleGames(Whist::DEFAULT_CONVENTIONS_NAME);
        $this->assertNotEmpty(
            iterator_to_array($defaultAllowedGames->getAll()),
        );
    }
}
