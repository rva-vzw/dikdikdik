<?php

declare(strict_types=1);

namespace App\Tests\integration\Domain\GameRules;

use App\Domain\TableConventions\Conventions;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class RuleSetTest extends KernelTestCase
{
    /** @test */
    public function itRetrievesTheSupportedGamesForTheTraditionalRuleSet(): void
    {
        /** @var Conventions $traditionalConventions */
        $traditionalConventions = $this->getContainer()->get('conventions.traditional');

        $this->assertCount(8, $traditionalConventions->allowedGames);
    }
}
