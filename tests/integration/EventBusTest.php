<?php

namespace App\Tests\integration;

use App\Domain\Table\Event\TableCleared;

final class EventBusTest extends AbstractEventSourcingIntegrationTest
{
    /** @test */
    public function itLogsMessagesOnTheBus(): void
    {
        // The setUp()-function publishes ClearTable to the command bus, so
        // we expect a TableCleared event in the store.

        $expectedEvent = new TableCleared(
            $this->tableIdentifier
        );

        $events = iterator_to_array($this->getEventStore()->getStreamForAggregate($this->tableIdentifier));

        $this->assertEquals([1 => $expectedEvent], $events);
    }
}
