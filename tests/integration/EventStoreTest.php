<?php

namespace App\Tests\integration;

use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\Event\TableCleared;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Seat;

final class EventStoreTest extends AbstractEventSourcingIntegrationTest
{
    /** @test */
    public function itRecordsEvents(): void
    {
        $tableIdentifier = $this->tableIdentifier;
        $cleared = new TableCleared(
            $tableIdentifier
        );

        $joined = new PlayerJoined(
            $tableIdentifier,
            PlayerIdentifier::fromString('4441d5ad-3a49-4611-bb23-6cf26d302e3c'),
            GameNumber::first(),
            Seat::three(),
            'dcb',
        );

        // The TableCleared event is already in the store, because of the setUp() function.
        $this->getEventStore()->save($joined, 2);

        $events = iterator_to_array($this->getEventStore()->getStreamForAggregate(
            $tableIdentifier
        ));

        $this->assertEquals([1 => $cleared, 2 => $joined], $events);
    }

    /** @test */
    public function itFindsExistingEventStream(): void
    {
        $tableIdentifier = $this->tableIdentifier;
        $cleared = new TableCleared(
            $tableIdentifier
        );

        $joined = new PlayerJoined(
            $tableIdentifier,
            PlayerIdentifier::fromString('4441d5ad-3a49-4611-bb23-6cf26d302e3c'),
            GameNumber::first(),
            Seat::three(),
            'dcb',
        );

        // The TableCleared event is already in the store, because of the setUp() function.
        $this->getEventStore()->save($joined, 2);

        $this->assertTrue($this->getEventStore()->hasStreamForAggregate($tableIdentifier));
    }
}
