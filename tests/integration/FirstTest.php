<?php

namespace App\Tests\integration;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class FirstTest extends KernelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    /** @test */
    public function itRunsATest(): void
    {
        $this->assertEquals(1, 1);
    }
}
