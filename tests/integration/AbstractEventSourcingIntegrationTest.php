<?php

namespace App\Tests\integration;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Command\ClearTable;
use App\Domain\Table\TableIdentifier;
use App\Kernel;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStoreHacks;
use RvaVzw\KrakBoemBundle\Messenger\MessengerCommandBus;
use RvaVzw\KrakBoemBundle\Messenger\MessengerEventBus;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Abstract class for integration tests.
 */
abstract class AbstractEventSourcingIntegrationTest extends KernelTestCase
{
    /** @var EventStore */
    private $eventStore;

    /** @var EventBus */
    private $eventBus;

    /** @var NormalizerInterface */
    private $normalizer;

    /** @var DenormalizerInterface */
    private $denormalizer;

    /** @var CommandBus */
    private $commandBus;

    // For the moment I assume I only need a single table and a single
    // score sheet for an integration test.

    /** @var TableIdentifier */
    protected $tableIdentifier;

    /** @var ScoreSheetIdentifier */
    protected $scoreSheetIdentifier;

    /** @var EventStoreHacks */
    protected $eventStreamHacks;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tableIdentifier = TableIdentifier::fromString('5e0355ac-b975-4194-a3a0-6fea7e454642');
        $this->scoreSheetIdentifier = ScoreSheetIdentifier::forTable($this->tableIdentifier);

        self::bootKernel();

        /** @var NormalizerInterface $normalizer */
        $normalizer = self::getContainer()->get(NormalizerInterface::class);
        $this->normalizer = $normalizer;

        /** @var DenormalizerInterface $denormalizer */
        $denormalizer = self::getContainer()->get(DenormalizerInterface::class);
        $this->denormalizer = $denormalizer;

        /** @var EventStore $eventStore */
        $eventStore = self::getContainer()->get(EventStore::class);
        $this->eventStore = $eventStore;

        /** @var MessageBusInterface $bus */
        $bus = self::getContainer()->get('messenger.bus.events');
        $this->eventBus = new MessengerEventBus($bus);

        /** @var MessageBusInterface $bus */
        $bus = self::getContainer()->get('messenger.bus.commands');
        $this->commandBus = new MessengerCommandBus($bus);

        /** @var EventStoreHacks $eventStreamHacks */
        $eventStreamHacks = self::getContainer()->get(EventStoreHacks::class);
        $this->eventStreamHacks = $eventStreamHacks;

        // As long as the real database is used for the event store in the tests,
        // (and maybe we should, because these are integration tests)
        // I have to delete existing event streams for the test aggregates.

        $this->eventStreamHacks->deleteStreamForAggregate($this->tableIdentifier);
        $this->eventStreamHacks->deleteStreamForAggregate($this->scoreSheetIdentifier);

        // Make sure all read models are reset:
        $this->commandBus->dispatch(new ClearTable($this->tableIdentifier));
    }

    protected function getEventStore(): EventStore
    {
        return $this->eventStore;
    }

    protected function getEventBus(): EventBus
    {
        return $this->eventBus;
    }

    protected function getNormalizer(): NormalizerInterface
    {
        return $this->normalizer;
    }

    protected function getDenormalizer(): DenormalizerInterface
    {
        return $this->denormalizer;
    }

    protected function getCommandBus(): CommandBus
    {
        return $this->commandBus;
    }

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }
}
