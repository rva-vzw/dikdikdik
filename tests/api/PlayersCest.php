<?php

declare(strict_types=1);

namespace App\Tests\api;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\TestData\TestDataBuilders\TestTable;
use App\Tests\ApiTester;

final class PlayersCest
{
    public function itReturnsPlayersAsASequentialArray(ApiTester $i): void
    {
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
            TestTable::table14()
        );

        $i->sendGET('sheet/'.$scoreSheetIdentifier->toString().'/players');
        $i->seeResponseIsJson();
        $i->seeResponseContainsJson([
            [
                'name' => 'secr',
                'status' => 'new',
                'playerIdentifier' => '87bdf1ee-5102-4c19-9975-2cb0a6b1d6f8',
                'lastKnownSeat' => 0,
            ],
            [
                'name' => 'dtl',
                'status' => 'new',
                'playerIdentifier' => '4941cd94-a72e-4593-8f19-98695e0937fe',
                'lastKnownSeat' => 1,
            ],
            [
                'name' => 'ddb',
                'status' => 'new',
                'playerIdentifier' => '5fba351e-5703-4d8d-8b0d-7b4d76f07e0f',
                'lastKnownSeat' => 2,
            ],
            [
                'name' => 'penn',
                'status' => 'new',
                'playerIdentifier' => '5d46004d-eb1b-41a6-af37-9a92c0acde2a',
                'lastKnownSeat' => 3,
            ],
        ]);
    }
}
