<?php

declare(strict_types=1);

namespace App\Tests\api;

use App\Tests\ApiTester;
use Codeception\Util\HttpCode;

final class GamesCest
{
    public function getGames(ApiTester $i): void
    {
        // Get the specifications for a random table. These should not contain
        // alone 5, since this is disabled by default.

        $i->sendGET('games/149218DD-86CE-4F21-8FBB-CCC45780E74D');
        $i->seeResponseIsJson();
        $i->seeResponseContainsJson([
            'name' => 'alone',
            'announcement' => 'single_player',
            'outcome' => 'number_of_tricks',
        ]);

        $i->dontSeeResponseContainsJson([
            'name' => 'alone5',
            'announcement' => 'single_player',
            'outcome' => 'number_of_tricks',
        ]);
    }

    public function getGamesWithAlone5(ApiTester $i): void
    {
        $tableId = '07DD90D7-F9C8-40F5-BB72-D4685FE68877';

        /** @var mixed[] $data */
        $data = json_encode(['alone5Enabled' => true]);

        $i->sendPUT(
            'table/'.$tableId.'/config',
            $data
        );

        $i->seeResponseCodeIs(HttpCode::NO_CONTENT);

        $i->sendGET('games/'.$tableId);

        $i->seeResponseIsJson();

        $i->seeResponseContainsJson([
            'name' => 'alone5',
            'announcement' => 'single_player',
            'outcome' => 'number_of_tricks',
        ]);

        $i->dontSeeResponseContainsJson([
            'name' => 'alone',
            'announcement' => 'single_player',
            'outcome' => 'number_of_tricks',
        ]);
    }
}
