<?php

declare(strict_types=1);

namespace App\Tests\api;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestTable;
use App\Tests\ApiTester;

final class GetLogCest
{
    // tests
    public function getLogOfNewGameWithDealerAnnounced(ApiTester $i): void
    {
        $scoreSheetIdentifier = ScoreSheetIdentifier::forTable(TestTable::table3());

        // If this fails, check whether your test data was loaded!
        $i->sendGET('sheet/'.$scoreSheetIdentifier->toString());
        $i->seeResponseIsJson();
        $i->seeResponseContainsJson([
            'log' => [
                // Log should be a sequential array.
                [
                    'game' => 1,
                    'scores' => [],
                    'note' => '',
                    'dealer' => TestIds::PLAYER_PENNINGMEESTER,
                    'factor' => 1,
                    'line' => 1,
                ],
            ],
        ]);
    }
}
