<?php
namespace App\Tests;

use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    public function changePlayerOrder(TableIdentifier $tableIdentifier, GameNumber $gameNumber, string $permutation): void
    {
        $this->openNewTab();
        $page = "/hack/table/{$tableIdentifier->toString()}/{$gameNumber->toInteger()}";
        $this->amOnPage($page);

        $this->submitForm(
            'form',
            ['permutation' => $permutation]
        );
        $this->closeTab();
    }
}
