<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestTable;
use App\Tests\AcceptanceTester;

final class WideScreenCest
{
    public function checkLogFormAtBottomOfBigScoreTableOnWideSceen(AcceptanceTester $i): void
    {
        $i->resizeWindow(1287, 670);
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_17);
        $i->waitForElement('#entry_14');
        $i->scrollTo('#entry_14');
        $i->seeElement('#bid_loggable');
    }

    public function checkScoreSheetIfFewGamesLoggedOnWideSceen(AcceptanceTester $i): void
    {
        $i->resizeWindow(1287, 670);
        $i->amOnPage('/en/table/'.TestTable::table21()->toString());
        // This is a bad test, because it also passes if #entry_1 'falls off' the page.
        $i->seeElement('#entry_1');
    }
}
