<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class MisereCest
{
    public function checkICanChoosePlayersToAnnounceMisere(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_9.'/log?bid=misere');

        // Can only check someone as winner when checked as announcer as well (#274)
        $i->dontSee('input[name="log_game[winningPlayers][]"][value="'.TestIds::PLAYER_DDB.'"]');

        $i->checkOption('input[name="log_game[announcers][]"][value="'.TestIds::PLAYER_DDB.'"]');
        $i->seeElement('input[name="log_game[winningPlayers][]"][value="'.TestIds::PLAYER_DDB.'"]');
        $i->see('Success?');
    }

    public function checkAnnouncingPlayersIsClearedAfterLogging(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_9.'/log?bid=misere');

        $i->seeElement('input[name="log_game[announcers][]"][value="'.TestIds::PLAYER_DDB.'"]');
        $i->checkOption('input[name="log_game[announcers][]"][value="'.TestIds::PLAYER_DDB.'"]');
        $i->click('#log_game_logGame');

        $i->waitForText('misère', 10, 'table');

        $i->selectOption('#bid_loggable', 'misere');
        $i->waitForElement('input[name="log_game[announcers][]"][value="'.TestIds::PLAYER_DDB.'"]');
        $i->dontSeeCheckboxIsChecked('input[name="log_game[announcers][]"][value="'.TestIds::PLAYER_DDB.'"]');
    }
}
