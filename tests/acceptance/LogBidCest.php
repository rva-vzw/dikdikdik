<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final readonly class LogBidCest
{
    public function checkItShowsLogFormWhenBidSelected(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_18);
        $i->selectOption('#bid_loggable', 'alone (6)');

        $i->waitForText(
            text: 'Log',
            selector: 'h2',
        );

        $i->seeOptionIsSelected('#log_game_loggable', 'alone (6)');
        $i->dontSeeElement('.no_bid');
    }

    public function checkItTranslatesLogForm(AcceptanceTester $i): void
    {
        $i->amOnPage('/nl/table/'.TestIds::TEST_TABLE_18.'/log?bid=alone');
        $i->see('Opschrijven', 'h2');
        $i->seeOptionIsSelected('#log_game_loggable', 'alleen (6)');
    }

    public function checkItHidesInputFieldsWhenSelectingNoBid(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_18.'/log?bid=alone');
        $i->seeElement('.game_detail');

        $i->selectOption('#log_game_loggable', '(choose)');

        $i->waitForElementNotVisible('.game_detail');
        $i->waitForElementNotVisible('.no_bid');
    }

    public function checkItShowsButtonsWhenNoBidAccepted(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_18.'/log?bid=no_bid_accepted');
        $i->dontSeeElement('.game_detail');
        $i->seeElement('.no_bid');
    }

    public function checkItScrollsToBottomOfTableAfterSubmittingBidForm(AcceptanceTester $i): void
    {
        // A table with lots of log lines
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_17.'/log?bid=no_bid_accepted');
        $i->click('#log_game_proposedNobodyAlong');
        // FIXME: This is a bad test, because it also sees 'Log' when it's not in the
        // viewport 🙁🙁
        $i->see('Log');
    }

    public function checkItHidesIrrelevantInputs(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_18.'/log?bid=prop_and_cop');
        $i->dontSeeElement('label[for="log_game_success_0"]');
    }
}
