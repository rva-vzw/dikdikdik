<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class StarCest
{
    public function checkIStarAPlayer(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_11.'/extra');

        // Button should not be enabled, because no player is selected
        // $i->waitForElementVisible("button#extra_actions_star");

        // Penningmeester is not present anymore, so can't be starred.
        $i->dontSeeElement('select[name="extra_actions[player]"] option[value="'.TestIds::PLAYER_PENNINGMEESTER.'"]');
        $i->selectOption('extra_actions[player]', TestIds::PLAYER_SECRETARIS);
        $i->waitForElementClickable('button#extra_actions_star');
        $i->click('button#extra_actions_star');

        $i->waitForElement("th[data-playerid='".TestIds::PLAYER_SECRETARIS."'] img.star");
    }

    public function checkICanStarTheDealer(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_19.'/extra');

        // Secretaris is dealer.
        $i->selectOption('extra_actions[player]', TestIds::PLAYER_SECRETARIS);
    }

    public function checkIStarAPlayerAfterLoggingAGame(AcceptanceTester $i): void
    {
        // See #246.
        // Let's log a misere first
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_26.'/log?bid=misere');

        $i->seeElement('input[name="log_game[announcers][]"][value="'.TestIds::PLAYER_DTL.'"]');
        $i->checkOption('input[name="log_game[announcers][]"][value="'.TestIds::PLAYER_DTL.'"]');
        $i->click('#log_game_logGame');

        $i->waitForText('misère', 10, 'table');

        // Now star secretaris
        $i->click('#extra-actions-link');

        $i->waitForElement('#extra_actions_player');
        $i->selectOption('extra_actions[player]', TestIds::PLAYER_SECRETARIS);
        $i->click('button#extra_actions_star');

        // Wait for star
        $i->waitForElement("th[data-playerid='".TestIds::PLAYER_SECRETARIS."'] img.star");
    }
}
