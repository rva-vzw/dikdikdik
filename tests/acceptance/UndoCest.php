<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestTable;
use App\Tests\AcceptanceTester;

final class UndoCest
{
    public function checkIUndoPassingAround(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_23);

        $i->seeElement('#entry_1 .multiplier');
        $i->click('#extra-actions-link');

        $i->waitForElement('#extra_actions_undo');
        $i->click('#extra_actions_undo');

        $i->waitForElement('#entry_1');
        $i->waitForElementNotVisible('#entry_1 .multiplier');
    }

    public function itShowsUndoLinesForTwoUndoneGames(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/scores/'.ScoreSheetIdentifier::forTable(TestTable::table25()));
        $i->see('prop & cop', '#note_2'); // original prop and cop
        $i->see('prop & cop', '#note_3'); // undo-line
    }
}
