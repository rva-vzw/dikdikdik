<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\Domain\Table\GameNumber;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestTable;
use App\Tests\AcceptanceTester;

final class PlayerOrderCest
{
    public function checkCorrectPlayersCanAnnounceAfterChangingOrder(AcceptanceTester $i): void
    {
        // Note that I don't expect the form for logging game to update anymore

//        // Drag and drop doesn't work :-(
//        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_21.'/players');
//        $i->dragAndDrop(
//            'li[data-playerid="'.TestIds::PLAYER_DVL.'"]',
//            'li[data-playerid="'.TestIds::PLAYER_SECRETARIS.'"]'
//        );
//        $i->click('#players_form_submit');

        // So let's use a hack to reorder the players
        $i->changePlayerOrder(TestTable::table21(), GameNumber::first(), '102345');

        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_21);

        $i->selectOption('#bid_loggable', 'misere');
        $i->waitForElementVisible('.announcement');

        $i->waitForElement('input[name="log_game[announcers][]"][value="'.TestIds::PLAYER_DTL.'"]');
        $i->dontSeeElement('input[name="log_game[announcers][]"][value="'.TestIds::PLAYER_SECRETARIS.'"]');

        // Dealer selection should be visible as well
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_21.'/players');
        $i->waitForElementVisible('select#players_form_dealerId');
    }
}
