<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class ProposeNobodyAlongCest
{
    public function checkProposeWithNobodyAlong(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_14.'/log?bid=no_bid_accepted');

        $i->click('Proposed, nobody along');

        $i->waitForText('ddb', 10, 'th.dealer');
    }
}
