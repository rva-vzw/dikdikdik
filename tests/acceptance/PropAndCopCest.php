<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class PropAndCopCest
{
    public function checkILogPropAndCop8(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_8.'/players');

        // This text seems to select a dealer first. No idea why.
        $i->waitForText('penn');
        $i->waitForElement('#players_form_dealerId option[value="'.TestIds::PLAYER_DTL.'"]');
        $i->selectOption('#players_form_dealerId', TestIds::PLAYER_DTL);

        $i->click('To the score sheet');

        $i->waitForText('dtl', 10, 'th.dealer');

        $i->selectOption('#bid_loggable', 'prop_and_cop');
        $i->waitForElementVisible('.announcement');

        $i->waitForElement('//select[@name="log_game[announcingPlayer1]"]/option[@value="'.TestIds::PLAYER_DDB.'"]');
        $i->selectOption('log_game[announcingPlayer1]', TestIds::PLAYER_DDB);
        $i->selectOption('log_game[announcingPlayer2]', TestIds::PLAYER_SECRETARIS);
        $i->click('#log_game_logGame');

        // Check scores in 1st line

        $i->waitForText('prop & cop', 10, '#note_1');
        $i->waitForText('2', 10, '#score_1_'.TestIds::PLAYER_DDB);
        $i->waitForText('2', 10, '#score_1_'.TestIds::PLAYER_SECRETARIS);
        $i->waitForText('-2', 10, '#score_1_'.TestIds::PLAYER_PENNINGMEESTER);
        $i->waitForText('-2', 10, '#score_1_'.TestIds::PLAYER_DTL);

        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_9.'/log?bid=prop_and_cop');
        // Check whether player select components were reset, #116.
        // (This won't be a problem anymore now the form is loaded over http)
        $i->seeOptionIsSelected('log_game[announcingPlayer1]', '');
        $i->seeOptionIsSelected('log_game[announcingPlayer2]', '');
    }

    public function checkDealerCantAnnounceOnTableOf5(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_10.'/log?bid=prop_and_cop');

        // dvl is delaer.
        $i->dontSeeElement('//select[@name="log_game[announcingPlayer1]"]/option[@value="'.TestIds::PLAYER_DVL.'"]');
    }

    public function checkICantAnnouncePropAndCopWithTwoSameAnnouncer(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_10.'/log?bid=prop_and_cop');

        $i->selectOption('log_game[announcingPlayer1]', TestIds::PLAYER_DDB);
        $i->selectOption('log_game[announcingPlayer2]', TestIds::PLAYER_DDB);
        $i->click('#log_game_logGame');

        // I still see the log form (so no exception). Dealer stays unchanged, no prop & cop logged.

        $i->seeOptionIsSelected('#log_game_loggable', 'prop & cop');

        // Let's check if the dealer is unchanged, and nothing was logged
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_10);

        $i->see('dvl', '.dealer');
        $i->dontSee('prop & cop', '#note_1');
    }

    public function checkItDoesNotScrollToTopAfterLoggingAGame(AcceptanceTester $i): void
    {
        $i->resizeWindow(733, 1100);
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_24.'/log?bid=prop_and_cop');

        $i->waitForElement('//select[@name="log_game[announcingPlayer1]"]/option[@value="'.TestIds::PLAYER_DDB.'"]');
        $i->selectOption('log_game[announcingPlayer1]', TestIds::PLAYER_DDB);
        $i->selectOption('log_game[announcingPlayer2]', TestIds::PLAYER_SECRETARIS);
        $i->click('#log_game_logGame');

        // Wait for scores to appear
        $i->waitForText('prop & cop', 10, '#note_1');

        // Test whether 'passed around' is still visible
        // FIXME: This doesn't fail if the button is not visible 😭
        $i->see('Log');
    }
}
