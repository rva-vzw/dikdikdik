<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class SoloCest
{
    public function checkICanAnnounceSoloOnTopOfTroel(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_9.'/log?bid=solo');

        $i->seeElement('input', ['id' => 'log_game_onTopOfTroel']);

        // While we're at it, check whether clicking the label
        // checks the checkbox.
        $i->click('label[for=log_game_onTopOfTroel]');
        $i->seeCheckboxIsChecked('troel');
    }
}
