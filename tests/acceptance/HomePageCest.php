<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\Tests\AcceptanceTester;

final class HomePageCest
{
    public function checkICanChooseLanguage(AcceptanceTester $i): void
    {
        $i->amOnPage('/');
        $i->seeLink('Nieuw scoreblad (NL)');
        $i->seeLink('New score sheet (EN)');
    }
}
