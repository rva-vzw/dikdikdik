<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class PlayersManagementCest
{
    public function itHidesTitleOfEmptyPlayerTables(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/20655aaa-538b-489e-b6bd-c4751204dc36/players');
        $i->dontSee('Active players:');
        $i->dontSee('Inactive players:');
    }

    public function itDoesNotShowMarkupIfPlayerStarred(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_28.'/players');
        $i->dontSee('secr<i class="fa fa-star"></i>');
    }
}
