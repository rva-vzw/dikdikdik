<?php

declare(strict_types=1);

/** @noinspection PhpUnhandledExceptionInspection */

namespace App\Tests\acceptance;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestTable;
use App\Tests\AcceptanceTester;

final class AddPlayerCest
{
    /**
     * See #15 and #279.
     */
    public function checkIAddAPlayer(AcceptanceTester $i): void
    {
        // Go to score sheet first:
        $i->amOnPage('/nl/scores/'.ScoreSheetIdentifier::forTable(TestTable::table1()));

        // Add player in new tab:
        $i->openNewTab();
        $i->amOnPage('/nl/table/'.TestIds::TEST_TABLE_1.'/players');

        $i->waitForElement('//input[@name="players_form[newPlayerName]"]');
        $i->fillField('players_form[newPlayerName]', 'freddy');
        $i->click('Toevoegen');
        $i->waitForText('freddy', 10, '.present-players');

        $i->dontSeeInField('players_form[newPlayerName]', 'freddy');
        // check translated delete link, #279
        $i->see('Verwijderen', '.present-players');

        $i->closeTab();
        // Player should be updated on score sheet
        $i->waitForText('freddy', 10, 'th.score');
    }

    public function checkIAddAPlayerToAnEmptyTable(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/246298BE-04B8-4869-9C7B-B2EAD6D00A57/players');

        $i->waitForElement('//input[@name="players_form[newPlayerName]"]');
        $i->fillField('players_form[newPlayerName]', 'freddy');
        $i->click('Add');
        $i->waitForText('freddy', 10, '.present-players');

        $i->dontSeeInField('players_form[newPlayerName]', 'freddy');
        // The 'players_form[newPlayerName]' field should have the input focus.
        // I didn't find a way to actually check the focus, so I'll search for the autofocus attribute for now.
        $i->waitForElement('input[name="players_form[newPlayerName]"][autofocus="autofocus"]');
    }

    public function checkItDisallowsReactivatingPlayersOnFullTable(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_28.'/players');
        $i->waitForElement('.gone-players li[data-playerid="'.TestIds::PLAYER_DDB.'"]');
        // Can't re-add players, because table is full
        $i->dontSeeElement('.gone-players li[data-playerid="'.TestIds::PLAYER_DDB.'"] button');
    }

    public function checkItDoesNotAddPlayerWithNoName(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_30.'/players');
        $i->click('Add');
        $i->wait(1);
        $i->dontSee('Active players');
    }
}
