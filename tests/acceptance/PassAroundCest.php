<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class PassAroundCest
{
    /**
     * See https://gitlab.com/rva-vzw/dikdikdik/-/issues/138.
     */
    public function checkScoreLineNumbersInHtml(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_19);

        $i->waitForElementVisible('#note_1');
        $i->see('compensation passed around', '#note_1');
        $i->see('alone (6)', '#note_2');

        $i->see('-1', '#score_1_'.TestIds::PLAYER_SECRETARIS);
        $i->see('-19', '#score_2_'.TestIds::PLAYER_SECRETARIS);
    }
}
