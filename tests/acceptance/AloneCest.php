<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final readonly class AloneCest
{
    public function itLogsAlone(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_18.'/log?bid=alone');
        $i->selectOption('#log_game_announcingPlayer1', 'penn');
        $i->click('button[type="submit"]');

        $i->waitForElement('#sheet');
        $i->see('alone (6)', '.note');
    }
}
