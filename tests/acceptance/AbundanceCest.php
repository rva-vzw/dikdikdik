<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class AbundanceCest
{
    public function itDoesNothingIfNoAnnouncerSelected(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_9.'/log?bid=abundance');

        $i->click('#log_game_logGame');

        // I didn't select an announcer; nothing should have happened.
        $i->seeOptionIsSelected('#log_game_loggable', 'abundance');
    }

    public function itTranslatesAbundanceOutcome(AcceptanceTester $i): void
    {
        $i->amOnPage('/nl/table/'.TestIds::TEST_TABLE_21.'/log?bid=abundance');

        $i->waitForElementVisible('input[name="log_game[success]"]');

        // Expect translated outcome:
        $i->seeOptionIsSelected('input[name="log_game[success]"]', 'erdoor');
    }
}
