<?php

declare(strict_types=1);

/** @noinspection PhpUnhandledExceptionInspection */

namespace App\Tests\acceptance;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestPlayer;
use App\TestData\TestDataBuilders\TestTable;
use App\Tests\AcceptanceTester;

final class DealerCest
{
    public function checkDealerIsHighlighted(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_3);

        $i->waitForText('penn', 10, 'th.dealer');
        $i->see('penn', 'th.dealer');
    }

    /**
     * See #7 and #29.
     *
     * If this test fails, your testdata might not be loaded.
     * Try running `make testdata`
     */
    public function checkIChooseAndChangeDealer(AcceptanceTester $i): void
    {
        // Show score sheet first
        $i->amOnPage('/en/scores/'.ScoreSheetIdentifier::forTable(TestTable::table4()));

        // Change dealer in other tab
        $i->openNewTab();
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_4.'/players');

        $i->selectOption('#players_form_dealerId', TestIds::PLAYER_DTL);
        $i->seeOptionIsSelected('#players_form_dealerId', 'dtl');

        // Click 'back to score sheet'
        $i->click('#players_form_submit');

        // It also seems that the way we check for the highlighted dealer (using
        // css selectors or using xpath) affects the test outcome. Which seems
        // unlikely.

        // Back to scoresheet:
        $i->closeTab();
        // dtl should be highlighted as dealer
        $i->waitForElement('th.dealer[data-playerid="'.TestIds::PLAYER_DTL.'"]');

        // Change dealer in other tab
        $i->openNewTab();
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_4.'/players');
        // Select another dealer
        $i->selectOption('#players_form_dealerId', TestIds::PLAYER_DDB);
        // Click 'back to score sheet'
        $i->click('#players_form_submit');

        $i->closeTab();
        $i->waitForElement('th.dealer[data-playerid="'.TestIds::PLAYER_DDB.'"]');

        // Reload the page, and see if dealer is still selected (see #29).
        $i->reloadPage();
        $i->scrollTo('#sheet');

        $i->waitForText('ddb', 10, 'th.dealer');
        $i->see('ddb', 'th.dealer');
    }

    public function checkDealerDropDownIsOnlyEnabledWhenIActuallyCanAnnounceADealer(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_5.'/players');
        $i->waitForText('penn');

        $enabledDealerDropDown = "//select[@id='players_form_dealerId' and not(@disabled='disabled')]";

        $i->waitForElement($enabledDealerDropDown);

        $disableSecretarisSelector = "//li[@data-playerid='".TestPlayer::secretaris()->toString()."']/span/button/i[contains(@class, 'delete-icon')]";
        $i->waitForElement($disableSecretarisSelector);
        $i->click("//li[@data-playerid='".TestPlayer::secretaris()->toString()."']/span/button");
        $i->waitForElementNotVisible($enabledDealerDropDown);

        $i->fillField('players_form[newPlayerName]', 'dme');
        $i->click('Add');
        $i->waitForElementVisible($enabledDealerDropDown);

        $i->selectOption('#players_form_dealerId', TestIds::PLAYER_PENNINGMEESTER);
        // Can still change the dealer, because no game was logged.
        $i->seeElement($enabledDealerDropDown);
    }

    /**
     * See https://gitlab.com/rva-vzw/dikdikdik/issues/55.
     */
    public function checkCantPassAroundWhenNoDealerSet(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/E839BE06-5903-4306-ACEE-3971F81AE767');
        $i->dontSee('Passed around');
    }

    /**
     * See https://gitlab.com/rva-vzw/dikdikdik/issues/34.
     */
    public function checkICantSelectDealerWhoLeft(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_11.'/players');

        // Wait for element in drop down.
        $i->waitForElement('//select[@id="players_form_dealerId"]/option[@value="'.TestIds::PLAYER_DTL.'"]');
        // Penningmeester has left, shouldn't be in drop down.
        $i->dontSeeElement('//select[@id="players_form_dealerId"]/option[@value="'.TestIds::PLAYER_PENNINGMEESTER.'"]');
    }

    public function checkPlayerConfigurationScreenShowsDealer(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_3.'/players');

        $i->seeOptionIsSelected('#players_form_dealerId', 'penn');
    }
}
