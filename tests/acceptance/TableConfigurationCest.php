<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class TableConfigurationCest
{
    public function checkCanConfigureTableBeforeLoggingFirstGame(AcceptanceTester $i): void
    {
        // This is a table with no game logged yet.
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_29.'/players');

        $i->waitForText('Game conventions:');
        $i->see('Game conventions:');
    }

    public function checkCantReconfigureTableAfterLoggingFirstGame(AcceptanceTester $i): void
    {
        // On this table, prop & cop was logged.
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_13.'/players');

        $i->waitForText('To the score sheet');
        $i->dontSee('Game conventions:');
    }

    public function checkSaveTableConfiguration(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_22.'/players');

        $i->checkOption('input[value="conventions.alone5"]');

        // Add player and see if 5 is still selected
        $i->fillField('players_form[newPlayerName]', 'freddy');
        $i->click('Add');
        $i->waitForText('freddy', 10, '.present-players');

        $i->seeOptionIsSelected('input[name="players_form[conventionsName]"]', 'conventions.alone5');
    }

    public function checkClickingLabelWorksForSelectingRuleSet(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_22.'/players');

        $i->checkOption('input[value="conventions.traditional"]');

        // Click the label for 'alone 5'
        $i->click('label[for="players_form_conventionsName_1"]');
        $i->seeOptionIsSelected('input[name="players_form[conventionsName]"]', 'conventions.alone5');
    }
}
