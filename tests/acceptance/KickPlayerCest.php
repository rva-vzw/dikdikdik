<?php

/** @noinspection PhpUnhandledExceptionInspection */

namespace App\Tests\acceptance;

use App\Domain\Table\Player\PlayerIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\AcceptanceTester;

final class KickPlayerCest
{
    /**
     * If this fails, make sure you correctly initialized the test data!
     *
     * See #17.
     */
    public function checkIKickAPlayerAndAddThemAgain(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_2.'/players');

        $i->waitForText('penn');

        // Click on the delete button for secretaris
        $selector1 = $this->getDeactivateButtonSelector(TestPlayer::secretaris());

        $i->waitForElementVisible($selector1);
        $i->click($selector1);
        $i->waitForElementNotVisible($selector1);

        // The delete button has disappeared. Now click on the button to join the secretaris player again.
        $selector2 = $this->getReactivateButtonSelector(TestPlayer::secretaris());
        $i->waitForElementVisible($selector2);
        $i->click($selector2);
        $i->waitForElementNotVisible($selector2);

        // The button with the x should be visible again.
        $i->seeElement($selector1);
    }

    private function getDeactivateButtonSelector(PlayerIdentifier $playerIdentifier): string
    {
        return "//li[@data-playerid='".$playerIdentifier->toString()."']/span/button[contains(text(), 'Delete')]";
    }

    private function getReactivateButtonSelector(PlayerIdentifier $playerIdentifier): string
    {
        return "ul.gone-players li[data-playerid='{$playerIdentifier}'] button";
    }

    /**
     * See #26.
     */
    public function checkIKickTwoPlayersAndReactivateTheLeftOne(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_6.'/players');
        $i->waitForText('penn');

        $i->waitForElementVisible($this->getDeactivateButtonSelector(TestPlayer::dtl()));

        // Deactivate secretaris
        $i->click($this->getDeactivateButtonSelector(TestPlayer::secretaris()));
        // Make sure deactivation completed:
        $i->waitForElementVisible($this->getReactivateButtonSelector(TestPlayer::secretaris()));

        // Deactivate dtl
        $i->waitForElementVisible($this->getDeactivateButtonSelector(TestPlayer::dtl()));
        $i->click($this->getDeactivateButtonSelector(TestPlayer::dtl()));
        // Have to wait every time, because otherwise you'll probably just
        $i->waitForElementVisible($this->getReactivateButtonSelector(TestPlayer::dtl()));

        // Now reactivate secretaris
        $i->click($this->getReactivateButtonSelector(TestPlayer::secretaris()));
        $i->waitForElementVisible($this->getDeactivateButtonSelector(TestPlayer::secretaris()));
    }

    /**
     * I discovered this additional bug while fixing #26.
     */
    public function checkIKickTheRightmostPlayerAndAddANewOne(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_27.'/players');
        $i->waitForText('penn');

        $i->click($this->getDeactivateButtonSelector(TestPlayer::penningmeester()));
        $i->waitForElementVisible($this->getReactivateButtonSelector(TestPlayer::penningmeester()));

        $i->fillField('players_form[newPlayerName]', 'freddy');
        $i->click('Add');

        $i->waitForText('freddy', 10, '.present-players');

        $i->dontSeeInField('players_form[newPlayerName]', 'freddy');
    }

    public function checkIKickAPlayerBeforeTheGameStarts(AcceptanceTester $i): void
    {
        // See #355.
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_31.'/players');

        // Add a player
        $i->waitForElement('//input[@name="players_form[newPlayerName]"]');
        $i->fillField('players_form[newPlayerName]', 'freddy');
        $i->click('Add');
        $i->waitForText('freddy', 10, '.present-players');

        // Remove them again.
        $i->click('Delete');
        $i->dontSee('freddy', '.present-players');
    }
}
