<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestTable;
use App\Tests\AcceptanceTester;

final class TranslationCest
{
    public function checkTurboComponentsRenderTranslated(AcceptanceTester $i): void
    {
        // Score sheet:
        $i->amOnPage('/nl/scores/'.ScoreSheetIdentifier::forTable(TestTable::table21()));

        // Log something in new tab:
        $i->openNewTab();
        $i->amOnPage('/nl/table/'.TestIds::TEST_TABLE_21.'/log?bid=no_bid_accepted');
        $i->click('Ronde pas');

        $i->closeTab();
        $i->waitForText('uitbetaling ronde pas', 10, 'td.note');
    }
}
