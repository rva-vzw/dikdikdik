<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestTable;
use App\Tests\AcceptanceTester;
use Webmozart\Assert\Assert;

final class ShareSheetCest
{
    public function itSharesDutchLinkOnDutchPage(AcceptanceTester $i): void
    {
        $i->amOnPage('/nl/table/'.TestIds::TEST_TABLE_14);
        $i->waitForText('penn');
        $i->waitForElement('//input[@name="share-link"]');

        $thingy = strval($i->grabValueFrom('share-link'));
        Assert::notFalse(strpos($thingy, '/nl/scores/'));
    }

    public function itTranslatesAlone5(AcceptanceTester $i): void
    {
        // See #149.

        // http://localhost:8080/en/scores/e0033ff5-a4e9-545a-b4e1-92ecff338506
        $i->amOnPage('/en/scores/'.ScoreSheetIdentifier::forTable(TestTable::table20())->toString());
        $i->waitForElement('#note_1');
        $i->see('alone (5)', '#note_1');
    }

    public function itShowsCompleteUrlInShareLink(AcceptanceTester $i): void
    {
        $i->amOnPage('/nl/table/'.TestIds::TEST_TABLE_14);
        $shareLink = strval($i->grabValueFrom('share-link'));
        Assert::regex($shareLink, '~^http(s)?://~');
        Assert::endsWith($shareLink, '/nl/scores/'.ScoreSheetIdentifier::forTable(TestTable::table14())->toString());
    }
}
