<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class NamesLineCest
{
    public function checkISeeNoFooterWhenTheresNoScoreLines(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_1);

        $i->see('penningmeester', 'thead th');
        // Player is only shown in header (not in footer) when there's no log entries, see #243
        $i->dontSee('penningmeester', 'tfoot th');
    }
}
