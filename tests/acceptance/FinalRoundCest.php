<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\TestData\TestDataBuilders\TestIds;
use App\Tests\AcceptanceTester;

final class FinalRoundCest
{
    public function checkICantAddPlayersInFinalRound(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_16.'/players');
        $i->waitForText('Manage players');
        $i->dontSee('New player');
    }

    public function checkAppDoesntScrollUpWhenClickingFinalRound(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_17.'/extra');

        // The strange thing is: this test doesn't fail
        // if I reintroduce the problem, 😢

        $i->click('Final round');

        $i->see('Log');
    }

    public function checkICantRemovePlayersInFinalRound(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_16.'/players');
        $i->waitForText('Manage players');
        $i->dontSee('Verwijderen', '.present-players');
    }

    public function checkICantAnnounceFinalRoundInFinalRound(AcceptanceTester $i): void
    {
        $i->amOnPage('/en/table/'.TestIds::TEST_TABLE_16);
        $i->dontSeeElement('#score-form .final-round');
    }
}
