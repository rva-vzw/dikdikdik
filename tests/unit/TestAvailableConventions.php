<?php

declare(strict_types=1);

namespace App\Tests\unit;

use App\Domain\Table\Loggable\GameSpecification\Abundance10Specification;
use App\Domain\Table\Loggable\GameSpecification\Abundance11Specification;
use App\Domain\Table\Loggable\GameSpecification\Abundance12Specification;
use App\Domain\Table\Loggable\GameSpecification\AbundanceSpecification;
use App\Domain\Table\Loggable\GameSpecification\Alone5Specification;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\GameSpecification\MisereOnTheTableSpecification;
use App\Domain\Table\Loggable\GameSpecification\MisereSpecification;
use App\Domain\Table\Loggable\GameSpecification\PropAndCopSpecification;
use App\Domain\Table\Loggable\GameSpecification\SoloslimSpecification;
use App\Domain\Table\Loggable\GameSpecification\SoloSpecification;
use App\Domain\Table\Loggable\GameSpecification\TroelaSpecification;
use App\Domain\Table\Loggable\GameSpecification\TroelSpecification;
use App\Domain\Table\Rules\PassAroundRules;
use App\Domain\TableConventions\AvailableConventions;
use App\Domain\TableConventions\Conventions;
use App\Domain\TableConventions\TableTariffs\IwwaTariffs;
use App\Domain\TableConventions\TableTariffs\TraditionalTariffs;

final readonly class TestAvailableConventions
{
    /** @var Conventions[] */
    private readonly array $ruleSets;

    public function __construct()
    {
        $this->ruleSets = [
            'conventions.traditional' => new Conventions(
                'conventions.traditional',
                [
                    new PropAndCopSpecification(),
                    new AloneSpecification(),
                    new AbundanceSpecification(),
                    new MisereSpecification(),
                    new TroelSpecification(),
                    new MisereOnTheTableSpecification(),
                    new SoloSpecification(),
                    new SoloslimSpecification(),
                ],
                new TraditionalTariffs(),
                PassAroundRules::Traditional,
            ),
            'conventions.alone5' => new Conventions(
                'conventions.alone5',
                [
                    new PropAndCopSpecification(),
                    new Alone5Specification(),
                    new AbundanceSpecification(),
                    new MisereSpecification(),
                    new TroelSpecification(),
                    new MisereOnTheTableSpecification(),
                    new SoloSpecification(),
                    new SoloslimSpecification(),
                ],
                new TraditionalTariffs(),
                PassAroundRules::Traditional,
            ),
            'conventions.iwwa' => new Conventions(
                'conventions.iwwa',
                [
                    new PropAndCopSpecification(),
                    new Alone5Specification(),
                    new AbundanceSpecification(),
                    new Abundance10Specification(),
                    new MisereSpecification(),
                    new Abundance11Specification(),
                    new Abundance12Specification(),
                    new TroelSpecification(),
                    new TroelaSpecification(),
                    new MisereOnTheTableSpecification(),
                    new SoloSpecification(),
                    new SoloslimSpecification(),
                ],
                new IwwaTariffs(),
                PassAroundRules::Simplified,
            ),
        ];
    }

    public function getAvailableConventions(): AvailableConventions
    {
        return new AvailableConventions(
            $this->ruleSets,
        );
    }
}
