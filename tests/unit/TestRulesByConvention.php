<?php

declare(strict_types=1);

namespace App\Tests\unit;

use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Loggable\GameSpecification\GameSpecifications;
use App\Domain\Table\Rules\PassAroundRules;
use App\Domain\Table\Rules\RulesByConvention;
use App\Domain\TableConventions\AvailableConventions;
use App\Domain\TableConventions\Conventions;

/**
 * Table rules by convention name for unit tests.
 *
 * I could actually use this for the real D.I. container as well. Maybe
 * that's more interesting than configuring all this in yaml. I'll have
 * to think about this.
 */
final readonly class TestRulesByConvention implements RulesByConvention
{
    private readonly AvailableConventions $availableConventions;

    public function __construct()
    {
        $this->availableConventions = (new TestAvailableConventions())->getAvailableConventions();
    }

    public function getPermissibleGames(string $conventionName): GameSpecifications
    {
        return $this->availableConventions->getPermissibleGames($conventionName);
    }

    public function getAllSpecifiedGames(): AllSpecifiedGames
    {
        return new AllSpecifiedGames(new \ArrayIterator(array_reduce(
            iterator_to_array($this->availableConventions->allPossibleConventions),
            fn (array $merged, Conventions $ruleSet) => array_merge($merged, $ruleSet->allowedGames),
            [],
        )));
    }

    /**
     * @param non-empty-string $conventionName
     */
    public function getPassAroundRules(string $conventionName): PassAroundRules
    {
        return $this->availableConventions->getConventionsByName($conventionName)->passAroundRules;
    }
}
