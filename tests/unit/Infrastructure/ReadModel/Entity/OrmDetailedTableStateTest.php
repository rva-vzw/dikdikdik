<?php

declare(strict_types=1);

namespace App\Tests\unit\Infrastructure\ReadModel\Entity;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\ReadModel\Entity\OrmDetailedTableState;
use PHPUnit\Framework\TestCase;

final class OrmDetailedTableStateTest extends TestCase
{
    /** @test */
    public function itConvertsToTableState(): void
    {
        $tableIdentifier = TableIdentifier::fromString('5327f0e3-a046-4f15-b69b-f6a7dad88d7e');
        $ormDetailedTableState = OrmDetailedTableState::create($tableIdentifier);
        $ormDetailedTableState->finalGameNumber = 4;
        $ormDetailedTableState->dealerChangeable = true;

        $expected = new TableState(
            $tableIdentifier,
            null,
            GameNumber::first(),
            true,
            true,
            0,
            GameNumber::fromInteger(4),
            OrmDetailedTableState::RULESET_UNKNOWN,
        );

        $this->assertEquals($expected, $ormDetailedTableState->toTableState());
    }
}
