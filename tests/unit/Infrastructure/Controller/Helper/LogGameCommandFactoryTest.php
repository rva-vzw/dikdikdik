<?php

declare(strict_types=1);

namespace App\Tests\unit\Infrastructure\Controller\Helper;

use App\Domain\CurrentTime;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\TableIdentifier;
use App\Infrastructure\Controller\Helper\LogGameCommandFactory;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestPlayer;
use Codeception\PHPUnit\TestCase;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

final class LogGameCommandFactoryTest extends TestCase
{
    /** @var DenormalizerInterface */
    private $dummyDenormalizer;

    protected function setUp(): void
    {
        parent::setUp();
        /** @var DenormalizerInterface $denormalizerMock */
        $denormalizerMock = $this->createMock(DenormalizerInterface::class);

        $this->dummyDenormalizer = $denormalizerMock;
    }

    /** @test */
    public function itCreatesLogGameForAlone(): void
    {
        $tableIdentifier = TableIdentifier::fromString('C8E55623-74F8-448E-BF65-F68AE74D2C57');
        $aloneSpecification = new AloneSpecification();

        $params = [
            'announcement' => [
                'player1' => TestIds::PLAYER_DBFC,
            ],
            'outcome' => [
                // This is how it is passed via the ajax call right now.
                'tricks' => '6',
            ],
        ];
        $factory = new LogGameCommandFactory(
            $this->dummyDenormalizer,
            new class() implements CurrentTime {
                public function getCurrentTime(): \DateTimeImmutable
                {
                    return new \DateTimeImmutable('2021-04-03');
                }
            }
        );

        $actual = $factory->createCommandFromArray(
            $tableIdentifier,
            GameNumber::first(),
            $aloneSpecification,
            $params
        );

        $expected = new LogGame(
            $tableIdentifier,
            GameNumber::first(),
            $aloneSpecification,
            new SinglePlayerAnnouncement(
                TestPlayer::dbfc()
            ),
            NumberOfTricks::fromNumber(6),
            new \DateTimeImmutable('2021-04-03'),
        );

        $this->assertEquals($expected, $actual);
    }
}
