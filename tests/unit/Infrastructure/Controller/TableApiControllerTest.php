<?php

declare(strict_types=1);

/** @noinspection PhpUnhandledExceptionInspection */

namespace App\Tests\unit\Infrastructure\Controller;

use App\Domain\Table\Loggable\GameSpecification\AbundanceSpecification;
use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Loggable\GameSpecification\Alone5Specification;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\GameSpecification\MisereOnTheTableSpecification;
use App\Domain\Table\Loggable\GameSpecification\MisereSpecification;
use App\Domain\Table\Loggable\GameSpecification\PropAndCopSpecification;
use App\Domain\Table\Loggable\GameSpecification\SoloslimSpecification;
use App\Domain\Table\Loggable\GameSpecification\SoloSpecification;
use App\Domain\Table\Loggable\GameSpecification\TroelSpecification;
use App\Infrastructure\Controller\TableApiController;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

final class TableApiControllerTest extends TestCase
{
    /** @var CommandBus */
    private $dummyCommandBus;

    /** @var DenormalizerInterface */
    private $dummyDenormalizer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dummyCommandBus = new class() implements CommandBus {
            public function dispatch(Command ...$commands): void
            {
            }
        };

        $this->dummyDenormalizer = $this->createMock(DenormalizerInterface::class);
    }

    /** @test */
    public function testItDoesntJoinPlayerWithoutName(): void
    {
        $requestStub = $this->createMock(Request::class);
        $requestStub->method('getContent')
            ->willReturn(json_encode([
                'player_name' => '',
            ]));

        /** @var Request $request */
        $request = $requestStub;

        $controller = new TableApiController(
            $this->dummyCommandBus,
            $this->dummyDenormalizer,
            new AllSpecifiedGames(new \ArrayIterator([
                PropAndCopSpecification::NAME => new PropAndCopSpecification(),
                AloneSpecification::NAME => new AloneSpecification(),
                Alone5Specification::NAME => new Alone5Specification(),
                AbundanceSpecification::NAME => new AbundanceSpecification(),
                MisereSpecification::NAME => new MisereSpecification(),
                TroelSpecification::NAME => new TroelSpecification(),
                MisereOnTheTableSpecification::NAME => new MisereOnTheTableSpecification(),
                SoloSpecification::NAME => new SoloSpecification(),
                SoloslimSpecification::NAME => new SoloslimSpecification(),
            ])),
        );

        $actual = $controller->joinPlayer(
            $request,
            '6f58cdb7-8213-4f65-870a-c6e270dd8177',
            1,
            '34db98ce-bbbb-45f1-8699-8a78e2dd77a0'
        );

        $this->assertEquals(Response::HTTP_FORBIDDEN, $actual->getStatusCode());
    }
}
