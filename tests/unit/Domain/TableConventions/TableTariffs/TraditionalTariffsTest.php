<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\TableConventions\TableTariffs;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\Table\Event\Game\Alone;
use App\Domain\Table\Event\Game\Alone5;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;
use App\Domain\TableConventions\TableTariffs\TraditionalTariffs;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;

final class TraditionalTariffsTest extends TestCase
{
    /** @test */
    public function itCharges20FrancsPerPersonForAloneUnderTheTable(): void
    {
        $calculator = new TraditionalTariffs();

        $playersInvolved = new PlayersInvolved(
            PlayerIdentifiers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::penningmeester(),
                TestPlayer::ddb(),
                TestPlayer::secretaris(),
            ]),
            dealerIdentifier: TestPlayer::dtl()
        );

        $event = new Alone(
            TableIdentifier::fromString('cbf7f146-e6a4-41fc-b9b7-88177db160d3'),
            GameNumber::first(),
            new SinglePlayerAnnouncement(TestPlayer::penningmeester()),
            new NumberOfTricks(13),
            $playersInvolved,
            new \DateTimeImmutable('2024-03-20'),
        );

        $actualWinnings = $calculator->calculateWinnings($event);
        $expectedWinnings = PlainWinnings::singlePlayerWins(
            TestPlayer::penningmeester(),
            PlayerIdentifiers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::ddb(),
                TestPlayer::secretaris(),
            ]),
            20,
        );

        $this->assertEquals($expectedWinnings, $actualWinnings);
    }

    /** @test */
    public function itCharges20FrancsPerPersonForAlone5UnderTheTable(): void
    {
        $calculator = new TraditionalTariffs();

        $playersInvolved = new PlayersInvolved(
            PlayerIdentifiers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::penningmeester(),
                TestPlayer::ddb(),
                TestPlayer::secretaris(),
            ]),
            dealerIdentifier: TestPlayer::dtl()
        );

        $event = new Alone5(
            TableIdentifier::fromString('cbf7f146-e6a4-41fc-b9b7-88177db160d3'),
            GameNumber::first(),
            new SinglePlayerAnnouncement(TestPlayer::penningmeester()),
            new NumberOfTricks(13),
            $playersInvolved,
            new \DateTimeImmutable('2024-03-20'),
        );

        $actualWinnings = $calculator->calculateWinnings($event);
        $expectedWinnings = PlainWinnings::singlePlayerWins(
            TestPlayer::penningmeester(),
            PlayerIdentifiers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::ddb(),
                TestPlayer::secretaris(),
            ]),
            20,
        );

        $this->assertEquals($expectedWinnings, $actualWinnings);
    }
}
