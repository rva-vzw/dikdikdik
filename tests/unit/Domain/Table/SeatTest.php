<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Table;

use App\Domain\Table\Seat;
use PHPUnit\Framework\TestCase;

final class SeatTest extends TestCase
{
    /** @test */
    public function itFindsPreviousSeatOfSeat0(): void
    {
        $this->assertEquals(
            Seat::five(),
            Seat::zero()->getPrevious(),
        );
    }
}
