<?php

namespace App\Tests\unit\Domain\Table;

use App\Domain\Table\GameNumber;
use PHPUnit\Framework\TestCase;

final class GameNumberTest extends TestCase
{
    public function testGameNumberShouldBePositive(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $gameNumber = GameNumber::fromInteger(-1);
    }
}
