<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Table\Loggable\Outcome;

use App\Domain\Table\Exception\InvalidNumberOfTricks;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use PHPUnit\Framework\TestCase;

final class NumberOfTricksTest extends TestCase
{
    /** @test */
    public function testAtMost13Tricks(): void
    {
        $this->expectException(InvalidNumberOfTricks::class);
        $outcome = NumberOfTricks::fromNumber(14);
    }
}
