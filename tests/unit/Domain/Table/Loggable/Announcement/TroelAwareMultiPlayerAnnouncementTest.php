<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Exception\InvalidAnnouncement;
use App\Domain\Table\Loggable\Announcement\TroelAwareMultiPlayerAnnouncement;
use App\Domain\Table\Player\PlayerIdentifiers;
use Codeception\PHPUnit\TestCase;

final class TroelAwareMultiPlayerAnnouncementTest extends TestCase
{
    /** @test */
    public function itDoesNotConstructWithoutPlayers(): void
    {
        $this->expectException(InvalidAnnouncement::class);
        new TroelAwareMultiPlayerAnnouncement(PlayerIdentifiers::none(), false);
    }
}
