<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Exception\InvalidAnnouncement;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\TestData\TestDataBuilders\TestPlayer;
use Codeception\PHPUnit\TestCase;

final class TwoPlayerAnnouncementTest extends TestCase
{
    /**
     * @test
     */
    public function itRejectsDuplicatePlayers(): void
    {
        $this->expectException(InvalidAnnouncement::class);

        $announcement = new TwoPlayerAnnouncement(
            TestPlayer::dtl(),
            TestPlayer::dtl()
        );
    }
}
