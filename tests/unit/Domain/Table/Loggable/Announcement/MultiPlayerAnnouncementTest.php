<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Table\Loggable\Announcement;

use App\Domain\Table\Exception\InvalidAnnouncement;
use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Player\PlayerIdentifiers;
use PHPUnit\Framework\TestCase;

final class MultiPlayerAnnouncementTest extends TestCase
{
    /** @test */
    public function itDoesNotConstructWithoutPlayers(): void
    {
        $this->expectException(InvalidAnnouncement::class);
        new MultiPlayerAnnouncement(PlayerIdentifiers::none());
    }
}
