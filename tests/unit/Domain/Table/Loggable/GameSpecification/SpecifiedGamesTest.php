<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Table\Loggable\GameSpecification;

use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\GameSpecification\PropAndCopSpecification;
use PHPUnit\Framework\TestCase;

final class SpecifiedGamesTest extends TestCase
{
    /** @test */
    public function itRemovesAGame(): void
    {
        $specifiedGames = new AllSpecifiedGames(new \ArrayIterator([
            PropAndCopSpecification::NAME => new PropAndCopSpecification(),
            AloneSpecification::NAME => new AloneSpecification(),
        ]));

        $actual = $specifiedGames->without(AloneSpecification::NAME);
        $expected = new AllSpecifiedGames(new \ArrayIterator([
            PropAndCopSpecification::NAME => new PropAndCopSpecification(),
        ]));

        $this->assertEquals($expected, $actual);
    }
}
