<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Table\Player;

use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use Codeception\PHPUnit\TestCase;

final class SeatedPlayersTest extends TestCase
{
    /** @test */
    public function itKeepsPlayersInOrder(): void
    {
        $playerIdentifiers = [
            TestPlayer::dtl(),
            TestPlayer::secretaris(),
            TestPlayer::dlb(),
        ];

        $seatedPlayers = SeatedPlayers::fromArray($playerIdentifiers);

        $orderedIdentifiers = $seatedPlayers->toArray();

        $this->assertEquals($playerIdentifiers, $orderedIdentifiers);
    }

    /** @test */
    public function itInsertsAPlayer(): void
    {
        $actualResult = SeatedPlayers::fromArray([
            0 => TestPlayer::dtl(),
            2 => TestPlayer::secretaris(),
            3 => TestPlayer::dlb(),
        ])->withSeatedPlayer(Seat::one(), TestPlayer::penningmeester());

        $expectedResult = SeatedPlayers::fromArray([
            TestPlayer::dtl(),
            TestPlayer::penningmeester(),
            TestPlayer::secretaris(),
            TestPlayer::dlb(),
        ]);

        $this->assertEquals($expectedResult, $actualResult);
    }

    /** @test */
    public function itDeterminesTheNextPlayer(): void
    {
        $seatedPlayers = SeatedPlayers::fromArray([
            TestPlayer::dtl(),
            TestPlayer::penningmeester(),
            TestPlayer::secretaris(),
            TestPlayer::dlb(),
        ]);

        $nextToPenningmeester = $seatedPlayers->getPlayerNextTo(TestPlayer::penningmeester());

        $this->assertEquals(TestPlayer::secretaris(), $nextToPenningmeester);
    }

    public function testWithPlayerLeft(): void
    {
        $seatedPlayers = SeatedPlayers::fromArray([
            0 => TestPlayer::dtl(),
            1 => TestPlayer::penningmeester(),
            2 => TestPlayer::secretaris(),
            3 => TestPlayer::dlb(),
        ]);

        $actual = $seatedPlayers->withoutSeatedPlayer(TestPlayer::penningmeester());

        $expected = SeatedPlayers::fromArray([
            0 => TestPlayer::dtl(),
            2 => TestPlayer::secretaris(),
            3 => TestPlayer::dlb(),
        ]);

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itHonoursSeatNumbers(): void
    {
        $seatedPlayers = SeatedPlayers::fromArray([
            0 => TestPlayer::secretaris(),
            2 => TestPlayer::dtl(),
        ]);

        $this->assertEquals(TestPlayer::secretaris(), $seatedPlayers->getPlayerAtSeat(Seat::zero()));
        $this->assertFalse($seatedPlayers->seatIsTaken(Seat::one()));
        $this->assertEquals(TestPlayer::dtl(), $seatedPlayers->getPlayerAtSeat(Seat::two()));
    }

    public function testWithoutPlayerDoesntCareIfPlayerIsNotThere(): void
    {
        $seatedPlayers = SeatedPlayers::fromArray([
            0 => TestPlayer::secretaris(),
            2 => TestPlayer::dtl(),
        ]);

        // try removing non-present player.
        $actual = $seatedPlayers->withoutSeatedPlayer(TestPlayer::penningmeester());

        // nothing should have happened.
        $this->assertEquals($seatedPlayers, $actual);
    }

    /**
     * see https://gitlab.com/rva-vzw/dikdikdik/issues/74.
     *
     * @test
     */
    public function itReturnsPlayersOrderedBySeat(): void
    {
        $seatedPlayers = SeatedPlayers::fromArray([
            2 => TestPlayer::ddb(),
            1 => TestPlayer::dtl(),
        ]);

        $actual = array_values($seatedPlayers->toArray());
        $expected = [
            TestPlayer::dtl(),
            TestPlayer::ddb(),
        ];

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itCreatesWithoutPlayers(): void
    {
        $seatedPlayers = SeatedPlayers::none();

        $this->assertEquals([], $seatedPlayers->toArray());
    }
}
