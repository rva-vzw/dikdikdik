<?php

namespace App\Tests\unit\Domain\Table\Player;

use App\Domain\Table\Player\PlayerIdentifiers;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;

final class PlayerIdentifiersTest extends TestCase
{
    /** @test */
    public function itReturnsIdentifiersWithoutGivenIdentifiers(): void
    {
        $identifiers = PlayerIdentifiers::fromArray([
            TestPlayer::penningmeester(),
            TestPlayer::secretaris(),
            TestPlayer::ddb(),
            TestPlayer::dtl(),
        ]);

        $actual = $identifiers->without(
            TestPlayer::penningmeester(),
            TestPlayer::secretaris()
        );

        $expected = PlayerIdentifiers::fromArray([
            TestPlayer::ddb(),
            TestPlayer::dtl(),
        ]);

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itCreatesIdentifiersFromSingle(): void
    {
        $identifiers = PlayerIdentifiers::fromSingle(TestPlayer::dlb());
        $this->assertTrue($identifiers->contains(TestPlayer::dlb()));
    }
}
