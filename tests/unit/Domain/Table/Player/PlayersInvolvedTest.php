<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Table\Player;

use App\Domain\Table\Exception\DealerUnknown;
use App\Domain\Table\Exception\ItTakesFourToPlay;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\Player\SeatedPlayers;
use App\TestData\TestDataBuilders\TestPlayer;
use Codeception\PHPUnit\TestCase;

final class PlayersInvolvedTest extends TestCase
{
    /** @test */
    public function itChecksTheAmountOfPlayers(): void
    {
        $this->expectException(ItTakesFourToPlay::class);

        $playersInvolved = PlayersInvolved::from(
            SeatedPlayers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::secretaris(),
                TestPlayer::dlb(),
            ]),
            TestPlayer::secretaris()
        );
    }

    /** @test */
    public function itChecksThePresenceOfTheDealer(): void
    {
        $this->expectException(DealerUnknown::class);

        $playersInvolved = PlayersInvolved::from(
            SeatedPlayers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::secretaris(),
                TestPlayer::dlb(),
                TestPlayer::ddb(),
            ]),
            TestPlayer::penningmeester()
        );
    }

    /**
     * See: https://www.rijkvanafdronk.be/spelregels/aantal-spelers/.
     *
     * @test
     */
    public function testHasOnlyChecksRelevantPlayers(): void
    {
        $playersInvolved = PlayersInvolved::from(
            $this->get5SeatedPlayers(),
            TestPlayer::penningmeester()
        );

        // If penningmeester is the dealer, he isn't playing, since there are 5
        // people at the table.
        $this->assertFalse($playersInvolved->isPlaying(TestPlayer::penningmeester()));
    }

    /**
     * See: https://www.rijkvanafdronk.be/spelregels/aantal-spelers/.
     *
     * @test
     */
    public function testPlayersInvolvedOnTableOf5(): void
    {
        $playersInvolved = PlayersInvolved::from(
            $this->get5SeatedPlayers(),
            TestPlayer::secretaris()
        );

        $expectedResult = $this->get5SeatedPlayers()
            ->withoutSeatedPlayer(TestPlayer::secretaris())
            ->getPlayerIdentifiers();

        $actualResult = $playersInvolved->getPlayingPlayers();

        $this->assertEquals($expectedResult, $actualResult);
    }

    /**
     * See: https://www.rijkvanafdronk.be/spelregels/aantal-spelers/.
     *
     * @test
     */
    public function testPlayersInvolvedOnTableOf6(): void
    {
        $playersInvolved = PlayersInvolved::from(
            $this->get6SeatedPlayers(),
            TestPlayer::secretaris()
        );

        $expectedResult = $this->get6SeatedPlayers()
            ->withoutSeatedPlayer(TestPlayer::secretaris())
            ->withoutSeatedPlayer(TestPlayer::penningmeester())
            ->getPlayerIdentifiers();

        $actualResult = $playersInvolved->getPlayingPlayers();

        $this->assertEquals($expectedResult, $actualResult);
    }

    private function get5SeatedPlayers(): SeatedPlayers
    {
        return SeatedPlayers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::secretaris(),
                TestPlayer::dlb(),
                TestPlayer::ddb(),
                TestPlayer::penningmeester(),
            ]);
    }

    private function get6SeatedPlayers(): SeatedPlayers
    {
        return SeatedPlayers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::secretaris(),
                TestPlayer::dlb(),
                TestPlayer::ddb(),
                TestPlayer::penningmeester(),
                TestPlayer::dvl(),
            ]);
    }
}
