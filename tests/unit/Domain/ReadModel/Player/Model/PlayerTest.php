<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\Player\Model;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayerStatus;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;

final class PlayerTest extends TestCase
{
    /** @test */
    public function itActivatesPlayerWhenPutInAChair(): void
    {
        $player = Player::createGone(TestPlayer::diu(), Seat::zero(), 'diu')->seatedAt(Seat::zero());

        $this->assertEquals(PlayerStatus::ACTIVE, $player->getStatus());
    }

    /** @test */
    public function testAPlayerThatsGoneCannotBeTheDealer(): void
    {
        $player = Player::createGone(TestPlayer::penningmeester(), Seat::zero(), 'pen')
            ->seatedAt(Seat::zero())
            ->dealing();

        // A player that takes place on a seat, becomes active.
        $this->assertTrue($player->isPresent());
        $this->assertTrue($player->isDealer());

        $player = $player->gone();
        $this->assertFalse($player->isPresent());
        $this->assertFalse($player->isDealer());
    }

    /** @test */
    public function itDoesntChangeNewPlayerStatusWhenPlayerSitsDown(): void
    {
        $player = Player::createNew(TestPlayer::diu(), Seat::zero(), 'diu')->seatedAt(Seat::zero());

        $this->assertEquals(PlayerStatus::NEW, $player->getStatus());
    }
}
