<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\Player\Model;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\Player\Model\PlayerStatus;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;

final class PlayersGroupTest extends TestCase
{
    /** @test */
    public function itAddsAPlayer(): void
    {
        $player = Player::createActive(TestPlayer::penningmeester(), Seat::zero(), 'pen');
        $playersGroup = PlayersGroup::fromArray([$player]);
        $this->assertEquals($player, $playersGroup->getPlayer(TestPlayer::penningmeester()));
    }

    /** @test */
    public function itAllowsAtMostOneDealer(): void
    {
        $playersGroup = PlayersGroup::fromArray([
            (Player::createActive(TestPlayer::penningmeester(), Seat::zero(), 'pen')->dealing()),
            (Player::createActive(TestPlayer::secretaris(), Seat::one(), 'sec')->dealing()),
            (Player::createActive(TestPlayer::dtl(), Seat::two(), 'dtl')),
            (Player::createActive(TestPlayer::diu(), Seat::three(), 'diu')),
        ]);

        $penn = $playersGroup->getPlayer(TestPlayer::penningmeester());
        $secr = $playersGroup->getPlayer(TestPlayer::secretaris());

        $this->assertFalse($penn->isDealer());
        $this->assertTrue($secr->isDealer());
    }

    /** @test */
    public function itOrdersActivePlayersBySeat(): void
    {
        $player5d4 = Player::createActive(
            TestPlayer::penningmeester(),
            Seat::three(),
            'penn',
        );
        $player72a = Player::createGone(
            TestPlayer::dvl(),
            Seat::one(),
            'dvl',
        )->withStars(1);
        $player730 = Player::createActive(
            TestPlayer::ddl(),
            Seat::one(),
            'ddl',
        );
        $players = PlayersGroup::fromArray([
            $player5d4,
                $player72a,
                $player730,
        ]);

        $expected = [
            $player730,
            $player5d4,
        ];

        $actual = array_values(iterator_to_array($players->getPresent()));

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itCanHaveAtMostOnePlayerOnASeat(): void
    {
        $player5d4 = Player::createActive(
            TestPlayer::penningmeester(),
            Seat::three(),
            'penn',
        );
        $player72a = Player::createActive(
            TestPlayer::dvl(),
            Seat::three(),
            'dvl',
        );

        $playersGroup = PlayersGroup::fromArray([$player5d4, $player72a]);

        $this->assertEquals(
            PlayerStatus::GONE,
            $playersGroup->getPlayer(TestPlayer::penningmeester())->getStatus()
        );
    }

    /** @test */
    public function itPutsPlayerOnSeat(): void
    {
        $player = Player::createGone(
            TestPlayer::penningmeester(),
            Seat::three(),
            'pen',
        );

        $playersGroup = PlayersGroup::fromArray([$player])
            ->withPlayerOnSeat(TestPlayer::penningmeester(), Seat::one());

        $actual = $playersGroup->getPlayer(TestPlayer::penningmeester());
        $expected = Player::createActive(
            TestPlayer::penningmeester(),
            Seat::one(),
            'pen',
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itMarksLeavingActivePlayerAsGone(): void
    {
        $activePlayer = Player::createActive(
            TestPlayer::penningmeester(),
            Seat::zero(),
            'pen',
        );

        $inactivePlayer = Player::createGone(
            TestPlayer::penningmeester(),
            Seat::zero(),
            'pen',
        );

        $playersGroup = PlayersGroup::fromArray([$activePlayer]);

        $actual = $playersGroup->withoutPlayer(TestPlayer::penningmeester());
        $expected = PlayersGroup::fromArray([$inactivePlayer]);

        $this->assertEquals($expected, $actual);
    }
}
