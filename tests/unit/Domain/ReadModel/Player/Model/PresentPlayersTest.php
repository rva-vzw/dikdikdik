<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\Player\Model;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\Player\Model\PresentPlayer;
use App\Domain\ReadModel\Player\Model\PresentPlayers;
use App\Domain\Table\Player\NamedPlayer;
use App\Domain\Table\Seat;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;

final class PresentPlayersTest extends TestCase
{
    /** @test */
    public function itDeactivatesPlayersIfLessThanFour(): void
    {
        $presentPlayers = new PresentPlayers([
            new PresentPlayer(TestPlayer::dtl(), 'dtl', true),
            new PresentPlayer(TestPlayer::penningmeester(), 'penn', true),
            new PresentPlayer(TestPlayer::secretaris(), 'sec', true),
            new PresentPlayer(TestPlayer::ddl(), 'ddl', true),
        ]);

        $this->assertTrue($presentPlayers->hasFourActive());

        $presentPlayers = $presentPlayers->withoutPlayer(TestPlayer::ddl());

        $this->assertFalse($presentPlayers->hasFourActive());
    }

    /** @test */
    public function itInactivatesProperPlayersWhenSelectingDealer(): void
    {
        $actual = (new PresentPlayers([
            new PresentPlayer(TestPlayer::dtl(), 'dtl', false),
            new PresentPlayer(TestPlayer::penningmeester(), 'penn', false),
            new PresentPlayer(TestPlayer::secretaris(), 'sec', false),
            new PresentPlayer(TestPlayer::ddl(), 'ddl', false),
            new PresentPlayer(TestPlayer::diu(), 'diu', false),
            new PresentPlayer(TestPlayer::dpb(), 'dpb', false),
        ]))->withDealer(TestPlayer::penningmeester());

        $expected = new PresentPlayers([
            new PresentPlayer(TestPlayer::dtl(), 'dtl', true),
            new PresentPlayer(TestPlayer::penningmeester(), 'penn', false),
            new PresentPlayer(TestPlayer::secretaris(), 'sec', true),
            new PresentPlayer(TestPlayer::ddl(), 'ddl', true),
            new PresentPlayer(TestPlayer::diu(), 'diu', false),
            new PresentPlayer(TestPlayer::dpb(), 'dpb', true),
        ]);

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itRemovesPlayer(): void
    {
        $actual = PresentPlayers::none()
            ->withInactivePlayer(Seat::zero(), new NamedPlayer(TestPlayer::dvl(), 'dvl'))
            ->withInactivePlayer(Seat::one(), new NamedPlayer(TestPlayer::dbfc(), 'dbfc'))
            ->withoutPlayer(TestPlayer::dvl());

        $expected = PresentPlayers::none()
            ->withInactivePlayer(Seat::one(), new NamedPlayer(TestPlayer::dbfc(), 'dbfc'));

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itDoesNotAddPlayersThatAreGone(): void
    {
        $playersGroup = PlayersGroup::fromArray([
            Player::createActive(TestPlayer::secretaris(), Seat::zero(), 'secr'),
            Player::createActive(TestPlayer::penningmeester(), Seat::one(), 'penn'),
            Player::createActive(TestPlayer::dtl(), Seat::two(), 'dtl'),
            Player::createGone(TestPlayer::diu(), Seat::three(), 'diu'),
            Player::createActive(TestPlayer::dpb(), Seat::four(), 'dpb'),
        ]);

        $presentPlayers = PresentPlayers::fromPlayersGroup($playersGroup);
        $presentPlayerCount = count(iterator_to_array($presentPlayers));

        $this->assertEquals(4, $presentPlayerCount);
    }
}
