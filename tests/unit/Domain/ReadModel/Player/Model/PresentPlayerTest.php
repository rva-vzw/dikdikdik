<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\Player\Model;

use App\Domain\ReadModel\Player\Model\PresentPlayer;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;

final class PresentPlayerTest extends TestCase
{
    /** @test */
    public function itActivatesAPlayer(): void
    {
        $player = PresentPlayer::createInactive(TestPlayer::dtl(), 'dtl')->playing();

        $this->assertTrue($player->isPlaying());
    }
}
