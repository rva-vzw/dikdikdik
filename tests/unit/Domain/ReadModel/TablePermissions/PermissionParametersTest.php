<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\TablePermissions;

use App\Domain\ReadModel\TablePermissions\PermissionParameters;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use PHPUnit\Framework\TestCase;

final class PermissionParametersTest extends TestCase
{
    /** @test */
    public function itIncreasesPlayerCount(): void
    {
        $tableIdentifier = TableIdentifier::fromString('500a829d-f90e-45e6-8f92-cf764c005ff2');
        $actual = PermissionParameters::forNewTable(
            $tableIdentifier,
        )->withAdditionalPlayer();

        $expected = new PermissionParameters(
            $tableIdentifier,
            null,
            1,
            false,
            GameNumber::first(),
            null,
            false,
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itUpdatesGameNumber(): void
    {
        $tableIdentifier = TableIdentifier::fromString('500a829d-f90e-45e6-8f92-cf764c005ff2');
        $actual = PermissionParameters::forNewTable(
            $tableIdentifier,
        )->forGame(GameNumber::second());

        $expected = new PermissionParameters(
            $tableIdentifier,
            null,
            0,
            false,
            GameNumber::second(),
            null,
            false,
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itRecognizesFullTable(): void
    {
        $tableIdentifier = TableIdentifier::fromString('4e0d56bd-cd86-4ad3-b521-8d2f453a1707');
        $permissionParameters = new PermissionParameters(
            $tableIdentifier,
            null,
            6,
            true,
            GameNumber::first(),
            null,
            false,
        );

        $this->assertFalse($permissionParameters->canAddPlayer());
    }
}
