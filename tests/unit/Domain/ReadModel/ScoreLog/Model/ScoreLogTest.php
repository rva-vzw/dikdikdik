<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\ScoreLog\Model;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\ScoreLog\Model\ScoreLog;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;

final class ScoreLogTest extends TestCase
{
    /** @test */
    public function itReturnsNoPlayersInvolvedWhenThereIsNotEnoughPlayers(): void
    {
        $scoreLog = ScoreLog::forPlayers(
            PlayersGroup::fromArray([
                (Player::createActive(TestPlayer::ddl(), Seat::zero(), 'ddl')),
                (Player::createActive(TestPlayer::diu(), Seat::one(), 'diu')),
                (Player::createActive(TestPlayer::ddb(), Seat::two(), 'ddb')),
                (Player::createGone(TestPlayer::dlb(), Seat::three(), 'dlb')),
            ])->withDealer(TestPlayer::ddb()),
            ScoreSheetIdentifier::forTable(TableIdentifier::fromString('c0f61b54-a018-4e6e-bdc6-42fc018d4e55')),
        );

        $this->assertNull($scoreLog->getPlayersInvolved());
    }

    /** @test */
    public function itReturnsPlayersInvolvedWhenThereIsMoreThanEnoughPlayers(): void
    {
        $scoreLog = ScoreLog::forPlayers(
            PlayersGroup::fromArray([
                (Player::createActive(TestPlayer::ddl(), Seat::zero(), 'ddl')),
                (Player::createActive(TestPlayer::diu(), Seat::one(), 'diu')),
                (Player::createActive(TestPlayer::ddb(), Seat::two(), 'ddb')),
                (Player::createActive(TestPlayer::secretaris(), Seat::three(), 'sec')),
                (Player::createActive(TestPlayer::penningmeester(), Seat::four(), 'pen')),
            ])->withDealer(TestPlayer::ddl()),
            ScoreSheetIdentifier::forTable(TableIdentifier::fromString('c0f61b54-a018-4e6e-bdc6-42fc018d4e55')),
        );

        $expected = new PlayersInvolved(
            PlayerIdentifiers::fromArray([TestPlayer::diu(), TestPlayer::ddb(), TestPlayer::secretaris(), TestPlayer::penningmeester()]),
            TestPlayer::ddl()
        );

        $this->assertEquals($expected, $scoreLog->getPlayersInvolved());
    }
}
