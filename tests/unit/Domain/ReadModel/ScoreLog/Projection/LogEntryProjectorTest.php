<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\ScoreLog\Projection;

use App\Domain\ReadModel\ReadModelUpdateNotifier;
use App\Domain\ReadModel\ScoreLog\Model\SimpleScoreLogEntry;
use App\Domain\ReadModel\ScoreLog\Projection\LogEntryProjector;
use App\Domain\ReadModel\ScoreLog\Service\LogEntryRepository;
use App\Domain\ReadModel\ScoreLog\Service\ScoreLogCorrector;
use App\Domain\ReadModel\TablesBySheet\TablesBySheet;
use App\Domain\ScoreSheet\Event\ScoreFactorDecreased;
use App\Domain\ScoreSheet\Event\ScoresLogged;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineBuilder;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\Event\TableCleared;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

final class LogEntryProjectorTest extends TestCase
{
    private TablesBySheet $dummyTablesBySheet;
    private ReadModelUpdateNotifier $dummyNotifier;
    private ScoreSheetIdentifier $scoreSheetIdentifier;
    private LoggerInterface $dummyLogger;
    private LogEntryRepository $dummyLogEntryRepository;
    private ScoreLogCorrector $dummyScoreLogCorrector;

    protected function setUp(): void
    {
        $tableIdentifier = TableIdentifier::fromString('b11d8118-b4e0-466d-a5a1-d05320aa2d12');
        $this->scoreSheetIdentifier = ScoreSheetIdentifier::forTable(
            $tableIdentifier,
        );

        $this->dummyNotifier = $this->createMock(ReadModelUpdateNotifier::class);

        $this->dummyTablesBySheet = $this->createMock(TablesBySheet::class);
        $this->dummyTablesBySheet->method('getTableByScoreSheet')
            ->with($this->scoreSheetIdentifier)
            ->willReturn($tableIdentifier);

        $this->dummyLogEntryRepository = $this->createMock(LogEntryRepository::class);
        $this->dummyScoreLogCorrector = new ScoreLogCorrector($this->dummyLogEntryRepository);
        $this->dummyLogger = $this->createMock(LoggerInterface::class);
    }

    /** @test */
    public function itLogsDealerAnnounced(): void
    {
        $tableIdentifier = TableIdentifier::fromString('050611b7-c9a9-422d-a737-280f3d88d34b');
        $scoreLogEntry = SimpleScoreLogEntry::create(
            GameNumber::first(),
            ScoreLineNumber::first(),
            ScoreSheetIdentifier::forTable(
                $tableIdentifier,
            ),
        );

        $logEntryRepositoryMock = $this->createMock(LogEntryRepository::class);
        $logEntryRepositoryMock->method('getOrCreateEntryForGame')
            ->willReturn($scoreLogEntry);
        $logEntryRepositoryMock->expects($this->once())
            ->method('saveLogEntry')
            ->with($scoreLogEntry->withDealer(TestPlayer::dtl()));

        $projector = new LogEntryProjector(
            $this->dummyTablesBySheet,
            $this->dummyNotifier,
            $logEntryRepositoryMock,
            $this->dummyScoreLogCorrector,
            $this->dummyLogger,
        );

        $projector->__invoke(
            new DealerAnnounced($tableIdentifier, TestPlayer::dtl(), GameNumber::first())
        );
    }

    /** @test */
    public function itLogsScores(): void
    {
        $scoreLine = ScoreLineBuilder::create('Misere', ScoreLineNumber::first())
            ->withFrancsForPlayer(TestPlayer::dtl(), -15)
            ->withFrancsForPlayer(TestPlayer::secretaris(), 5)
            ->withFrancsForPlayer(TestPlayer::penningmeester(), 5)
            ->withFrancsForPlayer(TestPlayer::dvl(), 5)
            ->build();

        $logEntryRepositoryMock = $this->createMock(LogEntryRepository::class);
        $scoreLogEntry = new SimpleScoreLogEntry(
            TestPlayer::dvl(),
            ScoreFactor::single(),
            GameNumber::first(),
            ScoreLine::empty(ScoreLineNumber::first()),
            $this->scoreSheetIdentifier,
        );
        $logEntryRepositoryMock->method('getOrCreateEntryForGame')
            ->willReturn(
                $scoreLogEntry,
            );

        $logEntryRepositoryMock->expects($this->once())
            ->method('saveLogEntry')
            ->with($scoreLogEntry->withScoreLine($scoreLine));

        $event = new ScoresLogged(
            $this->scoreSheetIdentifier,
            GameNumber::first(),
            $scoreLine,
            ScoreFactor::single()
        );

        $projector = new LogEntryProjector(
            $this->dummyTablesBySheet,
            $this->dummyNotifier,
            $logEntryRepositoryMock,
            $this->dummyScoreLogCorrector,
            $this->dummyLogger,
        );

        $projector->__invoke($event);
    }

    /** @test */
    public function itDecreasesScoreFactor(): void
    {
        $logEntryRepositoryMock = $this->createMock(LogEntryRepository::class);

        $logEntry = new SimpleScoreLogEntry(
            null,
            ScoreFactor::double(),
            GameNumber::first(),
            ScoreLine::empty(ScoreLineNumber::first()),
            $this->scoreSheetIdentifier,
        );
        $logEntryRepositoryMock->method('getOrCreateEntryForGame')
            ->willReturn($logEntry);

        $logEntryRepositoryMock->expects($this->once())
            ->method('saveLogEntry')
            ->with($logEntry->withScoreFactor(ScoreFactor::single()));

        $projector = new LogEntryProjector(
            $this->dummyTablesBySheet,
            $this->dummyNotifier,
            $logEntryRepositoryMock,
            $this->dummyScoreLogCorrector,
            $this->dummyLogger,
        );

        $projector->__invoke(new ScoreFactorDecreased(
            $this->scoreSheetIdentifier,
            GameNumber::first(),
            ScoreFactor::single(),
        ));
    }

    /**
     * @test
     */
    public function itDoesNotNotifyOnIrrelevantEvent(): void
    {
        $notifierMock = $this->createMock(ReadModelUpdateNotifier::class);
        $notifierMock->expects($this->never())->method('notify');

        $projector = new LogEntryProjector(
            $this->dummyTablesBySheet,
            $notifierMock,
            $this->dummyLogEntryRepository,
            $this->dummyScoreLogCorrector,
            $this->dummyLogger,
        );

        $event = new TableCleared(
            TableIdentifier::fromString('cc0a9dbb-5f1c-43e7-acfe-265ea2f9e271'),
        );

        $projector->__invoke($event);
    }
}
