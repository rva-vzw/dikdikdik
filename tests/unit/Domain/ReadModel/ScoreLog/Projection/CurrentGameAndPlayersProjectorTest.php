<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\ScoreLog\Projection;

use App\Domain\ReadModel\Player\Model\Player;
use App\Domain\ReadModel\Player\Model\PlayersGroup;
use App\Domain\ReadModel\ReadModelUpdateNotifier;
use App\Domain\ReadModel\ScoreLog\Model\CurrentGameAndPlayers;
use App\Domain\ReadModel\ScoreLog\Projection\CurrentGameAndPlayersProjector;
use App\Domain\ReadModel\ScoreLog\Service\CurrentGameAndPlayersRepository;
use App\Domain\ScoreSheet\Event\PaidOut;
use App\Domain\ScoreSheet\Score\MultipliedWinnings;
use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\Test;

final class CurrentGameAndPlayersProjectorTest extends TestCase
{
    private ReadModelUpdateNotifier $dummyReadModelUpdateNotifier;

    protected function setUp(): void
    {
        $this->dummyReadModelUpdateNotifier = $this->createMock(ReadModelUpdateNotifier::class);
    }

    /** @test */
    public function itDoesNotReactivateGonePlayerWhenTheyGetPaid(): void
    {
        $gameAndPlayers = new CurrentGameAndPlayers(
            GameNumber::first(),
            PlayersGroup::fromArray([
                Player::createActive(TestPlayer::dtl(), Seat::zero(), 'dtl'),
                Player::createActive(TestPlayer::ddb(), Seat::one(), 'ddb'),
                Player::createActive(TestPlayer::secretaris(), Seat::two(), 'sec'),
                Player::createActive(TestPlayer::penningmeester(), Seat::three(), 'penn'),
                Player::createGone(TestPlayer::dcb(), Seat::four(), 'dcb'),
            ])
        );

        $gameAndPlayersRepositoryMock = $this->createMock(CurrentGameAndPlayersRepository::class);
        $gameAndPlayersRepositoryMock->method('getGameAndPlayers')->willReturn($gameAndPlayers);
        $gameAndPlayersRepositoryMock->expects($this->never())
            // Nothing should change to the game number (it stays the same),
            // or to the players (an inactive player that gets paid, should not be re-activated,
            // because it's possible we're dealing with an undo-operation)
            ->method('saveGameAndPlayers');

        $projector = new CurrentGameAndPlayersProjector(
            $gameAndPlayersRepositoryMock,
            $this->dummyReadModelUpdateNotifier,
        );

        $projector->__invoke(
            new PaidOut(
                ScoreSheetIdentifier::forTable(TableIdentifier::fromString('dd95b5c9-c221-4435-9eaa-9261c457da61')),
                GameNumber::first(),
                MultipliedWinnings::plain(
                    new PlainWinnings([
                        (string) TestPlayer::secretaris() => -2,
                        (string) TestPlayer::dtl() => -2,
                        (string) TestPlayer::penningmeester() => 2,
                        (string) TestPlayer::dcb() => 2,
                    ]),
                ),
                'undo_prop_and_cop',
                ScoreLineNumber::second(),
            ),
        );
    }
}
