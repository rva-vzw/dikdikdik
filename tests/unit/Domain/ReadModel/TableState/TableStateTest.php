<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\TableState;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;

final class TableStateTest extends TestCase
{
    /** @test */
    public function itDisablesDealerSelectionWhenNotEnoughPlayers(): void
    {
        $tableState = new TableState(
            TableIdentifier::fromString('a913dff0-3e06-4f7d-b31f-e6546c7dbdb4'),
            null,
            GameNumber::first(),
            true,
            true,
            4,
            null,
            'tablestate.default',
        );

        $this->assertTrue($tableState->dealerChangeable);
        $tableState = $tableState->withPlayerLeft(TestPlayer::dlb());

        // Only 3 players; cannot select a dealer
        $this->assertFalse($tableState->dealerChangeable);
        // There is also no dealer when there's only 3 players
        $this->assertNull($tableState->dealer);
    }

    /** @test */
    public function itEnablesDealerSelectionWhenNewPlayerJoins(): void
    {
        $tableState = new TableState(
            TableIdentifier::fromString('a913dff0-3e06-4f7d-b31f-e6546c7dbdb4'),
            null,
            GameNumber::first(),
            true,
            false,
            4,
            null,
            'ruleset.traditional',
        );

        $tableState = $tableState->withAdditionalPlayer();

        $this->assertTrue($tableState->dealerChangeable);
    }

    /** @test */
    public function itEnablesDealerSelectionWhenPlayerLeavesAndEnoughPlayersAreLeft(): void
    {
        $tableState = new TableState(
            TableIdentifier::fromString('a913dff0-3e06-4f7d-b31f-e6546c7dbdb4'),
            null,
            GameNumber::first(),
            true,
            false,
            5,
            null,
            'ruleset.traditional',
        );

        $tableState = $tableState->withPlayerLeft(TestPlayer::dcb());

        $this->assertTrue($tableState->dealerChangeable);
    }

    /** @test */
    public function itAllowsNewPlayersOnEmptyTable(): void
    {
        $emptyTable = new TableState(
            TableIdentifier::fromString('bde3613d-52b4-4833-be9a-03afe9bf6590'),
            null,
            GameNumber::first(),
            true,
            false,
            0,
            null,
            'ruleset.traditional',
        );

        $this->assertTrue($emptyTable->hasFreePlaces());
    }

    /** @test */
    public function itUpdatesGameNumber(): void
    {
        $tableState = new TableState(
            TableIdentifier::fromString('affb9eec-f316-4c5d-a83a-6a7f694b5a6d'),
            null,
            GameNumber::first(),
            true,
            true,
            4,
            null,
            'ruleset.traditional',
        );

        $actual = $tableState->forGame(GameNumber::second());
        $expected = new TableState(
            TableIdentifier::fromString('affb9eec-f316-4c5d-a83a-6a7f694b5a6d'),
            null,
            GameNumber::second(),
            true,
            true,
            4,
            null,
            'ruleset.traditional',
        );

        $this->assertEquals($expected, $actual);
    }
}
