<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\TableState;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ReadModel\TableState\TableStateProjector;
use App\Domain\ReadModel\TableState\TableStateRepository;
use App\Domain\Table\Event\FinalRoundAnnounced;
use App\Domain\Table\Event\TableConfigured;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use PHPUnit\Framework\TestCase;

final class TableStateProjectorTest extends TestCase
{
    private TableIdentifier $tableIdentifier;

    private function getTableState(): TableState
    {
        $tableState = new TableState(
            $this->tableIdentifier,
            null,
            GameNumber::first(),
            true,
            true,
            4,
            null,
            'conventions.traditional',
        );

        return $tableState;
    }

    protected function setUp(): void
    {
        $this->tableIdentifier = new TableIdentifier('0ca1179f-573f-41d9-8682-a515eaf9147e');
    }

    /** @test */
    public function itAnnouncesFinalRound(): void
    {
        $tableState = $this->getTableState();

        $tableStateRepositoryMock = $this->createMock(TableStateRepository::class);
        $tableStateRepositoryMock->method('getTableState')->willReturn($tableState);
        $tableStateRepositoryMock->expects($this->once())
            ->method('saveTableState')
            ->with($tableState->withFinalGame(GameNumber::fromInteger(4)));

        $projector = new TableStateProjector(
            $tableStateRepositoryMock,
        );
        $projector->applyFinalRoundAnnounced(new FinalRoundAnnounced(
            $this->tableIdentifier,
            GameNumber::first(),
            GameNumber::fromInteger(4),
        ));
    }

    /** @test */
    public function itChoosesARuleSet(): void
    {
        $tableState = $this->getTableState();

        $tableStateRepositoryMock = $this->createMock(TableStateRepository::class);
        $tableStateRepositoryMock->method('getTableState')->willReturn($tableState);
        $tableStateRepositoryMock->expects($this->once())
            ->method('saveTableState')
            ->with($tableState->withConventions('ruleset.alone5'));

        $projector = new TableStateProjector(
            $tableStateRepositoryMock,
        );

        $projector->__invoke(new TableConfigured($this->tableIdentifier, 'ruleset.alone5'));
    }
}
