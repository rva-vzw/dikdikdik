<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\ReadModel\GameCounter;

use App\Domain\ReadModel\GameCounter\GameCounter;
use App\Domain\ReadModel\GameCounter\GameCounterProjector;
use App\Domain\ReadModel\GameCounter\GameCounterRepository;
use App\Domain\Table\Event\Undo\GameUndone;
use App\Domain\Table\TableIdentifier;
use PHPUnit\Framework\TestCase;

final class GameCounterProjectorTest extends TestCase
{
    /** @test */
    public function itDecreasesCounterWhenGameUndone(): void
    {
        $tableIdentifier = TableIdentifier::fromString('c4d5727e-4e93-491f-9933-3f8fda209a80');

        $gameCounter = GameCounter::createNew(
            $tableIdentifier,
            new \DateTimeImmutable('2022-02-09'),
        )->incrementedAt(new \DateTimeImmutable('2022-02-09'))
        // increment game counter twice, otherwise it will be deleted on undo
        ->incrementedAt(new \DateTimeImmutable('2022-02-09'));

        $repositoryMock = $this->createMock(GameCounterRepository::class);
        $repositoryMock->method('findCounterForTable')
            ->willReturn($gameCounter);
        $repositoryMock->expects($this->once())->method('saveGameCounter')
            ->with($gameCounter->decremented());

        $projector = new GameCounterProjector($repositoryMock);

        $event = $this->createMock(GameUndone::class);
        $event->method('getTableIdentifier')->willReturn($tableIdentifier);

        $projector->__invoke($event);
    }

    /** @test */
    public function itRemovesCounterWhenLastGameUndone(): void
    {
        $tableIdentifier = TableIdentifier::fromString('c4d5727e-4e93-491f-9933-3f8fda209a80');

        $gameCounter = GameCounter::createNew(
            $tableIdentifier,
            new \DateTimeImmutable('2022-02-09'),
        )->incrementedAt(new \DateTimeImmutable('2022-02-09'));

        $repositoryMock = $this->createMock(GameCounterRepository::class);
        $repositoryMock->method('findCounterForTable')
            ->willReturn($gameCounter);
        $repositoryMock->expects($this->once())->method('removeForTable')
            ->with($tableIdentifier);

        $projector = new GameCounterProjector($repositoryMock);

        $event = $this->createMock(GameUndone::class);
        $event->method('getTableIdentifier')->willReturn($tableIdentifier);

        $projector->__invoke($event);
    }
}
