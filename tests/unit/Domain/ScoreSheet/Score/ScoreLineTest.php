<?php

/** @noinspection PhpUnhandledExceptionInspection */

namespace App\Tests\unit\Domain\ScoreSheet\Score;

use App\Domain\ScoreSheet\Score\MultipliedWinnings;
use App\Domain\ScoreSheet\Score\PlainWinningsBuilder;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineBuilder;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\TestData\TestDataBuilders\TestIds;
use App\TestData\TestDataBuilders\TestPlayer;
use PHPUnit\Framework\TestCase;

final class ScoreLineTest extends TestCase
{
    /** @test */
    public function itAddsMultipliedWinnings(): void
    {
        $scoreLine = new ScoreLine(
            [
                TestIds::PLAYER_SECRETARIS => 2,
                TestIds::PLAYER_DVL => 2,
                TestIds::PLAYER_DLB => -2,
                TestIds::PLAYER_DBFC => -2,
            ],
            'Prop & cop',
            ScoreLineNumber::first()
        );

        $actual = $scoreLine->getNext(
            new MultipliedWinnings(
                PlainWinningsBuilder::create()
                    ->withWinner(TestPlayer::dvl(), 15)
                    ->withLoser(TestPlayer::dlb(), 5)
                    ->withLoser(TestPlayer::dbfc(), 5)
                    ->withLoser(TestPlayer::penningmeester(), 5)
                    ->build(),
                ScoreFactor::fromInteger(2)
            ),
            'Misere'
        );

        $this->assertEquals(2, $actual->getFrancsForPlayer(TestPlayer::secretaris()));
        $this->assertEquals(32, $actual->getFrancsForPlayer(TestPlayer::dvl()));
        $this->assertEquals(-12, $actual->getFrancsForPlayer(TestPlayer::dlb()));
        $this->assertEquals(-12, $actual->getFrancsForPlayer(TestPlayer::dbfc()));
        $this->assertEquals(-10, $actual->getFrancsForPlayer(TestPlayer::penningmeester()));
        // Latest note should be kept.
        $this->assertEquals('Misere', $actual->getNote());
    }

    /** @test */
    public function itCreatesFromMultipliedWinnings(): void
    {
        $scoreLine = ScoreLine::fromMultipliedWinnings(
            new MultipliedWinnings(
                PlainWinningsBuilder::create()
                    ->withWinner(TestPlayer::dbfc(), 2)
                    ->withWinner(TestPlayer::dvl(), 2)
                    ->withLoser(TestPlayer::dtl(), 2)
                    ->withLoser(TestPlayer::ddb(), 2)
                    ->build(),
                ScoreFactor::fromInteger(2)
            ),
            'Prop & cop',
            ScoreLineNumber::first()
        );

        $expected = ScoreLineBuilder::create('Prop & cop', ScoreLineNumber::first())
            ->withFrancsForPlayer(TestPlayer::dbfc(), 4)
            ->withFrancsForPlayer(TestPlayer::dvl(), 4)
            ->withFrancsForPlayer(TestPlayer::dtl(), -4)
            ->withFrancsForPlayer(TestPlayer::ddb(), -4)
            ->build();

        $this->assertEquals($expected, $scoreLine);
    }
}
