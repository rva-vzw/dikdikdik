<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace App\Tests\unit\Domain\ScoreSheet\Event;

use App\Domain\ScoreSheet\Event\ScoresLogged;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use App\TestData\TestDataBuilders\TestIds;
use PHPUnit\Framework\TestCase;

final class ScoresLoggedTest extends TestCase
{
    /** @test */
    public function itReturnsTheUnderlayingNote(): void
    {
        $event = new ScoresLogged(
            ScoreSheetIdentifier::forTable(
                TableIdentifier::fromString('46A0B057-5C03-444A-953E-E823306E6954')
            ),
            GameNumber::first(),
            new ScoreLine(
                [
                    TestIds::PLAYER_DPB => 15,
                    TestIds::PLAYER_DLB => -5,
                    TestIds::PLAYER_DBFC => -5,
                    TestIds::PLAYER_DDB => -5,
                ],
                'misere',
                ScoreLineNumber::first()
            ),
            ScoreFactor::single()
        );

        $this->assertEquals('misere', $event->getNote());
    }
}
