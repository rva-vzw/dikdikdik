<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\WriteModel\ScoreSheet\Aggregate;

use App\Domain\ScoreSheet\Event\PaidOut;
use App\Domain\ScoreSheet\Event\ScoreFactorDecreased;
use App\Domain\ScoreSheet\Event\ScoreFactorIncreased;
use App\Domain\ScoreSheet\Event\ScoresCorrected;
use App\Domain\ScoreSheet\Event\ScoresLogged;
use App\Domain\ScoreSheet\Event\UndoLogged;
use App\Domain\ScoreSheet\Score\MultipliedWinnings;
use App\Domain\ScoreSheet\Score\PlainWinningsBuilder;
use App\Domain\ScoreSheet\Score\ScoreFactor;
use App\Domain\ScoreSheet\Score\ScoreLine;
use App\Domain\ScoreSheet\Score\ScoreLineNumber;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\Table\GameNumber;
use App\Domain\Table\TableIdentifier;
use App\Domain\WriteModel\ScoreSheet\Aggregate\ScoreSheetDecider;
use App\Domain\WriteModel\ScoreSheet\Aggregate\ScoreSheetInternalState;
use App\Domain\WriteModel\ScoreSheet\Command\DecreaseScoreFactor;
use App\Domain\WriteModel\ScoreSheet\Command\IncreaseScoreFactor;
use App\Domain\WriteModel\ScoreSheet\Command\LogNoGame;
use App\Domain\WriteModel\ScoreSheet\Command\LogPlayedGame;
use App\Domain\WriteModel\ScoreSheet\Command\LogUndoneGame;
use App\TestData\TestDataBuilders\TestPlayer;
use RvaVzw\KrakBoem\Cqrs\Decider\Decider;
use RvaVzw\KrakBoem\TestHelper\DeciderTestCase;
use RvaVzw\KrakBoem\TestHelper\HappyPath;

/**
 * @extends DeciderTestCase<ScoreSheetInternalState>
 */
final class ScoreSheetDeciderTest extends DeciderTestCase
{
    private TableIdentifier $tableIdentifier;
    private ScoreSheetIdentifier $scoreSheetIdentifier;

    protected function setUp(): void
    {
        $this->tableIdentifier = TableIdentifier::fromString('2c9f7894-a7f5-4766-b6e2-9cc799132dd5');
        $this->scoreSheetIdentifier = ScoreSheetIdentifier::forTable($this->tableIdentifier);
    }

    protected function getDecider(): Decider
    {
        return new ScoreSheetDecider();
    }

    /** @test */
    public function itPaysOut(): void
    {
        $winningsFirstGame = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 2)
            ->withWinner(TestPlayer::dbfc(), 2)
            ->withLoser(TestPlayer::dtl(), 2)
            ->withLoser(TestPlayer::secretaris(), 2)
            ->build();

        $this->checkHappy(
            HappyPath::given()
                ->when(new LogPlayedGame(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    $winningsFirstGame,
                    'Prop & cop',
                ))->then(
                    new PaidOut(
                        $this->scoreSheetIdentifier,
                        GameNumber::first(),
                        new MultipliedWinnings($winningsFirstGame, ScoreFactor::single()),
                        'Prop & cop',
                        ScoreLineNumber::first(),
                    ),
                    new ScoresLogged(
                        $this->scoreSheetIdentifier,
                        GameNumber::first(),
                        ScoreLine::fromMultipliedWinnings(
                            new MultipliedWinnings($winningsFirstGame, ScoreFactor::single()),
                            'Prop & cop',
                            ScoreLineNumber::first(),
                        ),
                        ScoreFactor::single(),
                    ),
                ),
        );
    }

    /** @test */
    public function itPaysOutTheSecondGame(): void
    {
        $winningsFirstGame = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 2)
            ->withWinner(TestPlayer::dbfc(), 2)
            ->withLoser(TestPlayer::dtl(), 2)
            ->withLoser(TestPlayer::secretaris(), 2)
            ->build();
        $winningsSecondGame = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 12)
            ->withLoser(TestPlayer::dbfc(), 4)
            ->withLoser(TestPlayer::dtl(), 4)
            ->withLoser(TestPlayer::secretaris(), 4)
            ->build();

        $this->checkHappy(
            HappyPath::given(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    new MultipliedWinnings($winningsFirstGame, ScoreFactor::single()),
                    'Prop & cop',
                    ScoreLineNumber::first()
                ),
                new ScoresLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($winningsFirstGame, ScoreFactor::single()),
                        'Prop & cop',
                        ScoreLineNumber::first()
                    ),
                    ScoreFactor::single()
                ))->when(new LogPlayedGame(
                $this->scoreSheetIdentifier,
                GameNumber::second(),
                $winningsSecondGame,
                'Alone',
            ))->then(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::fromInteger(2),
                    new MultipliedWinnings($winningsSecondGame, ScoreFactor::single()),
                    'Alone',
                    ScoreLineNumber::fromInteger(2)
                ),
                new ScoresLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::fromInteger(2),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($winningsFirstGame, ScoreFactor::single()),
                        'Prop & cop',
                        ScoreLineNumber::first()
                    )->getNext(
                        new MultipliedWinnings($winningsSecondGame, ScoreFactor::single()),
                        'Alone'
                    ),
                    ScoreFactor::single()
                ),
            ),
        );
    }

    /** @test */
    public function itIncreasesTheScoreFactor(): void
    {
        $this->checkHappy(
            HappyPath::given()
                ->when(new IncreaseScoreFactor($this->scoreSheetIdentifier, GameNumber::first()))
                ->then(new ScoreFactorIncreased(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreFactor::double(),
                )),
        );
    }

    /** @test */
    public function itMultipliesPlainWinningsWithScoreFactor(): void
    {
        $winningsFirstGame = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 2)
            ->withWinner(TestPlayer::dbfc(), 2)
            ->withLoser(TestPlayer::dtl(), 2)
            ->withLoser(TestPlayer::secretaris(), 2)
            ->build();

        $this->checkHappy(
            HappyPath::given(new ScoreFactorIncreased(
                $this->scoreSheetIdentifier,
                GameNumber::first(),
                ScoreFactor::double(),
            ))->when(new LogPlayedGame(
                $this->scoreSheetIdentifier,
                GameNumber::first(),
                $winningsFirstGame,
                'Prop & cop',
            ))->then(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    new MultipliedWinnings($winningsFirstGame, ScoreFactor::fromInteger(2)),
                    'Prop & cop',
                    ScoreLineNumber::first()
                ),
                new ScoresLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($winningsFirstGame, ScoreFactor::fromInteger(2)),
                        'Prop & cop',
                        ScoreLineNumber::first()
                    ),
                    ScoreFactor::fromInteger(2)
                ),
            ),
        );
    }

    /** @test */
    public function itDecreasesTheScoreFactor(): void
    {
        $this->checkHappy(
            HappyPath::given(new ScoreFactorIncreased(
                $this->scoreSheetIdentifier,
                GameNumber::first(),
                ScoreFactor::double(),
            ))->when(new DecreaseScoreFactor($this->scoreSheetIdentifier, GameNumber::first()))
                ->then(new ScoreFactorDecreased(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreFactor::single(),
                )),
        );
    }

    /** @test */
    public function itTakesIntoAccountDecreasedScoreFactor(): void
    {
        $winnings = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 2)
            ->withWinner(TestPlayer::dbfc(), 2)
            ->withLoser(TestPlayer::dtl(), 2)
            ->withLoser(TestPlayer::secretaris(), 2)
            ->build();

        $this->checkHappy(
            HappyPath::given(
                new ScoreFactorIncreased(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreFactor::double(),
                ),
                new ScoreFactorDecreased(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreFactor::single(),
                ),
            )->when(new LogPlayedGame(
                $this->scoreSheetIdentifier,
                GameNumber::first(),
                $winnings,
                'Prop & cop',
            ))->then(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    new MultipliedWinnings($winnings, ScoreFactor::single()),
                    'Prop & cop',
                    ScoreLineNumber::first()
                ),
                new ScoresLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($winnings, ScoreFactor::single()),
                        'Prop & cop',
                        ScoreLineNumber::first()
                    ),
                    ScoreFactor::single()
                ),
            ),
        );
    }

    /** @test */
    public function itLogsWithoutGame(): void
    {
        $winnings = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 3)
            ->withLoser(TestPlayer::dbfc(), 1)
            ->withLoser(TestPlayer::dtl(), 1)
            ->withLoser(TestPlayer::secretaris(), 1)
            ->build();

        $this->checkHappy(
            HappyPath::given()
                ->when(new LogNoGame(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    $winnings,
                    'compensation_passed_around',
                ))->then(
                    new PaidOut(
                        $this->scoreSheetIdentifier,
                        GameNumber::first(),
                        MultipliedWinnings::plain($winnings),
                        'compensation_passed_around',
                        ScoreLineNumber::first()
                    ),
                    new ScoresCorrected(
                        $this->scoreSheetIdentifier,
                        GameNumber::first(),
                        ScoreLine::fromPlainWinnings(
                            $winnings,
                            'compensation_passed_around',
                            ScoreLineNumber::first()
                        ),
                    ),
                ),
        );
    }

    /** @test */
    public function itTakesIntoAccountLogLinesWithoutGame(): void
    {
        $firstWinnings = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 3)
            ->withLoser(TestPlayer::dbfc(), 1)
            ->withLoser(TestPlayer::dtl(), 1)
            ->withLoser(TestPlayer::secretaris(), 1)
            ->build();

        $nextWinnings = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 12)
            ->withLoser(TestPlayer::dbfc(), 4)
            ->withLoser(TestPlayer::dtl(), 4)
            ->withLoser(TestPlayer::secretaris(), 4)
            ->build();

        $this->checkHappy(
            HappyPath::given(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    MultipliedWinnings::plain($firstWinnings),
                    'some_correction',
                    ScoreLineNumber::first()
                ),
                new ScoresCorrected(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromPlainWinnings(
                        $firstWinnings,
                        'some_correction',
                        ScoreLineNumber::first()
                    ),
                ),
            )->when(
                new LogPlayedGame(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    $nextWinnings,
                    'Alone',
                ),
            )->then(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    new MultipliedWinnings($nextWinnings, ScoreFactor::single()),
                    'Alone',
                    ScoreLineNumber::second(),
                ),
                new ScoresLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($firstWinnings, ScoreFactor::single()),
                        'some_correction',
                        ScoreLineNumber::first(),
                    )->getNext(
                        new MultipliedWinnings($nextWinnings, ScoreFactor::single()),
                        'Alone',
                    ),
                    ScoreFactor::single(),
                ),
            ),
        );
    }

    /** @test */
    public function itLogsUndo(): void
    {
        $winningsFirstGame = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 2)
            ->withWinner(TestPlayer::dbfc(), 2)
            ->withLoser(TestPlayer::dtl(), 2)
            ->withLoser(TestPlayer::secretaris(), 2)
            ->build();
        $winningsUndone = PlainWinningsBuilder::create()
            ->withLoser(TestPlayer::penningmeester(), 2)
            ->withLoser(TestPlayer::dbfc(), 2)
            ->withWinner(TestPlayer::dtl(), 2)
            ->withWinner(TestPlayer::secretaris(), 2)
            ->build();

        $this->checkHappy(
            HappyPath::given(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    new MultipliedWinnings($winningsFirstGame, ScoreFactor::single()),
                    'Prop & cop',
                    ScoreLineNumber::first()
                ),
                new ScoresLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($winningsFirstGame, ScoreFactor::single()),
                        'Prop & cop',
                        ScoreLineNumber::first()
                    ),
                    ScoreFactor::single()
                )
            )->when(
                new LogUndoneGame(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    $winningsUndone,
                    'undo',
                ),
            )->then(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    new MultipliedWinnings($winningsUndone, ScoreFactor::single()),
                    'undo',
                    ScoreLineNumber::second(),
                ),
                new UndoLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($winningsFirstGame, ScoreFactor::single()),
                        'Prop & cop',
                        ScoreLineNumber::first()
                    )->getNext(
                        new MultipliedWinnings($winningsUndone, ScoreFactor::single()),
                        'undo',
                    ),
                ),
            ),
        );
    }

    /** @test */
    public function itLogsAgainAfterUndo(): void
    {
        $winningsFirstTry = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 2)
            ->withWinner(TestPlayer::dbfc(), 2)
            ->withLoser(TestPlayer::dtl(), 2)
            ->withLoser(TestPlayer::secretaris(), 2)
            ->build();
        $winningsUndone = PlainWinningsBuilder::create()
            ->withLoser(TestPlayer::penningmeester(), 2)
            ->withLoser(TestPlayer::dbfc(), 2)
            ->withWinner(TestPlayer::dtl(), 2)
            ->withWinner(TestPlayer::secretaris(), 2)
            ->build();
        $winningsNextTry = PlainWinningsBuilder::create()
            ->withWinner(TestPlayer::penningmeester(), 3)
            ->withWinner(TestPlayer::dbfc(), 3)
            ->withLoser(TestPlayer::dtl(), 3)
            ->withLoser(TestPlayer::secretaris(), 3)
            ->build();

        $this->checkHappy(
            HappyPath::given(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    new MultipliedWinnings($winningsFirstTry, ScoreFactor::single()),
                    'Prop & cop',
                    ScoreLineNumber::first()
                ),
                new ScoresLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($winningsFirstTry, ScoreFactor::single()),
                        'Prop & cop',
                        ScoreLineNumber::first()
                    ),
                    ScoreFactor::single()
                ),
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    new MultipliedWinnings($winningsUndone, ScoreFactor::single()),
                    'undo',
                    ScoreLineNumber::second(),
                ),
                new UndoLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($winningsFirstTry, ScoreFactor::single()),
                        'Prop & cop',
                        ScoreLineNumber::first()
                    )->getNext(
                        new MultipliedWinnings($winningsUndone, ScoreFactor::single()),
                        'undo',
                    ),
                )
            )->when(
                new LogPlayedGame(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    $winningsNextTry,
                    'Prop & cop',
                ),
            )->then(
                new PaidOut(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    new MultipliedWinnings($winningsNextTry, ScoreFactor::single()),
                    'Prop & cop',
                    ScoreLineNumber::third()
                ),
                new ScoresLogged(
                    $this->scoreSheetIdentifier,
                    GameNumber::first(),
                    ScoreLine::fromMultipliedWinnings(
                        new MultipliedWinnings($winningsNextTry, ScoreFactor::single()),
                        'Prop & cop',
                        ScoreLineNumber::third(),
                    ),
                    ScoreFactor::single(),
                ),
            ),
        );
    }
}
