<?php

namespace App\Tests\unit\Domain\WriteModel\ScoreSheet;

use App\Domain\ScoreSheet\Score\PlainWinnings;
use App\Domain\ScoreSheet\ScoreSheetIdentifier;
use App\Domain\ScoreSheet\Tariffs\TariffsByTable;
use App\Domain\Table\Event\Game\Abundance;
use App\Domain\Table\Event\Game\Alone;
use App\Domain\Table\Event\Game\PropAndCop;
use App\Domain\Table\Event\Game\Solo;
use App\Domain\Table\Event\Game\Troel;
use App\Domain\Table\Event\PassedAround;
use App\Domain\Table\Event\Undo\SoloUndone;
use App\Domain\Table\FinalGames;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TroelAwareSinglePlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\TwoPlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\AbundanceSpecification;
use App\Domain\Table\Loggable\GameSpecification\AllSpecifiedGames;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\GameSpecification\PropAndCopSpecification;
use App\Domain\Table\Loggable\GameSpecification\SoloSpecification;
use App\Domain\Table\Loggable\GameSpecification\TroelSpecification;
use App\Domain\Table\Loggable\Outcome\AllOrNothing;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\TableIdentifier;
use App\Domain\TableConventions\TableTariffs\TraditionalTariffs;
use App\Domain\WriteModel\ProcessManager\ScoreSheetProcessManager;
use App\Domain\WriteModel\ScoreSheet\Command\IncreaseScoreFactor;
use App\Domain\WriteModel\ScoreSheet\Command\LogNoGame;
use App\Domain\WriteModel\ScoreSheet\Command\LogPlayedGame;
use App\Domain\WriteModel\ScoreSheet\Command\LogUndoneGame;
use App\Domain\WriteModel\Table\Undo\UndoEventConversion;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\unit\DummyCommandBusExpectingCommands;
use App\Tests\unit\TestRulesByConvention;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;

final class ScoreSheetProcessManagerTest extends TestCase
{
    private TableIdentifier $tableIdentifier;
    private FinalGames $finalGamesStub;
    private UndoEventConversion $undoEventConversion;
    private AllSpecifiedGames $testSpecifiedGames;
    private TariffsByTable $tableTariffsStub;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tableIdentifier = TableIdentifier::fromString('1030eecd-c64f-4cb3-a4cd-4399ce54ac10');

        /** @var FinalGames $finalGamesStub */
        $finalGamesStub = $this->createMock(FinalGames::class);
        $this->finalGamesStub = $finalGamesStub;

        $this->testSpecifiedGames = (new TestRulesByConvention())->getAllSpecifiedGames();
        $this->undoEventConversion = new UndoEventConversion(
            $this->testSpecifiedGames,
        );

        $this->tableTariffsStub = $this->createMock(TariffsByTable::class);
        $this->tableTariffsStub->method('getTariffsForTable')
            ->willReturn(new TraditionalTariffs());
    }

    /** @test */
    public function itIncreasesScoreFactorWhenPassedAround(): void
    {
        $event = new PassedAround(
            $this->tableIdentifier,
            GameNumber::first(),
            1,
            $this->getFourInvolvedPlayers()
        );

        $expectedCommand = new IncreaseScoreFactor(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first()
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    /**
     * See: https://www.rijkvanafdronk.be/spelregels/scorefactor/.
     *
     * @test
     */
    public function itIncreasesScoreFactorWhenPassedAroundTwice(): void
    {
        $event = new PassedAround(
            $this->tableIdentifier,
            GameNumber::first(),
            2,
            $this->getPlayersInvolved()
        );

        // passing around twice in first game increases score factor of
        // game 2.
        $expectedCommand = new IncreaseScoreFactor(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::fromInteger(2)
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/.
     *
     * @test
     */
    public function itCharges2FrancsForAJustOne(): void
    {
        $playersInvolved = $this->getPlayersInvolved();

        $event = new PropAndCop(
            $this->tableIdentifier,
            GameNumber::first(),
            new TwoPlayerAnnouncement(
                TestPlayer::ddb(),
                TestPlayer::secretaris()
            ),
            NumberOfTricks::fromNumber(8),
            $playersInvolved,
            new \DateTimeImmutable('2021-04-03'),
        );

        $expectedCommand = new LogPlayedGame(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::twoPlayersWin(
                PlayerIdentifiers::fromArray([TestPlayer::ddb(), TestPlayer::secretaris()]),
                PlayerIdentifiers::fromArray([TestPlayer::dtl(), TestPlayer::penningmeester()]),
                2,
            ),
            PropAndCopSpecification::NAME
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    private function getPlayersInvolved(): PlayersInvolved
    {
        return new PlayersInvolved(
            PlayerIdentifiers::fromArray([
                TestPlayer::dtl(),
                TestPlayer::penningmeester(),
                TestPlayer::ddb(),
                TestPlayer::secretaris(),
            ]),
            TestPlayer::dtl()
        );
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#vraag-en-mee.
     *
     * @test
     */
    public function itCharges4FrancsForPropAndCop2TricksShort(): void
    {
        $event = new PropAndCop(
            $this->tableIdentifier,
            GameNumber::first(),
            new TwoPlayerAnnouncement(
                TestPlayer::ddb(),
                TestPlayer::secretaris()
            ),
            NumberOfTricks::fromNumber(6),
            $this->getPlayersInvolved(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $expectedCommand = new LogPlayedGame(
            ScoreSheetIdentifier::forTable(
                $this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::twoPlayersPay(
                PlayerIdentifiers::fromArray([TestPlayer::ddb(), TestPlayer::secretaris()]),
                PlayerIdentifiers::fromArray([TestPlayer::dtl(), TestPlayer::penningmeester()]),
                4,
            ),
            PropAndCopSpecification::NAME
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#alleen-6-slagen.
     *
     * @test
     */
    public function itCharges3FrancsForAlone1TrickShort(): void
    {
        $event = new Alone(
            $this->tableIdentifier,
            GameNumber::first(),
            new SinglePlayerAnnouncement(TestPlayer::dtl()),
            NumberOfTricks::fromNumber(5),
            $this->getPlayersInvolved(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $expectedCommand = new LogPlayedGame(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::singlePlayerPays(
                TestPlayer::dtl(),
                PlayerIdentifiers::fromArray([TestPlayer::penningmeester(), TestPlayer::ddb(), TestPlayer::secretaris()]),
                3,
            ),
            AloneSpecification::NAME
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#abondance-9-10-11-12.
     *
     * @test
     */
    public function itCharges5FrancsForSuccesfulAbundance(): void
    {
        $event = new Abundance(
            $this->tableIdentifier,
            GameNumber::first(),
            new SinglePlayerAnnouncement(TestPlayer::dtl()),
            AllOrNothing::asSuccess(),
            $this->getPlayersInvolved(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $expectedCommand = new LogPlayedGame(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::singlePlayerWins(
                TestPlayer::dtl(),
                PlayerIdentifiers::fromArray([TestPlayer::penningmeester(), TestPlayer::ddb(), TestPlayer::secretaris()]),
                5,
            ),
            AbundanceSpecification::NAME
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#aboncance-9-10-11-12.
     *
     * @test
     */
    public function itCharges15FrancsForFailedAbundance(): void
    {
        $event = new Abundance(
            $this->tableIdentifier,
            GameNumber::first(),
            new SinglePlayerAnnouncement(TestPlayer::dtl()),
            AllOrNothing::asFailure(),
            $this->getPlayersInvolved(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $expectedCommand = new LogPlayedGame(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::singlePlayerPays(
                TestPlayer::dtl(),
                PlayerIdentifiers::fromArray([TestPlayer::penningmeester(), TestPlayer::ddb(), TestPlayer::secretaris()]),
                5,
            ),
            AbundanceSpecification::NAME
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    private function getFourInvolvedPlayers(): PlayersInvolved
    {
        return new PlayersInvolved(
            PlayerIdentifiers::fromArray([
                TestPlayer::dlb(),
                TestPlayer::dpb(),
                TestPlayer::dvl(),
                TestPlayer::dbfc(),
            ]),
            TestPlayer::dvl()
        );
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#ronde-pas.
     *
     * @test
     * @covers \App\Domain\WriteModel\ScoreSheet\ScoreSheetProcessManager::applyPassedAround
     */
    public function itInsertsACorrectDummyGameWhenPassingAroundForThe2ndTimeOnATableOf5(): void
    {
        $playersInvolved = $this->getPlayersInvolved();

        $event = new PassedAround(
            $this->tableIdentifier,
            GameNumber::second(),
            2,
            $playersInvolved
        );

        /** @var Command[] $expectedCommands */
        $expectedCommands = [
            // score factor will be increased for game 3
            new IncreaseScoreFactor(
                ScoreSheetIdentifier::forTable($this->tableIdentifier),
                GameNumber::fromInteger(3)
            ),
            // score line for game 2 will contain the 4 francs for the dealer
            new LogNoGame(
                ScoreSheetIdentifier::forTable($this->tableIdentifier),
                GameNumber::fromInteger(2),
                PlainWinnings::dealerWins(1, $event->playersInvolved),
                'compensation_passed_around'
            ),
        ];

        $processmanager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands($expectedCommands),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processmanager->__invoke($event);
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#vraag-en-mee.
     *
     * @test
     * @covers \App\Domain\WriteModel\ScoreSheet\ScoreSheetProcessManager::applyPropAndCop
     */
    public function itPaysOutPropAndCopUnderTheTable(): void
    {
        $event = new PropAndCop(
            $this->tableIdentifier,
            GameNumber::first(),
            new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::penningmeester()),
            NumberOfTricks::all(),
            $this->getPlayersInvolved(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $expectedCommand = new LogPlayedGame(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::twoPlayersWin(
                PlayerIdentifiers::fromArray([TestPlayer::dtl(), TestPlayer::penningmeester()]),
                PlayerIdentifiers::fromArray([TestPlayer::ddb(), TestPlayer::secretaris()]),
                14,
            ),
            PropAndCopSpecification::NAME
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    /**
     * Test troel under the table.
     *
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#vraag-en-mee
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#troel
     *
     * @test
     * @covers \App\Domain\WriteModel\ScoreSheet\ScoreSheetProcessManager::applyTroel
     */
    public function itPaysOutTroelUnderTheTable(): void
    {
        $event = new Troel(
            $this->tableIdentifier,
            GameNumber::first(),
            new TwoPlayerAnnouncement(TestPlayer::dtl(), TestPlayer::penningmeester()),
            NumberOfTricks::all(),
            $this->getPlayersInvolved(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $expectedCommand = new LogPlayedGame(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::twoPlayersWin(
                PlayerIdentifiers::fromArray([TestPlayer::dtl(), TestPlayer::penningmeester()]),
                PlayerIdentifiers::fromArray([TestPlayer::secretaris(), TestPlayer::ddb()]),
                28,
            ),
            TroelSpecification::NAME
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    /**
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#solo.
     *
     * @test
     * @covers \App\Domain\WriteModel\ScoreSheet\ScoreSheetProcessManager::applySolo
     */
    public function itPaysOutSolo(): void
    {
        $event = new Solo(
            $this->tableIdentifier,
            GameNumber::first(),
            new TroelAwareSinglePlayerAnnouncement(TestPlayer::dtl(), false),
            AllOrNothing::asSuccess(),
            $this->getPlayersInvolved(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $expectedCommand = new LogPlayedGame(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::singlePlayerWins(
                TestPlayer::dtl(),
                PlayerIdentifiers::fromArray([TestPlayer::penningmeester(), TestPlayer::ddb(), TestPlayer::secretaris()]),
                75,
            ),
            SoloSpecification::NAME
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    /**
     * Test solo on top of troel.
     *
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#troel
     * See: https://www.rijkvanafdronk.be/puntentelling/puntentelling/#solo
     *
     * @test
     * @covers \App\Domain\WriteModel\ScoreSheet\ScoreSheetProcessManager::applySolo
     */
    public function itPaysOutSoloOnTopOfTroel(): void
    {
        $event = new Solo(
            $this->tableIdentifier,
            GameNumber::first(),
            new TroelAwareSinglePlayerAnnouncement(TestPlayer::dtl(), true),
            AllOrNothing::asSuccess(),
            $this->getPlayersInvolved(),
            new \DateTimeImmutable('2021-04-03'),
        );

        $expectedCommand = new LogPlayedGame(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::singlePlayerWins(
                TestPlayer::dtl(),
                PlayerIdentifiers::fromArray([TestPlayer::penningmeester(), TestPlayer::ddb(), TestPlayer::secretaris()]),
                150,
            ),
            SoloSpecification::NAME
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }

    /** @test */
    public function itLogsUndoneGame(): void
    {
        $event = new SoloUndone(
            $this->tableIdentifier,
            GameNumber::first(),
            new TroelAwareSinglePlayerAnnouncement(TestPlayer::dtl(), true),
            AllOrNothing::asSuccess(),
            $this->getPlayersInvolved(),
            0
        );

        $expectedCommand = new LogUndoneGame(
            ScoreSheetIdentifier::forTable($this->tableIdentifier),
            GameNumber::first(),
            PlainWinnings::singlePlayerPays(
                TestPlayer::dtl(),
                PlayerIdentifiers::fromArray([TestPlayer::penningmeester(), TestPlayer::ddb(), TestPlayer::secretaris()]),
                150,
            ),
            AllSpecifiedGames::getUndoGameName(SoloSpecification::NAME),
        );

        $processManager = new ScoreSheetProcessManager(
            new DummyCommandBusExpectingCommands([$expectedCommand]),
            $this->finalGamesStub,
            $this->undoEventConversion,
            $this->testSpecifiedGames,
            $this->tableTariffsStub,
        );

        $processManager->__invoke($event);
    }
}
