<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\WriteModel\Table;

use App\Domain\Table\Command\AnnounceDealer;
use App\Domain\Table\Command\AnnounceFinalRound;
use App\Domain\Table\Command\ClearTable;
use App\Domain\Table\Command\ConfigureTable;
use App\Domain\Table\Command\JoinPlayer;
use App\Domain\Table\Command\KickPlayer;
use App\Domain\Table\Command\LogGame;
use App\Domain\Table\Command\PassAround;
use App\Domain\Table\Command\ProposeAndNobodyGoesAlong;
use App\Domain\Table\Command\RejoinKnownPlayer;
use App\Domain\Table\Command\ReorderPlayers;
use App\Domain\Table\Command\StarPlayer;
use App\Domain\Table\Command\Undo;
use App\Domain\Table\Event\AllUndone;
use App\Domain\Table\Event\DealerAnnounced;
use App\Domain\Table\Event\DealerFailed;
use App\Domain\Table\Event\DealerLeft;
use App\Domain\Table\Event\FinalRoundAnnounced;
use App\Domain\Table\Event\Game\Alone;
use App\Domain\Table\Event\Game\Alone5;
use App\Domain\Table\Event\Game\Misere;
use App\Domain\Table\Event\PassedAround;
use App\Domain\Table\Event\PlayerJoined;
use App\Domain\Table\Event\PlayerLeft;
use App\Domain\Table\Event\PlayersReordered;
use App\Domain\Table\Event\PlayerStarred;
use App\Domain\Table\Event\ProposedWithNobodyAlong;
use App\Domain\Table\Event\TableCleared;
use App\Domain\Table\Event\TableConfigured;
use App\Domain\Table\Event\TableStartedPlaying;
use App\Domain\Table\Event\Undo\AloneUndone;
use App\Domain\Table\Event\Undo\FinalRoundAnnouncementUndone;
use App\Domain\Table\Event\Undo\PassAroundUndone;
use App\Domain\Table\Event\Undo\PlayerStarUndone;
use App\Domain\Table\Event\Undo\PropositionWithNobodyAlongUndone;
use App\Domain\Table\Exception\DuplicatePlayer;
use App\Domain\Table\Exception\IncorrectGameNumber;
use App\Domain\Table\Exception\ItTakesFourToPlay;
use App\Domain\Table\Exception\SeatAlreadyTaken;
use App\Domain\Table\Exception\TableFull;
use App\Domain\Table\Exception\UnknownPlayer;
use App\Domain\Table\Exception\UnsupportedByRules;
use App\Domain\Table\GameNumber;
use App\Domain\Table\Loggable\Announcement\AnnouncementValidator;
use App\Domain\Table\Loggable\Announcement\GameAnnouncement;
use App\Domain\Table\Loggable\Announcement\MultiPlayerAnnouncement;
use App\Domain\Table\Loggable\Announcement\SinglePlayerAnnouncement;
use App\Domain\Table\Loggable\GameSpecification\Alone5Specification;
use App\Domain\Table\Loggable\GameSpecification\AloneSpecification;
use App\Domain\Table\Loggable\Outcome\NumberOfTricks;
use App\Domain\Table\Loggable\Outcome\Winners;
use App\Domain\Table\Player\OrderedPlayers;
use App\Domain\Table\Player\PlayerIdentifier;
use App\Domain\Table\Player\PlayerIdentifiers;
use App\Domain\Table\Player\PlayersInvolved;
use App\Domain\Table\Player\SeatedPlayers;
use App\Domain\Table\Rules\Whist;
use App\Domain\Table\Seat;
use App\Domain\Table\TableIdentifier;
use App\Domain\WriteModel\Table\Aggregate\TableDecider;
use App\Domain\WriteModel\Table\Aggregate\TableInternalState;
use App\Domain\WriteModel\Table\Exception\NotAllowedInFinalRound;
use App\Domain\WriteModel\Table\Exception\TableBusy;
use App\Domain\WriteModel\Table\Undo\UndoEventConversion;
use App\TestData\TestDataBuilders\TestPlayer;
use App\Tests\unit\TestRulesByConvention;
use RvaVzw\KrakBoem\Cqrs\Decider\Decider;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\TestHelper\DeciderTestCase;
use RvaVzw\KrakBoem\TestHelper\HappyPath;
use RvaVzw\KrakBoem\TestHelper\UnhappyPath;

/**
 * @extends DeciderTestCase<TableInternalState>
 */
final class TableDeciderTest extends DeciderTestCase
{
    private TableIdentifier $tableIdentifier;
    private AnnouncementValidator $announcementValidator;
    private TestRulesByConvention $gamesByRuleSet;

    /**
     * @return array<non-empty-string, PlayerIdentifier>
     */
    private function getPlayersArray(int $numberOfPlayers): array
    {
        $players = [
            'secr' => TestPlayer::secretaris(),
            'dtl' => TestPlayer::dtl(),
            'ddb' => TestPlayer::ddb(),
            'penn' => TestPlayer::penningmeester(),
            'dlb' => TestPlayer::dlb(),
            'dvl' => TestPlayer::dvl(),
        ];

        if ($numberOfPlayers > count($players)) {
            throw new \InvalidArgumentException("Not enough test players. $numberOfPlayers requested, got ".count($players), );
        }

        return array_slice($players, 0, $numberOfPlayers);
    }

    protected function setUp(): void
    {
        $this->tableIdentifier = TableIdentifier::fromString('f9af0a74-cd16-4e6a-b1ba-a6f909ebfb6c');
        $this->announcementValidator = new class() implements AnnouncementValidator {
            public function isValidAnnouncement(GameAnnouncement $announcement, PlayersInvolved $playersInvolved): bool
            {
                return true;
            }
        };
        $this->gamesByRuleSet = new TestRulesByConvention();
    }

    protected function getDecider(): Decider
    {
        return new TableDecider(
            $this->announcementValidator,
            $this->gamesByRuleSet,
            new UndoEventConversion($this->gamesByRuleSet->getAllSpecifiedGames()),
        );
    }

    /** @test */
    public function itClearsATable(): void
    {
        $this->checkHappy(
            HappyPath::given()
                ->when(new ClearTable($this->tableIdentifier))
                ->then(
                    new TableCleared($this->tableIdentifier),
                ),
        );
    }

    /** @test */
    public function itJoinsAPlayer(): void
    {
        $this->checkHappy(
            HappyPath::given(
                new TableCleared($this->tableIdentifier),
            )->when(new JoinPlayer(
                $this->tableIdentifier,
                TestPlayer::secretaris(),
                GameNumber::first(),
                'sec',
                Seat::three(),
            ))->then(
                new PlayerJoined(
                    $this->tableIdentifier,
                    TestPlayer::secretaris(),
                    GameNumber::first(),
                    Seat::three(),
                    'sec',
                ),
            ),
        );
    }

    /** @test */
    public function itDoesNotAllowTwoPlayersOnTheSameSeat(): void
    {
        $this->checkUnhappy(
            UnhappyPath::given(
                new TableCleared($this->tableIdentifier),
                new PlayerJoined($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), Seat::three(), 'sec'),
            )->when(new JoinPlayer(
                $this->tableIdentifier,
                TestPlayer::penningmeester(),
                GameNumber::first(),
                'pen',
                Seat::three(),
            ))->then(SeatAlreadyTaken::class),
        );
    }

    /** @test */
    public function itDoesNotAllowTheSamePlayerTwiceAtTheSameTable(): void
    {
        $this->checkUnhappy(
            UnhappyPath::given(
                new TableCleared($this->tableIdentifier),
                new PlayerJoined($this->tableIdentifier, TestPlayer::dlb(), GameNumber::first(), Seat::three(), 'dlb'),
            )->when(new JoinPlayer(
                $this->tableIdentifier,
                TestPlayer::dlb(),
                GameNumber::first(),
                'dlb',
                Seat::zero(),
            ))->then(DuplicatePlayer::class),
        );
    }

    /** @test */
    public function itKicksAPlayer(): void
    {
        $this->checkHappy(
            HappyPath::given(
                new TableCleared($this->tableIdentifier),
                new PlayerJoined($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first(), Seat::three(), 'dbfc'),
            )->when(new KickPlayer(
                $this->tableIdentifier,
                TestPlayer::dbfc(),
                GameNumber::first(),
            ))->then(
                new PlayerLeft($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first()),
            )
        );
    }

    /** @test */
    public function itDoesNotKickTheSamePlayerTwice(): void
    {
        $this->checkUnhappy(
            UnhappyPath::given(
                new TableCleared($this->tableIdentifier),
                new PlayerJoined($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first(), Seat::three(), 'dbfc'),
                new PlayerLeft($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first()),
            )->when(new KickPlayer(
                $this->tableIdentifier,
                TestPlayer::dbfc(),
                GameNumber::first(),
            ))->then(UnknownPlayer::class),
        );
    }

    /** @test */
    public function itAnnouncesTheDealer(): void
    {
        $this->checkHappy(
            HappyPath::given(
                new TableCleared($this->tableIdentifier),
                new PlayerJoined($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first(), Seat::three(), 'dbfc'),
                new PlayerJoined($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), Seat::zero(), 'penn'),
                new PlayerJoined($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first(), Seat::one(), 'secr'),
                new PlayerJoined($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first(), Seat::two(), 'dtl'),
            )->when(new AnnounceDealer(
                $this->tableIdentifier,
                TestPlayer::dbfc(),
                GameNumber::first(),
            ))->then(new DealerAnnounced(
                $this->tableIdentifier,
                TestPlayer::dbfc(),
                GameNumber::first(),
            )),
        );
    }

    /** @test */
    public function itDoesNotAnnounceDealerWhenTheresNotEnoughPlayers(): void
    {
        $this->checkUnhappy(
            UnhappyPath::given(
                new TableCleared($this->tableIdentifier),
                new PlayerJoined($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first(), Seat::zero(), 'penn'),
            )->when(new AnnounceDealer(
                $this->tableIdentifier,
                TestPlayer::penningmeester(),
                GameNumber::first(),
            ))->then(ItTakesFourToPlay::class),
        );
    }

    /** @test */
    public function itChangesTheDealerAfterAnnouncingAnIncorrectDealer(): void
    {
        $this->checkHappy(
            HappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->when(
                new AnnounceDealer(
                    $this->tableIdentifier,
                    TestPlayer::dtl(),
                    GameNumber::first(),
                ),
            )->then(
                new DealerAnnounced(
                    $this->tableIdentifier,
                    TestPlayer::dtl(),
                    GameNumber::first(),
                ),
            ),
        );
    }

    /** @test */
    public function itEnforcesTheDealerToBePresent(): void
    {
        $this->checkUnhappy(
            UnhappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->when(
                new AnnounceDealer(
                    $this->tableIdentifier,
                    TestPlayer::dlb(), // DLB is not present on the test table with 4 players
                    GameNumber::first(),
                ),
            )->then(UnknownPlayer::class),
        );
    }

    /** @test */
    public function itDoesNotAnnounceTheSameDealerTwice(): void
    {
        $this->checkHappy(
            HappyPath::given(
            // Penningmeester is the dealer on this table:
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->when(new AnnounceDealer(
                $this->tableIdentifier,
                TestPlayer::penningmeester(),
                GameNumber::first(),
            ))->then(), // nothing has happened
        );
    }

    /** @test */
    public function itPassesAround(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable();
        $this->checkHappy(
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->when(new PassAround($this->tableIdentifier, GameNumber::first()))
                ->then(
                    new TableStartedPlaying($this->tableIdentifier),
                    new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers),
                ),
        );
    }

    /** @test */
    public function itPassesAroundTwice(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable();
        $this->checkHappy(
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->andGiven(
                    new TableStartedPlaying($this->tableIdentifier),
                    new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers),
                )
                ->when(new PassAround($this->tableIdentifier, GameNumber::first()))
                ->then(new PassedAround($this->tableIdentifier, GameNumber::first(), 2, $involvedPlayers)),
        );
    }

    /**
     * See https://www.rijkvanafdronk.be/spelregels/delen/.
     *
     * @test
     */
    public function itChangesDealerAfterThreeTimesPassingAround(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::dtl());

        $this->checkHappy(
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->andGiven(
                    new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
                    new TableStartedPlaying(
                        $this->tableIdentifier,
                    ),
                    new PassedAround(
                        $this->tableIdentifier,
                        GameNumber::first(),
                        1,
                        $involvedPlayers,
                    ),
                    new PassedAround(
                        $this->tableIdentifier,
                        GameNumber::first(),
                        2,
                        $involvedPlayers,
                    ),
                )->when(new PassAround($this->tableIdentifier, GameNumber::first()))
                ->then(
                    new PassedAround($this->tableIdentifier, GameNumber::first(), 3, $involvedPlayers),
                    new DealerFailed($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl()),
                    new DealerAnnounced($this->tableIdentifier, TestPlayer::ddb(), GameNumber::first()),
                ),
        );
    }

    /** @test */
    public function itDoesNotPassAroundWhenThereIsLessThanFourPlayers(): void
    {
        $this->checkUnhappy(
            UnhappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(3))
                ->when(new PassAround($this->tableIdentifier, GameNumber::first()))
                ->then(ItTakesFourToPlay::class),
        );
    }

    /** @test */
    public function itDoesNotPassAroundWhenWrongGameNumberGiven(): void
    {
        $this->checkUnhappy(UnhappyPath::given(
            ...$this->testTableCreatedWithNumberOfPlayers(4)
        )->when(new PassAround($this->tableIdentifier, GameNumber::second()))
            ->then(IncorrectGameNumber::class));
    }

    /**
     * See: https://www.rijkvanafdronk.be/spelregels/scorefactor/.
     *
     * @test
     */
    public function itProposesWithNobodyAlong(): void
    {
        $this->checkHappy(
            HappyPath::given(
            // secr, dtl, ddb, penn (dealer)
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->when(new ProposeAndNobodyGoesAlong($this->tableIdentifier, GameNumber::first()))
                ->then(
                    new TableStartedPlaying($this->tableIdentifier),
                    new ProposedWithNobodyAlong($this->tableIdentifier, GameNumber::first()),
                    // Game number is not increased, see
                    // - https://gitlab.com/rva-vzw/dikdikdik/issues/36
                    // - https://www.rijkvanafdronk.be/spelregels/scorefactor/
                    new DealerAnnounced($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first()),
                ),
        );
    }

    /**
     * See https://www.rijkvanafdronk.be/spelregels/scorefactor/.
     *
     * @test
     */
    public function itResetsPassAroundCountAfterProposingWithNobodyAlong(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::penningmeester());
        $this->checkHappy(
            HappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->andGiven(
                new DealerAnnounced($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first()),
                new TableStartedPlaying($this->tableIdentifier),
                new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers),
                new ProposedWithNobodyAlong($this->tableIdentifier, GameNumber::first())
            )->when(new PassAround($this->tableIdentifier, GameNumber::first()))
                // Passed around count should be 1 again:
                ->then(new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers)),
        );
    }

    /** @test */
    public function itLogsAlone(): void
    {
        $gameAnnouncement = new SinglePlayerAnnouncement(TestPlayer::penningmeester());
        $gameOutcome = NumberOfTricks::fromNumber(5);

        $this->checkHappy(
            HappyPath::given(
            // players are: sec, dtl, ddb, penn
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->andGiven(
                new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first())
            )->when(new LogGame(
                $this->tableIdentifier,
                GameNumber::first(),
                new AloneSpecification(),
                $gameAnnouncement,
                $gameOutcome,
                new \DateTimeImmutable('2021-04-03'),
            ))->then(
                new TableStartedPlaying($this->tableIdentifier),
                new Alone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    $gameAnnouncement,
                    $gameOutcome,
                    $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::dtl()),
                    new \DateTimeImmutable('2021-04-03'),
                ),
                new DealerAnnounced(
                    $this->tableIdentifier,
                    TestPlayer::ddb(),
                    GameNumber::second(),
                ),
            )
        );
    }

    /**
     * See https://www.rijkvanafdronk.be/spelregels/aantal-spelers/.
     *
     * @test
     * */
    public function itDoesNotAllowNewPlayerWhenTableFull(): void
    {
        $this->checkUnhappy(
            UnhappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(6))
                ->when(new JoinPlayer(
                    $this->tableIdentifier,
                    TestPlayer::dbfc(),
                    GameNumber::first(),
                    'dbfc',
                    null,
                ))->then(TableFull::class),
        );
    }

    /** @test */
    public function itIgnoresAPlayerJoiningTwice(): void
    {
        $this->checkHappy(
        // secretaris, dtl, ddb, penningmeester
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->when(new JoinPlayer(
                    $this->tableIdentifier,
                    TestPlayer::penningmeester(),
                    GameNumber::first(),
                    'pen',
                    null,
                ))->then(),
        );
    }

    /**
     * See https://gitlab.com/rva-vzw/dikdikdik/issues/73.
     *
     * @test
     */
    public function itResetsPassAroundCountWhenAnActualGameIsLogged(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::penningmeester());
        $this->checkHappy(
            HappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->andGiven(
                new DealerAnnounced($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first()),
                new TableStartedPlaying($this->tableIdentifier),
                new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers),
                new PassedAround($this->tableIdentifier, GameNumber::first(), 2, $involvedPlayers),
                new Misere(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    MultiPlayerAnnouncement::fromSingle(TestPlayer::dtl()),
                    Winners::none(),
                    $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::dtl()),
                    new \DateTimeImmutable('2021-04-03'),
                ),
                new DealerAnnounced(
                    $this->tableIdentifier,
                    TestPlayer::ddb(),
                    GameNumber::second(),
                ),
            )->when(new PassAround($this->tableIdentifier, GameNumber::second()))
                // Passed around count should be 1 again:
                ->then(new PassedAround(
                    $this->tableIdentifier,
                    GameNumber::second(),
                    1,
                    $involvedPlayers->withDealer(TestPlayer::ddb()),
                )),
        );
    }

    /**
     * See https://gitlab.com/rva-vzw/dikdikdik/-/issues/46.
     *
     * @test
     */
    public function itKicksTheDealerOnAFourPlayerTable(): void
    {
        $this->checkHappy(
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->andGiven(new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()))
                ->when(new KickPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()))
                ->then(
                    new DealerLeft($this->tableIdentifier, GameNumber::first()),
                    new PlayerLeft($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
                ),
        );
    }

    /**
     * See https://gitlab.com/rva-vzw/dikdikdik/-/issues/46.
     *
     * @test
     */
    public function itKicksTheDealerOnAFivePlayerTable(): void
    {
        $this->checkHappy(
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(5))
                ->andGiven(new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()))
                ->when(new KickPlayer($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()))
                ->then(
                    new DealerAnnounced($this->tableIdentifier, TestPlayer::ddb(), GameNumber::first()),
                    new PlayerLeft($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
                ),
        );
    }

    /** @test */
    public function itAnnouncesANewDealerNotAtFirstGameWhenThePreviousOneLeft(): void
    {
        $gameAnnouncement = new SinglePlayerAnnouncement(TestPlayer::penningmeester());
        $gameOutcome = NumberOfTricks::fromNumber(5);

        $this->checkHappy(
            HappyPath::given(
            // players are: sec, dtl, ddb, penn
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->andGiven(
                new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
                new TableStartedPlaying($this->tableIdentifier),
                new Alone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    $gameAnnouncement,
                    $gameOutcome,
                    $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::dtl()),
                    new \DateTimeImmutable('2021-04-03'),
                ),
                new DealerAnnounced(
                    $this->tableIdentifier,
                    TestPlayer::ddb(),
                    GameNumber::second(),
                ),
                new DealerLeft($this->tableIdentifier, GameNumber::second()),
                new PlayerLeft($this->tableIdentifier, TestPlayer::ddb(), GameNumber::second()),
                new PlayerJoined($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::second(), Seat::two(), 'dbfc'),
            )->when(new AnnounceDealer($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::second()))
                ->then(
                    new DealerAnnounced(
                        $this->tableIdentifier,
                        TestPlayer::dbfc(),
                        GameNumber::second(),
                    ),
                ),
        );
    }

    /** @test */
    public function itStarsAPlayer(): void
    {
        $this->checkHappy(
            HappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->when(
                new StarPlayer($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl()),
            )->then(
                new TableStartedPlaying($this->tableIdentifier),
                new PlayerStarred($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl(), 1),
            ),
        );
    }

    /** @test */
    public function itStarsAPlayerTwice(): void
    {
        $this->checkHappy(
            HappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->andGiven(
                new TableStartedPlaying($this->tableIdentifier),
                new PlayerStarred($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl(), 1),
            )->when(
                new StarPlayer($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl()),
            )->then(
                new PlayerStarred($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl(), 2),
            ),
        );
    }

    /** @test */
    public function itOnlyUndoesForCorrectGameNumber(): void
    {
        $this->checkUnhappy(
            UnhappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->when(
                new Undo($this->tableIdentifier, GameNumber::second()),
            )->then(IncorrectGameNumber::class),
        );
    }

    /** @test */
    public function itReannouncesDealerAfterUndoingThirdPassAround(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable();

        $this->checkHappy(
        // secr, dtl, ddb, penn; penn is dealer
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->andGiven(
                    new TableStartedPlaying($this->tableIdentifier),
                    new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers),
                    new PassedAround($this->tableIdentifier, GameNumber::first(), 2, $involvedPlayers),
                    new PassedAround($this->tableIdentifier, GameNumber::first(), 3, $involvedPlayers),
                    new DealerFailed($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl()),
                    new DealerAnnounced($this->tableIdentifier, TestPlayer::ddb(), GameNumber::first()),
                )->when(new Undo($this->tableIdentifier, GameNumber::first()))
                ->then(
                    new PassAroundUndone($this->tableIdentifier, GameNumber::first(), $involvedPlayers, 3),
                    new DealerAnnounced($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first()),
                ),
        );
    }

    /** @test */
    public function itUndoesAlone(): void
    {
        $gameAnnouncement = new SinglePlayerAnnouncement(TestPlayer::penningmeester());
        $gameOutcome = NumberOfTricks::fromNumber(5);
        $playersInvolved = $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::dtl());

        $this->checkHappy(
            HappyPath::given(
            // players are: sec, dtl, ddb, penn;
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->andGiven(
                new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
                new TableStartedPlaying($this->tableIdentifier),
                new Alone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    $gameAnnouncement,
                    $gameOutcome,
                    $playersInvolved,
                    new \DateTimeImmutable('2021-04-03'),
                ),
                new DealerAnnounced(
                    $this->tableIdentifier,
                    TestPlayer::ddb(),
                    GameNumber::second(),
                ),
            )->when(
            // Note that the game number for the undo command, is not the game number
            // of the thing that needs to be undone. It is the current game number of the
            // game sequence at the table.
                new Undo($this->tableIdentifier, GameNumber::second())
            )->then(
                new AloneUndone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    $gameAnnouncement,
                    $gameOutcome,
                    $playersInvolved,
                    0,
                ),
                new AllUndone($this->tableIdentifier),
                new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
            ),
        );
    }

    /** @test */
    public function itLogsAnotherGameAfterUndo(): void
    {
        $gameAnnouncement = new SinglePlayerAnnouncement(TestPlayer::penningmeester());
        $gameOutcome = NumberOfTricks::fromNumber(5);
        $playersInvolved = $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::dtl());

        $this->checkHappy(
            HappyPath::given(
            // players are: sec, dtl, ddb, penn;
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->andGiven(
                new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
                new TableStartedPlaying($this->tableIdentifier),
                new Alone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    $gameAnnouncement,
                    $gameOutcome,
                    $playersInvolved,
                    new \DateTimeImmutable('2021-04-03'),
                ),
                new DealerAnnounced(
                    $this->tableIdentifier,
                    TestPlayer::ddb(),
                    GameNumber::second(),
                ),
                new AloneUndone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    $gameAnnouncement,
                    $gameOutcome,
                    $playersInvolved,
                    0,
                ),
                new AllUndone($this->tableIdentifier),
                new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
            )->when(new LogGame(
                $this->tableIdentifier,
                GameNumber::first(), // new logged game for game number 1
                new AloneSpecification(),
                $gameAnnouncement,
                $gameOutcome,
                new \DateTimeImmutable('2022-01-29'),
            ))->then(
            // Back in business after everything was undone
                new TableStartedPlaying($this->tableIdentifier),
                new Alone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    $gameAnnouncement,
                    $gameOutcome,
                    $playersInvolved,
                    new \DateTimeImmutable('2022-01-29')
                ),
                new DealerAnnounced(
                    $this->tableIdentifier,
                    TestPlayer::ddb(),
                    GameNumber::second(),
                ),
            ),
        );
    }

    /** @test */
    public function itUndoesProposeWithNobodyAlong(): void
    {
        $this->checkHappy(
            HappyPath::given(
            // secr, dtl, ddb, penn (dealer)
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->andGiven(
                new TableStartedPlaying($this->tableIdentifier),
                new ProposedWithNobodyAlong($this->tableIdentifier, GameNumber::first()),
                new DealerAnnounced($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first()),
            )->when(
            // Since game number did not increase after proposing with nobody along,
            // we still have to pass GameNumber::first() to the undo command.
                new Undo($this->tableIdentifier, GameNumber::first()),
            )->then(
                new PropositionWithNobodyAlongUndone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    TestPlayer::penningmeester(),
                    0
                ),
                new AllUndone($this->tableIdentifier),
                // dealer should be restored as well:
                new DealerAnnounced(
                    $this->tableIdentifier,
                    TestPlayer::penningmeester(),
                    GameNumber::first(),
                ),
            ),
        );
    }

    /** @test */
    public function itUndoesPropositionWithNobodyAlongAfterPassingAround(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::penningmeester());
        $this->checkHappy(
            HappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->andGiven(
                new DealerAnnounced($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first()),
                new TableStartedPlaying($this->tableIdentifier),
                new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers),
                new ProposedWithNobodyAlong($this->tableIdentifier, GameNumber::first()),
                new DealerAnnounced($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first())
            )->when(new Undo($this->tableIdentifier, GameNumber::first()))
                ->then(
                    new PropositionWithNobodyAlongUndone(
                        $this->tableIdentifier,
                        GameNumber::first(),
                        TestPlayer::penningmeester(),
                        1,
                    ),
                    new DealerAnnounced($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first()),
                ),
        );
    }

    /** @test */
    public function itRestoresPassedAroundCountWhenUndoingPropositionWithNobodyAlong(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::penningmeester());
        $this->checkHappy(
            HappyPath::given(
            // secr, dtl, ddb, penn; penn deals
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->andGiven(
                new DealerAnnounced($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first()),
                new TableStartedPlaying($this->tableIdentifier),
                new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers),
                new ProposedWithNobodyAlong($this->tableIdentifier, GameNumber::first()),
                new DealerAnnounced($this->tableIdentifier, TestPlayer::secretaris(), GameNumber::first()),
                new PropositionWithNobodyAlongUndone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    TestPlayer::penningmeester(),
                    1,
                ),
                new DealerAnnounced($this->tableIdentifier, TestPlayer::penningmeester(), GameNumber::first()),
            )->when(new PassAround($this->tableIdentifier, GameNumber::first()))
                ->then(
                    new PassedAround(
                        $this->tableIdentifier,
                        GameNumber::first(),
                        2,
                        $involvedPlayers,
                    ),
                ),
        );
    }

    /** @test */
    public function itUndoesAPlayersStar(): void
    {
        $this->checkHappy(
            HappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->andGiven(
                new TableStartedPlaying($this->tableIdentifier),
                new PlayerStarred($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl(), 1),
            )->when(new Undo($this->tableIdentifier, GameNumber::first()))
                ->then(
                    new PlayerStarUndone(
                        $this->tableIdentifier,
                        GameNumber::first(),
                        TestPlayer::dtl(),
                        1,
                        TestPlayer::penningmeester(),
                        0,
                    ),
                    new AllUndone($this->tableIdentifier),
                ),
        );
    }

    /** @test */
    public function itRestarsAPlayerAfterUndo(): void
    {
        $this->checkHappy(
            HappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->andGiven(
                new TableStartedPlaying($this->tableIdentifier),
                new PlayerStarred($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl(), 1),
                new PlayerStarUndone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    TestPlayer::dtl(),
                    1,
                    TestPlayer::penningmeester(),
                    0,
                ),
                new AllUndone($this->tableIdentifier),
            )->when(
                new StarPlayer($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl()),
            )->then(
                new TableStartedPlaying($this->tableIdentifier),
                new PlayerStarred($this->tableIdentifier, GameNumber::first(), TestPlayer::dtl(), 1),
            ),
        );
    }

    /** @test */
    public function itReordersPlayersOnPlayingTable(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable();
        $this->checkHappy(
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->andGiven(
                    new TableStartedPlaying($this->tableIdentifier),
                    new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers),
                )->when(new ReorderPlayers(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    OrderedPlayers::fromArray([
                        TestPlayer::dtl(),
                        TestPlayer::secretaris(),
                        TestPlayer::ddb(),
                        TestPlayer::penningmeester(),
                    ]),
                ))->then(
                    new PlayersReordered(
                        $this->tableIdentifier,
                        GameNumber::first(),
                        SeatedPlayers::fromArray([
                            TestPlayer::dtl(),
                            TestPlayer::secretaris(),
                            TestPlayer::ddb(),
                            TestPlayer::penningmeester(),
                        ]),
                    ),
                ),
        );
    }

    /** @test */
    public function itPutsTableOnHoldWhenPlayersReordered(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable();
        $this->checkHappy(
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->andGiven(
                    new TableStartedPlaying($this->tableIdentifier),
                    new PassedAround($this->tableIdentifier, GameNumber::first(), 1, $involvedPlayers),
                    new PlayersReordered(
                        $this->tableIdentifier,
                        GameNumber::first(),
                        SeatedPlayers::fromArray([
                            TestPlayer::dtl(),
                            TestPlayer::secretaris(),
                            TestPlayer::ddb(),
                            TestPlayer::penningmeester(),
                        ]),
                    ),
                )->when(new ProposeAndNobodyGoesAlong(
                    $this->tableIdentifier,
                    GameNumber::first(),
                ))->then(
                    // The 'started playing' event shows that the table was put on hold before
                    new TableStartedPlaying($this->tableIdentifier),
                    new ProposedWithNobodyAlong($this->tableIdentifier, GameNumber::first()),
                    // new dealer after proposing with nobody along
                    new DealerAnnounced($this->tableIdentifier, TestPlayer::dtl(), GameNumber::first()),
                ),
        );
    }

    /** @test */
    public function itAnnouncesFinalRound(): void
    {
        $this->checkHappy(
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
            ->when(new AnnounceFinalRound($this->tableIdentifier, GameNumber::first()))
            ->then(
                new TableStartedPlaying($this->tableIdentifier),
                new FinalRoundAnnounced($this->tableIdentifier, GameNumber::first(), GameNumber::fromInteger(4)),
            ),
        );
    }

    /** @test */
    public function itDoesNotAnnounceFinalRoundTwice(): void
    {
        $this->checkUnhappy(
            UnhappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->andGiven(
                    new TableStartedPlaying($this->tableIdentifier),
                    new FinalRoundAnnounced($this->tableIdentifier, GameNumber::first(), GameNumber::fromInteger(4)),
                )->when(new AnnounceFinalRound($this->tableIdentifier, GameNumber::first()))
                ->then(NotAllowedInFinalRound::class),
        );
    }

    /** @test */
    public function itUndoesFinalRoundAnnouncement(): void
    {
        $this->checkHappy(
            HappyPath::given(...$this->testTableCreatedWithNumberOfPlayers(4))
                ->andGiven(
                    new TableStartedPlaying($this->tableIdentifier),
                    new FinalRoundAnnounced($this->tableIdentifier, GameNumber::first(), GameNumber::fromInteger(4)),
                )->when(new Undo($this->tableIdentifier, GameNumber::first()))
                ->then(
                    new FinalRoundAnnouncementUndone(
                        $this->tableIdentifier,
                        GameNumber::first(),
                        GameNumber::fromInteger(4),
                        TestPlayer::penningmeester(),
                        0,
                    ),
                    new AllUndone($this->tableIdentifier),
                ),
        );
    }

    /** @test */
    public function itConfiguresTheTable(): void
    {
        $this->checkHappy(
            HappyPath::given(new TableCleared($this->tableIdentifier))
                ->when(new ConfigureTable(
                    $this->tableIdentifier,
                    'ruleset.alone5',
                ))->then(
                    new TableConfigured(
                        $this->tableIdentifier,
                        'ruleset.alone5',
                    ),
                ),
        );
    }

    /** @test */
    public function itDoesNotReconfigureAfterTheFirstGame(): void
    {
        $gameAnnouncement = new SinglePlayerAnnouncement(TestPlayer::penningmeester());
        $gameOutcome = NumberOfTricks::fromNumber(5);

        $this->checkUnhappy(
            UnhappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->andGiven(
                new TableStartedPlaying($this->tableIdentifier),
                new Alone(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    $gameAnnouncement,
                    $gameOutcome,
                    $this->getInvolvedPlayersFor4PlayerTestTable()->withDealer(TestPlayer::dtl()),
                    new \DateTimeImmutable('2023-08-29'),
                ),
                new DealerAnnounced(
                    $this->tableIdentifier,
                    TestPlayer::secretaris(),
                    GameNumber::second(),
                ),
            )->when(new ConfigureTable($this->tableIdentifier, 'ruleset.alone5'))
            ->then(TableBusy::class),
        );
    }

    /** @test */
    public function itLogsAlone5IfConfigurationAllowsSo(): void
    {
        $gameAnnouncement = new SinglePlayerAnnouncement(TestPlayer::penningmeester());
        $gameOutcome = NumberOfTricks::fromNumber(5);

        $this->checkHappy(
            HappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4)
            )->andGiven(
                    new TableConfigured(
                        $this->tableIdentifier,
                        'conventions.alone5',
                    ),
                )->when(new LogGame(
                    $this->tableIdentifier,
                    GameNumber::first(),
                    new Alone5Specification(),
                    $gameAnnouncement,
                    $gameOutcome,
                    new \DateTimeImmutable('2023-08-30'),
                ))->then(
                    new TableStartedPlaying($this->tableIdentifier),
                    new Alone5(
                        $this->tableIdentifier,
                        GameNumber::first(),
                        $gameAnnouncement,
                        $gameOutcome,
                        $this->getInvolvedPlayersFor4PlayerTestTable(),
                        new \DateTimeImmutable('2023-08-30'),
                    ),
                    new DealerAnnounced(
                        $this->tableIdentifier,
                        TestPlayer::secretaris(),
                        GameNumber::second(),
                    ),
                ),
        );
    }

    /** @test */
    public function itRejoinsAKnownPlayer(): void
    {
        $this->checkHappy(
            HappyPath::given(
                new TableCleared($this->tableIdentifier),
                new PlayerJoined($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first(), Seat::three(), 'dbfc'),
                new PlayerLeft($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first()),
            )->when(new RejoinKnownPlayer($this->tableIdentifier, TestPlayer::dbfc(), GameNumber::first()))
            ->then(new PlayerJoined(
                $this->tableIdentifier,
                TestPlayer::dbfc(),
                GameNumber::first(),
                Seat::zero(),
                'dbfc',
            )),
        );
    }

    /** @test */
    public function itLoadsDefaultAllowedGamesWithInternalState(): void
    {
        $initialState = $this->getDecider()->getInitialState();

        $this->assertEquals(
            (new TestRulesByConvention())->getPermissibleGames(Whist::DEFAULT_CONVENTIONS_NAME),
            $initialState->allowedGames,
        );
    }

    /** @test */
    public function itRefusesToPassAroundForSimplifiedRules(): void
    {
        $involvedPlayers = $this->getInvolvedPlayersFor4PlayerTestTable();
        $this->checkUnhappy(
            UnhappyPath::given(
                ...$this->testTableCreatedWithNumberOfPlayers(4),
            )->andGiven(
                new TableConfigured($this->tableIdentifier, 'conventions.iwwa'),
                new TableStartedPlaying($this->tableIdentifier),
            )->when(new PassAround($this->tableIdentifier, GameNumber::first()))
            ->then(
                UnsupportedByRules::class,
            ),
        );
    }

    /**
     * Returns the events for creating a table with given number of players.
     *
     * If there's 4 or more players, the last player will be the dealer.
     *
     * @param int<1,max> $numberOfPlayers
     *
     * @return \Traversable<Event>
     */
    private function testTableCreatedWithNumberOfPlayers(int $numberOfPlayers): \Traversable
    {
        yield new TableCleared($this->tableIdentifier);

        $players = $this->getPlayersArray($numberOfPlayers);

        $playerCount = 0;

        foreach ($players as $name => $identifier) {
            yield new PlayerJoined(
                $this->tableIdentifier,
                $identifier,
                GameNumber::first(),
                Seat::fromInteger($playerCount++),
                $name,
            );
        }

        if ($playerCount >= 4) {
            yield new DealerAnnounced(
                $this->tableIdentifier,
                array_values($players)[count($players) - 1],
                GameNumber::first(),
            );
        }
    }

    private function getInvolvedPlayersFor4PlayerTestTable(): PlayersInvolved
    {
        $playerIdentifiers = array_values($this->getPlayersArray(4));

        return new PlayersInvolved(
            PlayerIdentifiers::fromArray($playerIdentifiers),
            $playerIdentifiers[3],
        );
    }
}
