# Dev environment

Only tested on Linux! It should work on any platform supporting docker, though.

## Stop any local services on port 3306 (database) and 8080 

The docker environment we'll be using will forward ports 3306 and 8080
to your local machine, which will allow you to run the acceptance tests from
outside the containers if you have php installed locally.

But for this to work, you should make sure that you're not running a webserver
or a database server on your local machine.

## Add an alias to `/etc/hosts`

This line has to be added to `/etc/hosts` to make server sent events work:
```
127.0.0.1 mercure
```

## Install the dependencies

Before you can run the code locally, you need to install the php and javascript
dependencies. You will need to run [composer](https://getcomposer.org/) and
[yarn](yarnpkg.com).

You can either install those locally, and then run

```
composer install
./bin console importmap:install
```

If you don't want to install these tools locally, you can run them with docker:

```
docker run --rm --interactive --tty -v "$PWD":/app composer install
# TODO: figure out how to do this with docker:
./bin console importmap:install
```

## Mercure

The application will listen to http://mercure:3000 for server sent events.
So for this to work, you'll have to modify your /etc/hosts files so that
mercure resolves to 127.0.0.1.

## Run the application

Once all dependencies are loaded, you can start
the webserver and database with docker compose:

```
docker compose up -d
```

If you run the thing for the first time, you need
to create a (empty) database:

```
docker compose exec php make reset
```

Your site is now at http://localhost:8080.


## Tests

To run the tests:
```
docker compose exec php-nodebug make test
```
This runs the tests without XDebug. You can use the php container to run the tests with XDebug enabled, but this seems still unstable with the current versions of PHP and XDebug.

## Run the make scripts locally

If you have php 8.1 installed on your machine, you can also run the make scripts
locally. But for this to work, you need to edit your `/etc/hosts` file
so that the hostnames webserver and db resolve to 172.0.0.1.

## XDebug and PhpStorm

To make XDebug work, I had to configure a php server (Settings, php, debug).
The server needs to be named 'webserver', you should configure a path mapping
from your document root to `/opt/app`.

To start debugging: click 'Run', 'Start listening for XDebug connections'.

To test: click 'Run', 'Break at first line in PHP scripts', and refresh your page.
PhpStorm should break at the first line of `public/index.php`, and should highlight
the `require_once`-line in that file.

If it works, you can uncheck 'Start listening for XDebug connections', and start
setting breakpoints in your project.