# Using XDebug

## Configure your firewall

You need to make sure that your containers can access your host computer via port 9000. I'm afraid that if you are
using Linux, you need to open port 9000 for everyone on the internet :-( 

```
# on ubuntu, you can use ufw:
sudo ufw allow 9000
```

## Debug with Visual Studio Code

[Visual Studio Code](https://code.visualstudio.com) makes a decent PHP editor,
if you install the [PHP Intelephense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client) add-on. If you also install
[PHP Debug](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug),
you can step-debug the code:

* Open the debug side pane (by clicking the bug, or pressing Ctrl+Shift+D).
* Click the settings-gear on the debug-pane. A file `launch.json` opens.
* Add the following configuration under the `configurations` key:
```
        {
            "name": "Listen for XDebug",
            "type": "php",
            "request": "launch",
            "port": 9000,
            "pathMappings": {
                "/opt/app": "${workspaceRoot}"
            }
        },
```
* Save
* In the debug side pane, in the drop-down next to 'debug and run', click 'Listen for XDebug'
* Set some breakpoints, surf to your app, and checks whether the breakpoints are hit.

## Debug with PHPStormm

PHPStorm is way more powerful for developing PHP applications, but also way more expensive. Setting it up for debugging is also more complicated, and the instructions below might be out of date.

### Configure PhpStorm

- Go to the PHP Debug settings in PhpStorm:

    > "Preferences/Settings" > "Languages & Frameworks" > "PHP" > "Debug"

- Change the Xdebug settings:

    - Debug port: 9000 (= remote_port set in `docker/php-fpm/php-ini-overrides.ini`)
    - Can accept external connections: true

- Go to the Server settings in PHPStorm:

    > "Preferences/Settings" > "Languages & Frameworks" > "PHP" > "Servers"

- Add a new server:

    - Name: devserver (This should match with `PHP_IDE_CONFIG` in docker-compose.yml)
    - Host: localhost
    - Port: 8080
    - Debugger: Xdebug
    - Use path mappings: true
    - Set up a path mapping from the project directory on your file
      system to `/opt/app` on the server. Make sure it is
      correctly applied, because this configuration screen in PhpStorm
      sometimes shows unexpected behavior.

Now if you click the 'Start listening for PHP Debug connections', it should
just work (TM).

To test whether it works, set a breakpoint in `WelcomeController::chooseLanguage()`, and
surf to http://localhost:8080/, to see whether the breakpoint is hit.

### Running command line scripts

It always seems tricky to use XDebug with command line scripts. The last time
it worked like this (from within docker):

```
XDEBUG_CONFIG='client_host=172.17.0.1' ./vendor/bin/codecept run integration
```

To find out the ip-address, you can use (outside docker):

```
ip addr | grep docker0
```

### Troubleshooting

If debugging doesn't work, try the following:

- Go to the Run/Debug configurations in PHPStorm:

  > "Run" > "Edit configurations..."

- Add a new configuration:

  - Select "PHP Remote Debug"
  - Name: Xdebug on Docker
  - Filter debug connection by IDE key: false

- Now select "Xdebug on Docker" in the debug toolbar thingy, and
  click the icon on the right ('Run "Xdebug on docker"').
