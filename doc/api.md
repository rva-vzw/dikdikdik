# *REST-ish* api 🎉

The dikdikdik score app provides a nice API, that you might want
to use yourself.

But note that this api is hardy used, and for a great deal untested. So if you are
interested in writing tests for the api: merge requests are appreciated!

If you run the dikdikdik-app on your server, the endpoint of the
API is at `/api`, relative to the base url of your application.

## `TableIdentifier` and `ScoreSheetIdentifier`

There is no authentication for dikdikdik, and there is no
authentication for the API either. The table you play cards on,
has an identifier, the `TableIdentifier`. You can invent one
yourself, it has to be a 
[version 4 UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_(random)).

With your table identifier, you can use the API to add or remove
players to your table, and to log games. You can, but anybody else
who knows your table identifier, can do so too. So this table
identifier should be kept secret.

Dikdikdik will assign a score sheet identifier to every table.
You can use the score sheet identifier to retrieve the current
scores. Since you can't change anything if you only know the
score sheet identifier, you can share this identifier to 
whoever you want to be able to see what happens on your table.

You can retrieve the score sheet identifier by sending a GET
request to `api/table/{tableId}`.

## `GameNumber` 

The consecutive games played at a table, are numbered, starting
from 1. For many API calls, you need to pass the game number;
I initially did this to prevent that e.g. the same game is
logged twice, but that doesn't really work out for passing around
or proposing with nobody along. 

But anyway, you still need to provide the game number for
many API calls.



## Warning: no error handling yet

Whenever the API thinks your request is invalid, because e.g.
you try to log a game when there is no known dealer, or you
provide an incorrect game number, the request will return an
error 500. Better error handling should be added at some point
in the future. (See #133.)

## Table API

### Table info

```
GET /api/table/{tableId}
```

To be fair, the only info you get here, is the score sheet identifier.
Example curl command:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c'
```

Example result:

```
{
  "table": "05247d0d-60e0-4f7a-ac4a-337320348d4c",
  "sheet": "0ff0bc45-a5b4-5d87-a46a-034b73318a52"
}
```

### Possible games

```
GET /api/games/{tableId}
```    

This will retrieve the list of possible games that can be logged
on the table with the given `TableId`. Note that the `TableId` is
relevant, since you can configure whether you need 5 or 6 tricks
if you want to play alone. For Dikdikdik these are two different
games: `alone5` and `alone`. Depending on the configuration of your
table, `alone5` or `alone` will be returned  in the list of games,
they won't be both in the list.

The list of games will also show what kind of announcement and what
kind of outcome is expected for every game. 

Example curl command:

```
curl 'http://localhost:8080/api/games/05247d0d-60e0-4f7a-ac4a-337320348d4c' 
```

Example response:

```
[
  {
    "name": "abundance",
    "announcement": "single_player",
    "outcome": "all_or_nothing"
  },
  {
    "name": "alone",
    "announcement": "single_player",
    "outcome": "number_of_tricks"
  },
  {
    "name": "misere_ot_table",
    "announcement": "troel_aware_multiplayer",
    "outcome": "winners"
  },
  {
    "name": "misere",
    "announcement": "multiplayer",
    "outcome": "winners"
  },
  {
    "name": "prop_and_cop",
    "announcement": "two_player",
    "outcome": "number_of_tricks"
  },
  {
    "name": "solo",
    "announcement": "troel_aware_single_player",
    "outcome": "all_or_nothing"
  },
  {
    "name": "soloslim",
    "announcement": "troel_aware_single_player",
    "outcome": "all_or_nothing"
  },
  {
    "name": "troel",
    "announcement": "two_player",
    "outcome": "number_of_tricks"
  }
]
```

## Players API

### Known players at your table

```
GET /api/sheet/{scoreSheetId}/players
```

Example curl command:

```
curl 'http://localhost:8080/api/sheet/0ff0bc45-a5b4-5d87-a46a-034b73318a52/players' 
```

Example response:

```
[
  {
    "name": "Fred",
    "status": "active",
    "playerIdentifier": "84e24e2f-773e-4bbc-943a-93830e97677c",
    "lastKnownSeat": 0
  },
  {
    "name": "Wilma",
    "status": "active",
    "playerIdentifier": "48857bad-3c20-44ad-a435-7a3c2bee141c",
    "lastKnownSeat": 1
  },
  {
    "name": "Barney",
    "status": "active",
    "playerIdentifier": "4c85bb44-2c31-4a5c-aad6-6349563bcb6e",
    "lastKnownSeat": 2
  },
  {
    "name": "Betty",
    "status": "active",
    "playerIdentifier": "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3",
    "lastKnownSeat": 3
  }
]
```

Status can theoretically be `new`, `active` or `gone`. (But I don't think
`new` is used.)
`lastKnownSeat` is the seat number of the
current seat of an active player, or the last seat a player
was on before they left.

### Add a player to the table

```
PUT /api/table/{tableId}/game/{gameNumber}/players/{playerId}
```

Players can join any time, as long as there are free seats at the
table. (A table has 6 seats.) `gameNumber` is the number of the
game at the time the player joins. And `playerId` is again a
random UUID4, that you can invent yourself.

You need to pass the following data:

* `player_name`: the name of the player
* `seat` (optional), the number of the seat a player will take. Seats are numbered 0 up to 5. If you leave out the seat number, dikdikdik will assign the first free seat available.

Example curl command:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/game/1/players/84e24e2f-773e-4bbc-943a-93830e97677c' \
  -X PUT --data '{"player_name":"Fred"}'
```

### Delete a player from your table

```
DELETE /api/table/{tableId}/game/{gameNumber}/players/{playerId}
```

You can use this API call when a player walks away, but you want to keep on playing. The
score of the leaving player is kept, but their seat is available again, so allowing new
players to join.

If you want to rejoin the same player again at a later time, just reuse their `PlayerId`.

Example curl command:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/game/1/players/84e24e2f-773e-4bbc-943a-93830e97677c' \
    -X DELETE
```

## Table configuration API

As long as no games were logged on your table, you can 'configure' the table. At this time,
the only thing configurable is the number of required tricks for playing alone.

```
PUT /api/table/{tableId}/config
```

As request data, you pass `{"alone5Enabled": false}` or `{"alone5Enabled": true}`.
(Note: `{"alone5Enabled": "false"}`, and
`{"alone5Enabled": "true"}` is accepted as well.)

Example curl command:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/config' \
  -X PUT --data '{"alone5Enabled":false}'
```

## Score sheet API

```
GET /api/sheet/{scoreSheetId}
```

This returns an object with the following properties:

* `log`: The score log; an array of score lines
* `dealer`: the player identifier of the current dealer
* `current_game`: the game number of the current game
* `playing_players`: player identifiers of players actually playing
  (this is relevant with more than 4 players seated at the table).

The properties of a score line, are:

* `note`: a note; this should be a translatable string
* `line`: number of the score line
* `scores`: the actual scores; an associative array mapping player identifiers to points
* `game`: the number of the game the score line applies to
* `factor`: the score factor that was used; this indicates whether the points earned were multiplied
* `dealer`: player identifier of the player that dealt the cards for the specific game

The score log contains lines for the past games. But also a line without scores for the current
game. And possibly score lines for future games, if they have a score factor greater than one.

Example curl command:

```
curl 'http://localhost:8080/api/sheet/0ff0bc45-a5b4-5d87-a46a-034b73318a52' 
```

Example result:

```
{
  "log": [
    {
      "note": "prop_and_cop",
      "line": 1,
      "scores": {
        "84e24e2f-773e-4bbc-943a-93830e97677c": 2,
        "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3": 2,
        "48857bad-3c20-44ad-a435-7a3c2bee141c": -2,
        "4c85bb44-2c31-4a5c-aad6-6349563bcb6e": -2
      },
      "game": 1,
      "factor": 1,
      "dealer": "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3"
    },
    {
      "note": "prop_and_cop",
      "line": 2,
      "scores": {
        "84e24e2f-773e-4bbc-943a-93830e97677c": -2,
        "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3": 6,
        "48857bad-3c20-44ad-a435-7a3c2bee141c": 2,
        "4c85bb44-2c31-4a5c-aad6-6349563bcb6e": -6
      },
      "game": 2,
      "factor": 1,
      "dealer": "84e24e2f-773e-4bbc-943a-93830e97677c"
    },
    {
      "note": "misere",
      "line": 3,
      "scores": {
        "84e24e2f-773e-4bbc-943a-93830e97677c": -12,
        "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3": 16,
        "48857bad-3c20-44ad-a435-7a3c2bee141c": 12,
        "4c85bb44-2c31-4a5c-aad6-6349563bcb6e": -16
      },
      "game": 3,
      "factor": 1,
      "dealer": "48857bad-3c20-44ad-a435-7a3c2bee141c"
    },
    {
      "note": "alone",
      "line": 4,
      "scores": {
        "84e24e2f-773e-4bbc-943a-93830e97677c": -16,
        "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3": 12,
        "48857bad-3c20-44ad-a435-7a3c2bee141c": 8,
        "4c85bb44-2c31-4a5c-aad6-6349563bcb6e": -4
      },
      "game": 4,
      "factor": 1,
      "dealer": "4c85bb44-2c31-4a5c-aad6-6349563bcb6e"
    },
    {
      "note": "",
      "line": 5,
      "scores": [],
      "game": 5,
      "factor": 2,
      "dealer": "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3"
    }
  ],
  "dealer": "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3",
  "current_game": 5,
  "playing_players": [
    "84e24e2f-773e-4bbc-943a-93830e97677c",
    "48857bad-3c20-44ad-a435-7a3c2bee141c",
    "4c85bb44-2c31-4a5c-aad6-6349563bcb6e",
    "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3"
  ]
}
```

## Game API

### Announcing the dealer

Before you can start logging a game, you need to announce a dealer. Announcing a dealer
will work as soon as 4 players are present at the table.

```
PUT /api/table/{tableId}/game/{gameNumber}/dealer/{playerId}
```

Example curl command:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/game/1/dealer/b35fdea5-9f59-4322-9ec4-bae18fbf6dc3' \
  -X PUT 
```

### Passing around

```
POST /api/table/{tableId}/game/{gameNumber}/pass-around
```

Example curl command:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/game/1/pass-around' -X POST
```

### Proposing with nobody along

```
POST /api/table/{tableId}/game/{gameNumber}/nobody-along
```

Example curl command:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/game/1/nobody-along' -X POST
```

### Logging a game

You can log a game if there's at least 4 players at the table, and if the dealer is known.

```
POST /api/table/{tableId}/game/{gameNumber}/result/{gameName}
```

`gameName` is the name of the game, this has to be one of the names provided by
the games api.

The following data has to be provided:

* `announcement`
* `outcome`

By looking at the results of the Games API, you can figure out what kind of data is
expected as `announcement` and `outcome` for the different game types. Below
you see an example for every announcement type and every outcome type.

#### Single player announcement

```
{
  player1: 'c59ebd17-877a-4ef4-959e-c487723d7c8f'
}
```

#### Two player announcement

```
{
  player1: 'c59ebd17-877a-4ef4-959e-c487723d7c8f',
  player2: '463be824-ea3e-4d43-8d1e-115393b8a6b7'
}
```

#### Multiplayer announcement

```
{
  playerSet: [
    'f7a3c6dd-e250-45cd-b895-7695446defff',
    '463be824-ea3e-4d43-8d1e-115393b8a6b7',
    '9809254b-d572-4c80-a473-37ee8a58129c'
  ]
}
```
#### Troel aware single player announcement

```
{
  onTopOfTroel: true,
  player1: 'c59ebd17-877a-4ef4-959e-c487723d7c8f'
}
```

#### Troel aware multiplayer announcement

```
{
  onTopOfTroel: false,
  playerSet: [
    'f7a3c6dd-e250-45cd-b895-7695446defff',
    '463be824-ea3e-4d43-8d1e-115393b8a6b7',
    '9809254b-d572-4c80-a473-37ee8a58129c'
  ]
}
```

#### All or nothing outcome 

```
{
  success: false
}
```

#### Number of tricks outcome

```
{
  tricks: 7
}
```

#### Winners outcome

```
{
  winners: [
    '463be824-ea3e-4d43-8d1e-115393b8a6b7',
    '9809254b-d572-4c80-a473-37ee8a58129c'
  ]
}
```

#### Example curl command

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/game/1/result/prop_and_cop' \
  --data '{
      "announcement":{
        "player1":"84e24e2f-773e-4bbc-943a-93830e97677c",
        "player2":"48857bad-3c20-44ad-a435-7a3c2bee141c"
      },
      "outcome":{"tricks":"7"}
  }' -X POST
```

### Final round

```
POST /api/table/{tableId}/game/{gameNumber}/final
```

Example curl command:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/game/2/final' \
  -X POST
```


### Correction

For the moment it is not possible to undo actions, in case you logged something wrong.
What you can do, is send a correction, to 'fix' the most recent score. Please note that
you can't change the score factor or current dealer doing so. 
(If you want to change the current dealer, you may use 'proposed, nobody along').

```
POST /api/table/{tableId}/game/{gameNumber}/correct
```

As data, you pass the corrections that should be applied to the score of each player, e.g.
using this curl command, the scores of players 84e... and 488... will be decreased by 2,
and the scores of the plyaers 4c8... and b35... will be increased by 2:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/game/2/correct' \
  --data '{
    "84e24e2f-773e-4bbc-943a-93830e97677c":"-2",
    "48857bad-3c20-44ad-a435-7a3c2bee141c":"-2",
    "4c85bb44-2c31-4a5c-aad6-6349563bcb6e":"2",
    "b35fdea5-9f59-4322-9ec4-bae18fbf6dc3":"2"
  }'
```

## Permissions API

```
GET /api/table/{tableId}/permissions
```

Example curl command:

```
curl 'http://localhost:8080/api/table/05247d0d-60e0-4f7a-ac4a-337320348d4c/permissions' 
```

Example result:

```
{
  "announce_dealer":false,
  "log_game":true,
  "announce_final_round":true,
  "add_player":true,
  "configure_table":false
}
```

* `announce_dealer` is true if a dealer can be announced. This is when there are enough players 
  to play, but no dealer was announced yet.
* `log_game` is true when a game can be logged. This is when there are enough players to play, and
  a dealer was announced.
* `announce_final_round` is true when the final round can be announced. You can announce the final
  round when there are enough players to play, and when the final round wasn't announced before.
* `add_player` is true when you can add a player. This is when there are free seats.
* `configure_table` is true when you can decide about the game rules. This is as long as no score has
  been logged.