# Changelog

Note: this Changelog is not maintained anymore since I stopped
using version numbers for dikdikdik.

For more information, see 
https://www.rijkvanafdronk.be/blog/gedaan-met-versienummers/

## [6.0] - 2023-02-24

### Changed

- dedicated page/form for players and rule configuration, #289
- dedicated form for logging games, #293
- dedicated form for undo, star, final round, #295

### Added

- link to feedback form, #297
- always allow changing the dealer, #290
- allow starring a player that's gone, #173

## Removed

- don't change dealer when adding player, #292
- don't allow players with no name, #291


## [5.1] - 2022-12-22

### Changed

- Use PHP 8.2, #281
- Use Symfony 6.2, #282
- Don't publish TableState updates when table cleared, #285
- Check if Mercure works during deploy, #287

## [5.0.2] - 2022-12-15

### Changed

- Run tests on php 8.2, #286

## [5.0.1] - 2022-11-04 

### Changed

- Fixed translation bug, #279

## [5.0] - 2022-09-13

### Added

- Deptrac is running again during QA stage of CI pipeline, #204
- Title 'scores' on the _writer's_ page, #264

### Changed

- Use services to run web server for gitlab ci web test job, #267
- Update guzzle, #268
- Updating read models, #269, #270, #265
- Added player name to `PlayerJoined` event, #271
- Moved classes around in namespaces, #266
- Fixed projecting old ScoresChanged events, #263
- Bugfix undo for inactive player, #276
- Only show announcers of misere as possible winners, #274

### Removed

- `IdentifyPlayer` command, #271

## [4.2] - 2022-06-11

### Added

- When a new player that never was active, leaves, their column will be deleted from the score sheet, #244
- Added 'chevron-down' after 'expandable' titles, and changed 'Players' title to 'Manage Player', hoping 
  that this way the user will easier find how to add an additional player, #241

### Changed

- Don't show table footer when there' s no log entries, #243
- New widget to manage active/inactive players and player order, #242
- Fixed graph layout on welcome screen, #245
- Installed security updates, #256

## [4.1.1] - 2022-04-28

### Changed

- Fixed starring players, #246

## [4.1] - 2022-04-02

### Changed

- No need to build the test data for the integration tests, #207
- PhpStan level 9, #221 🎉
- Symfony 6, #203, #232
- Only show undo button if undo is possible, #225

### Removed

- No more correction lines, #224
- Removed `SheetAffectingEvent`, `TableAwareEvent`, `PlayersAffectingEvent`, #227

## [4.0] - 2022-02-15

### Added

- undo 🎉🎉, #81, #226, #229, #233, #234, #235, #236, #238
- `ScoresCorrected` event, #230
- Event migration `ScoresLogged` -> `ScoresCorrected`, #231

### Changed

- use php 8.1, #181, #215
- fix app scrolling to top after logging a game, #223

### Removed

- removed `RenderingRequested` event, #199
- removed `ScoreFactorOrmRepository`, #201

## [3.2.2] - 2022-01-24

### Changed

- update the correction line area client side, #219
- avoid unneeded rendered html being send to client, #216
- don't crash when user tries to log invalid announcement, #220

## [3.2.1] - 2022-01-13

### Changed

- Fixed bug translation abundance outcome, #213

## [3.2] - 2022-01-12

### Changed

- Dedicated read model for score log, #193
- Dedicated read model for *table administration*, #195
- Dedicated read model for score log form, #194
- Dedicated read model for corrections and stars, #196
- Correction widget hidden when irrelevant, #208
- Send frontend updates after updating read models, #197
- Deprecate RenderingRequested event, #198
- Updated symfony/ux-twig-component and symfony/ux-live-component, #189
- php-cs-fixer v3.4, #210
- Simpler ScoreLog::getCurrentDealer(), #187
- Fixed configuring alone tricks, #211

## [3.1.1] - 2021-11-30

### Changed

- Include scheme and host in shown share link, #192

## [3.1.0] - 2021-11-30

### Changed

- Update php-cs-fixer, #184
- PublisherInterface -> HubInterface, #171
- Auto-select dealer when there's enough players, #172
- Put form at the bottom of the score sheet, #183
- Fixed layout problem, #176
- Collapse new player box when game busy, #175
- Update Symfony to 5.3.11, #188
- Removed workaround for upstream issue, #179

## [3.0.1] - 2021-11-18

- Update number of tricks while sliding, #181

### Changed

## [3.0] - 2021-11-05

### Changed

- Use composer v2 in gitlab ci, #160
- updated to symfony 5.3, updated most of the recipes as well, #161
- updated flex recipe for doctrine/doctrine-bundle, #162
- updated mercure and flex recipe for symfony/mercue-bundle, #163
- use more generics, #166
- replaced vue by symfony turbo and symfony components, #167
- installed some security updates, #174

### Removed

- PlayerStatus 'new', #168

## [2.0] - 2021-04-12

- usage statistics, #159

## [1.3] - 2021-02-02

### Changed

- php 8.0, #158

## [1.2] - 2020-12-10

### Added

- Change player order, #144
- Star player, #78.

### Changed

- Use wodby/php container, #144
- Moved orm-related code out of the domain namespace, #154
- Set minimal php version to 7.4, #153

## [1.1.1] - 2020-10-03

### Changed

- Upgrade to symfony 5.1.6, to fix issue with mercure, #152.

## [1.1] - 2020-08-22

### Changed

- Moved things around fixing dependencies, #120, #123.
- Use external package (rva-vzw/krakboem) for event sourcing and CQRS, #139.
- Use version 0.3 of rva-vzw/krakboem, #141
- Upgrade to Symfony 5, #141.
- Use version 0.6 of rva-vzw/krakboem, #146.
- Updated yarn and javascript dependencies, #148.
- Fixed translation on watcher page, #149.
- Use bootstrap-vue, #145.
- Use server sent events (mercure) to update the front end, #147.

## [1.0.1] - 2020-03-23

### Changed

- Bugfix HTML identifiers, #138.

## [1.0] - 2020-03-22

### Added

- Introduced `ScoreLineNumber`, got rid of 'dummy games', #129, #135
- Log event time in the event store, #103
- Api documentation, #106, #132.

### Changed

- Tried to bring more consistency to the API, #106, #132.
- Also show player names at bottom of table, #134.
- Bugfix upding player drop downs after adding player, #136.
- Renamed TLM as DDB in tests, #137.

## [0.5.1] - 2020-02-29

### Changed

- Fixed the bugs I introduced with 0.5, #131.

## [0.5] - 2020-02-29

### Changed

- Threw in some bootstrap in an attempt to make the app prettier, #130.
  (I messed up the html doing so 😭)

## [0.4] - 2020-02-25

### Added 

- Configure number of tricks required for playing alone, #127.

## [0.3.1] - 2020-02-22

### Changed

- Bugfix: Don't scroll away, when user clicks 'final round' or 'correction', #128

## [0.3] - 2020-02-19

### Added

- Read only view for fellow players or fans, #76, #117, #118, #124
- Final round, #77.
- Updated doctrine/annotations so that dev howto keeps working, #126

### Changed

- Download style sheet from correct location.

- Updated doctrine/annotations so that dev howto keeps working, #126

### Changed

- Download style sheet from correct location.

## [0.2.2] - 2020-02-04

### Added

- Act properly when dealer leaves, #46, #110

### Changed

- Only show the correction line interface if there's enough players to play, #115.
- Properly reset player select components after logging a game, #116.

## [0.2.1] - 2020-01-24

### Changed

- Better naming for API regarding proposing with nobody along, #113.
- Bugfix correction lines messing up other sheets, #114.

## [0.2] - 2020-01-22

### Added

- Correction lines, #93.
- More dealer related acceptance tests, #53
- Script to automatically clone remote instance, #112.

### Changed

- More stable web tests, #100.
- Refactored CurrentGame read model, #104
- Refactored ScoreLog read model, #105
- Refactored PlayersGroup read model, #49
- Improved replay, #108
- Don't show left players in dealer drop down, #34. **WARNING!** This breaks API backward compatibility!

## [0.1.1] - 2020-01-14

### Changed

- Fixed deployment procedure, #102.
- Added some urls to the game rules in the tests, #101.

## [0.1] - 2020-01-13

- Initial release
